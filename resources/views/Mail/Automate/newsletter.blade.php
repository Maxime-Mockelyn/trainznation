<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>{{ env("APP_NAME") }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!--<![endif]-->

    <style type="text/css">

        .ReadMsgBody { width: 100%; background-color: #D2D7D3; }
        .ExternalClass { width: 100%; background-color: #D2D7D3; }
        .a { text-decoration:none; }
        body { width: 100%; background-color: #d8eaf8; margin: 0; padding: 0; -webkit-font-smoothing: antialiased; font-family:OpenSans ,Arial, Helvetica Neue, Helvetica, sans-serif }
        @-ms-viewport{ width: device-width; }

        @font-face {
            font-family: "Montserrat";
            font-style: normal;
            font-weight: 400;
            src: url("Fonts/Montserrat-Regular.ttf") format("truetype"), url("https://fonts.gstatic.com/s/montserrat/v6/zhcz-_WihjSQC0oHJ9TCYPk_vArhqVIZ0nv9q090hN8.woff2") format("woff2");
        }
        @font-face {
            font-family: "Montserrat";
            font-style: Bold;
            font-weight: 700;
            src: url("Fonts/Montserrat-Bold.ttf") format("truetype"), url("https://fonts.gstatic.com/s/montserrat/v6/IQHow_FEYlDC4Gzy_m8fcoWiMMZ7xLd792ULpGE4W_Y.woff2") format('woff2');
        }
        @font-face {
            font-family: "OpenSans";
            font-style: normal;
            font-weight: 400;
            src: url("Fonts/OpenSans-Regular.ttf") format("truetype"), url("https://fonts.gstatic.com/s/opensans/v13/cJZKeOuBrn4kERxqtaUH3VtXRa8TVwTICgirnJhmVJw.woff2") format("woff2");
        }
        @font-face {
            font-family: "OpenSans";
            font-style: normal;
            font-weight: 600;
            src: url("Fonts/OpenSans-Semibold.ttf") format("truetype"), url("https://fonts.gstatic.com/s/opensans/v13/MTP_ySUJH_bn48VBG8sNSugdm0LZdjqr5-oayXSOefg.woff2") format("woff2");
        }

        @font-face {
            font-family: "OpenSans";
            font-style: extraBold;
            font-weight: 900;
            src: url("Fonts/OpenSans-ExtraBold.ttf") format("truetype"), url("https://fonts.gstatic.com/s/opensans/v13/EInbV5DfGHOiMmvb1Xr-hugdm0LZdjqr5-oayXSOefg.woff2") format('woff2');
        }
        @media only screen and (max-width: 639px){
            .wrapper{ width:100%;  padding: 0 !important; }
        }

        @media only screen and (max-width: 480px){
            .centerClass{ margin:0 auto !important; }
            .imgClass{ width:100% !important; height:auto; }
            .img{ text-align:center}
            .hide{ display:none;}
            .wrapper{ width:100%; padding: 0 !important; }
            .header{ width:100%; padding: 0 !important; background-image: url(http://placehold.it/320x400) !important; }
            .container{ width:300px;  padding: 0 !important; }
            .box{ width:250px;  padding: 0 !important; }
            .mobile{ width:300px; display:block; padding: 0 !important; text-align:center !important;}
            .mobile50{ width:300px; padding: 0 !important; text-align:center; }
            *[class="mobileOff"] { width: 0px !important; display: none !important; }
            *[class*="mobileOn"] { display: block !important; max-height:none !important; }
        }

        .MsoNormal {font-family:Montserrat, OpenSans ,Arial, Helvetica Neue, Helvetica, sans-serif !important;}
    </style>

    <!--[if gte mso 15]>
    <style type="text/css">
        table { font-size:1px; line-height:0; mso-margin-top-alt:1px;mso-line-height-rule: exactly; }
        * { mso-line-height-rule: exactly; }
    </style>
    <![endif]-->

</head>
<body marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" style="background-color:#D2D7D3;  font-family: Montserrat, OpenSans, Arial, sans-serif; color:#ffffff; text-decoration:none; margin:0; padding:0; min-width: 100%; -webkit-text-size-adjust:none; -ms-text-size-adjust:none;">

<!-- Start Background -->
<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff">
    <tr>
        <td width="100%" valign="top" align="center">


            <!-- Module Begin7.1-->
            <!-- Start Wrapper -->
            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#ECECEC">
                <tbody><tr>
                    <td height="10"> </td>
                </tr>
                <tr>
                    <td align="center">

                        <!-- Start Container -->
                        <table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
                            <tbody><tr>
                                <td height="40" align="center" class="mobile">

                                    <!-- Start Container -->
                                    <table width="400" cellpadding="0" cellspacing="0" border="0" class="container">
                                        <tbody>
                                        <tr>
                                            <td width="100" height="22" class="mobile50" align="center" style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; text-decoration:none; color: #FFFFFF; line-height:23px; font-weight:400;">
                                                <a href="" target="_blank" alias="" style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; text-decoration:none; color: #333333; line-height:23px; font-weight:400;">Voir en ligne</a>
                                            </td>
                                        </tr>
                                        </tbody></table>
                                    <!-- End Container -->

                                </td>

                            </tr>
                            </tbody></table>
                        <!-- End Container -->

                    </td>
                </tr>
                <tr>
                    <td height="10"> </td>
                </tr>
                </tbody></table>

            <!-- End Wrapper -->
            <!-- End Module -->



            <!-- Module Begin7.2-->
            <!-- Start Wrapper -->
            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#ffffff">
                <tbody><tr>
                    <td height="20"> </td>
                </tr>
                <tr>
                    <td align="center">

                        <!-- Start Container -->
                        <table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
                            <tbody><tr>
                                <td width="200" class="mobile" style="font-family: OpenSans, Arial, sans-serif; font-size:12px; line-height:18px;"><table width="100" border="0" align="left" cellpadding="0" cellspacing="0" class="container">
                                        <tbody><tr>
                                            <td align="center" valign="middle"><img src="{{ sourceImage('other/logo.png') }}" width="100" height="50"></td>
                                        </tr>
                                        </tbody></table></td>
                                <td height="40" style="" class="mobileOn"></td>
                                <td height="40" style="" class="mobileOn"></td>
                                <td align="center" class="mobile">

                                    <!-- Start Container -->
                                    <table width="400" cellpadding="0" cellspacing="0" border="0" class="container">
                                        <tbody>
                                        <tr>
                                            <td width="100" height="22" class="mobile50" align="center" style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; text-decoration:none; color: #FFFFFF; line-height:23px; font-weight:400;">
                                                <a href="{{ env("APP_URL") }}/" target="_blank" alias="" style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; text-decoration:none; color: #333333; line-height:23px; font-weight:400;">Accueil</a>
                                            </td>
                                            <td width="100" height="22" class="mobile50" align="center" style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; text-decoration:none; color: #FFFFFF; line-height:23px; font-weight:400;">
                                                <a href="{{ env("APP_URL") }}/blog" target="_blank" alias="" style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; text-decoration:none; color: #333333; line-height:23px; font-weight:400;">Blog</a></td>
                                            <td width="100" height="22" class="mobile50" align="center" style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; text-decoration:none; color: #FFFFFF; line-height:23px; font-weight:400;">
                                                <a href="{{ env("APP_URL") }}/download" target="_blank" alias="" style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; text-decoration:none; color: #333333; line-height:23px; font-weight:400;">Téléchargement</a></td>
                                            <td width="100" height="22" class="mobile50" align="center" style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; text-decoration:none; color: #FFFFFF; line-height:23px; font-weight:400;">
                                                <a href="{{ env("APP_URL") }}/tutoriel" target="_blank" alias="" style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; text-decoration:none; color: #333333; line-height:23px; font-weight:400;">Tutoriel</a></td>
                                            <td width="100" height="22" class="mobile50" align="center" style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; text-decoration:none; color: #FFFFFF; line-height:23px; font-weight:400;">
                                                <a href="{{ env("APP_URL") }}/wiki" target="_blank" alias="" style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; text-decoration:none; color: #333333; line-height:23px; font-weight:400;">Wiki</a></td>
                                        </tr>
                                        </tbody></table>
                                    <!-- End Container -->

                                </td>

                            </tr>
                            </tbody></table>
                        <!-- End Container -->

                    </td>
                </tr>
                <tr>
                    <td height="20"> </td>
                </tr>
                </tbody></table>

            <!-- End Wrapper -->
            <!-- End Module -->


            <!-- Module Begin7.3-->
            <!-- Start Wrapper -->
            <table width="100%" align="center" height="450" cellpadding="0" cellspacing="0" border="0" class="wrapper" style=" background-image: url({{ sourceImage('other/newsletter_header.png') }});  background-size:cover; -moz-background-size:cover; -ms-background-size:cover; -o-background-size:cover;-webkit-background-size:cover; background-position:center; background-repeat: no-repeat;">
                <tbody><tr>
                    <td align="center">
                        <!--[if gte mso 9]>
                        <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:100%">
                            <v:fill type="frame" src="http://placehold.it/1007x550" color="#ffffff" />
                            <v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0">
                                <div style="font-family: OpenSans, Arial, sans-serif; font-size:1px;line-height:1px">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                        <tbody>
                                        <tr>
                                            <td height="450" align="center">
                        <![endif]-->


                        <!-- Start Container -->
                        <table width="600" height="450" cellpadding="0" cellspacing="0" border="0" class="container">
                            <tbody><tr>
                                <td height="20" align="center" class="mobile" style="font-family: OpenSans, Arial, sans-serif; font-size:12px; line-height:24px;">
                                    <!-- Start Content -->

                                    <table width="400" border="0" align="left" cellpadding="0" cellspacing="0" class="container">
                                        <tbody><tr>
                                            <td align="center" style="font-family: OpenSans, Arial, sans-serif; font-size:50px; line-height:60px; font-weight:800; letter-spacing:2px; color:#FFFFFF;"><!-- Start Container -->

                                                <!-- End Container --></td>
                                        </tr>
                                        <tr>
                                            <td height="20"></td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: Montserrat, Arial, sans-serif; font-size:40px; line-height:50px; font-weight:800; letter-spacing:2px; color:#ffffff;">Newsletter du {{ now()->locale('fr_FR')->translatedFormat('j F Y') }}</td>
                                        </tr>
                                        <tr>
                                            <td height="20"></td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: OpenSans, Arial, sans-serif; font-size: 14px; color: #ffffff; line-height:26px; font-weight:400;">Toutes les semaines, découvrez les nouveautés du site trainznation</td>
                                        </tr>
                                        <tr>
                                            <td height="50"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <!-- End Content -->
                                </td>
                            </tr>
                            </tbody></table>
                        <!-- End Container -->

                        <!--[if gte mso 9]>
                        </td>
                        </tr>
                        </tbody>
                        </table>
                        </div>
                        </v:textbox>
                        </v:rect>
                        <![endif]-->

                    </td>
                </tr>
                </tbody></table>


            <!-- End Wrapper -->
            <!-- End Module -->

            @if($tutoriel_count != 0)
                <table width="100%" data-module="Module1.3" data-thumb="http://simailallmoduleraw.prim4design.com/thumbnails/1.3.png" align="center" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#FFFFFF" data-bgcolor="MainBG_Color9">
                    <tbody>
                    <tr>
                        <td height="50" style="font-family: OpenSans, Arial, sans-serif; font-size:10px; line-height:10px;"></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <!-- Start Container -->
                            <table width="600" cellpadding="0" cellspacing="0" align="center" border="0" class="container">
                                <tbody>

                                <tr>
                                    <td align="center" class="mobile" style="font-family: Montserrat, Arial, sans-serif; font-size: 28px; line-height:40px; letter-spacing:2px; color: #333333; font-weight:400;" data-color="H2l Color" data-size="H2l Size" data-min="14" data-max="45" mc:edit="">
                                        <multiline>Nouveau Tutoriels</multiline>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" height="20" class="mobile"></td>
                                </tr>
                                <tr>
                                    <td align="center" class="mobile" style="font-family: Montserrat, Arial, sans-serif; font-size: 13px; line-height:24px; letter-spacing:1px; color: #333333; font-weight:400;" data-color="H7C Color" data-size="H7C Size" data-min="10" data-max="24" mc:edit="">
                                        <multiline>du {{ now()->startOfWeek()->locale('fr_FR')->translatedFormat('j') }} au {{ now()->endOfWeek()->locale('fr_FR')->translatedFormat('j F Y') }}</multiline>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" height="10" class="mobile"></td>
                                </tr>
                                <tr>
                                    <td align="center" class="mobile" height="4">
                                        <table width="50" height="4" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#EB6361" data-bgcolor="Bottomline_Color">
                                            <tbody>
                                            <tr>
                                                <td></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" height="20" class="mobile"></td>
                                </tr>
                                </tbody>
                            </table>
                            <!-- End Container -->
                        </td>
                    </tr>
                    </tbody>
                </table>

                @foreach($tutoriels as $tutoriel)
                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#ececec">
                        <tbody><tr>
                            <td height="50" style="font-family: OpenSans, Arial, sans-serif; font-size:10px; line-height:10px;"></td>
                        </tr>
                        <tr>
                            <td align="center">

                                <!-- Start Container -->
                                <table width="900" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF" class="container" style="border-radius:4px">
                                    <tbody>
                                    <tr>
                                        <td height="30"></td>
                                    </tr>
                                    <tr>
                                        <td height="20" align="center"><table width="800" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF" class="container">
                                                <tbody>
                                                <tr>
                                                    <td width="30" height="20" align="center" class="mobile"></td>
                                                    <td width="300" align="center" valign="top" class="mobile" style="font-family: OpenSans, Arial, sans-serif; font-size:12px; line-height:18px;">
                                                        <!-- Start Container -->
                                                        <table width="300" border="0" align="center" cellpadding="0" cellspacing="0">
                                                            <tbody><tr>
                                                                <td style="font-family: Montserrat, Arial, sans-serif; font-size: 20px; line-height:30px; letter-spacing:1px; color: #333333; font-weight:400;">{{ $tutoriel->title }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style=" border-left:4px #8d8d8d solid; padding-left:10px; font-family: OpenSans, Arial, sans-serif; font-size: 10px; color: #8D8D8D; line-height:18px; font-weight:400;" data-border-left-color="Vertical_Line"><em>
                                                                        Publié le {{ $tutoriel->published_at->locale('fr_FR')->translatedFormat('j F Y') }} dans la catégorie {{ $tutoriel->category->name }}
                                                                    </em></td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20"> </td><!-- Spacer -->
                                                            </tr>
                                                            <tr>
                                                                <td style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; color: #8D8D8D; line-height:23px; font-weight:400;">{{ $tutoriel->short_content }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20"></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" class="mobile" style="font-size:14px; line-height:24px;"><table align="left" width="125px" border="0" cellspacing="0" cellpadding="0" class="container">
                                                                        <tbody><tr>
                                                                            <td><!-- Start Button -->
                                                                                <table width="125" cellpadding="0" cellspacing="0" align="center" border="0">
                                                                                    <tbody><tr>
                                                                                        <td width="130" height="45" bgcolor="#EB6361" align="center" valign="middle" style="font-size: 14px; color: #ffffff; line-height:24px;border-radius: 4px;">
                                                                                            <a href="{{ env("APP_URL") }}/tutoriel/{{ $tutoriel->tutoriel_sub_category_id }}/{{ $tutoriel->slug }}" target="_blank" alias="" style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; text-decoration:none; color: #FFFFFF; line-height:23px; font-weight:400;">Voir le tutoriel</a>
                                                                                        </td>
                                                                                    </tr>
                                                                                    </tbody></table>
                                                                                <!-- End Button --></td>
                                                                        </tr>
                                                                        </tbody></table>



                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <!-- End Container -->
                                                    </td>
                                                    <td height="20" align="center" class="mobile"></td>
                                                    <td width="400" align="center" valign="top" class="mobile" style="font-family: OpenSans, Arial, sans-serif; font-size:12px; line-height:18px;">
                                                        <!-- Start Content -->
                                                        <table width="400" cellpadding="0" cellspacing="0" border="0" class="container">
                                                            <tbody><tr>
                                                                <td width="400" align="center" class="mobile" style="font-family: OpenSans, Arial, sans-serif; font-size:12px; line-height:18px;">
                                                                    <img src="{{ sourceImage('learning/'.$tutoriel->id.'.png') }}" width="400" height="250" alt="" class="imgClass">
                                                                </td>
                                                            </tr>
                                                            </tbody></table>
                                                        <!-- End Content -->
                                                    </td>
                                                    <td width="30" class="mobile" style="font-family: OpenSans, Arial, sans-serif; font-size:12px; line-height:18px;" align="center">&nbsp;</td>
                                                </tr>
                                                </tbody>
                                            </table></td>
                                    </tr>
                                    <tr>
                                        <td height="30" class="hide"></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <!-- End Container -->

                            </td>
                        </tr>
                        <tr>
                            <td height="50" style="font-family: OpenSans, Arial, sans-serif; font-size:10px; line-height:10px;"> </td><!-- Spacer -->
                        </tr>
                        </tbody></table>
                @endforeach
            @endif

            @if($download_count != 0)
                <table width="100%" data-module="Module1.3" data-thumb="http://simailallmoduleraw.prim4design.com/thumbnails/1.3.png" align="center" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#FFFFFF" data-bgcolor="MainBG_Color9">
                    <tbody>
                    <tr>
                        <td height="50" style="font-family: OpenSans, Arial, sans-serif; font-size:10px; line-height:10px;"></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <!-- Start Container -->
                            <table width="600" cellpadding="0" cellspacing="0" align="center" border="0" class="container">
                                <tbody>

                                <tr>
                                    <td align="center" class="mobile" style="font-family: Montserrat, Arial, sans-serif; font-size: 28px; line-height:40px; letter-spacing:2px; color: #333333; font-weight:400;" data-color="H2l Color" data-size="H2l Size" data-min="14" data-max="45" mc:edit="">
                                        <multiline>Nouveau Objets en téléchargement</multiline>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" height="20" class="mobile"></td>
                                </tr>
                                <tr>
                                    <td align="center" class="mobile" style="font-family: Montserrat, Arial, sans-serif; font-size: 13px; line-height:24px; letter-spacing:1px; color: #333333; font-weight:400;" data-color="H7C Color" data-size="H7C Size" data-min="10" data-max="24" mc:edit="">
                                        <multiline>du {{ now()->startOfWeek()->locale('fr_FR')->translatedFormat('j') }} au {{ now()->endOfWeek()->locale('fr_FR')->translatedFormat('j F Y') }}</multiline>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" height="10" class="mobile"></td>
                                </tr>
                                <tr>
                                    <td align="center" class="mobile" height="4">
                                        <table width="50" height="4" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#EB6361" data-bgcolor="Bottomline_Color">
                                            <tbody>
                                            <tr>
                                                <td></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" height="20" class="mobile"></td>
                                </tr>
                                </tbody>
                            </table>
                            <!-- End Container -->
                        </td>
                    </tr>
                    </tbody>
                </table>

                @foreach($downloads as $download)
                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#ececec">
                        <tbody><tr>
                            <td height="50" style="font-family: OpenSans, Arial, sans-serif; font-size:10px; line-height:10px;"></td>
                        </tr>
                        <tr>
                            <td align="center">

                                <!-- Start Container -->
                                <table width="900" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF" class="container" style="border-radius:4px">
                                    <tbody>
                                    <tr>
                                        <td height="30"></td>
                                    </tr>
                                    <tr>
                                        <td height="20" align="center"><table width="800" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF" class="container">
                                                <tbody>
                                                <tr>
                                                    <td width="30" height="20" align="center" class="mobile"></td>
                                                    <td width="300" align="center" valign="top" class="mobile" style="font-family: OpenSans, Arial, sans-serif; font-size:12px; line-height:18px;">
                                                        <!-- Start Container -->
                                                        <table width="300" border="0" align="center" cellpadding="0" cellspacing="0">
                                                            <tbody><tr>
                                                                <td style="font-family: Montserrat, Arial, sans-serif; font-size: 20px; line-height:30px; letter-spacing:1px; color: #333333; font-weight:400;">{{ $download->title }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style=" border-left:4px #8d8d8d solid; padding-left:10px; font-family: OpenSans, Arial, sans-serif; font-size: 10px; color: #8D8D8D; line-height:18px; font-weight:400;" data-border-left-color="Vertical_Line"><em>
                                                                        Publié le {{ $download->published_at->locale('fr_FR')->translatedFormat('j F Y') }} dans la catégorie {{ $download->subcategorie->name }}
                                                                    </em></td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20"> </td><!-- Spacer -->
                                                            </tr>
                                                            <tr>
                                                                <td style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; color: #8D8D8D; line-height:23px; font-weight:400;">{{ $download->short_description }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20"></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" class="mobile" style="font-size:14px; line-height:24px;"><table align="left" width="125px" border="0" cellspacing="0" cellpadding="0" class="container">
                                                                        <tbody><tr>
                                                                            <td><!-- Start Button -->
                                                                                <table width="125" cellpadding="0" cellspacing="0" align="center" border="0">
                                                                                    <tbody><tr>
                                                                                        <td width="130" height="45" bgcolor="#EB6361" align="center" valign="middle" style="font-size: 14px; color: #ffffff; line-height:24px;border-radius: 4px;">
                                                                                            <a href="{{ env("APP_URL") }}/download/{{ $download->asset_sub_categorie_id }}/{{ $download->id }}" target="_blank" alias="" style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; text-decoration:none; color: #FFFFFF; line-height:23px; font-weight:400;">Voir l'objet</a>
                                                                                        </td>
                                                                                    </tr>
                                                                                    </tbody></table>
                                                                                <!-- End Button --></td>
                                                                        </tr>
                                                                        </tbody></table>



                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <!-- End Container -->
                                                    </td>
                                                    <td height="20" align="center" class="mobile"></td>
                                                    <td width="400" align="center" valign="top" class="mobile" style="font-family: OpenSans, Arial, sans-serif; font-size:12px; line-height:18px;">
                                                        <!-- Start Content -->
                                                        <table width="400" cellpadding="0" cellspacing="0" border="0" class="container">
                                                            <tbody><tr>
                                                                <td width="400" align="center" class="mobile" style="font-family: OpenSans, Arial, sans-serif; font-size:12px; line-height:18px;">
                                                                    <img src="{{ sourceImage('download/'.$download->id.'.png') }}" width="400" height="250" alt="" class="imgClass">
                                                                </td>
                                                            </tr>
                                                            </tbody></table>
                                                        <!-- End Content -->
                                                    </td>
                                                    <td width="30" class="mobile" style="font-family: OpenSans, Arial, sans-serif; font-size:12px; line-height:18px;" align="center">&nbsp;</td>
                                                </tr>
                                                </tbody>
                                            </table></td>
                                    </tr>
                                    <tr>
                                        <td height="30" class="hide"></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <!-- End Container -->

                            </td>
                        </tr>
                        <tr>
                            <td height="50" style="font-family: OpenSans, Arial, sans-serif; font-size:10px; line-height:10px;"> </td><!-- Spacer -->
                        </tr>
                        </tbody></table>
                @endforeach
            @endif

            @if($article_count != 0)
                <table width="100%" data-module="Module1.3" data-thumb="http://simailallmoduleraw.prim4design.com/thumbnails/1.3.png" align="center" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#FFFFFF" data-bgcolor="MainBG_Color9">
                    <tbody>
                    <tr>
                        <td height="50" style="font-family: OpenSans, Arial, sans-serif; font-size:10px; line-height:10px;"></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <!-- Start Container -->
                            <table width="600" cellpadding="0" cellspacing="0" align="center" border="0" class="container">
                                <tbody>

                                <tr>
                                    <td align="center" class="mobile" style="font-family: Montserrat, Arial, sans-serif; font-size: 28px; line-height:40px; letter-spacing:2px; color: #333333; font-weight:400;" data-color="H2l Color" data-size="H2l Size" data-min="14" data-max="45" mc:edit="">
                                        <multiline>Nouvelle articles</multiline>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" height="20" class="mobile"></td>
                                </tr>
                                <tr>
                                    <td align="center" class="mobile" style="font-family: Montserrat, Arial, sans-serif; font-size: 13px; line-height:24px; letter-spacing:1px; color: #333333; font-weight:400;" data-color="H7C Color" data-size="H7C Size" data-min="10" data-max="24" mc:edit="">
                                        <multiline>du {{ now()->startOfWeek()->locale('fr_FR')->translatedFormat('j') }} au {{ now()->endOfWeek()->locale('fr_FR')->translatedFormat('j F Y') }}</multiline>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" height="10" class="mobile"></td>
                                </tr>
                                <tr>
                                    <td align="center" class="mobile" height="4">
                                        <table width="50" height="4" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#EB6361" data-bgcolor="Bottomline_Color">
                                            <tbody>
                                            <tr>
                                                <td></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" height="20" class="mobile"></td>
                                </tr>
                                </tbody>
                            </table>
                            <!-- End Container -->
                        </td>
                    </tr>
                    </tbody>
                </table>

                @foreach($articles as $article)
                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#ececec">
                        <tbody><tr>
                            <td height="50" style="font-family: OpenSans, Arial, sans-serif; font-size:10px; line-height:10px;"></td>
                        </tr>
                        <tr>
                            <td align="center">

                                <!-- Start Container -->
                                <table width="900" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF" class="container" style="border-radius:4px">
                                    <tbody>
                                    <tr>
                                        <td height="30"></td>
                                    </tr>
                                    <tr>
                                        <td height="20" align="center"><table width="800" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF" class="container">
                                                <tbody>
                                                <tr>
                                                    <td width="30" height="20" align="center" class="mobile"></td>
                                                    <td width="300" align="center" valign="top" class="mobile" style="font-family: OpenSans, Arial, sans-serif; font-size:12px; line-height:18px;">
                                                        <!-- Start Container -->
                                                        <table width="300" border="0" align="center" cellpadding="0" cellspacing="0">
                                                            <tbody><tr>
                                                                <td style="font-family: Montserrat, Arial, sans-serif; font-size: 20px; line-height:30px; letter-spacing:1px; color: #333333; font-weight:400;">{{ $article->title }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style=" border-left:4px #8d8d8d solid; padding-left:10px; font-family: OpenSans, Arial, sans-serif; font-size: 10px; color: #8D8D8D; line-height:18px; font-weight:400;" data-border-left-color="Vertical_Line"><em>
                                                                        Publié le {{ $article->published_at->locale('fr_FR')->translatedFormat('j F Y') }} dans la catégorie {{ $article->categorie->name }}
                                                                    </em></td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20"> </td><!-- Spacer -->
                                                            </tr>
                                                            <tr>
                                                                <td style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; color: #8D8D8D; line-height:23px; font-weight:400;">{{ $article->short_content }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20"></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" class="mobile" style="font-size:14px; line-height:24px;"><table align="left" width="125px" border="0" cellspacing="0" cellpadding="0" class="container">
                                                                        <tbody><tr>
                                                                            <td><!-- Start Button -->
                                                                                <table width="125" cellpadding="0" cellspacing="0" align="center" border="0">
                                                                                    <tbody><tr>
                                                                                        <td width="130" height="45" bgcolor="#EB6361" align="center" valign="middle" style="font-size: 14px; color: #ffffff; line-height:24px;border-radius: 4px;">
                                                                                            <a href="{{ env("APP_URL") }}/blog/{{ $article->slug }}" target="_blank" alias="" style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; text-decoration:none; color: #FFFFFF; line-height:23px; font-weight:400;">Voir l'article</a>
                                                                                        </td>
                                                                                    </tr>
                                                                                    </tbody></table>
                                                                                <!-- End Button --></td>
                                                                        </tr>
                                                                        </tbody></table>



                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <!-- End Container -->
                                                    </td>
                                                    <td height="20" align="center" class="mobile"></td>
                                                    <td width="400" align="center" valign="top" class="mobile" style="font-family: OpenSans, Arial, sans-serif; font-size:12px; line-height:18px;">
                                                        <!-- Start Content -->
                                                        <table width="400" cellpadding="0" cellspacing="0" border="0" class="container">
                                                            <tbody><tr>
                                                                <td width="400" align="center" class="mobile" style="font-family: OpenSans, Arial, sans-serif; font-size:12px; line-height:18px;">
                                                                    <img src="{{ sourceImage('blog/'.$article->id.'.png') }}" width="400" height="250" alt="" class="imgClass">
                                                                </td>
                                                            </tr>
                                                            </tbody></table>
                                                        <!-- End Content -->
                                                    </td>
                                                    <td width="30" class="mobile" style="font-family: OpenSans, Arial, sans-serif; font-size:12px; line-height:18px;" align="center">&nbsp;</td>
                                                </tr>
                                                </tbody>
                                            </table></td>
                                    </tr>
                                    <tr>
                                        <td height="30" class="hide"></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <!-- End Container -->

                            </td>
                        </tr>
                        <tr>
                            <td height="50" style="font-family: OpenSans, Arial, sans-serif; font-size:10px; line-height:10px;"> </td><!-- Spacer -->
                        </tr>
                        </tbody></table>
                @endforeach
            @endif

            @if($wiki_count != 0)
                <table width="100%" data-module="Module1.3" data-thumb="http://simailallmoduleraw.prim4design.com/thumbnails/1.3.png" align="center" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#FFFFFF" data-bgcolor="MainBG_Color9">
                    <tbody>
                    <tr>
                        <td height="50" style="font-family: OpenSans, Arial, sans-serif; font-size:10px; line-height:10px;"></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <!-- Start Container -->
                            <table width="600" cellpadding="0" cellspacing="0" align="center" border="0" class="container">
                                <tbody>

                                <tr>
                                    <td align="center" class="mobile" style="font-family: Montserrat, Arial, sans-serif; font-size: 28px; line-height:40px; letter-spacing:2px; color: #333333; font-weight:400;" data-color="H2l Color" data-size="H2l Size" data-min="14" data-max="45" mc:edit="">
                                        <multiline>Nouvelle articles d'aide</multiline>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" height="20" class="mobile"></td>
                                </tr>
                                <tr>
                                    <td align="center" class="mobile" style="font-family: Montserrat, Arial, sans-serif; font-size: 13px; line-height:24px; letter-spacing:1px; color: #333333; font-weight:400;" data-color="H7C Color" data-size="H7C Size" data-min="10" data-max="24" mc:edit="">
                                        <multiline>du {{ now()->startOfWeek()->locale('fr_FR')->translatedFormat('j') }} au {{ now()->endOfWeek()->locale('fr_FR')->translatedFormat('j F Y') }}</multiline>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" height="10" class="mobile"></td>
                                </tr>
                                <tr>
                                    <td align="center" class="mobile" height="4">
                                        <table width="50" height="4" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#EB6361" data-bgcolor="Bottomline_Color">
                                            <tbody>
                                            <tr>
                                                <td></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" height="20" class="mobile"></td>
                                </tr>
                                </tbody>
                            </table>
                            <!-- End Container -->
                        </td>
                    </tr>
                    </tbody>
                </table>

                @foreach($wikis as $wiki)
                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#ececec">
                        <tbody><tr>
                            <td height="50" style="font-family: OpenSans, Arial, sans-serif; font-size:10px; line-height:10px;"></td>
                        </tr>
                        <tr>
                            <td align="center">

                                <!-- Start Container -->
                                <table width="900" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF" class="container" style="border-radius:4px">
                                    <tbody>
                                    <tr>
                                        <td height="30"></td>
                                    </tr>
                                    <tr>
                                        <td height="20" align="center"><table width="800" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF" class="container">
                                                <tbody>
                                                <tr>
                                                    <td width="30" height="20" align="center" class="mobile"></td>
                                                    <td width="800" align="center" valign="top" class="mobile" style="font-family: OpenSans, Arial, sans-serif; font-size:12px; line-height:18px;">
                                                        <!-- Start Container -->
                                                        <table width="300" border="0" align="center" cellpadding="0" cellspacing="0">
                                                            <tbody><tr>
                                                                <td style="font-family: Montserrat, Arial, sans-serif; font-size: 20px; line-height:30px; letter-spacing:1px; color: #333333; font-weight:400;">{{ $wiki->title }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style=" border-left:4px #8d8d8d solid; padding-left:10px; font-family: OpenSans, Arial, sans-serif; font-size: 10px; color: #8D8D8D; line-height:18px; font-weight:400;" data-border-left-color="Vertical_Line"><em>
                                                                        Publié le {{ $wiki->published_at->locale('fr_FR')->translatedFormat('j F Y') }} dans la catégorie {{ $wiki->category->name }}
                                                                    </em></td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20"> </td><!-- Spacer -->
                                                            </tr>
                                                            <tr>
                                                                <td style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; color: #8D8D8D; line-height:23px; font-weight:400;">{{ \Illuminate\Support\Str::limit($wiki->content, 100) }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20"></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" class="mobile" style="font-size:14px; line-height:24px;"><table align="left" width="125px" border="0" cellspacing="0" cellpadding="0" class="container">
                                                                        <tbody><tr>
                                                                            <td><!-- Start Button -->
                                                                                <table width="125" cellpadding="0" cellspacing="0" align="center" border="0">
                                                                                    <tbody><tr>
                                                                                        <td width="130" height="45" bgcolor="#EB6361" align="center" valign="middle" style="font-size: 14px; color: #ffffff; line-height:24px;border-radius: 4px;">
                                                                                            <a href="{{ env("APP_URL") }}/wiki/{{ $wiki->category->id }}/{{ $wiki->id }}" target="_blank" alias="" style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; text-decoration:none; color: #FFFFFF; line-height:23px; font-weight:400;">Voir l'article</a>
                                                                                        </td>
                                                                                    </tr>
                                                                                    </tbody></table>
                                                                                <!-- End Button --></td>
                                                                        </tr>
                                                                        </tbody></table>



                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <!-- End Container -->
                                                    </td>
                                                    <td height="20" align="center" class="mobile"></td>
                                                    <td width="30" class="mobile" style="font-family: OpenSans, Arial, sans-serif; font-size:12px; line-height:18px;" align="center">&nbsp;</td>
                                                </tr>
                                                </tbody>
                                            </table></td>
                                    </tr>
                                    <tr>
                                        <td height="30" class="hide"></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <!-- End Container -->

                            </td>
                        </tr>
                        <tr>
                            <td height="50" style="font-family: OpenSans, Arial, sans-serif; font-size:10px; line-height:10px;"> </td><!-- Spacer -->
                        </tr>
                        </tbody></table>
                @endforeach
            @endif

            <!-- Module Begin7.21-->
            <!-- Start Wrapper -->
            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#333333">
                <tbody>
                <tr>
                    <td height="30"></td>
                </tr>
                <tr>
                    <td align="center">

                        <!-- Start Container -->
                        <table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
                            <tbody>
                            <tr>
                                <td width="300" align="center" valign="top" class="mobile" style="font-family: OpenSans, Arial, sans-serif; font-size:12px; line-height:24px;">
                                    <!-- Start Content -->
                                    <table width="280" border="0" align="center" cellpadding="0" cellspacing="0" class="container">
                                        <tbody>
                                        <tr>
                                            <td height="20" align="center" valign="top"><table align="left" width="280" border="0" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                    <tr>
                                                        <td align="left" style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; color: #ececec; line-height:23px; font-weight:400;" class="mobile">2019 &copy; Trainznation.</td>
                                                    </tr>
                                                    </tbody>
                                                </table></td>
                                        </tr>
                                        </tbody></table>                                        <!-- End Container -->
                                </td>
                                <td height="20" style="" class="mobileOn"></td>
                                <td width="300" align="center" valign="top" class="mobile">
                                    <!-- Start Content -->
                                    <table width="200" border="0" align="right" cellpadding="0" cellspacing="0" class="container">
                                        <tbody>
                                        <tr>
                                            <td height="20" align="center" valign="top"><table align="center" width="200" border="0" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                    <tr>
                                                        <td><table width="200" border="0" align="center" cellpadding="0" cellspacing="0" class="container">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="center" height="20"><table align="left" width="150" border="0" cellspacing="0" cellpadding="0" class="container">
                                                                            <tbody>
                                                                            <tr>
                                                                                <td align="center" height="20">
                                                                                    <!-- Start Content -->
                                                                                    <table align="center" width="150" border="0" cellspacing="0" cellpadding="0">
                                                                                        <tbody><tr>
                                                                                            <td width="25%" align="center"><a href="#"><img src="/storage/other/facebookwht.png" width="16" height="16" alt="facebook"></a></td>
                                                                                            <td width="25%" align="center"><a href="#"><img src="/storage/other/twitterwht.png" width="16" height="16" alt="twitter"></a></td>
                                                                                            <td width="25%" align="center"><a href="#"><img src="/storage/other/youtubewht.png" width="16" height="16" alt="youtube"></a></td>
                                                                                        </tr>
                                                                                        </tbody></table>
                                                                                    <!-- End Content --></td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table></td>
                                                                </tr>
                                                                </tbody></table></td>
                                                    </tr>
                                                    </tbody>
                                                </table></td>
                                        </tr>
                                        </tbody></table>                                        <!-- End Container -->
                                </td>
                            </tr>
                            </tbody></table>
                        <!-- End Container -->

                    </td>
                </tr>
                <tr>
                    <td height="30"> </td><!-- Spacer -->
                </tr>
                </tbody></table>


            <!-- End Wrapper -->
            <!-- End Module -->
        </td>
    </tr>
</table>
<!-- End Background -->
</body>
</html>
