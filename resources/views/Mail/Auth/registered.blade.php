@extends("Mail.Layout.app")

@section("content")
    <table width="500" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="container">
        <tbody>
        <tr>
            <td align="center" bgcolor="#42729B" style="font-size: 35px; line-height: 30px; color: #ffffff; font-weight: 700;"></td>
        </tr>
        <tr>
            <td height="50"></td>
        </tr>
        <tr>
            <td align="center" class="mobile" style="font-size:12px; line-height:24px;"><table width="400" cellpadding="0" cellspacing="0" border="0" class="container">
                    <tbody><tr>
                        <td width="350" align="center" class="mobile" style="font-size:12px; line-height:24px;">
                            <!-- Start Content -->
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="container">
                                <tbody><tr>
                                    <td width="330" align="center" style="font-family: Montserrat, Arial, sans-serif; font-size: 28px; line-height:40px; letter-spacing:2px; color: #333333; font-weight:400;" data-color="H1 Color" data-size="H1 Size" data-min="12" data-max="40" mc:edit=""><multiline label="content">Hi.. Michele</multiline></td>
                                </tr>
                                <tr>
                                    <td height="20" style="font-size:10px; line-height:10px;"></td>
                                </tr>
                                <tr>
                                    <td height="20" align="center" style="font-family: Montserrat, Arial, sans-serif; font-size:16px; letter-spacing:1px; line-height:25px; color: #333333; font-weight:400"data-color="H4 Color" data-size="H4 Size" data-min="12" data-max="30" mc:edit=""><multiline label='content'>Your New Account is Ready</multiline></td>
                                </tr>
                                <tr>
                                    <td height="20" style="font-size:10px; line-height:10px;"> </td><!-- Spacer -->
                                </tr>
                                <tr>
                                    <td align="center" style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; color: #8D8D8D; line-height:23px; font-weight:400;" data-color="H6 Color" data-size="H6 Size" data-min="10" data-max="24" mc:edit=""><multiline label="content">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae.</multiline></td>
                                </tr>
                                </tbody></table>
                            <!-- End Content -->
                        </td>
                    </tr>
                    </tbody></table></td>
        </tr>
        <tr>
            <td align="center" height="50" class="mobile"></td>
        </tr>
        </tbody>
    </table>

    <table width="500" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff" style="border-radius:25px" class="container">
        <tbody>
        <tr>
            <td align="center" bgcolor="#42729B" style=" border-top-left-radius:4px; border-top-right-radius:4px; font-size: 35px; line-height: 30px; color: #ffffff; font-weight: 700;"></td>
        </tr>
        <tr>
            <td align="center" class="mobile"><table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#42729B" data-bgcolor="FootBoxBG_Color" class="container">
                    <tbody><tr>
                        <td width="350" align="center" class="mobile" style=" font-size:12px; line-height:24px;">
                            <!-- Start Content -->
                            <table width="280" cellpadding="0" cellspacing="0" border="0" class="container">
                                <tbody>
                                <tr>
                                    <td height="30"></td>
                                </tr>
                                <tr>
                                    <td height="20"><table width="115" class="container" align="center" border="0" cellspacing="0" cellpadding="0">
                                            <tbody><tr>
                                                <td><table width="115" height="45" cellpadding="0" cellspacing="0" align="center" border="0" style=" border:1px #ffffff solid; border-radius:2px" data-border-color="ButtonBorder_Color">
                                                        <tbody><tr>
                                                            <td align="center" height="36" style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; text-decoration:none; color: #FFFFFF; line-height:23px; font-weight:400;" data-color="H8Linkwht Color" data-size="H8Linkwht Size" data-min="8" data-max="24"><a href="" target="_blank" alias="" style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; text-decoration:none; color: #FFFFFF; line-height:23px; font-weight:400;" data-color="H8Linkwht Color" data-size="H8Linkwht Size" data-min="8" data-max="24" mc:edit=""><multiline>Login</multiline></a></td>
                                                        </tr>
                                                        </tbody></table></td>
                                            </tr>
                                            </tbody></table></td>
                                </tr>
                                <tr>
                                    <td height="30"></td>
                                </tr>
                                </tbody>
                            </table>
                            <!-- End Content -->
                        </td>
                    </tr>
                    </tbody></table></td>
        </tr>
        </tbody></table>
@endsection
