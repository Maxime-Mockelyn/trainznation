<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns:mc="http://www.w3.org/1999/xhtml">
<head>
    <title>{{ env("APP_NAME") }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!--<![endif]-->

    <style type="text/css">

        .ReadMsgBody { width: 100%; background-color: #D2D7D3; }
        .ExternalClass { width: 100%; background-color: #D2D7D3; }
        .a { text-decoration:none; }
        body { width: 100%; background-color: #d8eaf8; margin: 0; padding: 0; -webkit-font-smoothing: antialiased; font-family:OpenSans ,Arial, Helvetica Neue, Helvetica, sans-serif }
        @-ms-viewport{ width: device-width; }

        @font-face {
            font-family: "Montserrat";
            font-style: normal;
            font-weight: 400;
            src: url("Fonts/Montserrat-Regular.ttf") format("truetype"), url("https://fonts.gstatic.com/s/montserrat/v6/zhcz-_WihjSQC0oHJ9TCYPk_vArhqVIZ0nv9q090hN8.woff2") format("woff2");
        }
        @font-face {
            font-family: "Montserrat";
            font-style: Bold;
            font-weight: 700;
            src: url("Fonts/Montserrat-Bold.ttf") format("truetype"), url("https://fonts.gstatic.com/s/montserrat/v6/IQHow_FEYlDC4Gzy_m8fcoWiMMZ7xLd792ULpGE4W_Y.woff2") format('woff2');
        }
        @font-face {
            font-family: "OpenSans";
            font-style: normal;
            font-weight: 400;
            src: url("Fonts/OpenSans-Regular.ttf") format("truetype"), url("https://fonts.gstatic.com/s/opensans/v13/cJZKeOuBrn4kERxqtaUH3VtXRa8TVwTICgirnJhmVJw.woff2") format("woff2");
        }
        @font-face {
            font-family: "OpenSans";
            font-style: normal;
            font-weight: 600;
            src: url("Fonts/OpenSans-Semibold.ttf") format("truetype"), url("https://fonts.gstatic.com/s/opensans/v13/MTP_ySUJH_bn48VBG8sNSugdm0LZdjqr5-oayXSOefg.woff2") format("woff2");
        }

        @font-face {
            font-family: "OpenSans";
            font-style: extraBold;
            font-weight: 900;
            src: url("Fonts/OpenSans-ExtraBold.ttf") format("truetype"), url("https://fonts.gstatic.com/s/opensans/v13/EInbV5DfGHOiMmvb1Xr-hugdm0LZdjqr5-oayXSOefg.woff2") format('woff2');
        }
        @media only screen and (max-width: 639px){
            .wrapper{ width:100%;  padding: 0 !important; }
        }

        @media only screen and (max-width: 480px){
            .centerClass{ margin:0 auto !important; }
            .imgClass{ width:100% !important; height:auto; }
            .img{ text-align:center}
            .hide{ display:none;}
            .wrapper{ width:100%; padding: 0 !important; }
            .header{ width:100%; padding: 0 !important; background-image: url(http://placehold.it/320x400) !important; }
            .container{ width:300px;  padding: 0 !important; }
            .box{ width:250px;  padding: 0 !important; }
            .mobile{ width:300px; display:block; padding: 0 !important; text-align:center !important;}
            .mobile50{ width:300px; padding: 0 !important; text-align:center; }
            *[class="mobileOff"] { width: 0px !important; display: none !important; }
            *[class*="mobileOn"] { display: block !important; max-height:none !important; }
        }

        .MsoNormal {font-family:Montserrat, OpenSans ,Arial, Helvetica Neue, Helvetica, sans-serif !important;}
    </style>

    <!--[if gte mso 15]>
    <style type="text/css">
        table { font-size:1px; line-height:0; mso-margin-top-alt:1px;mso-line-height-rule: exactly; }
        * { mso-line-height-rule: exactly; }
    </style>
    <![endif]-->

</head>
<body marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" style="background-color:#D2D7D3;  font-family: Montserrat, OpenSans, Arial, sans-serif; color:#ffffff; text-decoration:none; margin:0; padding:0; min-width: 100%; -webkit-text-size-adjust:none; -ms-text-size-adjust:none;">

<!-- Start Background -->
<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#D2D7D3">
    <tbody><tr>
        <td width="100%" valign="top" align="center">

            <table width="100%" data-module="Notif15.1" data-thumb="thumbnails/Notif15.1.png" align="center" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#373737" data-bgcolor="MainBG_Color5">
                <tbody>
                    <tr>
                        <td height="50" style="font-family: OpenSans, Arial, sans-serif; font-size:10px; line-height:10px;"></td>
                    </tr>
                </tbody>
            </table>


            <table width="100%" data-module="Notif15.2" data-thumb="thumbnails/Notif15.2.png" align="center" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#373737" data-bgcolor="MainBG_Color5">
                <tbody>
                    <tr>
                        <td align="center">

                            <!-- Start Container -->
                            <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff" style="border-radius:25px" class="container">
                                <tbody>
                                    <tr>
                                        <td align="center" bgcolor="#c4c4c4" style=" border-top-left-radius:4px; border-top-right-radius:4px; font-size: 35px; line-height: 30px; color: #ffffff; font-weight: 700;"></td>
                                    </tr>
                                    <tr>
                                        <td align="center" class="mobile" style="font-size:12px; line-height:24px;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#42729B" data-bgcolor="BlueBoxBG_Color">
                                                <tbody>
                                                    <tr>
                                                        <td height="40"></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="20" align="center">
                                                            <img mc:edit="" editable="" src="{{ \Illuminate\Support\Facades\Storage::disk('public')->url('other/logo.png') }}" width="100" height="50">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="20"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" class="mobile">

                                                            <!-- Start Container -->
                                                            <table width="300" cellpadding="0" cellspacing="0" border="0" class="container">
                                                                <tbody>
                                                                    <tr>
                                                                        <td width="100" height="22" class="mobile50" align="center" style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; text-decoration:none; color: #FFFFFF; line-height:23px; font-weight:400;" data-color="H8Linkwht Color" data-size="H8Linkwht Size" data-min="8" data-max="24">
                                                                            <a href="{{ env("APP_URL") }}" target="_blank" alias="" style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; text-decoration:none; color: #FFFFFF; line-height:23px; font-weight:400;" data-color="H8Linkwht Color" data-size="H8Linkwht Size" data-min="8" data-max="24" mc:edit=""><multiline>Accueil</multiline></a>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <!-- End Container -->

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="40"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- End Container -->

                        </td>
                    </tr>
                </tbody>
            </table>


            @yield("content")




            <table width="100%" data-module="Notif15.8" data-thumb="thumbnails/Notif15.8.png" align="center" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#373737" data-bgcolor="MainBG_Color22">
                <tbody>
                <tr>
                    <td align="center">

                        <!-- Start Container -->
                        <table width="600" cellpadding="0" cellspacing="0" border="0" bgcolor="#42729B" data-bgcolor="BGcolor_footer" class="container">
                            <tbody>
                            <tr>
                                <td align="center" height="40"></td>
                            </tr>
                            <tr>
                                <td width="200" class="mobile" style="font-family: OpenSans, Arial, sans-serif; font-size:12px; line-height:24px;" align="center">
                                    <!-- Start Content -->
                                    <table align="center" width="150" border="0" cellspacing="0" cellpadding="0" class="container">
                                        <tbody>
                                        <tr>
                                            <td align="center" height="20">
                                                <!-- Start Content -->
                                                <table align="center" width="150" border="0" cellspacing="0" cellpadding="0">
                                                    <tbody><tr>
                                                        <td width="25%" align="center"><a href="https://facebook.com/trainznation"><img mc:edit="" editable="" src="{{ \Illuminate\Support\Facades\Storage::disk('public')->url("other/facebookwht.png") }}" width="16" height="16" alt="facebook"></a></td>
                                                        <td width="25%" align="center"><a href="https://twitter.com/trainznation"><img mc:edit="" editable="" src="{{ \Illuminate\Support\Facades\Storage::disk('public')->url("other/twitterwht.png") }}" width="16" height="16" alt="twitter"></a></td>
                                                        <!--<td width="25%" align="center"><a href="#"><img mc:edit="" editable="" src="images/pinterestwht.png" width="16" height="16" alt="dribble"></a></td>
                                                        <td width="25%" align="center"><a href="#"><img mc:edit="" editable="" src="images/dribbblewht.png" width="16" height="16" alt="youtube"></a></td>-->
                                                    </tr>
                                                    </tbody></table>
                                                <!-- End Content --></td>
                                        </tr>
                                        </tbody>
                                    </table>                                        <!-- End Container -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" height="20"></td>
                            </tr>
                            <tr>
                                <td align="center" class="mobile" style="font-family: OpenSans, Arial, sans-serif; font-size:12px; line-height:24px;">
                                    <!-- Start Content -->
                                    <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="container">
                                        <tbody>
                                        <tr>
                                            <td align="center" height="20" class="mobile" style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; color: #ececec; line-height:23px; font-weight:400;" data-color="H7 Color" data-size="H7 Size" data-min="10" data-max="24" mc:edit=""><multiline>copyright © 2019 Trainznation.eu</multiline></td>
                                        </tr>
                                        </tbody></table>                                        <!-- End Container -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" height="5"></td>
                            </tr>
                            <tr>
                                <td align="center" height="40"></td>
                            </tr>
                            </tbody></table>

                        <!-- End Container -->

                    </td>
                </tr>
                </tbody></table>

            <table width="100%" data-module="Notif15.9" data-thumb="thumbnails/Notif15.9.png" align="center" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#373737" data-bgcolor="MainBG_Color5">
                <tbody>
                <tr>
                    <td height="50" style="font-family: OpenSans, Arial, sans-serif; font-size:10px; line-height:10px;"></td>
                </tr>
                </tbody></table>



        </td>
    </tr>
    </tbody></table>
<!-- End Background -->


</body>
</html>
