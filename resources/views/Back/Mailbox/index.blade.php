@extends("Back.Layout.app")

@section("css")
    <link href="/assets/back/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
@endsection

@section("bread")
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Mailbox | Boite de Reception</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('Back.dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Mailbox </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Boite de reception ({{ $countMail }}) </a>

                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
    </div>
@endsection

@section("content")
<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand la la-table"></i>
										</span>
                    <h3 class="kt-portlet__head-title">
                        Ma Boite de reception ({{ $countMail }})
                    </h3>
                </div>
                @if($countMail != 0)
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                <a id="btnDeleteAllMail" href="{{ route('Back.Mailbox.deleteAll') }}" class="btn btn-danger btn-elevate btn-icon-sm">
                                    <i class="la la-trash"></i> Supprimer tous les mails
                                </a>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <div class="kt-portlet__body">
                <!--begin: Search Form -->
                <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                    <div class="row align-items-center">
                        <div class="col-xl-8 order-2 order-xl-1">
                            <div class="row align-items-center">
                                <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                    <div class="kt-input-icon kt-input-icon--left">
                                        <input type="text" class="form-control" placeholder="Rechercher..." id="generalSearch">
                                        <span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--end: Search Form -->

                <!--end: Datatable -->
            </div>

            <div class="kt-portlet__body kt-portlet__body--fit">

                <!--begin: Datatable -->
                <div class="kt-datatable" id="tableMailbox"></div>

                <!--end: Datatable -->
            </div>
        </div>
    </div>
</div>
@endsection

@section("script")
    <script src="/assets/back/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
    <script type="text/javascript">
        "use_strict";

        let TABLE = function () {
            let demo = function(){
                let options = {
                    data: {
                        type: "remote",
                        source: {
                            read: {
                                url: '/back/mailbox/loadMail'
                            },
                        },
                        pageSize: 20, // display 20 records per page
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: true,
                    },
                    // layout definition
                    layout: {
                        scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                        height: 550, // datatable's body's fixed height
                        footer: true, // display/hide footer,
                        icons: {
                            pagination: {
                                next: 'la la-angle-right',
                                prev: 'la la-angle-left',
                                first: 'la la-angle-double-left',
                                last: 'la la-angle-double-right',
                                more: 'la la-ellipsis-h'
                            },
                            sort: {
                                asc: 'la la-arrow-up',
                                desc: 'la la-arrow-down'
                            },
                            rowDetail: {
                                expand: 'fa fa-caret-down',
                                collapse: 'fa fa-caret-right'
                            }
                        }
                    },

                    // column sorting
                    sortable: true,

                    pagination: true,

                    translate: {
                        records: {
                            processing: 'Chargement de la liste...',
                            noRecords: 'Aucun Mail dans la boite de reception',
                        },
                        toolbar: {
                            pagination: {
                                items: {
                                    default: {
                                        first: 'Primero',
                                        prev: 'Anterior',
                                        next: 'Siguiente',
                                        last: 'Último',
                                        more: 'Más páginas',
                                        input: 'Número de página',
                                        select: 'Seleccionar tamaño de página',
                                    }
                                },
                            },
                        },
                    },

                    search: {
                        input: $('#generalSearch'),
                    },
                    columns: [
                        {
                            field: 'RecordId',
                            title: "#",
                            sortable: true,
                            type: 'number',
                            width: "30",
                            selector: {class: 'kt-checkbox--solid'},
                            textAlign: 'center',
                        },  {
                            field: "object",
                            title: "Sujet",
                        }, {
                            field: "fromAdress",
                            autoHide: false
                        }, {
                            field: "fromName",
                            title: "De:",
                            template: function (row) {
                                return row.fromName+' <'+row.fromAddress+'>';
                            }
                        }, {
                            field: "date",
                            title: "Date",
                            type: 'date',
                            format: "DD/MM/YYYY à H:I"
                        }, {
                            field: "Actions",
                            title: "Actions",
                            sortable: false,
                            width: 110,
                            overflow: 'visible',
                            autoHide: false,
                            template: function (row) {
                                return `
                                    <a href="/back/mailbox/`+row.id+`/delete" class="btn btn-sm btn-icon btn-danger"><i class="la la-trash"></i> </a>
                                `;
                            }
                        }
                    ]
                };
                let datatable = $('.kt-datatable').KTDatatable(options);
            };

            return {
                init: function () {
                    demo();
                }
            }
        }();
        jQuery(document).ready(function() {
            TABLE.init();
        });
    </script>
    <script type="text/javascript">
        (function ($) {
            $("#btnDeleteAllMail").on('click', function (e) {
                e.preventDefault()
                let btn = $(this)
                let url = btn.attr('href');

                btn.attr('disabled', true).html('<i class="la la-spinner la-spin"></i> Suppression en cours...')

                $.ajax({
                    url: url,
                    success: function (data) {
                        btn.removeAttr('disabled').html('<i class="la la-trash"></i> Supprimer tous les mails')
                        toastr.success("Le Processus de suppression à supprimé "+data.count, "Boite de Reception")
                    },
                    error: function () {
                        btn.removeAttr('disabled').html('<i class="la la-trash"></i> Supprimer tous les mails')
                        toastr.error("Erreur lors de l'execution du processus de suppression", "Boite de Reception")
                    }
                })
            })
        })(jQuery)
    </script>
@endsection
