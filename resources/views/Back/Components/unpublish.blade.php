@if(array_key_exists('id', $data))
    <a href="{{ route($data['url'], $data['id']) }}" class="btn btn-danger">
        @else
            <a href="{{ route($data['url']) }}" class="btn btn-danger">
                @endif
                <i class="la la-times-circle"></i>

                <span class="kt-subheader__btn-daterange-title">Dépublier {{ $data['terme'] }}</span>&nbsp;

            </a>
