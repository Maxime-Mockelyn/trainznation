@if(array_key_exists('id', $data))
    <a href="{{ route($data['url'], $data['id']) }}" class="btn btn-success">
        @else
            <a href="{{ route($data['url']) }}" class="btn btn-success">
                @endif
                <i class="la la-check-circle"></i>

                <span class="kt-subheader__btn-daterange-title">Publier {{ $data['terme'] }}</span>&nbsp;

            </a>
