@extends("Back.Layout.app")

@section("css")

@endsection

@section("bread")
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Utilisateurs | Tableau de Bord</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('Back.dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Utilisateurs </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Tableau de Bord </a>

                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
    </div>
@endsection

@section("content")
<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="flaticon2-graph"></i>
            </span>
            <h3 class="kt-portlet__head-title">
                Liste des Utilisateurs
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">

        <!--begin: Search Form -->
        <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
            <div class="row align-items-center">
                <div class="col-xl-8 order-2 order-xl-1">
                    <div class="row align-items-center">
                        <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                            <div class="kt-input-icon kt-input-icon--left">
                                <input type="text" class="form-control" placeholder="Recherche..." id="generalSearch">
                                <span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
                            </div>
                        </div>
                        <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                            <div class="kt-form__group kt-form__group--inline">
                                <div class="kt-form__label">
                                    <label>Status:</label>
                                </div>
                                <div class="kt-form__control">
                                    <select class="form-control bootstrap-select" id="kt_form_group">
                                        <option value="">All</option>
                                        <option value="0">Utilisateur</option>
                                        <option value="1">Administrateur</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 order-1 order-xl-2 kt-align-right">
                    <a href="#" class="btn btn-default">
                        <i class="la la-cart-plus"></i> Nouvelle Utilisateur
                    </a>
                    <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg d-xl-none"></div>
                </div>
            </div>
        </div>

        <!--end: Search Form -->
    </div>
    <div class="kt-portlet__body kt-portlet__body--fit">

        <!--begin: Datatable -->
        <table class="kt-datatable" id="html_table" width="100%">
            <thead>
                <tr>
                    <th title="Field #1">#</th>
                    <th title="Field #2">Identifiant</th>
                    <th title="Field #3">Adresse Mail</th>
                    <th title="Field #4">Groupe</th>
                    <th title="Field #5">Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td align="right">{{ $user->group }}</td>
                    <td>
                        <a href=""></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <!--end: Datatable -->
    </div>
</div>
@endsection

@section("script")
    <script type="text/javascript">
        let KTDatatableHtmlTableDemo = function() {
            // Private functions

            // demo initializer
            let demo = function() {

                let datatable = $('.kt-datatable').KTDatatable({
                    data: {
                        saveState: {cookie: false},
                    },
                    search: {
                        input: $('#generalSearch'),
                    },
                    columns: [
                        {
                            field: '#',
                            title: "ID",
                            sortable: false
                        },
                        {
                            field: 'Identifiant',
                            title: "Identifiant"
                        },{
                            field: 'Adresse Mail',
                            title: "Adresse Mail"
                        },

                        {
                            field: 'Groupe',
                            title: 'Groupe',
                            autoHide: false,
                            // callback function support for column rendering
                            template: function(row) {
                                let group = {
                                    0: {'title': 'Utilisateur', 'class': 'kt-badge--brand'},
                                    1: {'title': 'Administrateur', 'class': 'kt-badge--danger'},
                                };
                                console.log(group)
                                return '<span class="kt-badge ' + group[row.Groupe].class + ' kt-badge--inline kt-badge--pill">' + group[row.Groupe].title + '</span>';
                            },
                        },

                        {
                            field: 'action',
                            title: "Actions",
                            autoHide: false,
                        },
                    ],
                });

                $('#kt_form_group').on('change', function() {
                    datatable.search($(this).val().toLowerCase(), 'group');
                });

                $('#kt_form_status,#kt_form_type').selectpicker();

            };

            return {
                // Public functions
                init: function() {
                    // init dmeo
                    demo();
                },
            };
        }();

        jQuery(document).ready(function() {
            KTDatatableHtmlTableDemo.init();
        });
    </script>
@endsection
