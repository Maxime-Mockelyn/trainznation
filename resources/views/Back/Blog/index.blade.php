@extends("Back.Layout.app")

@section("css")
    <link href="/assets/back/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
@endsection

@section("bread")
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Tableau de Bord | Bienvenue {{ auth()->user()->name }}</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('Back.dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Blog </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Liste des Articles </a>

                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
    </div>
@endsection

@section("content")
<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand la la-table"></i>
										</span>
                    <h3 class="kt-portlet__head-title">
                        Liste des Catégories
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="kt-portlet__head-actions">
                            <a href="{{ route('Blog.Category.create') }}" class="btn btn-brand btn-elevate btn-icon-sm">
                                <i class="la la-plus"></i>
                                Nouvelle catégorie
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="tableCategorie">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Libellée</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td>{{ $category->id }}</td>
                                <td>{{ $category->name }}</td>
                                <td>
                                    <a href="{{ route('Blog.Category.edit', $category->id) }}" class="btn btn-sm btn-brand btn-elevate btn-icon"><i class="la la-edit"></i></a>
                                    <button id="btnDelete" type="button" class="btn btn-sm btn-danger btn-elevate btn-icon" data-href="{{ route('Blog.Category.delete', $category->id) }}"><i class="la la-trash"></i></button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <!--end: Datatable -->
            </div>
        </div>
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand la la-table"></i>
										</span>
                    <h3 class="kt-portlet__head-title">
                        Liste des Articles
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="kt-portlet__head-actions">
                            <a href="{{ route('Back.Blog.create') }}" class="btn btn-brand btn-elevate btn-icon-sm">
                                <i class="la la-plus"></i>
                                Nouvelle Article
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="tableArticle">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Titre</th>
                        <th>Catégorie</th>
                        <th>Publié</th>
                        <th>Twitter</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($blogs as $blog)
                        <tr>
                            <td>{{ $blog->id }}</td>
                            <td>
                                {{ $blog->title }}<br>
                                <i>{{ $blog->slug }}</i>
                            </td>
                            <td>{{ $blog->categorie->name }}</td>
                            <td>
                                @if($blog->published == 0)
                                    <span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill kt-badge--rounded"><i class="la la-times-circle"></i> Non Publier</span>
                                @else
                                    <span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--rounded"><i class="la la-check-circle"></i> Publier</span><br>
                                    <i>Date de Publication: {{ $blog->published_at->format("d/m/Y H:i") }}</i>
                                @endif
                            </td>
                            <td>
                                @if($blog->twitter == 0)
                                    <span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill kt-badge--rounded"><i class="la la-times-circle"></i> Non Publier</span>
                                @else
                                    <span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill kt-badge--rounded"><i class="la la-check-circle"></i> Publier</span><br>
                                @endif
                            </td>
                            <td>
                                @if($blog->published == 0)
                                    <a class="btn btn-success btn-light btn-icon" href="{{ route('Back.Blog.publish', $blog->id) }}"><i class="fa fa-unlock"></i></a>
                                @else
                                    <a class="btn btn-danger btn-light btn-icon" href="{{ route('Back.Blog.unPublish', $blog->id) }}"><i class="fa fa-lock"></i></a>
                                @endif
                                <a class="btn btn-sm btn-light btn-icon" href="{{ route('Back.Blog.show', $blog->slug) }}"><i class="fa fa-eye"></i></a>
                                @if($blog->published == 0)
                                        <a class="btn btn-sm btn-primary btn-icon" href="{{ route('Back.Blog.edit', $blog->id) }}"><i class="fa fa-edit"></i></a>
                                @endif
                                <a class="btn btn-sm btn-danger btn-icon" href="{{ route('Back.Blog.delete', $blog->slug) }}"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <!--end: Datatable -->
            </div>
        </div>
    </div>
</div>
@endsection

@section("script")
    <script src="/assets/back/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
    <script type="text/javascript">
        "use strict";
        var tableCategorie = function() {

            var initTable1 = function() {
                var table = $('#tableCategorie');

                // begin first table
                table.DataTable({
                    responsive: true,

                    // DOM Layout settings
                    dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

                    lengthMenu: [5, 10, 25, 50],

                    pageLength: 10,

                    language: {
                        "sProcessing":     "Traitement en cours...",
                        "sSearch":         "Rechercher&nbsp;:",
                        "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
                        "sInfo":           "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                        "sInfoEmpty":      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                        "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                        "sInfoPostFix":    "",
                        "sLoadingRecords": "Chargement en cours...",
                        "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                        "sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
                        "oPaginate": {
                            "sFirst":      "Premier",
                            "sPrevious":   "Pr&eacute;c&eacute;dent",
                            "sNext":       "Suivant",
                            "sLast":       "Dernier"
                        },
                        "oAria": {
                            "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
                            "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
                        },
                        "select": {
                            "rows": {
                                _: "%d lignes séléctionnées",
                                0: "Aucune ligne séléctionnée",
                                1: "1 ligne séléctionnée"
                            }
                        }
                    },

                    // Order settings
                    order: [[1, 'desc']],

                    columnDefs: [],
                });
            };

            return {

                //main function to initiate the module
                init: function() {
                    initTable1();
                },

            };

        }();
        var tableArticle = function() {

            var initTable1 = function() {
                var table = $('#tableArticle');

                // begin first table
                table.DataTable({
                    responsive: true,

                    // DOM Layout settings
                    dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

                    lengthMenu: [5, 10, 25, 50],

                    pageLength: 10,

                    language: {
                        "sProcessing":     "Traitement en cours...",
                        "sSearch":         "Rechercher&nbsp;:",
                        "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
                        "sInfo":           "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                        "sInfoEmpty":      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                        "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                        "sInfoPostFix":    "",
                        "sLoadingRecords": "Chargement en cours...",
                        "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                        "sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
                        "oPaginate": {
                            "sFirst":      "Premier",
                            "sPrevious":   "Pr&eacute;c&eacute;dent",
                            "sNext":       "Suivant",
                            "sLast":       "Dernier"
                        },
                        "oAria": {
                            "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
                            "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
                        },
                        "select": {
                            "rows": {
                                _: "%d lignes séléctionnées",
                                0: "Aucune ligne séléctionnée",
                                1: "1 ligne séléctionnée"
                            }
                        }
                    },

                    // Order settings
                    order: [[1, 'desc']],

                    columnDefs: [],
                });
            };

            return {

                //main function to initiate the module
                init: function() {
                    initTable1();
                },

            };

        }();

        jQuery(document).ready(function() {
            tableCategorie.init();
            tableArticle.init();
        });
    </script>
    <script type="text/javascript">
        (function ($) {
            $("#tableCategorie").on('click', '#btnDelete', function (e) {
                e.preventDefault()
                let btn = $(this)
                let url = btn.attr('data-href')

                btn.addClass('kt-spinner kt-spinner--light')

                $.ajax({
                    url: url,
                    method: "DELETE",
                    statusCode: {
                        500: function(data){
                            btn.removeClass('kt-spinner kt-spinner-light')
                            toastr.error("Une Erreur serveur à eu lieu", "Erreur Serveur !")
                        },
                        200: function(data){
                            btn.removeClass('kt-spinner kt-spinner-light')
                            toastr.success("La catégorie à été supprimer", "Suppression d'une catégorie")
                            btn.parents('tr').fadeOut();
                        }
                    }
                })
            })


            $("#tableArticle").on('click', '#btnDelete', function (e) {
                e.preventDefault()
                let btn = $(this)
                let url = btn.attr('data-href')

                btn.addClass('kt-spinner kt-spinner--light')

                $.ajax({
                    url: url,
                    method: "DELETE",
                    statusCode: {
                        500: function(data){
                            btn.removeClass('kt-spinner kt-spinner-light')
                            toastr.error("Une Erreur serveur à eu lieu", "Erreur Serveur !")
                        },
                        200: function(data){
                            btn.removeClass('kt-spinner kt-spinner-light')
                            toastr.success("L'article à été supprimer", "Suppression d'un Article")
                            btn.parents('tr').fadeOut();
                        }
                    }
                })
            })
                .on('click', "#btnUnlock", function (e) {
                    e.preventDefault()
                    let btn = $(this)
                    let url = btn.attr('data-href')

                    btn.attr('disabled', true)

                    $.ajax({
                        url: url,
                        method: "get",
                        success: function (data) {
                            btn.removeAttr('disabled')
                            if(data.twitter === true)
                            {
                                toastr.success("L'article à été publier avec succès !", "Publication d'un article")
                                toastr.success("L'article à été publier sur twitter !", "Publication d'un article sur twitter")
                            }else{
                                toastr.success("L'article à été publier avec succès !", "Publication d'un article")
                            }
                        },
                        error: function () {
                            btn.removeAttr('disabled')
                            toastr.error("Erreur Serveur !", "Erreur 500")
                        }
                    })
                })
                .on('click', "#btnLock", function (e) {
                    e.preventDefault()
                    let btn = $(this)
                    let url = btn.attr('data-href')

                    btn.attr('disabled', true)

                    $.ajax({
                        url: url,
                        method: "get",
                        success: function (data) {
                            btn.removeAttr('disabled')
                            toastr.success("L'article à été dépublier avec succès !", "Dépublication d'un article")
                        },
                        error: function () {
                            btn.removeAttr('disabled')
                            toastr.error("Erreur Serveur !", "Erreur 500")
                        }
                    })
                })


        })(jQuery)
    </script>
@endsection
