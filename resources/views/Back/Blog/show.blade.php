@extends("Back.Layout.app")

@section("css")
    <link href="/assets/back/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
@endsection

@section("bread")
    <div id="blog_id" data-value="{{ $blog->id }}"></div>
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Blog | {{ $blog->title }}</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('Back.dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Blog </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">{{ $blog->title }} </a>

                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                @back(["data" => ["url" => "Back.Blog.index"]]) @endback
                @edit(["data" => ["url" => "Back.Blog.edit", "terme" => "l'article", "id" => $blog->id]]) @endedit
                @delete(["data" => ["url" => "Back.Blog.delete", "terme" => "l'article", "id" => $blog->slug]]) @enddelete
                @if($blog->published == 0)
                    @publish(["data" => ["url" => "Back.Blog.publish", "terme" => "l'article", "id" => $blog->id]]) @endpublish
                @else
                    @unpublish(["data" => ["url" => "Back.Blog.unPublish", "terme" => "l'article", "id" => $blog->id]]) @endunpublish
                @endif
            </div>
        </div>
    </div>
@endsection

@section("content")
    @if($blog->published == 0)
        <div class="alert alert-info fade show" role="alert">
            <div class="alert-icon"><i class="flaticon-warning-sign"></i></div>
            <div class="alert-text">Cette article n'est pas encore publier, les informations de commentaire ne sont donc pas disponible !</div>
            <div class="alert-close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="la la-close"></i></span>
                </button>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-md-4">
            <div class="kt-portlet kt-portlet--height-fluid kt-widget19">
                <div class="kt-portlet__body kt-portlet__body--fit">
                    <div class="kt-widget19__pic kt-portlet-fit--top kt-portlet-fit--sides" style="min-height: 300px; background-image: url({{ sourceImage('blog/'.$blog->id.'.png') }})">
                        <h3 class="kt-widget19__title kt-font-light">
                            {{ $blog->title }}
                        </h3>
                        <div class="kt-widget19__shadow"></div>
                        <div class="kt-widget19__labels">
                            @if($blog->published == 1)
                                <a href="#" class="btn btn-label-light-o2 btn-success btn-bold btn-sm ">Publier</a>
                            @else
                                <a href="#" class="btn btn-label-light-o2 btn-danger btn-bold btn-sm ">Non Publier</a>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-widget19__wrapper">
                        <div class="kt-widget19__content">
                            <div class="kt-widget19__info">
                                <span class="kt-widget19__time">
                                {{ $blog->categorie->name }}
                            </span>
                            </div>
                            <div class="kt-widget19__stats">
														<span class="kt-widget19__number kt-font-brand">
                                                            {{ \App\Repository\Blog\BlogCommentRepository::getCountCommentFromBlog($blog->id) }}
														</span>
                                <a href="#" class="kt-widget19__comment">
                                    {{ formatPlural('Commentaire', \App\Repository\Blog\BlogCommentRepository::getCountCommentFromBlog($blog->id)) }}
                                </a>
                            </div>
                        </div>
                        <div class="kt-widget19__text">
                            {!! str_limit($blog->content, 100, '...') !!}
                        </div>
                    </div>
                    <div class="kt-widget19__action">
                        <a href="{{ route('Front.Blog.show', $blog->slug) }}" target="_blank" class="btn btn-sm btn-label-brand btn-bold">Voir sur le FrontEnd</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    {!! $blog->content !!}
                </div>
            </div>

            @if($blog->published == 1)
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Liste des Commentaires
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <table class="table table-striped- table-bordered table-hover table-checkable" id="tableComment">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Utilisateur</th>
                                <th>Commentaire</th>
                                <th>Etat</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($blog->comments as $comment)
                                <td>{{ $comment->id }}</td>
                                <td>
                                    <div class="kt-user-card-v2">
                                        <div class="kt-user-card-v2__pic">
                                            @if(auth()->user()->email == 'contact@trainznation.eu')
                                                <img class="" alt="Pic" src="{{ sourceImage('avatar/trainznation.png') }}" />
                                            @else
                                                @if(auth()->user()->account->avatar == 1)
                                                    <img class="" alt="Pic" src="{{ sourceImage('avatar/'.auth()->user()->id.'.png') }}" />
                                                @else
                                                    <img class="" alt="Pic" src="{{ sourceImage('avatar/placeholder.png') }}" />
                                                @endif
                                            @endif
                                        </div>
                                        <div class="kt-user-card-v2__details">
                                            <a class="kt-user-card-v2__name" href="#">{{ $comment->user->name }}</a>
                                            <span class="kt-user-card-v2__email">{{ formatDateHuman($comment->created_at) }}</span>
                                        </div>
                                    </div>
                                </td>
                                <td>{{ $comment->comment }}</td>
                                <td class="text-center">
                                    {!! stateComment($comment->state) !!}
                                </td>
                                <td>
                                    @if($comment->state == 0)
                                        <button id="btnApprouved" data-href="{{ route('Blog.Comment.approuved', [$blog->id, $comment->id]) }}" data-id="{{ $comment->id }}" class="btn btn-icon btn-success" data-toggle="kt-tooltip" title="Approvée le commentaire"><i class="la la-check"></i> </button>
                                    @else
                                        <button id="btnDesapprouved" data-href="{{ route('Blog.Comment.desapprouved', [$blog->id, $comment->id]) }}" data-id="{{ $comment->id }}" class="btn btn-icon btn-danger" data-toggle="kt-tooltip" title="Désapprouvée le commentaire"><i class="la la-times"></i> </button>
                                    @endif
                                </td>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection

@section("script")
    <script src="/assets/back/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
    <script type="text/javascript">
        "use strict";
        let tableComment = function() {

            let initTable1 = function() {
                let table = $('#tableComment');

                // begin first table
                table.DataTable({
                    responsive: true,

                    // DOM Layout settings
                    dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

                    lengthMenu: [5, 10, 25, 50],

                    pageLength: 10,

                    language: {
                        "sProcessing":     "Traitement en cours...",
                        "sSearch":         "Rechercher&nbsp;:",
                        "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
                        "sInfo":           "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                        "sInfoEmpty":      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                        "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                        "sInfoPostFix":    "",
                        "sLoadingRecords": "Chargement en cours...",
                        "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                        "sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
                        "oPaginate": {
                            "sFirst":      "Premier",
                            "sPrevious":   "Pr&eacute;c&eacute;dent",
                            "sNext":       "Suivant",
                            "sLast":       "Dernier"
                        },
                        "oAria": {
                            "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
                            "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
                        },
                        "select": {
                            "rows": {
                                _: "%d lignes séléctionnées",
                                0: "Aucune ligne séléctionnée",
                                1: "1 ligne séléctionnée"
                            }
                        }
                    },

                    // Order settings
                    order: [[1, 'desc']],

                    columnDefs: [],
                });
            };

            return {

                //main function to initiate the module
                init: function() {
                    initTable1();
                },

            };

        }();

        jQuery(document).ready(function() {
            tableComment.init();
        });
    </script>

    <script type="text/javascript">
        (function ($) {
            let blog_id = $("#blog_id").attr('data-value')

            $("#tableComment")
                .on('click', '#btnApprouved', function (e) {
                    e.preventDefault()
                    let btn = $(this)
                    let url = btn.attr('data-href')
                    let identifier = btn.attr('data-id')

                    btn.attr('disabled', true).html('<span class="kt-spinner kt-spinner--v2 kt-spinner--sm kt-spinner--brand"></span>')

                        $.ajax({
                            url: url,
                            success: function (data) {
                                btn
                                    .removeAttr('disabled')
                                    .removeClass('btn-success')
                                    .addClass('btn-danger')
                                    .attr('id', "btnDesapprouved")
                                    .attr('data-href', '/back/blog/'+blog_id+'/comment/'+identifier+'/desapprouved')
                                    .attr('title', "Désapprouvée le commentaire")
                                    .html('<i class="la la-times"></i>')

                                toastr.success("Le commentaire "+ identifier + " à été approuvée", "Commentaire")
                            },
                            error: function (jqxhr) {
                                btn
                                    .removeClass('btn-success')
                                    .addClass('btn-warning')
                                    .html('<i class="la la-warning"></i>')

                                toastr.error("Erreur lors de l'execution du script: "+jqxhr.responseText, "Erreur Serveur")
                            }
                        })
                })
                .on('click', '#btnDesapprouved', function (e) {
                    e.preventDefault()
                    let btn = $(this)
                    let url = btn.attr('data-href')
                    let identifier = btn.attr('data-id')

                    btn.attr('disabled', true).html('<span class="kt-spinner kt-spinner--v2 kt-spinner--sm kt-spinner--brand"></span>')

                    $.ajax({
                        url: url,
                        success: function (data) {
                            btn
                                .removeAttr('disabled')
                                .removeClass('btn-danger')
                                .addClass('btn-success')
                                .attr('id', "btnApprouved")
                                .attr('data-href', '/back/blog/'+blog_id+'/comment/'+identifier+'/approuved')
                                .attr('title', "Approuvée le commentaire")
                                .html('<i class="la la-check"></i>')

                            toastr.success("Le commentaire "+ identifier + " à été désapprouvée", "Commentaire")
                        },
                        error: function (jqxhr) {
                            btn
                                .removeClass('btn-danger')
                                .addClass('btn-warning')
                                .html('<i class="la la-warning"></i>')

                            toastr.error("Erreur lors de l'execution du script: "+jqxhr.responseText, "Erreur Serveur")
                        }
                    })
                })
        })(jQuery)
    </script>
@endsection
