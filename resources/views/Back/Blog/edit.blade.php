@extends("Back.Layout.app")

@section("css")
    <style type="text/css">
        .ck-editor__editable {
            min-height: 500px;
        }
    </style>
    <link rel="stylesheet" href="/assets/custom/vendors/editor-md/css/editormd.css">
@endsection

@section("bread")
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Blog | Edition d'un article</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('Back.dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Blog </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Article </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Edition d'un Article </a>

                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
    </div>
@endsection

@section("content")
<div class="row">
    <div class="col-md-8">
        <form action="{{ route('Back.Blog.update', $blog->id) }}" class="kt-form" method="POST">
            @csrf
            @method("PUT")
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Informations Générales
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Titre</label>
                                <input type="text" id="title" class="form-control" value="{{ $blog->title }}" name="title" onkeyup="getSlug()">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>URL</label>
                                <input type="text" id="slug" class="form-control" value="{{ $blog->slug }}" name="slug">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Categorie</label>
                                <select id="category_id" class="form-control kt-selectpicker" data-live-search="true" name="category_id">
                                    <option value="{{ $blog->categorie_id }}">{{ $blog->categorie->name }}</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Courte description</label>
                                <textarea class="form-control" name="short_content">{{ $blog->short_content }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__foot">
                    <div class="row align-items-center">
                        <div class="col-lg-12 kt-align-center">
                            <button type="submit" class="btn btn-success"><i class="la la-check-circle"></i> Valider</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-4">
        <form action="{{ route('Back.Blog.updateContent', $blog->id) }}" class="kt-form" method="POST" enctype="multipart/form-data">
            @csrf
            @method("PUT")
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon2-graph"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            Contenue
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="form-group">
                        @if(Storage::exists('blog/'.$blog->id.'.png'))
                            <img src="/storage/blog/{{ $blog->id }}.png" width="150" alt="{{ $blog->title }}">
                        @else
                            <img src="https://via.placeholder.com/150" width="150" alt="{{ $blog->title }}">
                        @endif
                        <br>
                            <label>Image de l'article</label>
                            <input type="file" name="file" class="form-control">
                    </div>
                    @if($blog->twitter == 1)
                        <div class="form-group">
                            <label>Tweet</label>
                            <textarea class="form-control" id="kt_maxlength_5" name="twitterText" maxlength="280" rows="6">{{ $blog->twitterText }}</textarea>
                        </div>
                    @endif
                    @if($blog->published == 1)
                    <div class="form-group">
                        <label>Date de publication</label>
                        <input type="text" class="form-control" id="kt_datetimepicker_1" readonly placeholder="Select date & time" />
                    </div>
                    @endif
                </div>
                <div class="kt-portlet__foot">
                    <div class="row align-items-center">
                        <div class="col-lg-12 kt-align-middle">
                            <button type="submit" class="btn btn-success"><i class="la la-check-circle"></i> Soumettre</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <form action="{{ route('Back.Blog.updateContenu', $blog->id) }}" class="kt-form" method="POST">
            @csrf
            @method("PUT")
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Description
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="form-group">
                        <div id="fieldDesc">
                            <textarea style="display:none;" name="description">{{ $blog->short_content }}&#10;{{ $blog->content }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__foot">
                    <div class="row align-items-center">
                        <div class="col-lg-6 kt-align-middle">
                            <button type="submit" class="btn btn-success"><i class="la la-check-circle"></i> Soumettre</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section("script")
    <script type="text/javascript" src="/assets/back/vendors/general/bootstrap-datetime-picker/js/locales/bootstrap-datetimepicker.fr.js"></script>
    <script src="/assets/custom/vendors/ckeditor/ckeditor.js"></script>
    <script src="/assets/custom/vendors/editor-md/editormd.js"></script>
    <script src="/assets/custom/vendors/editor-md/languages/en.js"></script>
    <script type="text/javascript">
        function sluggify(str)
        {
            str = str.replace(/^\s+|\s+$/g, ''); // trim
            str = str.toLowerCase();

            // remove accents, swap ñ for n, etc
            let from = "àáãäâèéëêìíïîòóöôùúüûñç·/_,:;";
            let to   = "aaaaaeeeeiiiioooouuuunc------";

            for (var i=0, l=from.length ; i<l ; i++) {
                str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
            }

            str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
                .replace(/\s+/g, '-') // collapse whitespace and replace by -
                .replace(/-+/g, '-'); // collapse dashes

            return str;

        }

        function getSlug()
        {
            let title = $("#title").val()
            let slug = $("#slug")

            let string = sluggify(title);

            slug.val(string)
        }
        (function ($) {
            $(".kt-selectpicker").selectpicker({
                container: 'body'
            });
            //createCkeditor("#description")
            let editor = editormd("fieldDesc", {
                width: '100%',
                height: '460px',
                path:"/assets/custom/vendors/editor-md/lib/"
            })
            $(".kt_datetimepicker_1").datetimepicker({
                todayHighlight: true,
                autoclose: true,
                pickerPosition: 'bottom-left',
                format: 'yyyy-mm-dd hh:ii:00',
                language: 'fr'
            })
            $('#kt_maxlength_5').maxlength({
                alwaysShow: true,
                warningClass: "kt-badge kt-badge--primary kt-badge--rounded kt-badge--inline",
                limitReachedClass: "kt-badge kt-badge--brand kt-badge--rounded kt-badge--inline"
            });
        })(jQuery)
    </script>
@endsection
