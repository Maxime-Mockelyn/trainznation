@extends("Back.Layout.app")

@section("css")
    <style type="text/css">
        .ck-editor__editable {
            min-height: 500px;
        }
    </style>
@endsection

@section("bread")
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Blog | Création d'un article</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('Back.dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Blog </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Article </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Création d'un Article </a>

                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="{{ route('Back.Blog.index') }}" class="btn kt-subheader__btn-primary">
                    <i class="la la-angle-double-left"></i>
                    Retour &nbsp;
                </a>
            </div>
        </div>
    </div>
@endsection

@section("content")
    <form action="{{ route('Back.Blog.store') }}" class="kt-form" method="POST">
        @csrf
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon2-graph"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        Nouvelle article
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Titre</label>
                            <input type="text" id="title" class="form-control" placeholder="Titre de l'article" name="title" onkeyup="getSlug()">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>URL</label>
                            <input type="text" id="slug" class="form-control" name="slug">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Categorie</label>
                            <select id="category_id" class="form-control kt-selectpicker" data-live-search="true" name="category_id">
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Courte description</label>
                            <textarea class="form-control" name="short_content"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 kt-align-center form-group">
                        <div class="kt-checkbox-inline">
                            <label class="kt-checkbox">
                                <input type="checkbox" name="published" value="1"> Publier
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 kt-align-center form-group">
                        <div class="kt-checkbox-inline">
                            <label class="kt-checkbox">
                                <input type="checkbox" name="twitter" value="1"> Twitter
                                <span></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__foot">
                <div class="row align-items-center">
                    <div class="col-lg-12 kt-align-right">
                        <button type="submit" class="btn btn-brand">Soumettre</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section("script")
    <script src="/assets/custom/vendors/ckeditor/ckeditor.js"></script>
    <script type="text/javascript">
        function sluggify(str)
        {
            str = str.replace(/^\s+|\s+$/g, ''); // trim
            str = str.toLowerCase();

            // remove accents, swap ñ for n, etc
            let from = "àáãäâèéëêìíïîòóöôùúüûñç·/_,:;";
            let to   = "aaaaaeeeeiiiioooouuuunc------";

            for (var i=0, l=from.length ; i<l ; i++) {
                str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
            }

            str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
                .replace(/\s+/g, '-') // collapse whitespace and replace by -
                .replace(/-+/g, '-'); // collapse dashes

            return str;

        }

        function getSlug()
        {
            let title = $("#title").val()
            let slug = $("#slug")

            let string = sluggify(title);

            slug.val(string)
        }
        (function ($) {
            $(".kt-selectpicker").selectpicker({
                container: 'body',
                search: true
            });
        })(jQuery)
    </script>
@endsection
