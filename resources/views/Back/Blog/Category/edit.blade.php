@extends("Back.Layout.app")

@section("css")

@endsection

@section("bread")
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Blog | Edition d'une catégorie</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('Back.dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Blog </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Catégorie </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Edition d'une catégorie </a>

                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
    </div>
@endsection

@section("content")
<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
            <form action="{{ route('Blog.Category.update', $category->id) }}" class="kt-form" id="editForm" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">Formulaire d'édition</h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <a href="{{ route('Back.Blog.index') }}" class="btn btn-clean kt-margin-r-10">
                            <i class="la la-arrow-left"></i>
                            <span class="kt-hidden-mobile">Retour</span>
                        </a>
                        <div class="btn-group">
                            <button type="submit" class="btn btn-brand" id="btnForm">
                                <i class="la la-check"></i>
                                <span class="kt-hidden-mobile">Sauvegarder</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="row">
                        <div class="col-xl-3"></div>
                        <div class="col-xl-6">
                            <div class="kt-section kt-section--first">
                                <div class="kt-section__body">
                                    <div class="form-group row">
                                        <label class="col-3 col-form-label">Nom de la catégorie</label>
                                        <div class="col-9">
                                            <input class="form-control" type="text" name="name" value="{{ $category->name }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3"></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section("script")

    <script type="text/javascript">
        (function ($) {
            $("#editForm").on('submit', function (e) {
                e.preventDefault()
                let form = $(this)
                let url = form.attr('action')
                let btn = $("#btnForm")
                let data = form.serializeArray()

                btn.addClass('kt-spinner kt-spinner--light')

                $.ajax({
                    url: url,
                    method: "PUT",
                    data: data,
                    statusCode: {
                        422: function (data) {
                            btn.removeClass('kt-spinner kt-spinner--light')
                            toastr.error(data.responseJSON.errors['name'][0], "Erreur 422 !")
                        },
                        200: function (data) {
                            btn.removeClass('kt-spinner kt-spinner--light')
                            toastr.success("La catégorie <strong>"+data.name+"</strong> à été edité", "Edition d'une catégorie")
                        },
                        500: function () {
                            btn.removeClass('kt-spinner kt-spinner--light')
                            toastr.error("Erreur lors du traitement Serveur", "Erreur 500 !")
                        }
                    }
                })
            })
        })(jQuery)
    </script>
@endsection
