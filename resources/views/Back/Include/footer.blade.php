<!-- begin:: Footer -->
<div class="kt-footer kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop">
    <div class="kt-footer__copyright">
        2019&nbsp;&copy;&nbsp;<a href="#" target="_blank" class="kt-link">Trainznation</a>
    </div>
</div>
