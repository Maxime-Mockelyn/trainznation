<div id="kt_header" class="kt-header  kt-header--fixed " data-ktheader-minimize="on">
    <div class="kt-container">

        <!-- begin:: Brand -->
        <div class="kt-header__brand   kt-grid__item" id="kt_header_brand">
            <a class="kt-header__brand-logo" href="{{ route('Account.index') }}">
                <img alt="Logo" src="{{ sourceImage('other/logo-carre.png') }}" class="kt-header__brand-logo-default" />
                <img alt="Logo" src="{{ sourceImage('other/logo-carre-sm.png') }}" class="kt-header__brand-logo-sticky" />
            </a>
        </div>

        <!-- end:: Brand -->

        <!-- begin: Header Menu -->
        <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
        <div class="kt-header-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_header_menu_wrapper">
            <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile ">
                <ul class="kt-menu__nav ">
                    <li class="{{ currentRouteAccount(route('Account.index')) }} kt-menu__item kt-menu__item--rel">
                        <a href="{{ route('Account.index') }}" class="kt-menu__link" id="tour_dashboard">
                            <i class="kt-menu__link-icon la la-home"></i>
                            <span class="kt-menu__link-text">Tableau de Bord</span>
                            <i class="kt-menu__ver-arrow la la-angle-right"></i>
                        </a>
                    </li>

                    <li class="{{ currentRouteAccount(route('Account.info')) }} kt-menu__item kt-menu__item--rel">
                        <a href="{{ route('Account.info') }}" class="kt-menu__link" id="tour_account">
                            <i class="kt-menu__link-icon la la-user"></i>
                            <span class="kt-menu__link-text">Mon Compte</span>
                            <i class="kt-menu__ver-arrow la la-angle-right"></i>
                        </a>
                    </li>

                    <li class="{{ currentRouteAccount(route('Account.badge')) }} kt-menu__item kt-menu__item--rel">
                        <a href="{{ route('Account.badge') }}" class="kt-menu__link" id="tour_badge">
                            <i class="kt-menu__link-icon la la-certificate"></i>
                            <span class="kt-menu__link-text">Mes Badges</span>
                            <i class="kt-menu__ver-arrow la la-angle-right"></i>
                        </a>
                    </li>

                    <li class="{{ currentRouteAccount(route('Account.contrib')) }} kt-menu__item kt-menu__item--rel">
                        <a href="{{ route('Account.contrib') }}" class="kt-menu__link" id="tour_contrib">
                            <i class="kt-menu__link-icon la la-comments"></i>
                            <span class="kt-menu__link-text">Mes Contributions</span>
                            <i class="kt-menu__ver-arrow la la-angle-right"></i>
                        </a>
                    </li>
                    <li class="{{ currentRouteAccount(route('Account.Invoice.index')) }} kt-menu__item kt-menu__item--rel">
                        <a href="{{ route('Account.Invoice.index') }}" class="kt-menu__link" id="tour_invoice">
                            <i class="kt-menu__link-icon la la-shopping-cart"></i>
                            <span class="kt-menu__link-text">Mes Factures</span>
                            <i class="kt-menu__ver-arrow la la-angle-right"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <!-- end: Header Menu -->

        <!-- begin:: Header Topbar -->
        <div class="kt-header__topbar kt-grid__item">

            <!--begin: Notifications -->
            <div class="kt-header__topbar-item dropdown">
                <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
										<span class="kt-header__topbar-icon kt-pulse kt-pulse--light">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect id="bound" x="0" y="0" width="24" height="24" />
													<path d="M2.56066017,10.6819805 L4.68198052,8.56066017 C5.26776695,7.97487373 6.21751442,7.97487373 6.80330086,8.56066017 L8.9246212,10.6819805 C9.51040764,11.267767 9.51040764,12.2175144 8.9246212,12.8033009 L6.80330086,14.9246212 C6.21751442,15.5104076 5.26776695,15.5104076 4.68198052,14.9246212 L2.56066017,12.8033009 C1.97487373,12.2175144 1.97487373,11.267767 2.56066017,10.6819805 Z M14.5606602,10.6819805 L16.6819805,8.56066017 C17.267767,7.97487373 18.2175144,7.97487373 18.8033009,8.56066017 L20.9246212,10.6819805 C21.5104076,11.267767 21.5104076,12.2175144 20.9246212,12.8033009 L18.8033009,14.9246212 C18.2175144,15.5104076 17.267767,15.5104076 16.6819805,14.9246212 L14.5606602,12.8033009 C13.9748737,12.2175144 13.9748737,11.267767 14.5606602,10.6819805 Z" id="Combined-Shape" fill="#000000" opacity="0.3" />
													<path d="M8.56066017,16.6819805 L10.6819805,14.5606602 C11.267767,13.9748737 12.2175144,13.9748737 12.8033009,14.5606602 L14.9246212,16.6819805 C15.5104076,17.267767 15.5104076,18.2175144 14.9246212,18.8033009 L12.8033009,20.9246212 C12.2175144,21.5104076 11.267767,21.5104076 10.6819805,20.9246212 L8.56066017,18.8033009 C7.97487373,18.2175144 7.97487373,17.267767 8.56066017,16.6819805 Z M8.56066017,4.68198052 L10.6819805,2.56066017 C11.267767,1.97487373 12.2175144,1.97487373 12.8033009,2.56066017 L14.9246212,4.68198052 C15.5104076,5.26776695 15.5104076,6.21751442 14.9246212,6.80330086 L12.8033009,8.9246212 C12.2175144,9.51040764 11.267767,9.51040764 10.6819805,8.9246212 L8.56066017,6.80330086 C7.97487373,6.21751442 7.97487373,5.26776695 8.56066017,4.68198052 Z" id="Combined-Shape" fill="#000000" />
												</g>
											</svg>

                                            <!--<i class="flaticon2-bell-alarm-symbol"></i>-->
											<span class="kt-pulse__ring"></span>
										</span>

                    <!--<span class="kt-badge kt-badge--light"></span>-->
                </div>
                <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">
                    <form>

                        <!--begin: Head -->
                        <div class="kt-head kt-head--skin-dark kt-head--fit-x kt-head--fit-b" style="background-image: url(/assets/back/media/misc/bg-1.jpg)">
                            <h3 class="kt-head__title">
                                Mes Notifications
                                &nbsp;
                                <span class="btn btn-success btn-sm btn-bold btn-font-md">23 new</span>
                            </h3>
                            <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-success kt-notification-item-padding-x" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active show" data-toggle="tab" href="#topbar_notifications_notifications" role="tab" aria-selected="true">Alertes</a>
                                </li>
                            </ul>
                        </div>

                        <!--end: Head -->
                        <div class="tab-content">
                            <div class="tab-pane active show" id="topbar_notifications_notifications" role="tabpanel">
                                <div class="kt-notification kt-margin-t-10 kt-margin-b-10 kt-scroll" data-scroll="true" data-height="300" data-mobile-height="200">
                                    <a href="#" class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <i class="flaticon2-line-chart kt-font-success"></i>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">
                                                New order has been received
                                            </div>
                                            <div class="kt-notification__item-time">
                                                2 hrs ago
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <i class="flaticon2-box-1 kt-font-brand"></i>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">
                                                New customer is registered
                                            </div>
                                            <div class="kt-notification__item-time">
                                                3 hrs ago
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <i class="flaticon2-chart2 kt-font-danger"></i>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">
                                                Application has been approved
                                            </div>
                                            <div class="kt-notification__item-time">
                                                3 hrs ago
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <i class="flaticon2-image-file kt-font-warning"></i>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">
                                                New file has been uploaded
                                            </div>
                                            <div class="kt-notification__item-time">
                                                5 hrs ago
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <i class="flaticon2-bar-chart kt-font-info"></i>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">
                                                New user feedback received
                                            </div>
                                            <div class="kt-notification__item-time">
                                                8 hrs ago
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <i class="flaticon2-pie-chart-2 kt-font-success"></i>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">
                                                System reboot has been successfully completed
                                            </div>
                                            <div class="kt-notification__item-time">
                                                12 hrs ago
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <i class="flaticon2-favourite kt-font-danger"></i>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">
                                                New order has been placed
                                            </div>
                                            <div class="kt-notification__item-time">
                                                15 hrs ago
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="kt-notification__item kt-notification__item--read">
                                        <div class="kt-notification__item-icon">
                                            <i class="flaticon2-safe kt-font-primary"></i>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">
                                                Company meeting canceled
                                            </div>
                                            <div class="kt-notification__item-time">
                                                19 hrs ago
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <i class="flaticon2-psd kt-font-success"></i>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">
                                                New report has been received
                                            </div>
                                            <div class="kt-notification__item-time">
                                                23 hrs ago
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <i class="flaticon-download-1 kt-font-danger"></i>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">
                                                Finance report has been generated
                                            </div>
                                            <div class="kt-notification__item-time">
                                                25 hrs ago
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <i class="flaticon-security kt-font-warning"></i>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">
                                                New customer comment recieved
                                            </div>
                                            <div class="kt-notification__item-time">
                                                2 days ago
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <i class="flaticon2-pie-chart kt-font-success"></i>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">
                                                New customer is registered
                                            </div>
                                            <div class="kt-notification__item-time">
                                                3 days ago
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <!--end: Notifications -->

            <div class="kt-header__topbar-item">
                <a class="kt-header__topbar-wrapper" href="{{ route('Account.Premium.index') }}" data-toggle="kt-tooltip" data-original-title="Souscrire à une offre premium">
										<span class="kt-header__topbar-icon">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <polygon id="Shape" points="0 0 24 0 24 24 0 24"/>
                                                    <polygon id="Path-3" fill="#000000" opacity="0.3" points="10.0888887 24 14.5333331 18 12.3111109 18 12.3111109 14 7.86666648 20 10.0888887 20"/>
                                                    <path d="M5.74714567,14.0425758 C4.09410362,12.9740356 3,11.1147886 3,9 C3,5.6862915 5.6862915,3 9,3 C11.7957591,3 14.1449096,4.91215918 14.8109738,7.5 L17.25,7.5 C19.3210678,7.5 21,9.17893219 21,11.25 C21,13.3210678 19.3210678,15 17.25,15 L8.25,15 C7.28817895,15 6.41093178,14.6378962 5.74714567,14.0425758 Z" id="Combined-Shape" fill="#000000"/>
                                                </g>
                                            </svg>

                                            <!--<i class="flaticon2-bell-alarm-symbol"></i>-->
										</span>

                    <!--<span class="kt-badge kt-badge--light"></span>-->
                </a>
            </div>

            <!--begin: User bar -->
            <div class="kt-header__topbar-item kt-header__topbar-item--user">
                <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                    <span class="kt-header__topbar-welcome">Bonjour,</span>
                    <span class="kt-header__topbar-username">{{ auth()->user()->name }}</span>
                    @if(auth()->user()->email == 'contact@trainznation.eu')
                        <img src="{{ sourceImage('avatar/trainznation.png') }}" alt="">
                    @else
                        @if(auth()->user()->account->avatar == 0)
                            <span class="kt-header__topbar-icon"><b>{{ firstString(auth()->user()->name) }}</b></span>
                        @else
                            <img src="{{ sourceImage('avatar/'.auth()->user()->id.'.png') }}" alt="">
                        @endif
                    @endif
                </div>
                <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">

                    <!--begin: Head -->
                    <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x" style="background-image: url(/assets/back/media/misc/bg-1.jpg)">
                        <div class="kt-user-card__avatar">
                            @if(auth()->user()->email == 'contact@trainznation.eu')
                                <img src="{{ sourceImage('avatar/trainznation.png') }}" alt="">
                            @else
                                @if(auth()->user()->account->avatar == 0)
                                    <span class="kt-header__topbar-icon"><b>{{ firstString(auth()->user()->name) }}</b></span>
                                @else
                                    <img src="{{ sourceImage('avatar/'.auth()->user()->id.'.png') }}" alt="">
                                @endif
                            @endif
                        </div>
                        <div class="kt-user-card__name">
                            {{ auth()->user()->name }}
                        </div>
                        <!--<div class="kt-user-card__badge">
                            <span class="btn btn-success btn-sm btn-bold btn-font-md">23 messages</span>
                        </div>-->
                    </div>

                    <!--end: Head -->

                    <!--begin: Navigation -->
                    <div class="kt-notification">
                        <a href="{{ route('Account.info') }}" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                <i class="flaticon2-user-1 kt-font-success"></i>
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-bold">
                                    Mon Compte
                                </div>
                                <div class="kt-notification__item-time">
                                    Gérez vos informations de compte
                                </div>
                            </div>
                        </a>
                        <a href="{{ route('Account.contrib') }}" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                <i class="flaticon2-bell kt-font-warning"></i>
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-bold">
                                    Mon Activité
                                </div>
                                <div class="kt-notification__item-time">
                                    Suivez en temps réel vos notifications & contributions
                                </div>
                            </div>
                        </a>
                        <a href="{{ route('home') }}" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                <i class="flaticon2-laptop kt-font-warning"></i>
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-bold">
                                    Retour au site
                                </div>
                            </div>
                        </a>
                        <div class="kt-notification__custom kt-space-between">
                            <a href="{{ route('logout') }}" class="btn btn-label btn-label-brand btn-sm btn-bold">Déconnexion</a>
                        </div>
                    </div>

                    <!--end: Navigation -->
                </div>
            </div>

            <!--end: User bar -->
        </div>

        <!-- end:: Header Topbar -->
    </div>
</div>
