@if($results->hasPages())
<div class="row">
    <div class="col-xl-12">

        <!--begin:: Components/Pagination/Default-->
        <div class="kt-portlet">
            <div class="kt-portlet__body">

                <!--begin: Pagination-->
                <div class="kt-pagination kt-pagination--brand">
                    <ul class="kt-pagination__links">
                        @if($results->onFirstPage())

                        @else
                            <li class="kt-pagination__link--next">
                                <a href="{{ $results->previousPageUrl() }}" disabled><i class="fa fa-angle-left kt-font-brand"></i></a>
                            </li>
                        @endif
                        @foreach($elements as $element)
                            @if(is_string($element))
                                 <li class="disabled">
                                     <a href="#">{{ $element }}</a>
                                 </li>
                            @endif

                            @if(is_array($element))
                                @foreach($element as $page => $url)
                                    @if($page == $results->currentpage())
                                        <li class="kt-pagination__link--active">
                                            <a href="#">{{ $page }}</a>
                                        </li>
                                    @else
                                        <li>
                                            <a href="{{ $url }}">{{ $page }}</a>
                                        </li>
                                    @endif
                                @endforeach
                            @endif
                        @endforeach
                        @if($results->hasMorePages())
                                <li class="kt-pagination__link--prev">
                                    <a href="{{ $results->nextPageUrl() }}"><i class="fa fa-angle-right kt-font-brand"></i></a>
                                </li>
                        @else

                        @endif
                    </ul>
                </div>

                <!--end: Pagination-->
            </div>
        </div>

        <!--end:: Components/Pagination/Default-->
    </div>
</div>
@endif
