<div class="kt-footer  kt-footer--extended  kt-grid__item" id="kt_footer" style="background-image: url('/assets/back/media/bg/bg-2.jpg');">
    <div class="kt-footer__top">
        <div class="kt-container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="kt-footer__section">
                        <h3 class="kt-footer__title">Trainznation</h3>
                        <div class="kt-footer__content">
                            Après avoir appris grace à plusieurs tutoriels vidéo et à des personnes déjà initialiser
                            à la création 3D pour trainz quoi de plus normal que de partager à son tour ?<br><br>
                            Passionnée par le web, la 3D et Trainz depuis 10 ans maintenant, j'aime partager mes compétences<br>
                            et mes découvertes avec les personnes qui ont les mêmes passions que moi.
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="kt-footer__section">
                        <h3 class="kt-footer__title">Liens rapides</h3>
                        <div class="kt-footer__content">
                            <div class="kt-footer__nav">
                                <div class="kt-footer__nav-section">
                                    <a href="{{ route('home') }}">Accueil</a>
                                    <a href="{{ route('Front.Blog.index') }}">Blog</a>
                                    <a href="{{ route('Front.Download.index') }}">Téléchargement</a>
                                </div>
                                <div class="kt-footer__nav-section">
                                    <a href="{{ route('Front.Tutoriel.index') }}">Tutoriel</a>
                                    <a href="{{ route('Front.Wiki.index') }}">Wiki</a>
                                    <a href="{{ route('Front.Contact.index') }}">Contact</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="kt-footer__section">
                        <h3 class="kt-footer__title">Souscrire à la newsletter</h3>
                        <div class="kt-footer__content">
                            <form action="{{ route('Newsletter.subscribe') }}" class="kt-footer__subscribe" method="POST">
                                @csrf
                                <div class="input-group">
                                    <input type="text" class="form-control" name="email" placeholder="Entrez votre adresse mail">
                                    <div class="input-group-append">
                                        <button class="btn btn-brand" type="submit">Nous rejoindre</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-footer__bottom">
        <div class="kt-container">
            <div class="kt-footer__wrapper">
                <div class="kt-footer__logo">
                    <a class="kt-header__brand-logo" href="{{ route('home') }}">
                        <img alt="Logo" src="{{ sourceImage('other/logo-carre-sm.png') }}" class="kt-header__brand-logo-sticky">
                    </a>
                    <div class="kt-footer__copyright">
                        2019&nbsp;&copy;&nbsp;
                        <a href="http://keenthemes.com/metronic" target="_blank">Trainznation</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
