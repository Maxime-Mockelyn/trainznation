@extends("Back.Layout.app")

@section("css")
@endsection

@section("bread")
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Tutoriel | {{ $tutoriel->title }}</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('Back.dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Tutoriel </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">{{ $tutoriel->title }} </a>

                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                @back(["data" => ["url" => "Back.Tutoriel.index"]]) @endback
                @edit(["data" => ["url" => "Back.Tutoriel.edit", "terme" => "le tutoriel", "id" => $tutoriel->slug]]) @endedit
                @delete(["data" => ["url" => "Back.Tutoriel.getDelete", "terme" => "le tutoriel", "id" => $tutoriel->slug]]) @enddelete
                @if($tutoriel->published == 0 || $tutoriel->published == 2)
                    @publish(["data" => ["url" => "Back.Tutoriel.getPublish", "terme" => "le tutoriel", "id" => $tutoriel->slug]]) @endpublish
                @else
                    @unpublish(["data" => ["url" => "Back.Tutoriel.getUnpublish", "terme" => "le tutoriel", "id" => $tutoriel->slug]]) @endunpublish
                @endif
            </div>
        </div>
    </div>
@endsection

@section("content")
<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet">
            <div id="tutoriel_id" data-id="{{ $tutoriel->id }}"></div>
            <div class="kt-portlet__body">
                <div class="kt-widget kt-widget--user-profile-3">
                    <div class="kt-widget__top">
                        <div class="kt-widget__media kt-hidden-">
                            <img src="{{ sourceImage('learning/'.$tutoriel->id.'.png') }}" alt="image">
                        </div>
                        <div class="kt-widget__content">
                            <h2 class="">{{ $tutoriel->title }} {!! stateDownload($tutoriel->published, $tutoriel->published_at) !!}</h2>
                            <div class="kt-widget__subhead">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect id="bound" x="0" y="0" width="24" height="24"/>
                                        <path d="M12,22 C7.02943725,22 3,17.9705627 3,13 C3,8.02943725 7.02943725,4 12,4 C16.9705627,4 21,8.02943725 21,13 C21,17.9705627 16.9705627,22 12,22 Z" id="Mask" fill="#000000" opacity="0.3"/>
                                        <path d="M11.9630156,7.5 L12.0475062,7.5 C12.3043819,7.5 12.5194647,7.69464724 12.5450248,7.95024814 L13,12.5 L16.2480695,14.3560397 C16.403857,14.4450611 16.5,14.6107328 16.5,14.7901613 L16.5,15 C16.5,15.2109164 16.3290185,15.3818979 16.1181021,15.3818979 C16.0841582,15.3818979 16.0503659,15.3773725 16.0176181,15.3684413 L11.3986612,14.1087258 C11.1672824,14.0456225 11.0132986,13.8271186 11.0316926,13.5879956 L11.4644883,7.96165175 C11.4845267,7.70115317 11.7017474,7.5 11.9630156,7.5 Z" id="Path-107" fill="#000000"/>
                                    </g>
                                </svg>
                                @if($tutoriel->time == 0)
                                    NC
                                @else
                                    {{ $tutoriel->time }}
                                @endif

                            </div>
                        </div>
                    </div>
                    <div class="kt-widget__bottom">
                        <div class="kt-widget__item">
                            <div class="kt-widget__icon">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect id="bound" x="0" y="0" width="24" height="24"/>
                                        <polygon id="Path-75" fill="#000000" opacity="0.3" points="5 15 3 21.5 9.5 19.5"/>
                                        <path d="M13.5,21 C8.25329488,21 4,16.7467051 4,11.5 C4,6.25329488 8.25329488,2 13.5,2 C18.7467051,2 23,6.25329488 23,11.5 C23,16.7467051 18.7467051,21 13.5,21 Z M8.5,13 C9.32842712,13 10,12.3284271 10,11.5 C10,10.6715729 9.32842712,10 8.5,10 C7.67157288,10 7,10.6715729 7,11.5 C7,12.3284271 7.67157288,13 8.5,13 Z M13.5,13 C14.3284271,13 15,12.3284271 15,11.5 C15,10.6715729 14.3284271,10 13.5,10 C12.6715729,10 12,10.6715729 12,11.5 C12,12.3284271 12.6715729,13 13.5,13 Z M18.5,13 C19.3284271,13 20,12.3284271 20,11.5 C20,10.6715729 19.3284271,10 18.5,10 C17.6715729,10 17,10.6715729 17,11.5 C17,12.3284271 17.6715729,13 18.5,13 Z" id="Combined-Shape" fill="#000000"/>
                                    </g>
                                </svg>
                            </div>
                            <div class="kt-widget__details">
                                <span class="kt-widget__title">Commentaires</span>
                                <span class="kt-widget__value">{{ \App\Repository\Tutoriel\TutorielCommentRepository::getCountFromTuto($tutoriel->id) }}</span>
                            </div>
                        </div>

                        <div class="kt-widget__item">
                            <div class="kt-widget__icon">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect id="bound" x="0" y="0" width="24" height="24"/>
                                        <path d="M4.22266882,4 L19.8367728,4.00001353 C21.3873185,4.00001353 22.6823897,5.1816009 22.8241881,6.72564925 C22.9414021,8.00199653 23.0000091,9.40113909 23.0000091,10.9230769 C23.0000091,12.7049599 22.9196724,14.4870542 22.758999,16.26936 L22.7589943,16.2693595 C22.6196053,17.8155637 21.3235899,19 19.7711155,19 L4.22267091,19.0000022 C2.6743525,19.0000022 1.38037032,17.8217109 1.23577882,16.2801587 C1.07859294,14.6043323 1,13.0109461 1,11.5 C1,9.98905359 1.07859298,8.39566699 1.23577893,6.7198402 L1.23578022,6.71984032 C1.38037157,5.17828994 2.67435224,4 4.22266882,4 Z" id="Combined-Shape" fill="#000000" opacity="0.3"/>
                                        <path d="M11.1821576,14.8052934 L15.5856084,11.7952868 C15.8135802,11.6394552 15.8720614,11.3283211 15.7162299,11.1003494 C15.6814583,11.0494808 15.6375838,11.0054775 15.5868174,10.970557 L11.1833666,7.94156929 C10.9558527,7.78507001 10.6445485,7.84263875 10.4880492,8.07015268 C10.4307018,8.15352258 10.3999996,8.25233045 10.3999996,8.35351969 L10.3999996,14.392514 C10.3999996,14.6686564 10.6238572,14.892514 10.8999996,14.892514 C11.000689,14.892514 11.0990326,14.8621141 11.1821576,14.8052934 Z" id="Path-10" fill="#000000"/>
                                    </g>
                                </svg>
                            </div>
                            @if(!empty($tutoriel->linkVideo))
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Vue</span>
                                    <span class="kt-widget__value">{{ \App\Library\Youtube\Youtube::getViewVideo($tutoriel->linkVideo) }}</span>
                                </div>
                            @else
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Vue</span>
                                    <span class="kt-widget__value">NC</span>
                                </div>
                            @endif
                        </div>

                        <div class="kt-widget__item">
                            <div class="kt-widget__icon">
                                <i class="flaticon-twitter-logo"></i>
                            </div>
                            <div class="kt-widget__details">
                                <span class="kt-widget__title">Publier sur Twitter</span>
                                <span class="kt-widget__value">
                                                    @if($tutoriel->twitter == 0)
                                        <i class="la la-close la-3x text-danger"></i>
                                    @else
                                        <i class="la la-check la-3x text-success" data-toggle="kt-popover" title="Texte publier sur twitter" content="{{ $tutoriel->twitterText }}"></i>
                                    @endif
                                        </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet kt-portlet--tabs">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-toolbar">
                    <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-success nav-tabs-line-2x" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#description" role="tab">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect id="bound" x="0" y="0" width="24" height="24"/>
                                        <path d="M13.5,6 C13.33743,8.28571429 12.7799545,9.78571429 11.8275735,10.5 C11.8275735,10.5 12.5,4 10.5734853,2 C10.5734853,2 10.5734853,5.92857143 8.78777106,9.14285714 C7.95071887,10.6495511 7.00205677,12.1418252 7.00205677,14.1428571 C7.00205677,17 10.4697177,18 12.0049375,18 C13.8025422,18 17,17 17,13.5 C17,12.0608202 15.8333333,9.56082016 13.5,6 Z" id="Path-17" fill="#000000"/>
                                        <path d="M9.72075922,20 L14.2792408,20 C14.7096712,20 15.09181,20.2754301 15.2279241,20.6837722 L16,23 L8,23 L8.77207592,20.6837722 C8.90818997,20.2754301 9.29032881,20 9.72075922,20 Z" id="Rectangle-49" fill="#000000" opacity="0.3"/>
                                    </g>
                                </svg> Description
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#comments" role="tab">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect id="bound" x="0" y="0" width="24" height="24"/>
                                        <polygon id="Path-75" fill="#000000" opacity="0.3" points="5 15 3 21.5 9.5 19.5"/>
                                        <path d="M13.5,21 C8.25329488,21 4,16.7467051 4,11.5 C4,6.25329488 8.25329488,2 13.5,2 C18.7467051,2 23,6.25329488 23,11.5 C23,16.7467051 18.7467051,21 13.5,21 Z M8.5,13 C9.32842712,13 10,12.3284271 10,11.5 C10,10.6715729 9.32842712,10 8.5,10 C7.67157288,10 7,10.6715729 7,11.5 C7,12.3284271 7.67157288,13 8.5,13 Z M13.5,13 C14.3284271,13 15,12.3284271 15,11.5 C15,10.6715729 14.3284271,10 13.5,10 C12.6715729,10 12,10.6715729 12,11.5 C12,12.3284271 12.6715729,13 13.5,13 Z M18.5,13 C19.3284271,13 20,12.3284271 20,11.5 C20,10.6715729 19.3284271,10 18.5,10 C17.6715729,10 17,10.6715729 17,11.5 C17,12.3284271 17.6715729,13 18.5,13 Z" id="Combined-Shape" fill="#000000"/>
                                    </g>
                                </svg> Commentaires
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#experience" role="tab">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect id="bound" x="0" y="0" width="24" height="24"/>
                                        <circle id="Combined-Shape" fill="#000000" opacity="0.3" cx="12" cy="9" r="8"/>
                                        <path d="M14.5297296,11 L9.46184488,11 L11.9758349,17.4645458 L14.5297296,11 Z M10.5679953,19.3624463 L6.53815512,9 L17.4702704,9 L13.3744964,19.3674279 L11.9759405,18.814912 L10.5679953,19.3624463 Z" id="Path-69" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                        <path d="M10,22 L14,22 L14,22 C14,23.1045695 13.1045695,24 12,24 L12,24 C10.8954305,24 10,23.1045695 10,22 Z" id="Rectangle-72-Copy-2" fill="#000000" opacity="0.3"/>
                                        <path d="M9,20 C8.44771525,20 8,19.5522847 8,19 C8,18.4477153 8.44771525,18 9,18 C8.44771525,18 8,17.5522847 8,17 C8,16.4477153 8.44771525,16 9,16 L15,16 C15.5522847,16 16,16.4477153 16,17 C16,17.5522847 15.5522847,18 15,18 C15.5522847,18 16,18.4477153 16,19 C16,19.5522847 15.5522847,20 15,20 C15.5522847,20 16,20.4477153 16,21 C16,21.5522847 15.5522847,22 15,22 L9,22 C8.44771525,22 8,21.5522847 8,21 C8,20.4477153 8.44771525,20 9,20 Z" id="Combined-Shape" fill="#000000"/>
                                    </g>
                                </svg> Expériences
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#technologie" role="tab">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect id="Rectangle-5" x="0" y="0" width="24" height="24"/>
                                        <path d="M6,7 C7.1045695,7 8,6.1045695 8,5 C8,3.8954305 7.1045695,3 6,3 C4.8954305,3 4,3.8954305 4,5 C4,6.1045695 4.8954305,7 6,7 Z M6,9 C3.790861,9 2,7.209139 2,5 C2,2.790861 3.790861,1 6,1 C8.209139,1 10,2.790861 10,5 C10,7.209139 8.209139,9 6,9 Z" id="Oval-7" fill="#000000" fill-rule="nonzero"/>
                                        <path d="M7,11.4648712 L7,17 C7,18.1045695 7.8954305,19 9,19 L15,19 L15,21 L9,21 C6.790861,21 5,19.209139 5,17 L5,8 L5,7 L7,7 L7,8 C7,9.1045695 7.8954305,10 9,10 L15,10 L15,12 L9,12 C8.27142571,12 7.58834673,11.8052114 7,11.4648712 Z" id="Combined-Shape" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                        <path d="M18,22 C19.1045695,22 20,21.1045695 20,20 C20,18.8954305 19.1045695,18 18,18 C16.8954305,18 16,18.8954305 16,20 C16,21.1045695 16.8954305,22 18,22 Z M18,24 C15.790861,24 14,22.209139 14,20 C14,17.790861 15.790861,16 18,16 C20.209139,16 22,17.790861 22,20 C22,22.209139 20.209139,24 18,24 Z" id="Oval-7-Copy" fill="#000000" fill-rule="nonzero"/>
                                        <path d="M18,13 C19.1045695,13 20,12.1045695 20,11 C20,9.8954305 19.1045695,9 18,9 C16.8954305,9 16,9.8954305 16,11 C16,12.1045695 16.8954305,13 18,13 Z M18,15 C15.790861,15 14,13.209139 14,11 C14,8.790861 15.790861,7 18,7 C20.209139,7 22,8.790861 22,11 C22,13.209139 20.209139,15 18,15 Z" id="Oval-7-Copy-3" fill="#000000" fill-rule="nonzero"/>
                                    </g>
                                </svg> Technologie
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="tab-content">
                    <div class="tab-pane active" id="description" role="tabpanel">
                        {!! $tutoriel->content !!}
                    </div>
                    <div class="tab-pane" id="comments" role="tabpanel">
                        <div class="kt-portlet kt-portlet--mobile">
                            <div class="kt-portlet__head kt-portlet__head--lg">
                                <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
                                    <h3 class="kt-portlet__head-title">
                                        Liste des Commentaires
                                    </h3>
                                </div>
                            </div>
                            <div class="kt-portlet__body">

                                <!--begin: Search Form -->
                                <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                                    <div class="row align-items-center">
                                        <div class="col-xl-8 order-2 order-xl-1">
                                            <div class="row align-items-center">
                                                <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                                    <div class="kt-input-icon kt-input-icon--left">
                                                        <input type="text" class="form-control" placeholder="Search..." id="generalSearchComment">
                                                        <span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                                    <div class="kt-form__group kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Etat de Publication:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <select class="form-control bootstrap-select" id="kt_form_status">
                                                                <option value="">All</option>
                                                                <option value="0">Non Publier</option>
                                                                <option value="1">Publier</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-4 order-1 order-xl-2 kt-align-right">
                                            <a href="#" class="btn btn-default kt-hidden">
                                                <i class="la la-cart-plus"></i> New Order
                                            </a>
                                            <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg d-xl-none"></div>
                                        </div>
                                    </div>
                                </div>

                                <!--end: Search Form -->
                            </div>
                            <div class="kt-portlet__body kt-portlet__body--fit">

                                <!--begin: Datatable -->
                                <table class="kt-datatable" id="listeComment" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Utilisateur</th>
                                        <th>Date de Publication</th>
                                        <th>Etat de publication</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($tutoriel->comments as $comment)
                                            <tr>
                                                <td>
                                                    <div class="kt-user-card-v2">
                                                        <div class="kt-user-card-v2__pic">
                                                            <img alt="photo" src="https://keenthemes.com/metronic/preview/assets/media/users/100_6.jpg">
                                                        </div>
                                                        <div class="kt-user-card-v2__details">
                                                            <a class="kt-user-card-v2__name" href="#">{{ $comment->user->name }}</a>
                                                            <span class="kt-user-card-v2__desc">{{ $comment->user->email }}</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="text-center">{{ $comment->published_at->format("d-m-Y H:i") }}</td>
                                                <td>
                                                    {!! stateComment($comment->published) !!}
                                                </td>
                                                <td>
                                                    <button id="btnModal" data-id="{{ $comment->id }}" class="btn btn-primary btn-icon" data-toggle="kt-tooltip" title="Voir le commentaire"><i class="la la-commenting"></i> </button>
                                                    @if($comment->published == 0)
                                                        <button id="btnPublish" class="btn btn-icon btn-success" data-href="{{ route("Tutoriel.Comment.publish", [$tutoriel->id, $comment->id]) }}" data-id="{{ $comment->id }}" data-toggle="kt-tooltip" title="Approuver le commentaire"><i class="la la-check-circle"></i> </button>
                                                    @else
                                                        <button id="btnUnpublish" class="btn btn-icon btn-danger" data-href="{{ route("Tutoriel.Comment.unpublish", [$tutoriel->id, $comment->id]) }}" data-id="{{ $comment->id }}" data-toggle="kt-tooltip" title="Désapprouver le commentaire"><i class="la la-times-circle"></i> </button>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                <!--end: Datatable -->
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="experience" role="tabpanel">
                        <div class="kt-portlet kt-portlet--mobile">
                            <div class="kt-portlet__head kt-portlet__head--lg">
                                <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
                                    <h3 class="kt-portlet__head-title">
                                        Liste des Experiences
                                    </h3>
                                </div>
                            </div>
                            <div class="kt-portlet__body">

                                <!--begin: Search Form -->
                                <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                                    <div class="row align-items-center">
                                        <div class="col-xl-8 order-2 order-xl-1">
                                            <div class="row align-items-center">
                                                <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                                    <div class="kt-input-icon kt-input-icon--left">
                                                        <input type="text" class="form-control" placeholder="Recherche..." id="generalSearchExperience">
                                                        <span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-4 order-1 order-xl-2 kt-align-right">
                                            <a data-toggle="modal" href="#addExperience" class="btn btn-default">
                                                <i class="la la-plus"></i> Ajouter une expérience
                                            </a>
                                            <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg d-xl-none"></div>
                                        </div>
                                    </div>
                                </div>

                                <!--end: Search Form -->
                            </div>
                            <div class="kt-portlet__body kt-portlet__body--fit">

                                <!--begin: Datatable -->
                                <table class="kt-datatable" id="listeExperience" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Désignation</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($tutoriel->experiences as $experience)
                                        <tr>
                                            <td>{{ $experience->experience }}</td>
                                            <td>
                                                <button id="btnDelete" data-href="{{ route("Tutoriel.Experience.delete", [$tutoriel->id, $experience->id]) }}" data-id="{{ $experience->id }}" class="btn btn-icon btn-icon-sm btn-danger"><i class="la la-trash"></i> </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                                <!--end: Datatable -->
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="technologie" role="tabpanel">
                        <div class="kt-portlet kt-portlet--mobile">
                            <div class="kt-portlet__head kt-portlet__head--lg">
                                <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
                                    <h3 class="kt-portlet__head-title">
                                        Liste des Technologie
                                    </h3>
                                </div>
                            </div>
                            <div class="kt-portlet__body">

                                <!--begin: Search Form -->
                                <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                                    <div class="row align-items-center">
                                        <div class="col-xl-8 order-2 order-xl-1">
                                            <div class="row align-items-center">
                                                <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                                    <div class="kt-input-icon kt-input-icon--left">
                                                        <input type="text" class="form-control" placeholder="Recherche..." id="generalSearchTechnologie">
                                                        <span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-4 order-1 order-xl-2 kt-align-right">
                                            <a data-toggle="modal" href="#addTechnologie" class="btn btn-default">
                                                <i class="la la-plus"></i> Ajouter une Technologie
                                            </a>
                                            <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg d-xl-none"></div>
                                        </div>
                                    </div>
                                </div>

                                <!--end: Search Form -->
                            </div>
                            <div class="kt-portlet__body kt-portlet__body--fit">

                                <!--begin: Datatable -->
                                <table class="kt-datatable" id="listeTechnologie" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Technologie</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($tutoriel->technos as $techno)
                                        <tr>
                                            <td>{{ $techno->techno }}</td>
                                            <td>
                                                <button id="btnDelete" data-href="{{ route("Tutoriel.Technologie.delete", [$tutoriel->id, $techno->id]) }}" data-id="{{ $techno->id }}" class="btn btn-icon btn-icon-sm btn-danger"><i class="la la-trash"></i> </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                                <!--end: Datatable -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalComment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div id="modal-body-ajax"></div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="addExperience" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nouvelle Experience</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form id="formExperience" action="{{ route('Tutoriel.Experience.store', $tutoriel->id) }}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">Désignation:</label>
                        <input type="text" class="form-control" name="experience">
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" id="btnFormExperience" class="btn btn-primary">Ajouter</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="addTechnologie" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nouvelle Technologie</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form id="formTechnologie" action="{{ route('Tutoriel.Technologie.store', $tutoriel->id) }}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">Désignation:</label>
                        <input type="text" class="form-control" name="technologie">
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" id="btnFormTechnologie" class="btn btn-primary">Ajouter</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section("script")
    <script type="text/javascript">
        "use_strict";

        let Table = function () {
            let demo = function () {
                let datatable = $("#listeComment").KTDatatable({
                    data: {
                        saveState: {cookie: false}
                    },
                    search: {
                        input: $("#generalSearchComment")
                    },
                });

                $('#kt_form_status').on('change', function() {
                    datatable.search($(this).val().toLowerCase(), 'Status');
                });

                $('#kt_form_status,#kt_form_type').selectpicker();
            };

            let demo1 = function () {
                let datatable = $("#listeExperience").KTDatatable({
                    data: {
                        saveState: {cookie: false}
                    },
                    search: {
                        input: $("#generalSearchExperience")
                    }
                });
            };

            let demo2 = function () {
                let datatable = $("#listeTechnologie").KTDatatable({
                    data: {
                        saveState: {cookie: false}
                    },
                    search: {
                        input: $("#generalSearchTechnologie")
                    }
                });
            };

            return {
                // Public functions
                init: function() {
                    // init dmeo
                    demo();
                    demo1()
                    demo2()
                },
            };
        }();

        jQuery(document).ready(function() {
            Table.init();
        });
    </script>
    <script type="text/javascript">
        (function ($) {
            let identifier = $("#tutoriel_id").attr('data-id')
            $("#listeComment")
                .on('click', '#btnModal', function (e) {
                e.preventDefault()
                let btn = $(this)
                let comment_id = btn.attr('data-id')

                btn.html('<i class="la la-spinner la-spin"></i>')

                $.ajax({
                    url: '/back/tutoriel/'+identifier+'/comment/'+comment_id+'/getComment',
                    success: function (data) {
                        btn.html('<i class="la la-commenting"></i>')
                        $("#exampleModalLabel").html(data.title)
                        $("#modal-body-ajax").html(data.content)
                        $("#modalComment").modal('show')
                    }
                })
            })
                .on('click', '#btnPublish', function (e) {
                    e.preventDefault()
                    let btn = $(this)
                    let url = btn.attr('data-href')
                    let id = btn.attr('data-id')

                    KTApp.progress(btn)

                    $.ajax({
                        url: url,
                        success: function (data) {
                            KTApp.unprogress(btn)
                            btn
                                .removeClass('btn-success')
                                .addClass('btn-danger')
                                .attr('data-href', '/back/tutoriel/'+identifier+'/comment/'+id+'/unpublish')
                                .attr('data-id', id)
                                .html('<i class="la la-times-circle"></i>')

                            toastr.success("Le commentaire de <strong>"+data.user+"</strong> à été approuvé", "Commentaire")
                        }
                    })
                })
                .on('click', '#btnUnpublish', function (e) {
                    e.preventDefault()
                    let btn = $(this)
                    let url = btn.attr('data-href')
                    let id = btn.attr('data-id')

                    KTApp.progress(btn)

                    $.ajax({
                        url: url,
                        success: function (data) {
                            KTApp.unprogress(btn)
                            btn
                                .removeClass('btn-danger')
                                .addClass('btn-success')
                                .attr('data-href', '/back/tutoriel/'+identifier+'/comment/'+id+'/publish')
                                .attr('data-id', id)
                                .html('<i class="la la-check-circle"></i>')

                            toastr.success("Le commentaire de <strong>"+data.user+"</strong> à été désapprouvé", "Commentaire")
                        }
                    })
                })

            $("#listeExperience")
                .on('click', '#btnDelete', function (e) {
                    e.preventDefault()
                    let btn = $(this)
                    let url = btn.attr('data-href')
                    let id = btn.attr('data-id')

                    KTApp.progress(btn)

                    $.ajax({
                        url: url,
                        method: "DELETE",
                        success: function (data) {
                            KTApp.unprogress(btn)
                            btn.parents('tr').fadeOut()
                            toastr.success("L'experience à été supprimer", "Experience")
                        }
                    })
                })

            $("#listeTechnologie")
                .on('click', '#btnDelete', function (e) {
                    e.preventDefault()
                    let btn = $(this)
                    let url = btn.attr('data-href')
                    let id = btn.attr('data-id')

                    KTApp.progress(btn)

                    $.ajax({
                        url: url,
                        method: "DELETE",
                        success: function (data) {
                            KTApp.unprogress(btn)
                            btn.parents('tr').fadeOut()
                            toastr.success("La technologie à été supprimer", "Experience")
                        }
                    })
                })

            $("#formExperience").on('submit', function (e) {
                e.preventDefault()
                let form = $(this)
                let url = form.attr('action')
                let btn = $("#btnFormExperience")
                let data = form.serializeArray()

                KTApp.progress(btn)

                $.ajax({
                    url: url,
                    method: "POST",
                    data: data,
                    success: function (data) {
                        KTApp.unprogress(btn)
                        let tr = data.tr
                        $(".modal").modal('hide')
                        $("#listeExperience tbody").prepend(tr)
                        tr.fadeIn()

                        toastr.success("Une expérience à été ajouter", "Expérience")
                    }
                })
            })
            $("#formTechnologie").on('submit', function (e) {
                e.preventDefault()
                let form = $(this)
                let url = form.attr('action')
                let btn = $("#btnFormTechnologie")
                let data = form.serializeArray()

                KTApp.progress(btn)

                $.ajax({
                    url: url,
                    method: "POST",
                    data: data,
                    success: function (data) {
                        KTApp.unprogress(btn)
                        let tr = data.tr
                        $(".modal").modal('hide')
                        $("#listeTechnologie tbody").prepend(tr)
                        tr.fadeIn()

                        toastr.success("Une Technologie à été ajouter", "Technologie")
                    }
                })
            })
        })(jQuery)
    </script>
@endsection
