@extends("Back.Layout.app")

@section("css")
    <style type="text/css">
        .ck-editor__editable {
            min-height: 500px;
        }
    </style>
    <link rel="stylesheet" href="/assets/custom/vendors/editor-md/css/editormd.css">
@endsection

@section("bread")
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Tutoriel</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('Back.dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Tutoriel </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Edition d'un Tutoriel: {{ $tutoriel->title }} </a>

                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
    </div>
@endsection

@section("content")
    <div class="row">
        <div class="col-md-6">
            <form action="{{ route('Back.Tutoriel.update', $tutoriel->slug) }}" class="kt-form" method="POST">
                @csrf
                @method("PUT")
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Informations <strong>Générales</strong>
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Titre</label>
                                    <input type="text" id="title" class="form-control" value="{{ $tutoriel->title }}" name="title" onkeyup="getSlug()">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>URL</label>
                                    <input type="text" id="slug" class="form-control" name="slug" value="{{ $tutoriel->slug }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Courte description</label>
                                    <textarea class="form-control" name="short_content">{{ $tutoriel->short_content }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Catégorie</label>
                                    <select class="form-control kt-selectpicker" id="categorie_id" name="category_id" data-live-search="true" onchange="loadSubCategorie()">
                                        <option value="{{ $tutoriel->category->id }}">{{ $tutoriel->category->name }}</option>
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="subcategories"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Auteur</label>
                                    <select class="form-control kt-selectpicker" name="user_id">
                                        @foreach($users as $user)
                                            <option value="{{ $user->id }}" data-content="<div class='row'><div class='col-md-1'>@if($user->account->avatar == 1) @if($user->id == 1) <img src='/storage/avatar/trainznation.png' width='24'/> @else <img src='/storage/avatar/{{ $user->id }}.png' width='24'/> @endif @else <img src='/storage/avatar/placeholder.png' width='24'/> @endif</div><div class='col-md-11 kt-align-middle'>{{ $user->name }}</div></div>">{{ $user->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Date de Publication</label>
                                    <input type="text" class="form-control datetimepicker" name="published_at" value="{{ $tutoriel->published_at }}" placeholder="Selectionner une date et une heure" />
                                </div>
                            </div>
                        </div>
                        <br><br>
                        <div class="row">
                            <div class="col-md-4 kt-align-center form-group">
                                <div class="kt-checkbox-inline">
                                    <label class="kt-checkbox">
                                        <input type="checkbox" name="state" @if($tutoriel->published == 1) checked @endif value="1"> En Ligne
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4 kt-align-center form-group">
                                <div class="kt-checkbox-inline">
                                    <label class="kt-checkbox">
                                        <input type="checkbox" name="sources" @if($tutoriel->source == 1) checked @endif value="1"> Sources
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4 kt-align-center form-group">
                                <div class="kt-checkbox-inline">
                                    <label class="kt-checkbox">
                                        <input type="checkbox" name="premium" @if($tutoriel->premium == 1) checked @endif value="1"> Premium
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="row align-items-center">
                            <div class="col-lg-6 kt-align-left">
                                <button type="submit" class="btn btn-brand"><i class="fa fa-check-circle"></i> Envoyer</button>
                            </div>
                            <div class="col-lg-6 kt-align-right">
                                <button type="button" class="btn btn-warning"><i class="fa fa-bell"></i> Notifier</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-6">
            <form class="kt-form" action="{{ route('Back.Tutoriel.updateContent', $tutoriel->slug) }}" enctype="multipart/form-data" method="POST">
                @csrf
                @method("PUT")
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Contenu
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <img src="/storage/learning/{{ $tutoriel->id }}.png" width="200" alt="">
                                    <label>Image à la Une</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="customFile" name="images">
                                        <label class="custom-file-label" for="customFile">Choisir</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <img src="/storage/learning/{{ $tutoriel->id }}_cover.png" width="200" alt="">
                                    <label>Image de fond</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="customFile" name="cover">
                                        <label class="custom-file-label" for="customFile">Choisir</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label>Youtube</label>
                                    <input type="text" class="form-control" value="{{ $tutoriel->youtube_id }}" name="youtube_id">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Durée</label>
                                    <input type="text" class="form-control" name="time" value="{{ $tutoriel->time }}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Sources</label>
                                    @if($tutoriel->sources == 1)
                                        <input type="text" class="form-control" value="{{ $tutoriel->sources->pathSource }}" name="sourceFile">
                                        <i class="la la-file-archive-o"></i> {{ $tutoriel->sources->pathSource }}
                                    @else
                                        <input type="text" class="form-control" value="" name="sourceFile">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="row align-items-center">
                            <div class="col-lg-12 kt-align-left">
                                <button type="button" id="clearComment" data-uri="" class="btn btn-warning"><i class="la la-trash"></i> Vider les commentaires</button>
                                <button type="submit" class="btn btn-brand"><i class="la la-upload"></i> Upload</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <form class="kt-form" action="{{ route('Back.Tutoriel.updateContenu', $tutoriel->slug) }}" method="POST">
                @csrf
                @method("PUT")
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Description
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div id="fieldDesc">
                            <textarea style="display:none;" name="description">{{ $tutoriel->short_content }}&#10;{{ $tutoriel->content }}</textarea>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="row align-items-center">
                            <div class="col-lg-12 kt-align-center">
                                <button type="submit" class="btn btn-brand"><i class="la la-check-circle"></i> Envoyer</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="/assets/back/vendors/general/bootstrap-datetime-picker/js/locales/bootstrap-datetimepicker.fr.js"></script>
    <script src="/assets/custom/vendors/ckeditor/ckeditor.js"></script>
    <script src="/assets/custom/vendors/editor-md/editormd.js"></script>
    <script src="/assets/custom/vendors/editor-md/languages/en.js"></script>
    <script type="text/javascript">
        $("#twitterText").hide()
        $("#datePubUlte").hide()
        $("#datePubAnt").hide()

        function twitterCheck()
        {
            if($('input[name=twitter]').is(':checked')){
                $("#twitterText").show()
            }else{
                $("#twitterText").hide()
            }
        }

        function sluggify(str)
        {
            str = str.replace(/^\s+|\s+$/g, ''); // trim
            str = str.toLowerCase();

            // remove accents, swap ñ for n, etc
            let from = "àáãäâèéëêìíïîòóöôùúüûñç·/_,:;";
            let to   = "aaaaaeeeeiiiioooouuuunc------";

            for (var i=0, l=from.length ; i<l ; i++) {
                str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
            }

            str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
                .replace(/\s+/g, '-') // collapse whitespace and replace by -
                .replace(/-+/g, '-'); // collapse dashes

            return str;

        }

        function getSlug()
        {
            let title = $("#title").val()
            let slug = $("#slug")

            let string = sluggify(title);

            slug.val(string)
        }

        function pubUltiCheck()
        {
            if($('input[name=pubUlti]').is(':checked')){
                $("#datePubUlte").show()
            }else{
                $("#datePubUlte").hide()
            }
        }

        function pubAntCheck()
        {
            if($('input[name=pubAnt]').is(':checked')){
                $("#datePubAnt").show()
            }else{
                $("#datePubAnt").hide()
            }
        }

        function loadSubCategorie()
        {
            let div = $("#subcategories")
            let categorie = $("#categorie_id").val();

            $.ajax({
                url: "/back/tutoriel/listSubCategorie/"+categorie,
                method: "GET",
                success: function (data) {
                    div.html(data.content)
                }
            })
        }

        (function ($) {
            $(".kt-selectpicker").selectpicker({
                container: 'body'
            });
            //createCkeditor("#description")
            let editor = editormd("fieldDesc", {
                width: '100%',
                height: '460px',
                path:"/assets/custom/vendors/editor-md/lib/"
            })
            $(".datetimepicker").datetimepicker({
                todayHighlight: true,
                autoclose: true,
                pickerPosition: 'bottom-left',
                format: 'yyyy-mm-dd hh:ii:00',
                language: 'fr'
            })

            $('#twitterText').maxlength({
                threshold: 5,
                warningClass: "kt-badge kt-badge--primary kt-badge--rounded kt-badge--inline",
                limitReachedClass: "kt-badge kt-badge--brand kt-badge--rounded kt-badge--inline"
            });
        })(jQuery)
    </script>
@endsection
