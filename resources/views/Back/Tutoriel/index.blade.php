@extends("Back.Layout.app")

@section("css")
    <link href="/assets/back/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
@endsection

@section("bread")
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Tutoriel | Liste des Tutoriel</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('Back.dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Tutoriel </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Liste des tutoriels </a>

                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
    </div>
@endsection

@section("content")
    <div class="row">
        <div class="col-md-12">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

                <!-- begin:: Content Head -->
                <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                    <div class="kt-subheader__main">
                        <h3 class="kt-subheader__title">
                            Tutoriels
                        </h3>
                        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                        <div class="kt-subheader__group" id="kt_subheader_search">
									<span class="kt-subheader__desc" id="kt_subheader_total">
										{{ \App\Repository\Tutoriel\TutorielRepository::countTutoriel() }} {{ formatPlural('Tutoriel', \App\Repository\Tutoriel\TutorielRepository::countTutoriel()) }}</span>
                            <form class="kt-margin-l-20" id="kt_subheader_search_form">
                                <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                                    <input type="text" class="form-control" placeholder="Rechercher..." id="generalSearch">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--right">
												<span>
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
														<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
															<rect id="bound" x="0" y="0" width="24" height="24" />
															<path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" id="Path-2" fill="#000000" fill-rule="nonzero" opacity="0.3" />
															<path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" id="Path" fill="#000000" fill-rule="nonzero" />
														</g>
													</svg>

                                                    <!--<i class="flaticon2-search-1"></i>-->
												</span>
											</span>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="kt-subheader__toolbar">
                        <a href="#" class="">
                        </a>
                        <a href="{{ route('Back.Tutoriel.create') }}" class="btn btn-label-brand btn-bold">
                            Ajouter un Tutoriel
                        </a>
                    </div>
                </div>

                <!-- end:: Content Head -->

                <!-- begin:: Content -->
                {{ $tutoriels->links('Back.Include.paginate', ["results" => $tutoriels]) }}
                <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="panelAsset">

                    <!--begin:: Widgets/Applications/User/Profile3-->
                    @foreach($tutoriels as $tutoriel)
                        <div class="kt-portlet" id="">
                            <div id="tutoriel_id" data-id="{{ $tutoriel->id }}"></div>
                            <div class="kt-portlet__body">
                                <div class="kt-widget kt-widget--user-profile-3">
                                    <div class="kt-widget__top">
                                        <div class="kt-widget__media kt-hidden-">
                                            <img src="{{ sourceImage('learning/'.$tutoriel->id.'.png') }}" alt="image">
                                        </div>
                                        <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden">
                                            JM
                                        </div>
                                        <div class="kt-widget__content">
                                            <div class="kt-widget__head">
                                                <a href="{{ route('Back.Tutoriel.show', $tutoriel->slug) }}" class="kt-widget__username">
                                                    {{ $tutoriel->title }}
                                                    {!! stateDownload($tutoriel->published, $tutoriel->published_at) !!}
                                                </a>
                                                <div class="kt-widget__action">
                                                    <a href="{{ route('Back.Tutoriel.show', $tutoriel->slug) }}" class="btn btn-label-info btn-sm btn-upper"><i class="la la-eye"></i> Voir la fiche</a>&nbsp;
                                                    <a href="{{ route('Back.Tutoriel.edit', $tutoriel->slug) }}" class="btn btn-label-success btn-sm btn-upper"><i class="la la-edit"></i> Editer</a>&nbsp;
                                                    <button id="btnDelete" data-href="{{ route('Back.Tutoriel.delete', $tutoriel->slug) }}" type="button" class="btn btn-label-danger btn-sm btn-upper"><i class="la la-times-circle"></i> Supprimer</button>&nbsp;
                                                    @if($tutoriel->published == 0 || $tutoriel->published == 2)
                                                        <button id="btnPublish" data-href="{{ route('Back.Tutoriel.publish', $tutoriel->id) }}" data-id="{{ $tutoriel->id }}" type="button" class="btn btn-label-success btn-sm btn-upper"><i class="la la-unlock"></i> Publier</button>&nbsp;
                                                    @else
                                                        <button id="btnUnpublish" data-href="{{ route('Back.Tutoriel.unpublish', $tutoriel->id) }}" data-id="{{ $tutoriel->id }}" type="button" class="btn btn-label-danger btn-sm btn-upper"><i class="la la-lock"></i> Dépublier</button>&nbsp;
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="kt-widget__subhead">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect id="bound" x="0" y="0" width="24" height="24"/>
                                                        <path d="M12,22 C7.02943725,22 3,17.9705627 3,13 C3,8.02943725 7.02943725,4 12,4 C16.9705627,4 21,8.02943725 21,13 C21,17.9705627 16.9705627,22 12,22 Z" id="Mask" fill="#000000" opacity="0.3"/>
                                                        <path d="M11.9630156,7.5 L12.0475062,7.5 C12.3043819,7.5 12.5194647,7.69464724 12.5450248,7.95024814 L13,12.5 L16.2480695,14.3560397 C16.403857,14.4450611 16.5,14.6107328 16.5,14.7901613 L16.5,15 C16.5,15.2109164 16.3290185,15.3818979 16.1181021,15.3818979 C16.0841582,15.3818979 16.0503659,15.3773725 16.0176181,15.3684413 L11.3986612,14.1087258 C11.1672824,14.0456225 11.0132986,13.8271186 11.0316926,13.5879956 L11.4644883,7.96165175 C11.4845267,7.70115317 11.7017474,7.5 11.9630156,7.5 Z" id="Path-107" fill="#000000"/>
                                                    </g>
                                                </svg>
                                                @if($tutoriel->time == 0)
                                                    NC
                                                @else
                                                    {{ $tutoriel->time }}
                                                @endif

                                            </div>
                                            <div class="kt-widget__info">
                                                <div class="kt-widget__desc">
                                                    {!! str_limit($tutoriel->content, 100, '...') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-widget__bottom">
                                        <div class="kt-widget__item">
                                            <div class="kt-widget__icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect id="bound" x="0" y="0" width="24" height="24"/>
                                                        <polygon id="Path-75" fill="#000000" opacity="0.3" points="5 15 3 21.5 9.5 19.5"/>
                                                        <path d="M13.5,21 C8.25329488,21 4,16.7467051 4,11.5 C4,6.25329488 8.25329488,2 13.5,2 C18.7467051,2 23,6.25329488 23,11.5 C23,16.7467051 18.7467051,21 13.5,21 Z M8.5,13 C9.32842712,13 10,12.3284271 10,11.5 C10,10.6715729 9.32842712,10 8.5,10 C7.67157288,10 7,10.6715729 7,11.5 C7,12.3284271 7.67157288,13 8.5,13 Z M13.5,13 C14.3284271,13 15,12.3284271 15,11.5 C15,10.6715729 14.3284271,10 13.5,10 C12.6715729,10 12,10.6715729 12,11.5 C12,12.3284271 12.6715729,13 13.5,13 Z M18.5,13 C19.3284271,13 20,12.3284271 20,11.5 C20,10.6715729 19.3284271,10 18.5,10 C17.6715729,10 17,10.6715729 17,11.5 C17,12.3284271 17.6715729,13 18.5,13 Z" id="Combined-Shape" fill="#000000"/>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="kt-widget__details">
                                                <span class="kt-widget__title">Commentaires</span>
                                                <span class="kt-widget__value">{{ \App\Repository\Tutoriel\TutorielCommentRepository::getCountFromTuto($tutoriel->id) }}</span>
                                            </div>
                                        </div>

                                        <div class="kt-widget__item">
                                            <div class="kt-widget__icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect id="bound" x="0" y="0" width="24" height="24"/>
                                                        <path d="M4.22266882,4 L19.8367728,4.00001353 C21.3873185,4.00001353 22.6823897,5.1816009 22.8241881,6.72564925 C22.9414021,8.00199653 23.0000091,9.40113909 23.0000091,10.9230769 C23.0000091,12.7049599 22.9196724,14.4870542 22.758999,16.26936 L22.7589943,16.2693595 C22.6196053,17.8155637 21.3235899,19 19.7711155,19 L4.22267091,19.0000022 C2.6743525,19.0000022 1.38037032,17.8217109 1.23577882,16.2801587 C1.07859294,14.6043323 1,13.0109461 1,11.5 C1,9.98905359 1.07859298,8.39566699 1.23577893,6.7198402 L1.23578022,6.71984032 C1.38037157,5.17828994 2.67435224,4 4.22266882,4 Z" id="Combined-Shape" fill="#000000" opacity="0.3"/>
                                                        <path d="M11.1821576,14.8052934 L15.5856084,11.7952868 C15.8135802,11.6394552 15.8720614,11.3283211 15.7162299,11.1003494 C15.6814583,11.0494808 15.6375838,11.0054775 15.5868174,10.970557 L11.1833666,7.94156929 C10.9558527,7.78507001 10.6445485,7.84263875 10.4880492,8.07015268 C10.4307018,8.15352258 10.3999996,8.25233045 10.3999996,8.35351969 L10.3999996,14.392514 C10.3999996,14.6686564 10.6238572,14.892514 10.8999996,14.892514 C11.000689,14.892514 11.0990326,14.8621141 11.1821576,14.8052934 Z" id="Path-10" fill="#000000"/>
                                                    </g>
                                                </svg>
                                            </div>
                                            @if(!empty($tutoriel->linkVideo))
                                                <div class="kt-widget__details">
                                                    <span class="kt-widget__title">Vue</span>
                                                    <span class="kt-widget__value">{{ \App\Library\Youtube\Youtube::getViewVideo($tutoriel->linkVideo) }}</span>
                                                </div>
                                            @else
                                                <div class="kt-widget__details">
                                                    <span class="kt-widget__title">Vue</span>
                                                    <span class="kt-widget__value">NC</span>
                                                </div>
                                            @endif
                                        </div>

                                        <div class="kt-widget__item">
                                            <div class="kt-widget__icon">
                                                <i class="flaticon-twitter-logo"></i>
                                            </div>
                                            <div class="kt-widget__details">
                                                <span class="kt-widget__title">Publier sur Twitter</span>
                                                <span class="kt-widget__value">
                                                    @if($tutoriel->twitter == 0)
                                                        <i class="la la-close la-3x text-danger"></i>
                                                    @else
                                                        <i class="la la-check la-3x text-success" data-toggle="kt-popover" title="Texte publier sur twitter" content="{{ $tutoriel->twitterText }}"></i>
                                                    @endif
                                        </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                <!--end:: Widgets/Applications/User/Profile3-->
                    {{ $tutoriels->links('Back.Include.paginate', ["results" => $tutoriels]) }}
                </div>

                <!-- end:: Content -->
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript">
        (function ($) {

        })(jQuery)
    </script>
@endsection
