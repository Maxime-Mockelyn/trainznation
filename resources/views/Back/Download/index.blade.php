@extends("Back.Layout.app")

@section("css")
    <link href="/assets/back/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
@endsection

@section("bread")
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Téléchargement</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('Back.dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Téléchargement </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Liste des Téléchargement </a>

                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
    </div>
@endsection

@section("content")
<div class="row">
    <div class="col-md-12">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

            <!-- begin:: Content Head -->
            <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Téléchargements
                    </h3>
                    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                    <div class="kt-subheader__group" id="kt_subheader_search">
									<span class="kt-subheader__desc" id="kt_subheader_total">
										{{ \App\Repository\Asset\AssetRepository::countAll() }} {{ formatPlural('Téléchargement', \App\Repository\Asset\AssetRepository::countAll()) }}</span>
                        <form class="kt-margin-l-20" id="kt_subheader_search_form">
                            <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                                <input type="text" class="form-control" placeholder="Rechercher..." id="generalSearch">
                                <span class="kt-input-icon__icon kt-input-icon__icon--right">
												<span>
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
														<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
															<rect id="bound" x="0" y="0" width="24" height="24" />
															<path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" id="Path-2" fill="#000000" fill-rule="nonzero" opacity="0.3" />
															<path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" id="Path" fill="#000000" fill-rule="nonzero" />
														</g>
													</svg>

                                                    <!--<i class="flaticon2-search-1"></i>-->
												</span>
											</span>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="kt-subheader__toolbar">
                    <a href="#" class="">
                    </a>
                    <a href="{{ route('Back.Download.create') }}" class="btn btn-label-brand btn-bold">
                        Ajouter un téléchargement
                    </a>
                </div>
            </div>

            <!-- end:: Content Head -->

            <!-- begin:: Content -->
            {{ $assets->links('Back.Include.paginate', ["results" => $assets]) }}
            <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="panelAsset">

                <!--begin:: Widgets/Applications/User/Profile3-->
                @foreach($assets as $asset)
                <div class="kt-portlet" id="">
                    <div id="asset_id" data-id="{{ $asset->id }}"></div>
                    <div class="kt-portlet__body">
                        <div class="kt-widget kt-widget--user-profile-3">
                            <div class="kt-widget__top">
                                <div class="kt-widget__media kt-hidden-">
                                    <img src="{{ sourceImage('download/'.$asset->id.'.png') }}" alt="image">
                                </div>
                                <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden">
                                    JM
                                </div>
                                <div class="kt-widget__content">
                                    <div class="kt-widget__head">
                                        <a href="{{ route('Back.Download.show', $asset->id) }}" class="kt-widget__username">
                                            {{ $asset->designation }}
                                            {!! stateDownload($asset->published, $asset->published_at) !!}
                                            @if($asset->tree == 1)
                                                <i class="flaticon2-box kt-font-dark" data-toggle="kt-tooltip" title="Comporte un fichier 3D"></i>
                                            @endif
                                        </a>
                                        <div class="kt-widget__action">
                                            <a href="{{ route('Back.Download.show', $asset->id) }}" class="btn btn-label-info btn-sm btn-upper"><i class="la la-eye"></i> Voir la fiche</a>&nbsp;
                                            <a href="{{ route('Back.Download.edit', $asset->id) }}" class="btn btn-label-success btn-sm btn-upper"><i class="la la-edit"></i> Editer</a>&nbsp;
                                            <a href="{{ route('Back.Download.delete', $asset->id) }}" class="btn btn-label-danger btn-sm btn-upper"><i class="la la-times-circle"></i> Supprimer</a>&nbsp;
                                            @if($asset->published == 0 || $asset->published == 2)
                                                <a href="{{ route('Back.Download.publish', $asset->id) }}" class="btn btn-label-success btn-sm btn-upper"><i class="la la-unlock"></i> Publier</a>&nbsp;
                                            @else
                                            <a href="{{ route('Back.Download.unpublish', $asset->id) }}" class="btn btn-label-danger btn-sm btn-upper"><i class="la la-lock"></i> Dépublier</a>&nbsp;
                                            @endif
                                        </div>
                                    </div>
                                    <div class="kt-widget__subhead">
                                        <a href="{{ route('Download.redirect', $asset->id) }}"><i class="flaticon2-download"></i> {{ route('Download.redirect', $asset->id) }}</a>

                                    </div>
                                    <div class="kt-widget__info">
                                        <div class="kt-widget__desc">
                                            {!! $asset->short_description !!}
                                        </div>
                                        @if($asset->published == 0 || $asset->published == 2)
                                        <div class="kt-widget__progress">
                                            <div class="kt-widget__text">
                                                Progression de la construction
                                            </div>
                                            <div class="progress" style="height: 5px;width: 100%;">
                                                <div class="progress-bar {{ stateAssetPercent($asset->percent) }}" role="progressbar" style="width: {{ $asset->percent }}%;" aria-valuenow="{{ $asset->percent }}" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                            <div class="kt-widget__stats">
                                                {{ $asset->percent }}%
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="kt-widget__bottom">
                                <div class="kt-widget__item">
                                    <div class="kt-widget__icon">
                                        <i class="flaticon-download"></i>
                                    </div>
                                    <div class="kt-widget__details">
                                        <span class="kt-widget__title">Téléchargement</span>
                                        <span class="kt-widget__value">{{ $asset->count }}</span>
                                    </div>
                                </div>

                                <div class="kt-widget__item">
                                    <div class="kt-widget__icon">
                                        <i class="flaticon-clock"></i>
                                    </div>
                                    <div class="kt-widget__details">
                                        <span class="kt-widget__title">Compatibilité</span>
                                        <div class="kt-section__content kt-section__content--solid">
                                            <div class="kt-badge kt-badge__pics">
                                                @foreach($asset->compatibilities as $compatibility)
                                                    @if($compatibility->state == 0)
                                                        <span class="kt-badge kt-badge--danger kt-badge--md" data-toggle="kt-tooltip" title="Non Compatible">{{ $compatibility->trainz_build }}</span>
                                                    @elseif($compatibility->state == 1)
                                                        <span class="kt-badge kt-badge--warning kt-badge--md" data-toggle="kt-tooltip" title="Problème de compatibilité">{{ $compatibility->trainz_build }}</span>
                                                    @elseif($compatibility->state == 2)
                                                        <span class="kt-badge kt-badge--success kt-badge--md" data-toggle="kt-tooltip" title="Compatible">{{ $compatibility->trainz_build }}</span>
                                                    @else
                                                        <span class="kt-badge kt-badge--info kt-badge--md" data-toggle="kt-tooltip" title="Test à effectuer">{{ $compatibility->trainz_build }}</span>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="kt-widget__item">
                                    <div class="kt-widget__icon">
                                        <i class="flaticon-twitter-logo"></i>
                                    </div>
                                    <div class="kt-widget__details">
                                        <span class="kt-widget__title">Publier sur Twitter</span>
                                        <span class="kt-widget__value">
                                            @if($asset->twitter == 0)
                                                <i class="la la-close la-3x text-danger"></i>
                                            @else
                                                <i class="la la-check la-3x text-success" data-toggle="kt-popover" title="Texte publier sur twitter" content="{{ $asset->twitterText }}"></i>
                                            @endif
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

                <!--end:: Widgets/Applications/User/Profile3-->
                {{ $assets->links('Back.Include.paginate', ["results" => $assets]) }}
            </div>

            <!-- end:: Content -->
        </div>
    </div>
</div>
@endsection

@section("script")

    <script type="text/javascript">
        (function ($) {

            $("#generalSearch").on('keyup', function (e) {
                e.preventDefault()
                let input = $(this)
                let value = input.val()

                $("#panelAsset")
                    .html()

                KTApp.blockPage({
                    overlayColor: "#000000",
                    type: 'v2',
                    state: 'success',
                    message: 'Veuillez Patienter...'
                })

                $.ajax({
                    url: '/back/download/search/'+value,
                    success: function (data) {
                        KTApp.unblockPage();

                        $("#panelAsset").html(data)
                    }
                })
            })
        })(jQuery)
    </script>
@endsection
