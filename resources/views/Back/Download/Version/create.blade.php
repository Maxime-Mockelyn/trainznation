@extends("Back.Layout.app")

@section("css")
    <style type="text/css">
        .ck-editor__editable {
            min-height: 500px;
        }
    </style>
@endsection

@section("bread")
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Téléchargement</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('Back.dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Téléchargement </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">{{ $asset->designation }} </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Ajout d'une version de téléchargement </a>

                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                @back(["data" => ["url" => "Back.Download.show", "id" => $asset->id]]) @endback
            </div>
        </div>
    </div>
@endsection

@section("content")
    <div class="row">
        <div class="col-md-12">
            <div id="errorForm"></div>
            <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
                <form action="{{ route('Download.Version.store', $asset->id) }}" class="kt-form" id="createForm" method="POST" enctype="multipart/form-data">
                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">Formulaire de d'édition</h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <div class="btn-group">
                                <button type="submit" class="btn btn-brand" id="btnForm">
                                    <i class="la la-check"></i>
                                    <span class="kt-hidden-mobile">Sauvegarder</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="row">
                            <div class="col-xl-3"></div>
                            <div class="col-xl-6">
                                <div class="kt-section kt-section--first">
                                    <div class="kt-section__body">
                                        @csrf
                                        <div class="form-group row">
                                            <label class="col-2 col-form-label">Numéro de Version</label>
                                            <div class="col-10">
                                                <input class="form-control form-control-lg" type="text" name="version_text" placeholder="Version de l'objet (1,2,3,1.0,etc...)">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <textarea id="description" class="form-control" name="version_log" placeholder="Description de la version"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-2 col-form-label">Lien de Version</label>
                                            <div class="col-10">
                                                <input class="form-control form-control-lg" type="text" name="version_link" placeholder="Lien Mega de la version">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="/assets/custom/vendors/ckeditor/ckeditor.js"></script>
    <script type="text/javascript">
        (function ($) {
            $(".kt-selectpicker").selectpicker({
                container: 'body'
            });

            createCkeditor('#description')

            $("#createForm").on('submit', function (e) {
                e.preventDefault()
                let form = $(this)
                let url = form.attr('action')
                let btn = $("#btnForm")
                let data = form.serializeArray()

                progressBtn(btn)

                $.ajax({
                    url: url,
                    type: "POST",
                    data: data,
                    success: function (data) {
                        unprogressBtn(btn)
                        toastr.success("La Version <strong>"+data.version+"</strong> à été ajouté")
                        window.setTimeout(function () {
                            window.location.href='/back/download/'+data.asset_id
                        }, 2000)
                    },
                    error: function (resultat, statut, erreur) {
                        unprogressBtn(btn)
                        toastr.error(resultat.responseJSON.message, "Erreur 500")
                    }
                })
            })

        })(jQuery)
    </script>
@endsection
