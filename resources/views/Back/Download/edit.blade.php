@extends("Back.Layout.app")

@section("css")
    <style type="text/css">
        .ck-editor__editable {
            min-height: 500px;
        }
    </style>
    <link rel="stylesheet" href="/assets/custom/vendors/editor-md/css/editormd.css">
@endsection

@section("bread")
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Téléchargement</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('Back.dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Téléchargement </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">{{ $asset->designation }} </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Edition du téléchargement </a>

                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                @back(["data" => ["url" => "Back.Download.show", "id" => $asset->id]]) @endback
            </div>
        </div>
    </div>
@endsection

@section("content")
    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('Back.Download.update', $asset->id) }}" class="kt-form" method="POST" enctype="multipart/form-data">
                @csrf
                @method("PUT")
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon">
                                <i class="flaticon2-edit"></i>
                            </span>
                            <h3 class="kt-portlet__head-title">
                                Informations Générales
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Titre</label>
                                    <input type="text" class="form-control form-control-lg" name="designation" value="{{ $asset->designation }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>KUID</label>
                                    <input type="text" class="form-control form-control-lg" name="kuid" value="{{ $asset->kuid }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Lien de téléchargement</label>
                                    <input type="text" class="form-control form-control-lg" name="downloadLink" value="{{ $asset->downloadLink }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Courte description</label>
                                    <textarea class="form-control" name="short_description"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Catégorie</label>
                                    <select id="category_id" class="form-control kt-select2" name="category_id" onchange="loadSubCategorie()">
                                        <option value=""></option>
                                        <option value="{{ $asset->asset_categorie_id }}">{{ $asset->categorie->name }}</option>
                                        <option value="">-------------------------------</option>
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div id="subcategories"></div>
                            </div>
                        </div>
                        <div class="kt-separator"></div>
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label>Publication</label>
                                    <div class="kt-radio-list">
                                        <label class="kt-radio kt-radio--solid kt-radio--danger">
                                            <input type="radio" name="published" value="0" @if($asset->published == 0) checked @endif> Non Publier
                                            <span></span>
                                        </label>
                                        <label class="kt-radio kt-radio--solid kt-radio--warning">
                                            <input type="radio" name="published" value="2" @if($asset->published == 2) checked @endif onclick="showPublishedAt()"> Prévoir la publication
                                            <span></span>
                                        </label>
                                        <label class="kt-radio kt-radio--solid kt-radio--success">
                                            <input type="radio" name="published" value="1" @if($asset->published == 1) checked @endif> Publier
                                            <span></span>
                                        </label>
                                    </div>
                                    <br>
                                    <div id="divPublishedAt">
                                        <label>Date de Publication</label>
                                        <input type="text" class="form-control datetimepicker" name="published_at" readonly value="{{ $asset->published_at }}" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label>Fichier de configuration</label>
                                    <div class="kt-radio-list">
                                        <label class="kt-radio kt-radio--solid">
                                            <input type="radio" name="config" value="0" @if($asset->config == 0) checked @endif> Aucun Fichier
                                            <span></span>
                                        </label>
                                        <label class="kt-radio kt-radio--solid">
                                            <input type="radio" name="config" value="1" @if($asset->config == 1) checked @endif onclick="showConfigFile()"> Fichier disponible
                                            <span></span>
                                        </label>
                                    </div>
                                    <div id="divConfigFile">
                                        <br>
                                        <label>Fichier</label>
                                        <input type="file" class="form-control" name="configFile">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <label>Modèle 3D</label>
                                <div class="kt-radio-list">
                                    <label class="kt-radio kt-radio--solid">
                                        <input type="radio" name="tree" value="0" @if($asset->tree == 0) checked @endif> Aucun modèle
                                        <span></span>
                                    </label>
                                    <label class="kt-radio kt-radio--solid">
                                        <input type="radio" name="tree" value="1" @if($asset->tree == 1) checked @endif onclick="showModeleFile()"> Modèle disponible
                                        <span></span>
                                    </label>
                                </div>
                                <div id="divModeleFile">
                                    <br>
                                    <label>Fichier du modèle 3D</label>
                                    <input type="file" name="modeleFile" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <label>Publication sur twitter</label>
                                <div class="kt-radio-list">
                                    <label class="kt-radio kt-radio--solid">
                                        <input type="radio" name="twitter" value="0" @if($asset->twitter == 0) checked @endif> Non
                                        <span></span>
                                    </label>
                                    <label class="kt-radio kt-radio--solid">
                                        <input type="radio" name="twitter" value="1" @if($asset->twitter == 1) checked @endif onclick="showTwitterText()"> Oui
                                        <span></span>
                                    </label>
                                </div>
                                <div id="divTwitterText">
                                    <br>
                                    <label>Texte à publier sur Twitter</label>
                                    <textarea class="form-control" id="twitterText" name="twitterText" maxlength="280" rows="6">{{ $asset->twitterText }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="row align-items-center">
                            <div class="col-lg-12 kt-align-center">
                                <button type="submit" class="btn btn-success"><i class="la la-check-circle"></i> Soumettre</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <form action="{{ route('Back.Download.updateContenu', $asset->id) }}" class="kt-form" method="POST">
                @csrf
                @method("PUT")
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon">
                                <i class="flaticon2-pen"></i>
                            </span>
                            <h3 class="kt-portlet__head-title">
                                Contenue
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="form-group">
                            <div id="fieldDesc">
                                <textarea style="display:none;" name="description">{{ $asset->short_description }}&#10;{{ $asset->description }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="row align-items-center">
                            <div class="col-lg-12 kt-align-center">
                                <button type="submit" class="btn btn-success"><i class="la la-check-circle"></i> Soumettre</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-4">
            <form action="{{ route('Back.Download.updateContent', $asset->id) }}" class="kt-form" method="POST" enctype="multipart/form-data">
                @csrf
                @method("PUT")
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon">
                                <i class="flaticon2-image-file"></i>
                            </span>
                            <h3 class="kt-portlet__head-title">
                                Images
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="form-group">
                            @if(\Illuminate\Support\Facades\Storage::disk('public')->exists('download/'.$asset->id.'.png') == true)
                                <img src="/storage/download/{{ $asset->id }}.png" alt="{{ $asset->designation }}" width="150">
                            @else
                                <img src="https://via.placeholder.com/150" alt="{{ $asset->designation }}" width="150">
                            @endif
                            <br>
                            <label>Image de l'objet</label>
                            <input type="file" class="form-control" name="images">
                        </div>
                        <div class="form-group">
                            <label>Pourcentage d'avancement</label>
                            <div class="kt-ion-range-slider">
                                <input type="hidden" id="percent" name="percent"/>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="row align-items-center">
                            <div class="col-lg-12 kt-align-center">
                                <button type="submit" class="btn btn-success"><i class="la la-check-circle"></i> Soumettre</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <form action="{{ route('Back.Download.updatePrice', $asset->id) }}" class="kt-form" method="POST">
                @csrf
                @method("PUT")
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon">
                                <i class="fa fa-euro-sign"></i>
                            </span>
                            <h3 class="kt-portlet__head-title">
                                Prix
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="form-group">
                            <label>Type</label>
                            <div class="kt-radio-list">
                                <label class="kt-radio kt-radio--solid">
                                    <input type="radio" name="pricing" value="0" @if($asset->pricing == 0) checked @endif> Gratuit
                                    <span></span>
                                </label>
                                <label class="kt-radio kt-radio--solid">
                                    <input type="radio" name="pricing" value="1" @if($asset->pricing == 1) checked @endif onclick="showPrice()"> Payant
                                    <span></span>
                                </label>
                            </div>
                            <div id="divPrice">
                                <br>
                                <label>Montant de l'objet</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="0.00" name="price">
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="fa fa-euro-sign kt-font-brand"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="row align-items-center">
                            <div class="col-lg-12 kt-align-center">
                                <button type="submit" class="btn btn-success"><i class="la la-check-circle"></i> Soumettre</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="/assets/back/vendors/general/bootstrap-datetime-picker/js/locales/bootstrap-datetimepicker.fr.js"></script>
    <script src="/assets/custom/vendors/editor-md/editormd.js"></script>
    <script src="/assets/custom/vendors/editor-md/languages/en.js"></script>
    <script type="text/javascript">

        function loadSubCategorie()
        {
            let div = $("#subcategories")
            let categorie = $("#category_id").val();

            $.ajax({
                url: "/back/download/listSubCategorie/"+categorie,
                method: "GET",
                success: function (data) {
                    div.html(data.content)
                }
            })
        }

        function showPublishedAt()
        {
            if($('input[name=published]:checked')) {
                $("#divPublishedAt").show()
            }
        }
        function showPrice()
        {
            if($('input[name=pricing]:checked')) {
                $("#divPrice").show()
            }
        }

        function showConfigFile()
        {
            if($('input[name=config]:checked')) {
                $("#divConfigFile").show()
            }
        }

        function showModeleFile()
        {
            if($('input[name=modele]:checked')) {
                $("#divModeleFile").show()
            }
        }

        function showTwitterText()
        {
            if($('input[name=twitter]:checked')) {
                $("#divTwitterText").show()
            }
        }

        (function ($) {

            $("#divPublishedAt").hide()
            $("#divConfigFile").hide()
            $("#divModeleFile").hide()
            $("#divTwitterText").hide()
            $("#divPrice").hide()

            $("#category_id").select2({
                placeholder: "Selectionnez une catégorie..."
            })
            $(".datetimepicker").datetimepicker({
                todayHighlight: true,
                autoclose: true,
                pickerPosition: 'bottom-left',
                format: 'yyyy-mm-dd hh:ii:00',
                language: 'fr'
            })
            $('#twitterText').maxlength({
                alwaysShow: true,
                warningClass: "kt-badge kt-badge--primary kt-badge--rounded kt-badge--inline",
                limitReachedClass: "kt-badge kt-badge--brand kt-badge--rounded kt-badge--inline"
            });
            let editor = editormd("fieldDesc", {
                width: '100%',
                height: '460px',
                path:"/assets/custom/vendors/editor-md/lib/"
            })
            $("#percent").ionRangeSlider({
                min: 0,
                max: 100,
                from: 1
            });
        })(jQuery)
    </script>
@endsection
