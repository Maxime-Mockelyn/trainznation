@extends("Back.Layout.app")

@section("css")
    <link href="/assets/back/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
@endsection

@section("bread")
    <!-- begin:: Content Head -->
    <div class="kt-subheader  kt-grid__item" id="kt_subheader">
        <div class="kt-container kt-container--fluid">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">Téléchargement</h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('Back.dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">Téléchargement </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">{{ $asset->designation }} </a>

                    <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                </div>
            </div>
            <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">
                    @back(["data" => ["url" => "Back.Download.index"]]) @endback
                    @edit(["data" => ["url" => "Back.Download.edit", "terme" => "l'objet", "id" => $asset->id]]) @endedit
                    @delete(["data" => ["url" => "Back.Download.delete", "terme" => "l'objet", "id" => $asset->id]]) @enddelete
                    @if($asset->published == 0 || $asset->published == 2)
                        @publish(["data" => ["url" => "Back.Download.publish", "terme" => "l'objet", "id" => $asset->id]]) @endpublish
                    @else
                        @unpublish(["data" => ["url" => "Back.Download.unpublish", "terme" => "l'objet", "id" => $asset->id]]) @endunpublish
                    @endif
                    @if($asset->tree == true)
                        <a href="{{ route('Back.Download.3d', $asset->id) }}" class="btn kt-subheader__btn-daterange" id="kt_dashboard_daterangepicker">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect id="bound" x="0" y="0" width="24" height="24"/>
                                    <path d="M1.5,5 L4.5,5 C5.32842712,5 6,5.67157288 6,6.5 L6,17.5 C6,18.3284271 5.32842712,19 4.5,19 L1.5,19 C0.671572875,19 1.01453063e-16,18.3284271 0,17.5 L0,6.5 C-1.01453063e-16,5.67157288 0.671572875,5 1.5,5 Z M18.5,5 L22.5,5 C23.3284271,5 24,5.67157288 24,6.5 L24,17.5 C24,18.3284271 23.3284271,19 22.5,19 L18.5,19 C17.6715729,19 17,18.3284271 17,17.5 L17,6.5 C17,5.67157288 17.6715729,5 18.5,5 Z" id="Combined-Shape" fill="#000000"/>
                                    <rect id="Rectangle-7-Copy-2" fill="#000000" opacity="0.3" x="8" y="5" width="7" height="14" rx="1.5"/>
                                </g>
                            </svg>
                            <span class="kt-subheader__btn-daterange-title">Vue 3D</span>&nbsp;
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section("content")
    <div id="asset_id" data-id="{{ $asset->id }}"></div>
    @if($asset->published == 0)
        <div class="alert alert-info fade show" role="alert">
            <div class="alert-icon"><i class="flaticon-warning-sign"></i></div>
            <div class="alert-text">Cette asset n'est pas publier, certaine fonction sont désactiver !</div>
            <div class="alert-close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="la la-close"></i></span>
                </button>
            </div>
        </div>
    @endif
    @if($asset->published == 2)
        <div class="alert alert-warning fade show" role="alert">
            <div class="alert-icon"><i class="flaticon-warning-sign"></i></div>
            <div class="alert-text">Cette asset est prévue à la publication <strong>{{ formatDateHuman($asset->published_at)}}</strong>, certaine fonction ne sont toujours pas disponible</div>
            <div class="alert-close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="la la-close"></i></span>
                </button>
            </div>
        </div>
    @endif
<div class="row">
    <div class="col-md-4">
        <div class="kt-portlet kt-portlet--height-fluid kt-widget19">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-widget19__pic kt-portlet-fit--top kt-portlet-fit--sides" style="min-height: 300px; background-image: url({{ sourceImage('download/'.$asset->id.'.png') }})">
                    <h3 class="kt-widget19__title kt-font-light">
                        {{ $asset->designation }}
                    </h3>
                    <div class="kt-widget19__shadow"></div>
                    <div class="kt-widget19__labels">
                        @if($asset->published == 1)
                            <a href="#" class="btn btn-label-light-o2 btn-success btn-bold btn-sm "><i class="la la-check"></i> Publier</a>
                        @elseif($asset->published == 0)
                            <a href="#" class="btn btn-label-light-o2 btn-danger btn-bold btn-sm "><i class="la la-times"></i> Non Publier</a>
                        @else
                            <a href="#" class="btn btn-label-light-o2 btn-warning btn-bold btn-sm "><i class="la la-clock-o"></i> Prévue</a>
                        @endif
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="kt-widget19__wrapper">
                    <div class="kt-widget19__content">
                        <div class="kt-widget19__info">
                            <a href="#" class="kt-widget19__username">
                                {{ $asset->kuid }}
                            </a>
                            <span class="kt-widget19__time">
                                {{ $asset->categorie->name }}, {{ $asset->subcategorie->name }}
                            </span>
                        </div>
                        <div class="kt-widget19__stats">
														<span class="kt-widget19__number kt-font-brand">
															{{ $asset->count }}
														</span>
                            <a href="#" class="kt-widget19__comment">
                                {{ formatPlural('Téléchargement', $asset->count) }}
                            </a>
                        </div>
                    </div>
                    <div class="kt-widget19__text">
                        {!! $asset->short_description !!}
                    </div>
                    <div class="kt-section">
                        <ul class="kt-nav kt-nav--bold kt-nav--md-space kt-nav--v3 kt-margin-t-20 kt-margin-b-20" role="tablist">
                            <li class="kt-nav__item">
                                <a class="kt-nav__link active" data-toggle="tab" href="#description" role="tab">
                                    <span class="kt-nav__link-icon"><i class="flaticon-edit"></i></span>
                                    <span class="kt-nav__link-text">Description</span>
                                </a>
                            </li>
                            <li class="kt-nav__item">
                                <a class="kt-nav__link" data-toggle="tab" href="#compatibilities" role="tab" aria-selected="true">
                                    <span class="kt-nav__link-icon"><i class="flaticon-lock"></i></span>
                                    <span class="kt-nav__link-text">Compatibilité</span>
                                </a>
                            </li>
                            <li class="kt-nav__item">
                                <a class="kt-nav__link" data-toggle="tab" href="#version" role="tab" >
                                    <span class="kt-nav__link-icon"><i class="flaticon-download"></i></span>
                                    <span class="kt-nav__link-text">Versions</span>
                                </a>
                            </li>
                            @if($asset->config == 1)
                                <li class="kt-nav__item">
                                    <a class="kt-nav__link" data-toggle="tab" href="#config" role="tab" >
                                        <span class="kt-nav__link-icon"><i class="flaticon-file"></i></span>
                                        <span class="kt-nav__link-text">Config.txt</span>
                                    </a>
                                </li>
                            @endif
                            <li class="kt-nav__item">
                                <a class="kt-nav__link" data-toggle="tab" href="#price" role="tab" >
                                    <span class="kt-nav__link-icon"><i class="fa fa-euro-sign"></i></span>
                                    <span class="kt-nav__link-text">Prix</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="kt-widget19__action">
                    <a href="{{ route('Front.Download.show', [$asset->subcategorie->id, $asset->id]) }}" target="_blank" class="btn btn-sm btn-label-brand btn-bold">Voir sur le FrontEnd</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="tab-content">
            <div class="tab-pane active" id="description" role="tabpanel">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon">
                                <i class="flaticon-edit"></i>
                            </span>
                            <h3 class="kt-portlet__head-title">
                                Description
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-actions">
                                <a href="{{ route('Back.Download.edit', $asset->id) }}" class="btn btn-clean btn-sm">
                                    <i class="flaticon-edit-1"></i> Editer la description
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div id="contenue" data-id="{{ $asset->id }}"></div>
                    </div>
                </div>
            </div>

            <div class="tab-pane" id="compatibilities" role="tabpanel">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon">
                                <i class="flaticon-lock"></i>
                            </span>
                            <h3 class="kt-portlet__head-title">
                                Liste des versions compatibles
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-actions">
                                <a href="{{ route('Back.Download.Compatibility.create', $asset->id) }}" class="btn btn-clean btn-sm">
                                    <i class="flaticon-plus"></i> Ajouter une version
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="table-responsive">
                            <table id="listeCompatibilite" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Version</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($asset->compatibilities->sortBy('trainz_build') as $compatibility)
                                        <tr>
                                            <td class="text-center" width="85%">
                                                @if($compatibility->state == 0)
                                                    <span class="kt-badge kt-badge--danger kt-badge--xl" data-toggle="kt-tooltip" title="Non Compatible">{{ $compatibility->trainz_build }}</span>
                                                @elseif($compatibility->state == 1)
                                                    <span class="kt-badge kt-badge--warning kt-badge--xl" data-toggle="kt-tooltip" title="Problème de compatibilité">{{ $compatibility->trainz_build }}</span>
                                                @elseif($compatibility->state == 2)
                                                    <span class="kt-badge kt-badge--success kt-badge--xl" data-toggle="kt-tooltip" title="Compatible">{{ $compatibility->trainz_build }}</span>
                                                @else
                                                    <span class="kt-badge kt-badge--info kt-badge--xl" data-toggle="kt-tooltip" title="Test à effectuer">{{ $compatibility->trainz_build }}</span>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('Back.Download.Compatibility.edit', [$asset->id, $compatibility->id]) }}" class="btn btn-primary btn-icon" data-toggle="kt-tooltip" title="Editer la version">
                                                    <i class="la la-edit"></i>
                                                </a>
                                                <button id="btnDelete" data-href="{{ route('Back.Download.Compatibility.delete', [$asset->id, $compatibility->id]) }}" class="btn btn-danger btn-icon" data-toggle="kt-tooltip" title="Supprimer la version">
                                                    <i class="la la-times-circle"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane" id="version" role="tabpanel">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon">
                                <i class="flaticon-lock"></i>
                            </span>
                            <h3 class="kt-portlet__head-title">
                                Liste des versions de l'objet
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-actions">
                                <a href="{{ route('Download.Version.create', $asset->id) }}" class="btn btn-clean btn-sm">
                                    <i class="flaticon-plus"></i> Ajouter une version
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="table-responsive">
                            <table id="listeVersion" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Version</th>
                                    <th>Horodatage</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($asset->versions->sortBy('created_at') as $version)
                                    <tr>
                                        <td>{{ $version->version_text }}</td>
                                        <td>{{ formatDate($version->updated_at) }}</td>
                                        <td>
                                            <button id="btnModal" data-id="{{ $version->id }}" class="btn btn-icon" data-toggle="kt-tooltip" title="Voir les Informations"><i class="la la-file"></i> </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            @if($asset->config == 1)
                <div class="tab-pane" id="config" role="tabpanel">
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon">
                                <i class="flaticon-edit"></i>
                            </span>
                                <h3 class="kt-portlet__head-title">
                                    Fichier Config.txt
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                        <pre>
                            <code class="plaintext">
                                {{ file_get_contents('storage/config/'.str_slug($asset->categorie->name).'/'.str_slug($asset->subcategorie->name).'/'.pregReplaceKuid($asset->kuid).'.txt') }}
                            </code>
                        </pre>
                        </div>
                    </div>
                </div>
            @endif
            <div class="tab-pane" id="price" role="tabpanel">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon">
                                <i class="fa fa-euro-sign"></i>
                            </span>
                            <h3 class="kt-portlet__head-title">
                                Prix de l'objet
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <table class="table table-striped table-bordered">
                            <tbody>
                                <tr>
                                    <td>L'objet est-il payant ?</td>
                                    <td>
                                        @if($asset->pricing == 0)
                                            <span class="kt-badge kt-badge--inline kt-badge--danger">Non</span>
                                        @else
                                            <span class="kt-badge kt-badge--inline kt-badge--success">Oui</span>
                                        @endif
                                    </td>
                                </tr>
                                @if($asset->pricing == 1)
                                    <tr>
                                        <td>Prix</td>
                                        <td>{{ formatCurrency($asset->price) }}</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div id="modal-body-ajax"></div>
            </div>
        </div>
    </div>
</div>

@endsection

@section("script")
    <script src="/assets/back/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
    <script src="/assets/custom/vendors/markdown-js/markdown.js"></script>
    <script type="text/javascript">
        function loadDescription()
        {
            let description = $("#contenue")

            $.ajax({
                url: '/back/download/'+description.attr('data-id')+'/getContenue',
                success: function (data) {
                    let md_content = data[0]
                    description.html(markdown.toHTML(md_content))
                }
            })
        }

        (function ($) {
            loadDescription()
            let identifier = $("#asset_id").attr('data-id');

            $("#listeCompatibilite")
                .on('click', '#btnDelete', function (e) {
                    let btn = $(this)
                    let url = btn.attr('data-href');
                    swal.fire({
                        title: "Vous allez supprimer un indicateur de compatibilité.",
                        text: "Etes-vous sur ?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: 'Oui, je veux supprimer !'
                    }).then(function (result) {
                        if(result.value){
                            $.ajax({
                                url: url,
                                type: "DELETE",
                                success: function () {
                                    swal.fire(
                                        'Indicateur de compatibilite',
                                        "Cette indicateur à été supprimer",
                                        "success"
                                    )
                                    btn.parents('tr').fadeOut()
                                },
                                error: function () {
                                    swal.fire(
                                        'Erreur 500',
                                        "Erreur lors de l'execution de la commande",
                                        "error"
                                    )
                                }
                            })
                        }
                    })
                })

            $("#listeVersion").on('click', '#btnModal', function (e) {
                e.preventDefault();
                let btn = $(this)
                let version_id = btn.attr('data-id')

                $.ajax({
                    url: '/back/download/'+identifier+'/version/'+version_id+'/getInfo',
                    success: function (data) {
                        $("#exampleModalLabel").html(data.title)
                        $("#modal-body-ajax").html(data.content)
                        $("#modalInfo").modal('show')
                    }
                })
            })
        })(jQuery)
    </script>
@endsection
