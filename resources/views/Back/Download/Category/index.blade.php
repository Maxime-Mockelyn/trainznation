@extends("Back.Layout.app")

@section("css")
    <link href="/assets/back/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
@endsection

@section("bread")
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Téléchargement</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('Back.dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Téléchargement </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Liste des Catégories </a>

                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
    </div>
@endsection

@section("content")
<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand la la-table"></i>
										</span>
                    <h3 class="kt-portlet__head-title">
                        Liste des Catégories
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="kt-portlet__head-actions">
                            <button class="btn btn-brand btn-elevate btn-icon-sm" data-toggle="modal" data-target="#addCategory">
                                <i class="la la-plus"></i>
                                Nouvelle catégorie
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="tableCategorie">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Libellée</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td>{{ $category->id }}</td>
                                <td>{{ $category->name }}</td>
                                <td>
                                    <a href="{{ route('Download.Category.edit', $category->id) }}" class="btn btn-sm btn-brand btn-elevate btn-icon"><i class="la la-edit"></i></a>
                                    <button id="btnDelete" type="button" class="btn btn-sm btn-danger btn-elevate btn-icon" data-href="{{ route('Download.Category.delete', $category->id) }}"><i class="la la-trash"></i></button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <!--end: Datatable -->
            </div>
        </div>
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand la la-table"></i>
										</span>
                    <h3 class="kt-portlet__head-title">
                        Liste des Sous Catégories
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="kt-portlet__head-actions">
                            <button class="btn btn-brand btn-elevate btn-icon-sm" data-toggle="modal" data-target="#addSubCategory">
                                <i class="la la-plus"></i>
                                Nouvelle Sous Catégorie
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="tableSubCategorie">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Libellée</th>
                        <th>Catégorie</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($subs as $sub)
                        <tr>
                            <td>{{ $sub->id }}</td>
                            <td>{{ $sub->name }}</td>
                            <td>{{ $sub->categorie->name }}</td>
                            <td>
                                <a href="{{ route('Download.Subcategory.edit', $sub->id) }}" class="btn btn-sm btn-brand btn-elevate btn-icon"><i class="la la-edit"></i></a>
                                <button id="btnDelete" type="button" class="btn btn-sm btn-danger btn-elevate btn-icon" data-href="{{ route('Download.Subcategory.delete', $sub->id) }}"><i class="la la-trash"></i></button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <!--end: Datatable -->
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="addCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nouvelle Catégorie</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form id="formCategory" action="{{ route('Download.Category.store') }}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">Nom de la Catégorie:</label>
                        <input type="text" class="form-control" name="name">
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" id="btnFormCategory" class="btn btn-primary">Ajouter</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="addSubCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nouvelle Sous Catégorie</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form id="formSubcategory" action="{{ route('Download.Subcategory.store') }}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">Catégorie Parente:</label>
                        <select name="categorie_id" class="form-control">
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">Nom de la sous Catégorie:</label>
                        <input type="text" class="form-control" name="name">
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" id="btnFormSubcategory" class="btn btn-primary">Ajouter</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section("script")
    <script src="/assets/back/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
    <script type="text/javascript">
        "use strict";
        var tableCategorie = function() {

            var initTable1 = function() {
                var table = $('#tableCategorie');

                // begin first table
                table.DataTable({
                    responsive: true,

                    language: {
                        "sProcessing":     "Traitement en cours...",
                        "sSearch":         "Rechercher&nbsp;:",
                        "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
                        "sInfo":           "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                        "sInfoEmpty":      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                        "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                        "sInfoPostFix":    "",
                        "sLoadingRecords": "Chargement en cours...",
                        "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                        "sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
                        "oPaginate": {
                            "sFirst":      "Premier",
                            "sPrevious":   "Pr&eacute;c&eacute;dent",
                            "sNext":       "Suivant",
                            "sLast":       "Dernier"
                        },
                        "oAria": {
                            "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
                            "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
                        },
                        "select": {
                            "rows": {
                                _: "%d lignes séléctionnées",
                                0: "Aucune ligne séléctionnée",
                                1: "1 ligne séléctionnée"
                            }
                        }
                    },
                });
            };

            return {

                //main function to initiate the module
                init: function() {
                    initTable1();
                },

            };

        }();
        var tableSubCategorie = function() {

            var initTable1 = function() {
                var table = $('#tableSubCategorie');

                // begin first table
                table.DataTable({
                    responsive: true,

                    lengthMenu: [5, 10, 25, 50],

                    pageLength: 10,

                    language: {
                        "sProcessing":     "Traitement en cours...",
                        "sSearch":         "Rechercher&nbsp;:",
                        "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
                        "sInfo":           "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                        "sInfoEmpty":      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                        "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                        "sInfoPostFix":    "",
                        "sLoadingRecords": "Chargement en cours...",
                        "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                        "sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
                        "oPaginate": {
                            "sFirst":      "Premier",
                            "sPrevious":   "Pr&eacute;c&eacute;dent",
                            "sNext":       "Suivant",
                            "sLast":       "Dernier"
                        },
                        "oAria": {
                            "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
                            "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
                        },
                        "select": {
                            "rows": {
                                _: "%d lignes séléctionnées",
                                0: "Aucune ligne séléctionnée",
                                1: "1 ligne séléctionnée"
                            }
                        }
                    },

                    // Order settings
                    order: [[1, 'desc']],

                    columnDefs: [],
                });
            };

            return {

                //main function to initiate the module
                init: function() {
                    initTable1();
                },

            };

        }();

        jQuery(document).ready(function() {
            tableCategorie.init();
            tableSubCategorie.init();
        });
    </script>
    <script type="text/javascript">
        (function ($) {
            $("#formCategory").on('submit', function (e) {
                e.preventDefault()
                let form = $(this)
                let url = form.attr('action')
                let btn = $("#btnFormCategory")
                let data = form.serializeArray()

                KTApp.progress(btn)

                $.ajax({
                    url: url,
                    method: "POST",
                    data: data,
                    statusCode: {
                        200: function (data) {
                            KTApp.unprogress(btn)
                            toastr.success("La catégorie <strong>"+data.name+"</strong> à été ajouté!", "Ajout d'une catégorie")
                            let tr = data.tr
                            form.find('input').val('')
                            $("#tableCategorie tbody").prepend(tr)
                            tr.hide().fadeIn()
                        },
                        500: function (data) {
                            KTApp.unprogress(btn)
                            toastr.error("Erreur lors du traitement serveur", "Erreur 500 !")
                        }
                    }
                })
            })
            $("#formSubcategory").on('submit', function (e) {
                e.preventDefault()
                let form = $(this)
                let url = form.attr('action')
                let btn = $("#btnFormSubcategory")
                let data = form.serializeArray()

                KTApp.progress(btn)

                $.ajax({
                    url: url,
                    method: "POST",
                    data: data,
                    statusCode: {
                        200: function (data) {
                            KTApp.unprogress(btn)
                            toastr.success("La sous catégorie <strong>"+data.name+"</strong> à été ajouté!", "Ajout d'une sous catégorie")
                            let tr = data.tr
                            form.find('input').val('')
                            $("#tableSubCategorie tbody").prepend(tr)
                            tr.hide().fadeIn()
                        },
                        500: function (data) {
                            KTApp.unprogress(btn)
                            toastr.error("Erreur lors du traitement serveur", "Erreur 500 !")
                        }
                    }
                })
            })
            $("#tableCategorie").on('click', '#btnDelete', function (e) {
                e.preventDefault()
                let btn = $(this)
                let url = btn.attr('data-href')

                KTApp.progress(btn)

                $.ajax({
                    url: url,
                    method: "DELETE",
                    statusCode: {
                        200: function (data) {
                            KTApp.unprogress(btn)
                            toastr.success("Une catégorie à été supprimer !", "Suppression d'une catégorie")
                            btn.parents('tr').fadeOut()
                        },
                        500: function (data) {
                            KTApp.unprogress(btn)
                            toastr.error("Erreur lors du traitement serveur", "Erreur 500 !")
                        }
                    }
                })
            })
            $("#tableSubCategorie").on('click', '#btnDelete', function (e) {
                e.preventDefault()
                let btn = $(this)
                let url = btn.attr('data-href')

                KTApp.progress(btn)

                $.ajax({
                    url: url,
                    method: "DELETE",
                    statusCode: {
                        200: function (data) {
                            KTApp.unprogress(btn)
                            toastr.success("Une sous catégorie à été supprimer !", "Suppression d'une sous catégorie")
                            btn.parents('tr').fadeOut()
                        },
                        500: function (data) {
                            KTApp.unprogress(btn)
                            toastr.error("Erreur lors du traitement serveur", "Erreur 500 !")
                        }
                    }
                })
            })
        })(jQuery)
    </script>
@endsection
