@extends("Back.Layout.app")

@section("css")
    <link href="/assets/back/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
@endsection

@section("bread")
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Téléchargement</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('Back.dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Téléchargement </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">{{ $asset->designation }} </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Edition d'une compatibilité </a>

                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
    </div>
@endsection

@section("content")
    <div class="row">
        <div class="col-md-12">
            <div id="errorForm"></div>
            <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
                <form action="{{ route('Back.Download.Compatibility.update', [$asset->id, $compatibility->id]) }}" class="kt-form" id="createForm" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="PUT">
                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">Formulaire de création</h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <a href="{{ route('Back.Download.index') }}" class="btn btn-clean kt-margin-r-10">
                                <i class="la la-arrow-left"></i>
                                <span class="kt-hidden-mobile">Retour</span>
                            </a>
                            <div class="btn-group">
                                <button type="submit" class="btn btn-brand" id="btnForm">
                                    <i class="la la-check"></i>
                                    <span class="kt-hidden-mobile">Sauvegarder</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="row">
                            <div class="col-xl-3"></div>
                            <div class="col-xl-6">
                                <div class="kt-section kt-section--first">
                                    <div class="kt-section__body">
                                        @csrf
                                        <div class="form-group row">
                                            <label class="col-2 col-form-label">Build</label>
                                            <div class="col-10">
                                                <input class="form-control form-control-lg" type="text" name="trainz_build" Value="{{ $compatibility->trainz_build }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-form-label col-lg-2 col-sm-12">Compatible</label>
                                            <div class="col-lg-10 col-md-9 col-sm-12">
                                                <select id="state" class="form-control kt-selectpicker" name="state" data-live-search="true">
                                                    <option value="{{ $compatibility->state }}" data-content="{{ stateCompatibilityBadgeEdit($compatibility->state) }}"></option>
                                                    <option value="">---------------------------------</option>
                                                    <option value="0" data-content="<span class='kt-badge kt-badge--danger kt-badge--inline kt-badge--rounded'>Non Compatible</span>"></option>
                                                    <option value="1" data-content="<span class='kt-badge kt-badge--warning kt-badge--inline kt-badge--rounded'>Problème de compatibilité</span>"></option>
                                                    <option value="2" data-content="<span class='kt-badge kt-badge--success kt-badge--inline kt-badge--rounded'>Compatible</span>"></option>
                                                    <option value="3" data-content="<span class='kt-badge kt-badge--info kt-badge--inline kt-badge--rounded'>Test à effectuer</span>"></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript">
        (function ($) {
            $(".kt-selectpicker").selectpicker({
                container: 'body'
            });

            $("#createForm").on('submit', function (e) {
                e.preventDefault()
                let form = $(this)
                let url = form.attr('action')
                let btn = $("#btnForm")
                let data = form.serializeArray()

                progressBtn(btn)

                $.ajax({
                    url: url,
                    type: "PUT",
                    data: data,
                    success: function (data) {
                        unprogressBtn(btn)
                        toastr.success("La compatibilité de version <strong>" + data.data.trainz_build + "</strong> à été Editer")
                        window.setTimeout(function () {
                            window.location.href='/back/download/'+data.data.asset_id
                        }, 2000)
                    },
                    error: function (resultat, statut, erreur) {
                        unprogressBtn(btn)
                        toastr.error(resultat.responseJSON.message, "Erreur 500")
                    }
                })
            })

        })(jQuery)
    </script>
@endsection
