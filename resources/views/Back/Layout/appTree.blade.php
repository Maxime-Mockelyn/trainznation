<!DOCTYPE html>

<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4 & Angular 7
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en">

<!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>{{ env("APP_NAME") }} - Wiki</title>
    <meta name="description" content="Updates and statistics">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--begin::Fonts -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {
                "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
            },
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <!--end::Fonts -->

    <!--begin::Page Vendors Styles(used by this page) -->


    <!--end::Page Vendors Styles -->

    <!--begin:: Global Mandatory Vendors -->
    <link href="/assets/back/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />

    <!--end:: Global Mandatory Vendors -->
    <link href="/assets/back/vendors/general/tether/dist/css/tether.css" rel="stylesheet" type="text/css" />
    <link href="/assets/back/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css" rel="stylesheet" type="text/css" />
    <link href="/assets/back/vendors/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
    <link href="/assets/back/vendors/general/bootstrap-timepicker/css/bootstrap-timepicker.css" rel="stylesheet" type="text/css" />
    <link href="/assets/back/vendors/general/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
    <link href="/assets/back/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" />
    <link href="/assets/back/vendors/general/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
    <link href="/assets/back/vendors/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" type="text/css" />
    <link href="/assets/back/vendors/general/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="/assets/back/vendors/general/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css" />
    <link href="/assets/back/vendors/general/nouislider/distribute/nouislider.css" rel="stylesheet" type="text/css" />
    <link href="/assets/back/vendors/general/owl.carousel/dist/assets/owl.carousel.css" rel="stylesheet" type="text/css" />
    <link href="/assets/back/vendors/general/owl.carousel/dist/assets/owl.theme.default.css" rel="stylesheet" type="text/css" />
    <link href="/assets/back/vendors/general/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css" />
    <link href="/assets/back/vendors/general/summernote/dist/summernote.css" rel="stylesheet" type="text/css" />
    <link href="/assets/back/vendors/general/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/back/vendors/general/animate.css/animate.css" rel="stylesheet" type="text/css" />
    <link href="/assets/back/vendors/general/toastr/build/toastr.css" rel="stylesheet" type="text/css" />
    <link href="/assets/back/vendors/general/morris.js/morris.css" rel="stylesheet" type="text/css" />
    <link href="/assets/back/vendors/general/sweetalert2/dist/sweetalert2.css" rel="stylesheet" type="text/css" />
    <link href="/assets/back/vendors/general/socicon/css/socicon.css" rel="stylesheet" type="text/css" />
    <link href="/assets/back/vendors/custom/vendors/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />
    <link href="/assets/back/vendors/custom/vendors/flaticon/flaticon.css" rel="stylesheet" type="text/css" />
    <link href="/assets/back/vendors/custom/vendors/flaticon2/flaticon.css" rel="stylesheet" type="text/css" />
    <link href="/assets/back/vendors/general/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.6/styles/default.min.css">
    <!--end:: Global Optional Vendors -->

    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="/assets/back/css/demo1/style.bundle.css" rel="stylesheet" type="text/css" />

    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->
    <link href="/assets/back/css/demo1/skins/header/base/light.css" rel="stylesheet" type="text/css" />
    <link href="/assets/back/css/demo1/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
    <link href="/assets/back/css/demo1/skins/brand/dark.css" rel="stylesheet" type="text/css" />
    <link href="/assets/back/css/demo1/skins/aside/dark.css" rel="stylesheet" type="text/css" />

    @yield("css")

    <!--end::Layout Skins -->
    <link rel="shortcut icon" href="/favicon.ico" />
</head>

<!-- end::Head -->

<!-- begin::Body -->
<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

<!-- begin:: Page -->

<!-- begin:: Header Mobile -->
<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
    <div class="kt-header-mobile__logo">
        <a href="{{ route('Front.Wiki.index') }}">
            <img alt="Logo" src="/assets/back/media/logos/logo-light.png" />
        </a>
    </div>
    <div class="kt-header-mobile__toolbar">
        <button class="kt-header-mobile__toggler kt-header-mobile__toggler--left" id="kt_aside_mobile_toggler"><span></span></button>
        <button class="kt-header-mobile__toggler" id="kt_header_mobile_toggler"><span></span></button>
        <button class="kt-header-mobile__topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
    </div>
</div>

<!-- end:: Header Mobile -->
<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

        <!-- begin:: Aside -->
        <button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
        @include("Back.Include.menu2")

        <!-- end:: Aside -->
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

            <!-- begin:: Header -->
            @include("Back.Include.topnav2")

            <!-- end:: Header -->
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

                @yield("bread")

                <!-- end:: Content Head -->

                <!-- begin:: Content -->
                <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
                    @yield("content")
                    <!--End::Dashboard 1-->
                </div>

                <!-- end:: Content -->
            </div>

            @include("Back.Include.footer")

            <!-- end:: Footer -->
        </div>
    </div>
</div>

<!-- end:: Page -->

<!-- end::Quick Panel -->

<!-- begin::Scrolltop -->
<div id="kt_scrolltop" class="kt-scrolltop">
    <i class="fa fa-arrow-up"></i>
</div>

<!-- end::Scrolltop -->

<!-- end::Demo Panel -->

<!-- begin::Global Config(global config for global JS sciprts) -->
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "dark": "#282a3c",
                "light": "#ffffff",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
            }
        }
    };
</script>

<!-- end::Global Config -->

<!--begin:: Global Mandatory Vendors -->
<script src="/assets/back/vendors/general/jquery/dist/jquery.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/popper.js/dist/umd/popper.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/moment/min/moment.min.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/wnumb/wNumb.js" type="text/javascript"></script>

<!--end:: Global Mandatory Vendors -->

<script src="/assets/back/vendors/general/jquery-form/dist/jquery.form.min.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/block-ui/jquery.blockUI.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="/assets/back/vendors/custom/js/vendors/bootstrap-datepicker.init.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="/assets/back/vendors/custom/js/vendors/bootstrap-timepicker.init.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/bootstrap-maxlength/src/bootstrap-maxlength.js" type="text/javascript"></script>
<script src="/assets/back/vendors/custom/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/bootstrap-switch/dist/js/bootstrap-switch.js" type="text/javascript"></script>
<script src="/assets/back/vendors/custom/js/vendors/bootstrap-switch.init.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/select2/dist/js/select2.full.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/ion-rangeslider/js/ion.rangeSlider.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/typeahead.js/dist/typeahead.bundle.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/handlebars/dist/handlebars.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/inputmask/dist/jquery.inputmask.bundle.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/inputmask/dist/inputmask/inputmask.date.extensions.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/inputmask/dist/inputmask/inputmask.numeric.extensions.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/nouislider/distribute/nouislider.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/owl.carousel/dist/owl.carousel.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/autosize/dist/autosize.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/clipboard/dist/clipboard.min.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/dropzone/dist/dropzone.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/summernote/dist/summernote.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/markdown/lib/markdown.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
<script src="/assets/back/vendors/custom/js/vendors/bootstrap-markdown.init.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/bootstrap-notify/bootstrap-notify.min.js" type="text/javascript"></script>
<script src="/assets/back/vendors/custom/js/vendors/bootstrap-notify.init.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>
<script src="/assets/back/vendors/custom/js/vendors/jquery-validation.init.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/toastr/build/toastr.min.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/raphael/raphael.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/morris.js/morris.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>
<script src="/assets/back/vendors/custom/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js" type="text/javascript"></script>
<script src="/assets/back/vendors/custom/vendors/jquery-idletimer/idle-timer.min.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/waypoints/lib/jquery.waypoints.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/counterup/jquery.counterup.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/es6-promise-polyfill/promise.min.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
<script src="/assets/back/vendors/custom/js/vendors/sweetalert2.init.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/jquery.repeater/src/lib.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/jquery.repeater/src/jquery.input.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/jquery.repeater/src/repeater.js" type="text/javascript"></script>
<script src="/assets/back/vendors/general/dompurify/dist/purify.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.js" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.6/highlight.min.js"></script>

<!--end:: Global Optional Vendors -->

<!--begin::Global Theme Bundle(used by all pages) -->
<script src="/assets/back/js/demo1/scripts.bundle.js" type="text/javascript"></script>

<!--end::Global Theme Bundle -->

<!--begin::Global App Bundle(used by all pages) -->
<script src="/assets/back/app/bundle/app.bundle.js" type="text/javascript"></script>
<script src="/js/app.js"></script>
<script type="text/javascript">
    (function ($) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    })(jQuery)
</script>
@yield("script")

{!! Toastr::render() !!}
<!--end::Global App Bundle -->
</body>

<!-- end::Body -->
</html>
