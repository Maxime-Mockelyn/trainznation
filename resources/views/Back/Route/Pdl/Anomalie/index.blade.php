@extends("Back.Layout.app")

@section("css")
    <link href="/assets/back/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
@endsection

@section("bread")
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Route | Pays de la Loire</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('Back.dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Route </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Pays de la Loire </a>

                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
    </div>
@endsection

@section("content")

    <div class="kt-portlet">
        <div class="kt-portlet__body  kt-portlet__body--fit">
            <div class="row row-no-padding row-col-separator-xl">
                <div class="col-md-12 col-lg-6 col-xl-3">

                    <!--begin::Total Profit-->
                    <div class="kt-widget24">
                        <div class="kt-widget24__details">
                            <div class="kt-widget24__info">
                                <h4 class="kt-widget24__title">
                                    Build Actuel
                                </h4>
                                <span class="kt-widget24__desc">
															Version {{ $build->version }}
														</span>
                            </div>
                            <span class="kt-widget24__stats kt-font-brand">
														{{ $build->build }}
													</span>
                        </div>
                        <div class="progress progress--sm">
                            <div class="progress-bar {{ stateAnomalieProgress($percent) }}" role="progressbar" style="width: {{ $percent }}%;" aria-valuenow="{{ $percent }}" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <div class="kt-widget24__action">
													<span class="kt-widget24__change">
														Avancement
													</span>
                            <span class="kt-widget24__number">
														{{ $percent }}
													</span>
                        </div>
                    </div>

                    <!--end::Total Profit-->
                </div>
            </div>
        </div>
    </div>
    @if($percent >= 95)
        <div class="row">
            <div class="col-md-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__body">
                        <button class="btn btn-font-lg btn-primary" data-toggle="modal" data-target="#upNextVersion">Passer à la version {{ $build->version+1 }}</button>
                    </div>
                </div>
            </div>
        </div>
    @endif
<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand la la-table"></i>
										</span>
                    <h3 class="kt-portlet__head-title">
                        Liste des Anomalie
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="kt-portlet__head-actions">
                            <button class="btn btn-brand btn-elevate btn-icon-sm" data-toggle="modal" data-target="#addAnomalie">
                                <i class="la la-plus"></i>
                                Nouvelle Anomalie
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="tableAnomalie">
                    <thead>
                    <tr>
                        <th>Etat</th>
                        <th>Anomalie</th>
                        <th>Correction</th>
                        <th>Lieu</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($anomalies as $anomaly)
                            <tr>
                                <td class="{{ stateAnomalieBack($anomaly->state) }}">&nbsp;</td>
                                <td>
                                    @if(empty($anomaly->anomalie))
                                        <i>Néant</i>
                                    @else
                                        {{ $anomaly->anomalie }}
                                    @endif
                                </td>
                                <td>{{ $anomaly->correction }}</td>
                                <td>{{ $anomaly->lieu }}</td>
                                <td>
                                    <a href="{{ route('Route.Pdl.Anomalie.edit', $anomaly->id) }}" class="btn btn-sm btn-primary btn-icon"><i class="la la-edit"></i> </a>
                                    <button id="btnDelete" class="btn btn-sm btn-danger btn-icon" data-href="{{ route('Route.Pdl.Anomalie.delete', $anomaly->id) }}"><i class="la la-trash"></i> </button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <!--end: Datatable -->
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="addAnomalie" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nouvelle Anomalie</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form id="createForm" action="{{ route('Route.Pdl.Anomalie.store') }}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">Anomalie:</label>
                        <input type="text" class="form-control" id="recipient-name" name="anomalie">
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">Correction:</label>
                        <input type="text" class="form-control" id="recipient-name" name="correction">
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">Lieu:</label>
                        <input type="text" class="form-control" id="recipient-name" name="lieu">
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">Etat:</label>
                        <select name="state" id="recipient-name" class="form-control">
                            <option value="0">Non Commencer</option>
                            <option value="1">En cours</option>
                            <option value="2">Terminer</option>
                        </select>
                    </div>
                </div>
            
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Ajouter</button>
                </div>
            </form>
        </div>
    </div>
</div>
    <div class="modal fade" id="upNextVersion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Passage à la version {{ $build->version+1 }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <form id="createForm" action="{{ route('Route.Pdl.upVersion', $build->version+1) }}" method="post">
                    @csrf
                    <input type="hidden" name="state_37" value="1">
                    <input type="hidden" name="state_45" value="1">
                    <input type="hidden" name="state_46" value="1">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="form-control-label">Anomalie:</label>
                            <textarea name="description" id="description" cols="30" rows="10"></textarea>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Ajouter</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="/assets/back/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
    <script type="text/javascript">
        "use strict";
        var tableAnomalie = function() {

            var initTable1 = function() {
                var table = $('#tableAnomalie');

                // begin first table
                table.DataTable({
                    responsive: true,

                    // DOM Layout settings
                    dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

                    lengthMenu: [5, 10, 25, 50],

                    pageLength: 10,

                    language: {
                        "sProcessing":     "Traitement en cours...",
                        "sSearch":         "Rechercher&nbsp;:",
                        "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
                        "sInfo":           "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                        "sInfoEmpty":      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                        "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                        "sInfoPostFix":    "",
                        "sLoadingRecords": "Chargement en cours...",
                        "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                        "sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
                        "oPaginate": {
                            "sFirst":      "Premier",
                            "sPrevious":   "Pr&eacute;c&eacute;dent",
                            "sNext":       "Suivant",
                            "sLast":       "Dernier"
                        },
                        "oAria": {
                            "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
                            "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
                        },
                        "select": {
                            "rows": {
                                _: "%d lignes séléctionnées",
                                0: "Aucune ligne séléctionnée",
                                1: "1 ligne séléctionnée"
                            }
                        }
                    },

                    // Order settings
                    order: [[1, 'desc']],

                    columnDefs: [],
                });
            };

            return {

                //main function to initiate the module
                init: function() {
                    initTable1();
                },

            };

        }();

        jQuery(document).ready(function() {
            tableAnomalie.init();
        });
    </script>
    <script type="text/javascript">
        (function ($) {
            $("#description").summernote();

            $("#tableAnomalie").on('click', '#btnDelete', function (e) {
                e.preventDefault()
                let btn = $(this)
                let url = btn.attr('data-href')

                btn.addClass('kt-spinner kt-spinner--light')

                $.ajax({
                    url: url,
                    method: "DELETE",
                    statusCode: {
                        500: function(data){
                            btn.removeClass('kt-spinner kt-spinner-light')
                            toastr.error("Une Erreur serveur à eu lieu", "Erreur Serveur !")
                        },
                        200: function(data){
                            console.log(data)
                            btn.removeClass('kt-spinner kt-spinner-light')
                            toastr.success("L'anomalie à été supprimer", "Suppression d'une Anomalie")
                            btn.parents('tr').fadeOut();

                            window.setTimeout(function () {
                                window.location.href='/back/route/pdl/anomalie'
                            }, 1200)
                        }
                    }
                })
            })


        })(jQuery)
    </script>
@endsection
