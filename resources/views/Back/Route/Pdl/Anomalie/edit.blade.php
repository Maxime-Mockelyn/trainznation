@extends("Back.Layout.app")

@section("css")

@endsection

@section("bread")
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Route | Pays de la Loire</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('Back.dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Route </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Pays de la Loire </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Editer une anomalie </a>

                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
    </div>
@endsection

@section("content")
<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand la la-table"></i>
										</span>
                    <h3 class="kt-portlet__head-title">
                        Editer une anomalie
                    </h3>
                </div>
            </div>
                <div class="kt-portlet__body">
                    <form action="{{ route('Route.Pdl.Anomalie.update', $anomaly->id) }}" method="POST">
                        @csrf
                        <input type="hidden" name="_method" value="PUT">
                        <!--begin: Datatable -->

                        <div class="form-group">
                            <label for="recipient-name" class="form-control-label">Anomalie:</label>
                            <input type="text" class="form-control" id="recipient-name" name="anomalie" value="{{ $anomaly->anomalie }}">
                        </div>

                        <div class="form-group">
                            <label for="recipient-name" class="form-control-label">Correction:</label>
                            <input type="text" class="form-control" id="recipient-name" name="correction" value="{{ $anomaly->correction }}">
                        </div>

                        <div class="form-group">
                            <label for="recipient-name" class="form-control-label">Lieu:</label>
                            <input type="text" class="form-control" id="recipient-name" name="lieu" value="{{ $anomaly->lieu }}">
                        </div>

                        <div class="form-group">
                            <label for="recipient-name" class="form-control-label">Etat:</label>
                            <select name="state" id="recipient-name" class="form-control">
                                <option value="{{ $anomaly->state }}">
                                    @if($anomaly->state == 0)
                                        Non Commencer
                                    @elseif($anomaly->state == 1)
                                        En cours
                                    @elseif($anomaly->state == 2)
                                        Terminer
                                    @endif
                                </option>
                                <option value="0">Non Commencer</option>
                                <option value="1">En cours</option>
                                <option value="2">Terminer</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Editer</button>
                    </form>
                    <!--end: Datatable -->
                </div>
        </div>
    </div>
</div>

@endsection

@section("script")

@endsection
