@extends("Back.Layout.app")

@section("css")
    <link href="/assets/back/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
@endsection

@section("bread")
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Route | Pays de la Loire</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('Back.dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Route </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Pays de la Loire </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Téléchargement </a>

                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
    </div>
@endsection

@section("content")
<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand la la-table"></i>
										</span>
                    <h3 class="kt-portlet__head-title">
                        Liste des téléchargement
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="kt-portlet__head-actions">
                            <a class="btn btn-brand btn-elevate btn-icon-sm" href="{{ route('Route.Pdl.Download.create') }}">
                                <i class="la la-plus"></i>
                                Nouveau téléchargement
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="tableDownload">
                    <thead>
                    <tr>
                        <th>Type</th>
                        <th>Version</th>
                        <th>Build</th>
                        <th>Info</th>
                        <th>Etat</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($downloads as $download)
                            <tr>
                                <td><img src="{{ \App\HelperClass\PdlDownload::formatTypeDownload($download->typeDownload) }}" alt="" width="100"></td>
                                <td>{{ $download->version }}</td>
                                <td>{{ $download->build }}</td>
                                <td>
                                    Pays de la Loire <i>({{ \App\HelperClass\PdlDownload::formatReleaseDownload($download->typeRelease) }} {{ $download->version }}:{{ $download->build }})</i>
                                </td>
                                <td>{!! \App\HelperClass\PdlDownload::formatPublishDownload($download->published) !!}</td>
                                <td>
                                    @if($download->published == 0)
                                        <a href="{{ route('Route.Pdl.Download.activate', $download->id) }}" class="btn btn-success btn-icon" data-toggle="kt-tooltip" title="Publier le téléchargement"><i class="la la-check"></i> </a>
                                    @else
                                        <a href="{{ route('Route.Pdl.Download.desactivate', $download->id) }}" class="btn btn-danger btn-icon" data-toggle="kt-tooltip" title="Désactiver le téléchargement"><i class="la la-times"></i> </a>
                                    @endif
                                        <a href="{{ route('Route.Pdl.Download.delete', $download->id) }}" class="btn btn-danger btn-icon" data-toggle="kt-tooltip" title="Supprimer le téléchargement"><i class="la la-trash-o"></i> </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <!--end: Datatable -->
            </div>
        </div>
    </div>
</div>
@endsection

@section("script")
    <script src="/assets/back/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
    <script type="text/javascript">
        "use strict";
        var tableAnomalie = function() {

            var initTable1 = function() {
                var table = $('#tableDownload');

                // begin first table
                table.DataTable({
                    responsive: true,

                    // DOM Layout settings
                    dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

                    lengthMenu: [5, 10, 25, 50],

                    pageLength: 10,

                    language: {
                        "sProcessing":     "Traitement en cours...",
                        "sSearch":         "Rechercher&nbsp;:",
                        "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
                        "sInfo":           "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                        "sInfoEmpty":      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                        "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                        "sInfoPostFix":    "",
                        "sLoadingRecords": "Chargement en cours...",
                        "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                        "sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
                        "oPaginate": {
                            "sFirst":      "Premier",
                            "sPrevious":   "Pr&eacute;c&eacute;dent",
                            "sNext":       "Suivant",
                            "sLast":       "Dernier"
                        },
                        "oAria": {
                            "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
                            "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
                        },
                        "select": {
                            "rows": {
                                _: "%d lignes séléctionnées",
                                0: "Aucune ligne séléctionnée",
                                1: "1 ligne séléctionnée"
                            }
                        }
                    },

                    // Order settings
                    order: [[1, 'desc']],

                    columnDefs: [],
                });
            };

            return {

                //main function to initiate the module
                init: function() {
                    initTable1();
                },

            };

        }();

        jQuery(document).ready(function() {
            tableAnomalie.init();
        });
    </script>
    <script type="text/javascript">
        (function ($) {



        })(jQuery)
    </script>
@endsection
