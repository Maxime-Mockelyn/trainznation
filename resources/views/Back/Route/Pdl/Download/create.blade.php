@extends("Back.Layout.app")

@section("css")
    <link rel="stylesheet" href="/assets/custom/vendors/editor-md/css/editormd.css">
@endsection

@section("bread")
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Route | Pays de la Loire</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('Back.dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Route </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Pays de la Loire </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Téléchargement </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Ajout d'un téléchargement </a>

                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
    </div>
@endsection

@section("content")
    <form action="{{ route('Route.Pdl.Download.store') }}" method="POST">
        @csrf
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="flaticon2-plus"></i>
            </span>
                    <h3 class="kt-portlet__head-title">
                        Nouveau Téléchargement
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="form-group">
                    <label>Version</label>
                    <input type="text" name="version" class="form-control form-control-lg" placeholder="Version de la map">
                </div>
                <div class="form-group">
                    <label>Build</label>
                    <input type="text" name="build" class="form-control form-control-lg" placeholder="Build de la map">
                </div>
                <div class="form-group">
                    <label>Type de Téléchargement</label>
                    <select class="form-control" name="typeDownload">
                        <option value="1">Map</option>
                        <option value="2">Dépendance</option>
                        <option value="3">Session</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Type de Release</label>
                    <select class="form-control" name="typeRelease">
                        <option value="0">Correctif</option>
                        <option value="1">Alpha</option>
                        <option value="2">Beta</option>
                        <option value="3">RC</option>
                        <option value="4">Final</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Lien</label>
                    <input type="text" name="linkDownload" class="form-control form-control-lg" placeholder="Lien du téléchargement">
                </div>
                <div class="form-group">
                    <div id="fieldDesc">
                        <textarea style="display:none;" name="note"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="kt-checkbox-inline">
                        <label class="kt-checkbox">
                            <input type="checkbox" name="published" value="1"> Publier le téléchargement
                            <span></span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__foot">
                <div class="row align-items-center">
                    <div class="col-lg-12 kt-align-right">
                        <button type="submit" class="btn btn-brand">Valider</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section("script")
    <script src="/assets/custom/vendors/editor-md/editormd.js"></script>
    <script src="/assets/custom/vendors/editor-md/languages/en.js"></script>
    <script type="text/javascript">
        (function($){
            let editor = editormd("fieldDesc", {
                width: '100%',
                height: '460px',
                path:"/assets/custom/vendors/editor-md/lib/"
            })
        })(jQuery)
    </script>
@endsection
