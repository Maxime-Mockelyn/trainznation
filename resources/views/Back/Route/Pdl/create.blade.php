@extends("Back.Layout.app")

@section("css")
    <link rel="stylesheet" href="/assets/custom/vendors/editor-md/css/editormd.css">
@endsection

@section("bread")
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Route | Pays de la Loire</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('Back.dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Route </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Pays de la Loire </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Création de contenue </a>

                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
    </div>
@endsection

@section("content")
    <form action="{{ route('Back.Route.Pdl.store') }}" class="kt-form" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="kt-portlet">
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="kt-portlet">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">
                                        Création du contenu
                                    </h3>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div class="form-group">
                                    <label>Titre</label>
                                    <input type="text" name="title" class="form-control form-control-lg" placeholder="Titre du contenu">
                                </div>
                                <div class="form-group">
                                    <div id="fieldDesc">
                                        <textarea style="display:none;" name="contents"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="kt-portlet">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">
                                        Image & Disposition
                                    </h3>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div class="form-group">
                                    <label>Disposition</label>
                                    <div class="kt-radio-list">
                                        <label class="kt-radio kt-radio--solid">
                                            <input type="radio" name="disposition" value="left" checked> Gauche
                                            <span></span>
                                        </label>
                                        <label class="kt-radio kt-radio--solid">
                                            <input type="radio" name="disposition" value="right"> Droite
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Image</label>
                                    <input type="file" class="form-control" name="images">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__foot">
                <div class="row align-items-center">
                    <div class="col-lg-12 kt-align-center">
                        <button type="submit" class="btn btn-success"><i class="la la-check-circle"></i> Soumettre</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section("script")
    <script src="/assets/custom/vendors/editor-md/editormd.js"></script>
    <script src="/assets/custom/vendors/editor-md/languages/en.js"></script>
<script type="text/javascript">
    (function($){
        let editor = editormd("fieldDesc", {
            width: '100%',
            height: '460px',
            path:"/assets/custom/vendors/editor-md/lib/"
        })
    })(jQuery)
</script>
@endsection
