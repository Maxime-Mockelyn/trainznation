@extends("Back.Layout.app")

@section("css")
    <link href="/assets/back/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
@endsection

@section("bread")
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Route | Pays de la Loire</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('Back.dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Route </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Pays de la Loire </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Gallerie </a>

                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="#" class="btn kt-subheader__btn-daterange" id="kt_dashboard_daterangepicker" data-toggle="kt-tooltip" title="" data-placement="left" data-original-title="Select dashboard daterange">
                    <span class="kt-subheader__btn-daterange-title" id="kt_dashboard_daterangepicker_title">Today</span>&nbsp;

                </a>
            </div>
        </div>
    </div>
@endsection

@section("content")

<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand la la-table"></i>
										</span>
                    <h3 class="kt-portlet__head-title">
                        Liste des Catégorie
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="kt-portlet__head-actions">
                            <button class="btn btn-brand btn-elevate btn-icon-sm" data-toggle="modal" data-target="#addCategorie">
                                <i class="la la-plus"></i>
                                Nouvelle Catégorie
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="tableCategorie">
                    <thead>
                    <tr>
                        <th>Désignation</th>
                        <th>Nombre d'image</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td>{{ $category->name }}</td>
                                <td class="text-center"><span class="kt-badge kt-badge--brand kt-badge--lg">{{ \App\Repository\Route\PdlGalleryRepository::countPictFromCategory($category->id) }}</span></td>
                                <td>
                                    <a href="{{ route('Pdl.Gallery.show', $category->id) }}" class="btn btn-icon btn-sm btn-primary"><i class="la la-eye"></i> </a>
                                    <button id="btnDelete" data-href="{{ route('Route.Pdl.Gallery.delete', $category->id) }}" class="btn btn-icon btn-sm btn-danger"><i class="la la-times-circle"></i> </button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <!--end: Datatable -->
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="addCategorie" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nouvelle Anomalie</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form id="createForm" action="{{ route('Route.Pdl.Gallery.store') }}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">Nom de la catégorie:</label>
                        <input type="text" class="form-control" id="recipient-name" name="name">
                    </div>
                </div>
            
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Ajouter</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section("script")
    <script src="/assets/back/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
    <script type="text/javascript">
        "use strict";
        var tableCategorie = function() {

            var initTable1 = function() {
                var table = $('#tableCategorie');

                // begin first table
                table.DataTable({
                    responsive: true,

                    // DOM Layout settings
                    dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

                    lengthMenu: [5, 10, 25, 50],

                    pageLength: 10,

                    language: {
                        "sProcessing":     "Traitement en cours...",
                        "sSearch":         "Rechercher&nbsp;:",
                        "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
                        "sInfo":           "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                        "sInfoEmpty":      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                        "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                        "sInfoPostFix":    "",
                        "sLoadingRecords": "Chargement en cours...",
                        "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                        "sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
                        "oPaginate": {
                            "sFirst":      "Premier",
                            "sPrevious":   "Pr&eacute;c&eacute;dent",
                            "sNext":       "Suivant",
                            "sLast":       "Dernier"
                        },
                        "oAria": {
                            "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
                            "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
                        },
                        "select": {
                            "rows": {
                                _: "%d lignes séléctionnées",
                                0: "Aucune ligne séléctionnée",
                                1: "1 ligne séléctionnée"
                            }
                        }
                    },

                    // Order settings
                    order: [[1, 'desc']],

                    columnDefs: [],
                });
            };

            return {

                //main function to initiate the module
                init: function() {
                    initTable1();
                },

            };

        }();

        jQuery(document).ready(function() {
            tableCategorie.init();
        });
    </script>
    <script type="text/javascript">
        (function ($) {
            $("#description").summernote();

            $("#tableCategorie").on('click', '#btnDelete', function (e) {
                e.preventDefault()
                let btn = $(this)
                let url = btn.attr('data-href')

                btn.addClass('kt-spinner kt-spinner--light')

                $.ajax({
                    url: url,
                    method: "DELETE",
                    statusCode: {
                        500: function(data){
                            btn.removeClass('kt-spinner kt-spinner-light')
                            toastr.error("Une Erreur serveur à eu lieu", "Erreur Serveur !")
                        },
                        200: function(data){
                            console.log(data)
                            btn.removeClass('kt-spinner kt-spinner-light')
                            toastr.success("La catégorie à été supprimer", "Suppression d'une catégorie")
                            btn.parents('tr').fadeOut();

                        }
                    }
                })
            })


        })(jQuery)
    </script>
@endsection
