@extends("Back.Layout.app")

@section("css")
    <style type="text/css">
        .ck-editor__editable {
            min-height: 500px;
        }
    </style>
@endsection

@section("bread")
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Route | Pays de la Loire</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('Back.dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Route </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Pays de la Loire </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Edition de contenue </a>

                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
    </div>
@endsection

@section("content")
    <form action="{{ route('Back.Route.Pdl.update', $pdl->id) }}" class="kt-form" method="post">
        @csrf
        @method("PUT")
        <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
            <div class="kt-portlet__head kt-portlet__head--lg" style="">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">Edition d'un contenue <small>Ligne Pays de la Loire</small></h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <a href="{{ route('Back.Route.Pdl.index') }}" class="btn btn-clean kt-margin-r-10">
                        <i class="la la-arrow-left"></i>
                        <span class="kt-hidden-mobile">Retour</span>
                    </a>
                    <div class="btn-group">
                        <button type="submit" class="btn btn-brand">
                            <i class="la la-check"></i>
                            <span class="kt-hidden-mobile">Valider</span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">
                    <div class="row">
                        <div class="col-xl-2"></div>
                        <div class="col-xl-8">
                            <div class="kt-section kt-section--first">
                                <div class="kt-section__body">
                                    <div class="form-group row">
                                        <label class="col-3 col-form-label">Titre</label>
                                        <div class="col-9">
                                            <input class="form-control" type="text" name="title" value="{{ $pdl->title }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 col-form-label">Contenue</label>
                                        <div class="col-9">
                                            <textarea class="form-control" name="contents" id="content" cols="30" rows="10">{{ $pdl->content }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 col-form-label">Disposition</label>
                                        <div class="col-9">
                                            <div class="kt-radio-list">
                                                <label class="kt-radio">
                                                    <input type="radio" name="disposition" value="right" @if($pdl->right == 1) checked @endif> Droite
                                                    <span></span>
                                                </label>
                                                <label class="kt-radio">
                                                    <input type="radio" name="disposition" value="left" @if($pdl->left == 1) checked @endif> Gauche
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-2"></div>
                    </div>
            </div>
        </div>
    </form>
@endsection

@section("script")
    <script src="/assets/custom/vendors/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
    (function($){
        createCkeditor("#content")
    })(jQuery)
</script>
@endsection
