@extends("Back.Layout.app")

@section("css")
    <style type="text/css">
        .ck-editor__editable {
            min-height: 500px;
        }
    </style>
    <link rel="stylesheet" href="/assets/custom/vendors/editor-md/css/editormd.css">
@endsection

@section("bread")
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Notification</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('Back.dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Notification </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Création d'une notification </a>

                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
    </div>
@endsection

@section("content")
    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('Back.Notification.store') }}" class="kt-form" method="POST">
                @csrf
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Informations <strong>Générales</strong>
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Titre de la notification</label>
                                    <input type="text" id="title" class="form-control" placeholder="Titre de la notification" name="title">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Lien de la notification</label>
                                    <input type="text" id="title" class="form-control" placeholder="Format: /sector/content" name="link">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Contenue de la notification</label>
                                    <textarea class="form-control" name="contents"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Utilisateur à notifier</label>
                                    <select class="form-control kt-selectpicker" name="user_id">
                                        @foreach($users as $user)
                                            <option value="{{ $user->id }}" data-content="<div class='row'><div class='col-md-1'>@if($user->account->avatar == 1) @if($user->id == 1) <img src='/storage/avatar/trainznation.png' width='24'/> @else <img src='/storage/avatar/{{ $user->id }}.png' width='24'/> @endif @else <img src='/storage/avatar/placeholder.png' width='24'/> @endif</div><div class='col-md-11 kt-align-middle'>{{ $user->name }}</div></div>">{{ $user->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Images de la notification</label>
                                    <div class="row">
                                        @foreach($images as $image)
                                            <div class="col-md-2 col-sm-12">
                                                <label class="kt-option">
																<span class="kt-option__control">
																	<span class="kt-radio kt-radio--check-bold">
																		<input type="radio" name="icons" value="{{ $image['id'] }}" checked>
																		<span></span>
																	</span>
																</span>
                                                    <span class="kt-option__label">
																	<span class="kt-option__head">
																		<span class="kt-option__title">
																			{{ $image['text'] }}
																		</span>
																	</span>
																	<span class="kt-option__body">
																		<img src="{{ sourceImage($image['icon']) }}"
                                                                             alt="{{ $image['text'] }}"
                                                                             width="80" />
																	</span>
																</span>
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br><br>
                        <div class="row">
                            <div class="col-md-4 kt-align-center form-group">
                                <div class="kt-checkbox-inline">
                                    <label class="kt-checkbox">
                                        <input type="checkbox" name="state" value="1"> Notifier tous les utilisateurs
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="row align-items-center">
                            <div class="col-lg-6 kt-align-left">
                                <button type="submit" class="btn btn-brand"><i class="fa fa-check-circle"></i> Notifier</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="/assets/back/vendors/general/bootstrap-datetime-picker/js/locales/bootstrap-datetimepicker.fr.js"></script>
    <script src="/assets/custom/vendors/ckeditor/ckeditor.js"></script>
    <script src="/assets/custom/vendors/editor-md/editormd.js"></script>
    <script src="/assets/custom/vendors/editor-md/languages/en.js"></script>
    <script type="text/javascript">
        $("#twitterText").hide()
        $("#datePubUlte").hide()
        $("#datePubAnt").hide()

        function twitterCheck()
        {
            if($('input[name=twitter]').is(':checked')){
                $("#twitterText").show()
            }else{
                $("#twitterText").hide()
            }
        }

        function sluggify(str)
        {
            str = str.replace(/^\s+|\s+$/g, ''); // trim
            str = str.toLowerCase();

            // remove accents, swap ñ for n, etc
            let from = "àáãäâèéëêìíïîòóöôùúüûñç·/_,:;";
            let to   = "aaaaaeeeeiiiioooouuuunc------";

            for (var i=0, l=from.length ; i<l ; i++) {
                str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
            }

            str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
                .replace(/\s+/g, '-') // collapse whitespace and replace by -
                .replace(/-+/g, '-'); // collapse dashes

            return str;

        }

        function getSlug()
        {
            let title = $("#title").val()
            let slug = $("#slug")

            let string = sluggify(title);

            slug.val(string)
        }

        function pubUltiCheck()
        {
            if($('input[name=pubUlti]').is(':checked')){
                $("#datePubUlte").show()
            }else{
                $("#datePubUlte").hide()
            }
        }

        function pubAntCheck()
        {
            if($('input[name=pubAnt]').is(':checked')){
                $("#datePubAnt").show()
            }else{
                $("#datePubAnt").hide()
            }
        }

        function loadSubCategorie()
        {
            let div = $("#subcategories")
            let categorie = $("#categorie_id").val();

            $.ajax({
                url: "/back/tutoriel/listSubCategorie/"+categorie,
                method: "GET",
                success: function (data) {
                    div.html(data.content)
                }
            })
        }

        (function ($) {
            $(".kt-selectpicker").selectpicker({
                container: 'body'
            });
            //createCkeditor("#description")
            let editor = editormd("fieldDesc", {
                width: '100%',
                height: '460px',
                path:"/assets/custom/vendors/editor-md/lib/"
            })
            $(".datetimepicker").datetimepicker({
                todayHighlight: true,
                autoclose: true,
                pickerPosition: 'bottom-left',
                format: 'yyyy-mm-dd hh:ii:00',
                language: 'fr'
            })

            $('#twitterText').maxlength({
                threshold: 5,
                warningClass: "kt-badge kt-badge--primary kt-badge--rounded kt-badge--inline",
                limitReachedClass: "kt-badge kt-badge--brand kt-badge--rounded kt-badge--inline"
            });
        })(jQuery)
    </script>
@endsection
