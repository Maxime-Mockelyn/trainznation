@extends("Back.Layout.app")

@section("css")
    <link href="/assets/back/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
@endsection

@section("bread")
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Wiki | Tableau de Bord</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('Back.dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Wiki </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Tableau de Bord </a>

                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
    </div>
@endsection

@section("content")
<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand la la-table"></i>
										</span>
                    <h3 class="kt-portlet__head-title">
                        Liste des Catégories
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="kt-portlet__head-actions">
                            <a href="#addCategory" data-toggle="modal" class="btn btn-brand btn-elevate btn-icon-sm">
                                <i class="la la-plus"></i>
                                Nouvelle catégorie
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="tableCategorie">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Libellée</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td>{{ $category->id }}</td>
                                <td>{{ $category->name }}</td>
                                <td>
                                    <a href="{{ route('Back.Wiki.Category.edit', $category->id) }}" class="btn btn-sm btn-brand btn-elevate btn-icon"><i class="la la-edit"></i></a>
                                    <button id="btnDelete" type="button" class="btn btn-sm btn-danger btn-elevate btn-icon" data-href="{{ route('Back.Wiki.Category.delete', $category->id) }}"><i class="la la-trash"></i></button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <!--end: Datatable -->
            </div>
        </div>

        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand la la-table"></i>
										</span>
                    <h3 class="kt-portlet__head-title">
                        Liste des Articles
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="kt-portlet__head-actions">
                            <a href="{{ route('Back.Wiki.create') }}" class="btn btn-brand btn-elevate btn-icon-sm text-white">
                                <i class="la la-plus"></i>
                                Nouvelle Article
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="tableArticle">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Titre</th>
                        <th>Catégorie</th>
                        <th>Publier</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($articles as $article)
                        <tr>
                            <td>{{ $article->id }}</td>
                            <td>
                                {{ $article->title }}<br>
                                <i>{{ $article->user->name }}</i>
                            </td>
                            <td>{{ $article->category->name }}</td>
                            <td>
                                @if($article->published == 0)
                                    <span class="kt-badge kt-badge--inline kt-badge--danger">Non Publier</span>
                                @else
                                    <span class="kt-badge kt-badge--inline kt-badge--success">Publier</span><br>
                                    @if($article->published_at >= now()->format('Y-m-d 00:00:00') && $article->published_at <= now()->format("Y-m-d 23:59:59"))
                                        <i>Publier {{ $article->published_at->diffForHumans() }}</i>
                                    @else
                                        <i>Publier le {{ $article->published_at->format('d/m/Y à H:i') }}</i>
                                    @endif
                                @endif
                            </td>
                            <td>
                                @if($article->published == 0)
                                    <button id="btnPublish" data-href="{{ route('Back.Wiki.publish', $article->id) }}" class="btn btn-sm btn-success btn-elevate btn-icon" data-toggle="kt-tooltip" title="Publier le wiki"><i class="la la-unlock"></i> </button>
                                @else
                                    <button id="btnUnpublish" data-href="{{ route('Back.Wiki.unpublish', $article->id) }}" class="btn btn-sm btn-danger btn-elevate btn-icon" data-toggle="kt-tooltip" title="Dépublier le wiki"><i class="la la-lock"></i> </button>
                                @endif
                                    <a id="btnShow" href="{{ route('Back.Wiki.show', $article->id) }}" class="btn btn-sm btn-brand btn-elevate btn-icon" data-toggle="kt-tooltip" title="Voir le wiki"><i class="la la-eye"></i> </a>
                                @if($article->published == 0)
                                    <a id="btnEdit" href="{{ route('Back.Wiki.edit', $article->id) }}" class="btn btn-sm btn-info btn-elevate btn-icon" data-toggle="kt-tooltip" title="Editer le wiki"><i class="la la-edit"></i> </a>
                                    <button id="btnDelete" data-href="{{ route('Back.Wiki.delete', $article->id) }}" class="btn btn-sm btn-danger btn-elevate btn-icon" data-toggle="kt-tooltip" title="Supprimer le wiki"><i class="la la-trash"></i> </button>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <!--end: Datatable -->
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nouvelle catégorie</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form action="{{ route('Back.Wiki.Category.store') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">Nom de la catégorie:</label>
                        <input type="text" class="form-control" id="recipient-name" name="name">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btnFormAddCategory" class="btn btn-primary">Créer la catégorie</button>
                </div>
            </form>

        </div>
    </div>
</div>
@endsection

@section("script")
    <script src="/assets/back/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
    <script type="text/javascript">
        "use strict";
        var tableCategorie = function() {

            var initTable1 = function() {
                var table = $('#tableCategorie');

                // begin first table
                table.DataTable({
                    responsive: true,

                    // DOM Layout settings
                    dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

                    lengthMenu: [5, 10, 25, 50],

                    pageLength: 10,

                    language: {
                        "sProcessing":     "Traitement en cours...",
                        "sSearch":         "Rechercher&nbsp;:",
                        "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
                        "sInfo":           "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                        "sInfoEmpty":      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                        "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                        "sInfoPostFix":    "",
                        "sLoadingRecords": "Chargement en cours...",
                        "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                        "sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
                        "oPaginate": {
                            "sFirst":      "Premier",
                            "sPrevious":   "Pr&eacute;c&eacute;dent",
                            "sNext":       "Suivant",
                            "sLast":       "Dernier"
                        },
                        "oAria": {
                            "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
                            "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
                        },
                        "select": {
                            "rows": {
                                _: "%d lignes séléctionnées",
                                0: "Aucune ligne séléctionnée",
                                1: "1 ligne séléctionnée"
                            }
                        }
                    },

                    // Order settings
                    order: [[1, 'desc']],

                    columnDefs: [],
                });
            };

            return {

                //main function to initiate the module
                init: function() {
                    initTable1();
                },

            };

        }();
        var tableArticle = function() {

            var initTable1 = function() {
                var table = $('#tableArticle');

                // begin first table
                table.DataTable({
                    responsive: true,

                    // DOM Layout settings
                    dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

                    lengthMenu: [5, 10, 25, 50],

                    pageLength: 10,

                    language: {
                        "sProcessing":     "Traitement en cours...",
                        "sSearch":         "Rechercher&nbsp;:",
                        "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
                        "sInfo":           "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                        "sInfoEmpty":      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                        "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                        "sInfoPostFix":    "",
                        "sLoadingRecords": "Chargement en cours...",
                        "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                        "sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
                        "oPaginate": {
                            "sFirst":      "Premier",
                            "sPrevious":   "Pr&eacute;c&eacute;dent",
                            "sNext":       "Suivant",
                            "sLast":       "Dernier"
                        },
                        "oAria": {
                            "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
                            "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
                        },
                        "select": {
                            "rows": {
                                _: "%d lignes séléctionnées",
                                0: "Aucune ligne séléctionnée",
                                1: "1 ligne séléctionnée"
                            }
                        }
                    },

                    // Order settings
                    order: [[1, 'desc']],

                    columnDefs: [],
                });
            };

            return {

                //main function to initiate the module
                init: function() {
                    initTable1();
                },

            };

        }();

        jQuery(document).ready(function() {
            tableCategorie.init();
            tableArticle.init();
        });
    </script>
    <script type="text/javascript">
        (function ($) {
            $("#tableCategorie").on('click', '#btnDelete', function (e) {
                e.preventDefault()
                let btn = $(this)
                let url = btn.attr('data-href')

                KTApp.progress(btn)

                $.ajax({
                    url: url,
                    method: "DELETE",
                    statusCode: {
                        500: function(data){
                            KTApp.unprogress(btn)
                            toastr.error("Une Erreur serveur à eu lieu", "Erreur Serveur !")
                        },
                        200: function(data){
                            KTApp.unprogress(btn)
                            toastr.success("La catégorie à été supprimer", "Suppression d'une catégorie")
                            btn.parents('tr').fadeOut();
                        }
                    }
                })
            })


            $("#tableArticle").on('click', '#btnDelete', function (e) {
                e.preventDefault()
                let btn = $(this)
                let url = btn.attr('data-href')

                KTApp.progress(btn)

                $.ajax({
                    url: url,
                    method: "DELETE",
                    statusCode: {
                        500: function(data){
                            KTApp.unprogress(btn)
                            toastr.error("Une Erreur serveur à eu lieu", "Erreur Serveur !")
                        },
                        200: function(data){
                            KTApp.unprogress(btn)
                            toastr.success("L'article à été supprimer", "Suppression d'un Article")
                            btn.parents('tr').fadeOut();
                        }
                    }
                })
            })
                .on('click', "#btnPublish", function (e) {
                    e.preventDefault()
                    let btn = $(this)
                    let url = btn.attr('data-href')

                    KTApp.progress(btn)

                    $.ajax({
                        url: url,
                        method: "get",
                        success: function (data) {
                            KTApp.unprogress(btn)

                            btn.removeClass('btn-success').addClass('btn-danger')
                            btn.attr('data-href', "/back/wiki/"+data.id+"/unpublish")
                            btn.html('<i class="la la-lock"></i>')
                            btn.attr('title', "Dépublier le wiki")
                            $("#btnEdit").hide()
                            $("#btnDelete").hide()

                            if(data.twitter === true)
                            {
                                toastr.success("L'article à été publier avec succès !", "Publication d'un article")
                                toastr.success("L'article à été publier sur twitter !", "Publication d'un article sur twitter")
                            }else{
                                toastr.success("L'article à été publier avec succès !", "Publication d'un article")
                            }
                        },
                        error: function () {
                            KTApp.unprogress(btn)
                            toastr.error("Erreur Serveur !", "Erreur 500")
                        }
                    })
                })
                .on('click', "#btnUnpublish", function (e) {
                    e.preventDefault()
                    let btn = $(this)
                    let url = btn.attr('data-href')

                    KTApp.progress(btn)

                    $.ajax({
                        url: url,
                        method: "get",
                        success: function (data) {
                            KTApp.unprogress(btn)

                            btn.removeClass('btn-danger').addClass('btn-success')
                            btn.attr('data-href', "/back/wiki/"+data.id+"/publish")
                            btn.html('<i class="la la-unlock"></i>')
                            btn.attr('title', "Publier le wiki")
                            $("#btnEdit").show()
                            $("#btnDelete").show()

                            toastr.success("L'article à été dépublier avec succès !", "Dépublication d'un article")
                        },
                        error: function () {
                            KTApp.unprogress(btn)
                            toastr.error("Erreur Serveur !", "Erreur 500")
                        }
                    })
                })


        })(jQuery)
    </script>
@endsection
