@extends("Back.Layout.app")

@section("css")

@endsection

@section("bread")
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Wiki | {{ $wiki->category->name }} | {{ $wiki->title }}</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('Back.dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Wiki </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">{{ $wiki->category->name }} </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">{{ $wiki->title }} </a>

                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                @back(["data" => ["url" => "Back.Wiki.index"]]) @endback
                @edit(["data" => ["url" => "Back.Wiki.edit", "terme" => "l'article", "id" => $wiki->id]]) @endedit
                @delete(["data" => ["url" => "Back.Wiki.getDelete", "terme" => "l'article", "id" => $wiki->id]]) @enddelete
                @if($wiki->published == 0 || $wiki->published == 2)
                    @publish(["data" => ["url" => "Back.Wiki.getPublish", "terme" => "l'article", "id" => $wiki->id]]) @endpublish
                @else
                    @unpublish(["data" => ["url" => "Back.Wiki.getUnpublish", "terme" => "l'article", "id" => $wiki->id]]) @endunpublish
                @endif
            </div>
        </div>
    </div>
@endsection

@section("content")
    <div class="row">
        <div class="col-md-3">
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <table class="table">
                        <tr>
                            <td class="font-weight-bold">Titre</td>
                            <td>{{ $wiki->title }}</td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold">Catégory</td>
                            <td>{{ $wiki->category->name }}</td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold">Etat</td>
                            <td>
                                @if($wiki->published == 0)
                                    <span class="kt-badge kt-badge--inline kt-badge--danger">Non Publier</span>
                                @else
                                    <span class="kt-badge kt-badge--inline kt-badge--success">Publier</span><br>
                                    @if($wiki->published_at >= now()->format('Y-m-d 00:00:00') && $wiki->published_at <= now()->format("Y-m-d 23:59:59"))
                                        <i>Publier {{ $wiki->published_at->diffForHumans() }}</i>
                                    @else
                                        <i>Publier le {{ $wiki->published_at->format('d/m/Y à H:i') }}</i>
                                    @endif
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon2-open-text-book"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            Contenue
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    {!! $wiki->content !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript">
        (function ($) {

        })(jQuery)
    </script>
@endsection
