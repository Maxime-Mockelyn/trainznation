@extends("Back.Layout.app")

@section("css")
    <style type="text/css">
        .ck-editor__editable {
            min-height: 500px;
        }
    </style>
    <link rel="stylesheet" href="/assets/custom/vendors/editor-md/css/editormd.css">
@endsection

@section("bread")
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Wiki | Edition d'un article</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('Back.dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Wiki </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Edition: <strong>{{ $wiki->title }}</strong> </a>

                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
    </div>
@endsection

@section("content")
    <form action="{{ route('Back.Wiki.update', $wiki->id) }}" class="kt-form" id="formAdd">
        <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
            <div class="kt-portlet__head kt-portlet__head--lg" style="">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">Edition d'un article</h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <a href="{{ route('Back.Wiki.index') }}" class="btn btn-clean kt-margin-r-10">
                        <i class="la la-arrow-left"></i>
                        <span class="kt-hidden-mobile">Retour</span>
                    </a>
                    <div class="btn-group">
                        <button id="btnFormSave" type="submit" class="btn btn-brand">
                            <i class="la la-check"></i>
                            <span class="kt-hidden-mobile">Sauvegarder</span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">

                    <div class="row">
                        <div class="col-xl-2"></div>
                        <div class="col-xl-8">
                            <div class="kt-section kt-section--first">
                                <div class="kt-section__body">
                                    <div class="form-group row">
                                        <label class="col-3 col-form-label-lg">Titre</label>
                                        <div class="col-md-9">
                                            <input class="form-control form-control-lg" type="text" name="title" value="{{ $wiki->title }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 col-form-label">Catégorie</label>
                                        <div class="col-9">
                                            <select class="form-control kt-select2" id="kt-select2" name="category_id">
                                                <option value="{{ $wiki->category->id }}">{{ $wiki->category->name }}</option>
                                                <option value="">--------------------</option>
                                                @foreach($categories as $category)
                                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <div id="fieldDesc">
                                                <textarea style="display:none;" name="contents">{{ $wiki->content }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-2"></div>
                    </div>
            </div>
        </div>
    </form>
@endsection

@section("script")
    <script src="/assets/custom/vendors/editor-md/editormd.js"></script>
    <script src="/assets/custom/vendors/editor-md/languages/en.js"></script>
    <script type="text/javascript">
        (function ($) {
            $(".kt-select2").select2({
                placeholder: "Selectionner une catégorie",
                container: 'body'
            })

            $("#formAdd").on('submit', function (e) {
                e.preventDefault()
                let form = $(this)
                let url = form.attr('action')
                let btn = $("#btnFormSave")
                let data = form.serializeArray()

                KTApp.progress(btn)

                $.ajax({
                    url: url,
                    method: "PUT",
                    data: data,
                    success: function (data) {
                        KTApp.unprogress(btn)
                        toastr.success("L'article <strong>"+ data.title +"</strong> à été Editer", "Edition d'un wiki")
                        setTimeout(function () {
                            window.location.href='/back/wiki/'
                        }, 1500)
                    },
                    error: function (response) {
                        KTApp.unprogress(btn)
                        toastr.error("Erreur serveur", "Erreur 500")
                    },
                    statusCode: {
                        422: function (responseJson) {
                            toastr.error("Certains Champs sont requis", "Champs requis", {
                                positionClass: "toast-top-full-width",
                            })
                        }
                    }
                })
            })
            let editor = editormd("fieldDesc", {
                width: '100%',
                height: '460px',
                path:"/assets/custom/vendors/editor-md/lib/"
            })
        })(jQuery)
    </script>
@endsection
