@extends("Back.Layout.app")

@section("css")

@endsection

@section("bread")
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Suggestions | Tableau de Bord</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('Back.dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Suggestions </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Tableau de Bord </a>

                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
    </div>
@endsection

@section("content")
<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="flaticon2-graph"></i>
            </span>
            <h3 class="kt-portlet__head-title">
                Liste des Suggestions
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">

        <!--begin: Search Form -->
        <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
            <div class="row align-items-center">
                <div class="col-xl-8 order-2 order-xl-1">
                    <div class="row align-items-center">
                        <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                            <div class="kt-input-icon kt-input-icon--left">
                                <input type="text" class="form-control" placeholder="Recherche..." id="generalSearch">
                                <span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
                            </div>
                        </div>
                        <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                            <div class="kt-form__group kt-form__group--inline">
                                <div class="kt-form__label">
                                    <label>Status:</label>
                                </div>
                                <div class="kt-form__control">
                                    <select class="form-control bootstrap-select" id="kt_form_state">
                                        <option value="">All</option>
                                        <option value="0">Soumis</option>
                                        <option value="1">Valider</option>
                                        <option value="2">Rejeter</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                            <div class="kt-form__group kt-form__group--inline">
                                <div class="kt-form__label">
                                    <label>Type:</label>
                                </div>
                                <div class="kt-form__control">
                                    <select class="form-control bootstrap-select" id="kt_form_type">
                                        <option value="">All</option>
                                        <option value="0">Matériel</option>
                                        <option value="1">Route</option>
                                        <option value="2">Autre</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!--end: Search Form -->
    </div>
    <div class="kt-portlet__body kt-portlet__body--fit">

        <!--begin: Datatable -->
        <table class="kt-datatable" id="html_table" width="100%">
            <thead>
                <tr>
                    <th title="Field #1">#</th>
                    <th title="Field #2">Date</th>
                    <th title="Field #3">Type</th>
                    <th title="Field #4">Sujet</th>
                    <th title="Field #5">Auteur</th>
                    <th title="Field #6">Etat</th>
                    <th title="Field #7">Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($suggestions as $suggestion)
                <tr>
                    <td>{{ $suggestion->id }}</td>
                    <td>{{ $suggestion->created_at->format("d/m/Y H:i") }}</td>
                    <td>
                        <img src="{{ \App\HelperClass\Suggestion::typeImageSuggestion($suggestion->type) }}"
                             alt="{{ \App\HelperClass\Suggestion::typeSuggestion($suggestion->type) }}"
                             data-toggle="kt-tooltip" data-title="{{ \App\HelperClass\Suggestion::typeSuggestion($suggestion->type) }}"
                             width="100"
                        />
                    </td>
                    <td>{{ $suggestion->sujet }}</td>
                    <td>{{ $suggestion->name }}</td>
                    <td>{!! \App\HelperClass\Suggestion::stateBackSuggestion($suggestion->state) !!}</td>
                    <td>
                        <a href="" class="btn btn-light btn-elevate-hover btn-circle btn-icon"><i class="la la-eye"></i> </a>
                        <a href="" class="btn btn-light btn-elevate-hover btn-circle btn-icon"><i class="la la-edit"></i> </a>
                        <a href="" class="btn btn-danger btn-elevate-hover btn-circle btn-icon"><i class="la la-trash-o"></i> </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <!--end: Datatable -->
    </div>
</div>
@endsection

@section("script")
    <script type="text/javascript">
        let KTDatatableHtmlTableDemo = function() {
            // Private functions

            // demo initializer
            let demo = function() {

                let datatable = $('.kt-datatable').KTDatatable({
                    data: {
                        saveState: {cookie: false},
                    },
                    search: {
                        input: $('#generalSearch'),
                    },
                    columns: [
                        {
                            field: '#',
                            title: "ID",
                            sortable: false
                        },
                        {
                            field: "date",
                            title: "Date",
                            sortable: true
                        },
                        {
                            field: "type",
                            title: "Type",
                            template: function (row) {
                                let type = {
                                    0: {'title': 'Materiel', 'class': '/storage/other/icons/train.png'},
                                    1: {'title': 'Route', 'class': '/storage/other/icons/road.png'},
                                    2: {'title': 'Autre', 'class': '/storage/other/icons/other.png'},
                                };
                                return '<img src="'+type[row.Type].class+'" alt="'+type[row.Type].title+'" width="100" data-toggle="kt-tooltip" data-original-title="'+type[row.Type].title+'"/>'
                            },
                        },
                        {
                            field: "sujet",
                            title: "Sujet",
                            sortable: false
                        },
                        {
                            field: "auteur",
                            title: "Auteur",
                            sortable: true
                        },
                        {
                            field: "state",
                            title: "State",
                            template: function (row) {
                                let status = {
                                    0: {'title': 'Soumis', 'class': 'kt-badge--brand'},
                                    1: {'title': 'Valider', 'class': 'kt-badge--success'},
                                    2: {'title': 'Rejeter', 'class': 'kt-badge--danger'},
                                };
                                return '<span class="kt-badge ' + status[row.State].class + ' kt-badge--inline kt-badge--pill">' + status[row.State].title + '</span>';
                            },
                        },
                    ],
                });

                $('#kt_form_state').on('change', function() {
                    datatable.search($(this).val().toLowerCase(), 'state');
                });

                $('#kt_form_type').on('change', function() {
                    datatable.search($(this).val().toLowerCase(), 'type');
                });

                $('#kt_form_state,#kt_form_type').selectpicker();

            };

            return {
                // Public functions
                init: function() {
                    // init dmeo
                    demo();
                },
            };
        }();

        jQuery(document).ready(function() {
            KTDatatableHtmlTableDemo.init();
        });
    </script>
@endsection
