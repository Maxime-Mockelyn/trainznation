@extends("Back.Layout.app")

@section("css")

@endsection

@section("bread")
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Suggestions | {{ $suggestion->sujet }}</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('Back.dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Suggestions </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">{{ $suggestion->sujet }} </a>

                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
    </div>
@endsection

@section("content")
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="kt-widget kt-widget--user-profile-3">
                <div class="kt-widget__top">
                    <div class="kt-widget__media kt-hidden-">
                        <img src="{{ \App\HelperClass\Suggestion::typeImageSuggestion($suggestion->type) }}" alt="image">
                    </div>
                    <div class="kt-widget__content">
                        <div class="kt-widget__head">
                            <a href="#" class="kt-widget__username kt-hidden-">
                                {{ $suggestion->sujet }}
                                {!! \App\HelperClass\Suggestion::stateSuggestion($suggestion->state, 'back') !!}
                            </a>

                            <a href="#" class="kt-widget__title kt-hidden">Nexa - Next generation SAAS</a>

                            <div class="kt-widget__action">
                                @if($suggestion->state == 0)
                                    <a href="#suggestAccept" data-toggle="modal" class="btn btn-sm btn-success btn-upper btn--icon"><i class="flaticon2-correct"></i> Accepter</a>
                                    <a href="{{ route('Back.Suggestion.rejected', $suggestion->id) }}" class="btn btn-sm btn-danger btn-upper btn--icon"><i class="flaticon2-close-cross"></i> Refuser</a>
                                @endif
                            </div>
                        </div>

                        <div class="kt-widget__subhead kt-hidden-">
                            <a href="#"><i class="flaticon2-user-outline-symbol"></i>{{ $suggestion->name }}</a>
                        </div>

                        <div class="kt-widget__info">
                            <div class="kt-widget__desc">
                                {{ $suggestion->suggestion }}
                            </div>

                            <div class="kt-widget__stats d-flex align-items-center flex-fill">
                                <div class="kt-widget__item">
                                <span class="kt-widget__date">
                                    Date de création
                                </span>
                                    <div class="kt-widget__label">
                                        <span class="btn btn-label-brand btn-sm btn-bold btn-upper">{{ $suggestion->created_at->format("d/m/Y à H:i") }}</span>
                                    </div>
                                </div>

                                <div class="kt-widget__item">
                                <span class="kt-widget__date">
                                    Date de Mise à jour
                                </span>
                                    <div class="kt-widget__label">
                                        <span class="btn btn-label-danger btn-sm btn-bold btn-upper">{{ $suggestion->updated_at->format("d/m/Y à H:i") }}</span>
                                    </div>
                                </div>

                                <div class="kt-widget__item flex-fill">
                                    <span class="kt-widget__subtitel">Progression</span>
                                    {!! \App\HelperClass\Suggestion::stateBackProgress($suggestion->state) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="flaticon2-graph"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    Timeline de la requete
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-xl-3"></div>
                <div class="col-xl-6">
                    <div class="kt-timeline-v1 kt-timeline-v1--justified">
                        <div class="kt-timeline-v1__items">
                            @if(\App\HelperClass\Suggestion::countTimelineForSuggestion($suggestion->id) != 0)
                                <div class="kt-timeline-v1__marker"></div>
                            @else
                                <div class="row">
                                    <div class="col kt-align-center">
                                        <span class="btn btn-label-danger"><i class="la la-times-circle"></i> Aucune donnée dans la timeline</span>
                                    </div>
                                </div>
                            @endif
                            @foreach($suggestion->timelines as $timeline)
                            <div class="kt-timeline-v1__item">
                                <div class="kt-timeline-v1__item-circle">
                                    <div class="{{ \App\HelperClass\Suggestion::stateBackSuggestion($suggestion->state, true) }}"></div>
                                </div>
                                <span class="kt-timeline-v1__item-time kt-font-brand">
															@if($timeline->created_at > now()->addDay())
                                                                {{ $timeline->created_at->format("d/m/Y à H:i") }}
                                                            @else
                                                                {{ $timeline->created_at->diffForHumans() }}
                                                            @endif
														</span>
                                <div class="kt-timeline-v1__item-content">
                                    <div class="kt-timeline-v1__item-title">
                                        {{ $timeline->title }}
                                    </div>
                                    <div class="kt-timeline-v1__item-body">
                                        <p>
                                            {!! $timeline->text !!}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-xl-3"></div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="suggestAccept" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Acceptation de la suggestion</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <form action="{{ route('Back.Suggestion.accepted', $suggestion->id) }}" method="POST">
                    <div class="modal-body">
                            @csrf
                            <div class="form-group">
                                <label for="recipient-name" class="form-control-label">Titre:</label>
                                <input type="text" class="form-control" name="title" placeholder="Requete N°{{ $suggestion->id }}: {{ $suggestion->sujet }} (Accepter)" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="form-control-label">Texte:</label>
                                <textarea class="form-control" name="text" id="message-text">Votre requete à été accepter</textarea>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Accepter la suggestion</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="suggestReject" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Rejet de la suggestion</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <form action="{{ route('Back.Suggestion.rejected', $suggestion->id) }}" method="POST">
                    <div class="modal-body">
                        @csrf
                        <div class="form-group">
                            <label for="recipient-name" class="form-control-label">Titre:</label>
                            <input type="text" class="form-control" name="title" placeholder="Requete N°{{ $suggestion->id }}: {{ $suggestion->sujet }} (Rejeter)" id="recipient-name">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="form-control-label">Texte:</label>
                            <textarea class="form-control" name="text" id="message-text"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Rejeter la suggestion</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section("script")

@endsection
