@extends("Back.Layout.appTree")

@section("bread")
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Wiki | {{ $category->name }}</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('Front.Wiki.index') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Wiki </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">{{ $category->name }} </a>

                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
    </div>
@endsection

@section("content")
<div class="kt-grid-nav kt-grid-nav--skin-light">
    <div class="kt-grid-nav__row">
        @foreach($categories as $category)
        <a href="{{ route('Front.Wiki.showCategory', $category->id) }}" class="kt-grid-nav__item">
            <span class="kt-grid-nav__icon">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--xl {{ randColorWikiCategory() }}">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect id="bound" x="0" y="0" width="24" height="24"/>
                        <circle id="Oval" fill="#000000" cx="12" cy="12" r="8"/>
                    </g>
                </svg>
            </span>
            <span class="kt-grid-nav__title">{{ $category->name }}</span>
            <span class="kt-grid-nav__desc">{{ \App\Repository\Wiki\WikiRepository::countArticleFromCategory($category->id) }} {{ formatPlural('Article', \App\Repository\Wiki\WikiRepository::countArticleFromCategory($category->id)) }}</span>
        </a>
        @endforeach
    </div>
</div>

<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Liste des Articles de la catégorie: <strong>{{ $category->name }}</strong>
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="kt-notification-v2">
            @foreach($articles as $article)
            <a href="{{ route('Front.Wiki.show', [$category->id, $article->id]) }}" class="kt-notification-v2__item">
                <div class="kt-notification-v2__item-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--brand">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect id="bound" x="0" y="0" width="24" height="24"/>
                            <circle id="Oval" fill="#000000" cx="12" cy="12" r="8"/>
                        </g>
                    </svg>
                </div>
                <div class="kt-notification-v2__itek-wrapper">
                    <div class="kt-notification-v2__item-title">
                        {{ $article->title }}
                    </div>
                    <div class="kt-notification-v2__item-desc">
                        Mise à jour le {{ $article->updated_at->format('d/m/Y à H:i') }}
                    </div>
                </div>
            </a>
            @endforeach
        </div>
    </div>
</div>
@endsection

@section("script")

@endsection
