@extends("Back.Layout.appTree")

@section("bread")
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Wiki | {{ $article->title }}</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('Front.Wiki.index') }}" class="kt-subheader__breadcrumbs-home"><i
                            class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">Wiki </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">{{ $article->title }} </a>

                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
    </div>
@endsection

@section("content")
    <div class="kt-grid-nav kt-grid-nav--skin-light">
        <div class="kt-grid-nav__row">
            @foreach($categories as $category)
                <a href="{{ route('Front.Wiki.showCategory', $category->id) }}" class="kt-grid-nav__item">
            <span class="kt-grid-nav__icon">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                     height="24px" viewBox="0 0 24 24" version="1.1"
                     class="kt-svg-icon kt-svg-icon--xl {{ randColorWikiCategory() }}">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect id="bound" x="0" y="0" width="24" height="24"/>
                        <circle id="Oval" fill="#000000" cx="12" cy="12" r="8"/>
                    </g>
                </svg>
            </span>
                    <span class="kt-grid-nav__title">{{ $category->name }}</span>
                    <span class="kt-grid-nav__desc">{{ \App\Repository\Wiki\WikiRepository::countArticleFromCategory($category->id) }} {{ formatPlural('Article', \App\Repository\Wiki\WikiRepository::countArticleFromCategory($category->id)) }}</span>
                </a>
            @endforeach
        </div>
    </div>
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="flaticon2-graph"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    {{ $article->title }}
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div id="content" data-id="{{ $article->id }}" data-category="{{ $article->wiki_category_id }}"></div>
        </div>
        <div class="kt-portlet__foot">
            <div class="row align-items-center">
                <div class="col-lg-6 m--valign-middle">
                    Poster par <strong>{{ $article->user->name }}</strong> le
                    <strong>{{ $article->published_at->format('d/m/Y à H:i') }}</strong>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="/assets/custom/vendors/markdown-js/markdown.js"></script>
    <script type="text/javascript">
        function loadContent() {
            let content = $("#content")

            $.ajax({
                url: '/wiki/'+content.attr('data-category')+'/'+content.attr('data-id')+'/getContenue',
                success: function (data) {
                    let md_content = data[0]
                    content.html(markdown.toHTML(md_content))
                }
            })
        }

        (function ($) {
            loadContent()
        })(jQuery)
    </script>
@endsection
