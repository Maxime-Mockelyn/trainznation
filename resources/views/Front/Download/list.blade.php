@extends("Front.Layout.app")
@section("title") Téléchargements @endsection
@section("description") Liste des Téléchargements @endsection

@section("css")

@endsection

@section("content")
    <section class="imagebg">
        <div class="background-image-holder">
            <img src="{{ generateHeader($subcategorie->name) }}" alt="">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Téléchargement - {{ $subcategorie->name }}</h1>
                    <ol class="breadcrumbs">
                        <li>
                            <a href="#">{{ env("APP_NAME") }}</a>
                        </li>
                        <li>Téléchargement</li>
                        <li>{{ $subcategorie->categorie->name }}</li>
                        <li>{{ $subcategorie->name }}</li>
                    </ol>
                    <hr>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section class="space--sm">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="masonry masonry--tiles">
                        <div class="masonry__container row">
                            <div class="masonry__item col-md-6"></div>
                            @foreach($assets as $asset)
                            <div class="masonry__item col-md-6">
                                <div class="product product--tile bg--secondary text-center">
                                    <span class="label bg--error">Sale</span>
                                    @if($asset->pricing == 0)
                                        <span class="label bg--success">Gratuit</span>
                                    @else
                                        <span class="label bg--error">Payant</span>
                                    @endif
                                    <a href="{{ route('Front.Download.show', [$asset->asset_sub_categorie_id, $asset->id]) }}">
                                        <img alt="Image" src="/storage/download/{{ $asset->id }}.png" />
                                    </a>
                                    <a class="block" href="{{ route('Front.Download.show', [$asset->asset_sub_categorie_id, $asset->id]) }}">
                                        <div>
                                            <h5>{{ $asset->designation }}</h5>
                                            <span> {{ $asset->short_description }}</span>
                                        </div>
                                        <div>
                                            @if($asset->pricing == 1)
                                                <span class="h4 inline-block">{{ formatCurrency($asset->price) }}</span>
                                            @endif
                                        </div>
                                    </a>
                                </div>
                            </div>
                            @endforeach
                            <!--end item-->
                        </div>
                        <!--end masonry container-->
                    </div>
                    <!--end masonry-->
                </div>
            </div>
        </div>
    </section>
@endsection

@section("script")

@endsection
