@extends("Front.Layout.app")
@section("title") {{ $asset->designation }} @endsection
@section("description") {{ substr($asset->short_description, 0, 120) }} @endsection

@section("css")

@endsection

@section("content")
    <section class="imagebg">
        <div class="background-image-holder">
            @if(env('APP_ENV') == 'production')
                <img src="{{ \Illuminate\Support\Facades\Storage::disk('s3')->url('download/'.$asset->id.'.png') }}" alt="">
            @else
                <img src="{{ \Illuminate\Support\Facades\Storage::disk('public')->url('download/'.$asset->id.'.png') }}" alt="">
            @endif
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Téléchargement - {{ $asset->designation }}</h1>
                    <ol class="breadcrumbs">
                        <li>
                            <a href="#">{{ env("APP_NAME") }}</a>
                        </li>
                        <li>Téléchargement</li>
                        <li>{{ $asset->categorie->name }}</li>
                        <li>{{ $asset->subcategorie->name }}</li>
                        <li>{{ $asset->designation }}</li>
                    </ol>
                    <hr>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section class="space--md">
        <div class="container">
            <div class="row justify-content-around">
                <div class="col-md-7 col-lg-6">
                    <div class="slider border--round boxed--border" data-paging="true" data-arrows="true">
                        <ul class="slides">
                            <li>
                                <img alt="Image" src="/storage/download/{{ $asset->id }}.png" />
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-5 col-lg-4">
                    <h2>{{ $asset->designation }}</h2>
                    <span class="text-muted">{{ $asset->categorie->name }} - {{ $asset->subcategorie->name }}</span>
                    <br>
                    <div class="text-block">
                        <!--<span class="h4 type--strikethrough inline-block">$549.99</span>-->
                        @if($asset->pricing == 1)
                            <span class="h2 inline-block">{{ formatCurrency($asset->price) }}</span>
                        @endif
                    </div>
                    <p>
                        {{ $asset->short_description }}
                    </p>
                    <ul class="accordion accordion-2 accordion--oneopen">
                        <li class="active">
                            <div class="accordion__title">
                                <span class="h5">Informations</span>
                            </div>
                            <div class="accordion__content">
                                <ul class="bullets">
                                    <li>
                                        <span><strong>Kuid</strong>: {{ $asset->kuid }}</span>
                                    </li>
                                    <li>
                                        <span><strong>Date de Création</strong>: {{ $asset->created_at->format("d/m/Y à H:i") }}</span>
                                    </li>
                                    <li>
                                        <span><strong>Date de Mise à jour</strong>: {{ $asset->updated_at->format("d/m/Y à H:i") }}</span>
                                    </li>
                                    <li>
                                        <span><strong>Téléchargement</strong>: {{ $asset->count }}</span>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <div class="accordion__title">
                                <span class="h5">Compatibilité</span>
                            </div>
                            <div class="accordion__content">
                                @foreach($asset->compatibilities->sortBy('trainz_build') as $compatibility)
                                    <span class="badge {{ stateCompatibilityBadge($compatibility->state) }}" data-tooltip="{{ stateCompatibilityTooltip($compatibility->state) }}">{{ $compatibility->trainz_build }}</span>
                                @endforeach
                            </div>
                        </li>
                    </ul>
                    @if($asset->pricing == 0)
                        <a href="{{ route('Download.redirect', $asset->id) }}" class="btn btn-primary btn-block">Télécharger l'objet</a>
                    @else
                        @auth()
                            @if(auth()->user()->account->premium == 1)
                                <a href="{{ route('Download.redirect', $asset->id) }}" class="btn btn-primary btn-block">Télécharger l'objet</a>
                            @else
                                <a href="{{ route("Front.Download.cart", [$asset->asset_sub_categorie_id, $asset->id]) }}" class="btn btn-primary btn-block">Acheter l'objet</a>
                            @endif
                        @else
                            <a href="{{ route("Front.Download.cart", [$asset->asset_sub_categorie_id, $asset->id]) }}" class="btn btn-primary btn-block">Acheter l'objet</a>
                        @endauth
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <span class="h4">Description</span>
                    <hr>
                    <div id="description" data-id="{{ $asset->id }}" data-sub="{{ $asset->asset_sub_categorie_id }}"></div>
                    <br>
                </div>
            </div>
        </div>
    </section>
@endsection

@section("script")
    <script src="/assets/custom/vendors/markdown-js/markdown.js"></script>
    <script type="text/javascript">
        function loadDescription()
        {
            let description = $("#description")

            $.ajax({
                url: '/download/'+description.attr('data-sub')+'/'+description.attr('data-id')+'/getContenue',
                success: function (data) {
                    let md_content = data[0]
                    description.html(markdown.toHTML(md_content))
                }
            })
        }

        (function ($) {
            loadDescription()
        })(jQuery)
    </script>
@endsection
