@extends("Front.Layout.app")
@section("title") {{ $asset->designation }} @endsection
@section("description") {{ substr($asset->short_description, 0, 120) }} @endsection

@section("css")
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
@endsection

@section("content")
    <section class="imagebg">
        <div class="background-image-holder">
            @if(env('APP_ENV') == 'production')
                <img src="{{ \Illuminate\Support\Facades\Storage::disk('s3')->url('download/'.$asset->id.'.png') }}" alt="">
            @else
                <img src="{{ \Illuminate\Support\Facades\Storage::disk('public')->url('download/'.$asset->id.'.png') }}" alt="">
            @endif
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Achat d'un Objet</h1>
                    <ol class="breadcrumbs">
                        <li>
                            <a href="#">{{ env("APP_NAME") }}</a>
                        </li>
                        <li>Achat d'un Objet</li>
                        <li>{{ $asset->designation }}</li>
                    </ol>
                    <hr>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section>
        <div class="container">
            <div class="alert bg--success">
                <div class="alert__body">
                    <h2>Paiement Accepter</h2>
                    <span>Le Paiement à été accepter, un mail vous à été envoyer à <strong>{{ $invoice->email }}</strong> comportant votre facture et le lien vers l'objet à télécharger</span>
                </div>
                <div class="alert__close">×</div>
            </div>
            <form action="{{ route('Front.Download.checkout', [$asset->asset_sub_categorie_id, $asset->id]) }}" class="cart-form" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-4">
                        <div class="product">
                            <span class="label">QTE: 1</span>
                            <img alt="Image" src="/storage/download/{{ $asset->id }}.png" />
                            <div>
                                <h5>{{ $asset->designation }}</h5>
                                <span> {{ $asset->short_description }}</span>
                            </div>
                            <div>
                                <span class="h4 inline-block">{{ formatCurrency($asset->price) }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt--2">
                    <div class="col-md-12">
                        <div class="boxed boxed--border cart-total">
                            <div class="row">
                                <div class="col-6">
                                    <span class="h5">Sous total:</span>
                                </div>
                                <div class="col-6 text-right">
                                    <span>{{ formatCurrency($asset->price) }}</span>
                                </div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="col-6">
                                    <span class="h5">Total:</span>
                                </div>
                                <div class="col-6 text-right">
                                    <span class="h5">{{ formatCurrency($asset->price) }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection

@section("script")
    <script src="/assets/custom/vendors/inputmask/inputmask.js"></script>
    <script src="/assets/custom/vendors/inputmask/inputmask.binding.js"></script>
    <script type="text/javascript">
        (function ($) {
            $(":input").inputmask();
        })(jQuery)
    </script>
@endsection
