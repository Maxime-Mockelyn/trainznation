@extends("Front.Layout.app")

@section("css")

@endsection

@section("content")
    <section class="imagebg">
        <div class="background-image-holder">
            @if(env('APP_ENV') == 'production')
                <img src="{{ \Illuminate\Support\Facades\Storage::disk('s3')->url('download/'.$asset->id.'.png') }}" alt="">
            @else
                <img src="{{ \Illuminate\Support\Facades\Storage::disk('public')->url('download/'.$asset->id.'.png') }}" alt="">
            @endif
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Téléchargement - {{ $asset->designation }} - Vue 3D</h1>
                    <ol class="breadcrumbs">
                        <li>
                            <a href="#">{{ env("APP_NAME") }}</a>
                        </li>
                        <li>Téléchargement</li>
                        <li>{{ $asset->categorie->name }}</li>
                        <li>{{ $asset->subcategorie->name }}</li>
                        <li>{{ $asset->designation }}</li>
                        <li>Vue 3D</li>
                    </ol>
                    <hr>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section class="space--md">
        <div class="container-fluid">
            <div id="view"></div>
        </div>
    </section>
    <div id="asset_id" data-id="{{ $asset->id }}"></div>
    <div id="categorie_name" data-id="{{ str_slug($asset->categorie->name) }}"></div>
    <div id="sub_name" data-id="{{ str_slug($asset->subcategorie->name) }}"></div>
@endsection

@section("script")
    <script src="/vendor/threeJs/three.js"></script>
    <script src="/vendor/threeJs/js/libs/inflate.min.js"></script>
    <script src="/vendor/threeJs/js/loaders/FBXLoader.js"></script>
    <script src="/vendor/threeJs/js/loaders/TGALoader.js"></script>
    <script src="/vendor/threeJs/js/controls/OrbitControls.js"></script>
    <script src="/vendor/threeJs/js/WebGL.js"></script>
    <script src="/vendor/threeJs/js/libs/stats.min.js"></script>

    <script type="text/javascript">
        let identifier = $("#asset_id").attr('data-id')
        let categorie_id = $("#categorie_name").attr('data-id')
        let sub_name = $("#sub_name").attr('data-id')

        if(WEBGL.isWebGLAvailable() === false)
        {
            document.body.appendChild( WEBGL.getWebGLErrorMessage() );
        }

        let container, stats, controls;
        let camera, scene, renderer, light;
        let clock = new THREE.Clock();
        let mixer;
        init();
        animate();

        function init() {
            container = document.getElementById('view');

            camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 2000 );
            camera.position.set( 100, 200, 300 );
            controls = new THREE.OrbitControls( camera );
            controls.target.set( 0, 100, 0 );
            controls.update();
            scene = new THREE.Scene();
            scene.background = new THREE.Color( 0xa0a0a0 );
            scene.fog = new THREE.Fog( 0xa0a0a0, 200, 1000 );
            light = new THREE.HemisphereLight( 0xffffff, 0x444444 );
            light.position.set( 0, 200, 0 );
            scene.add( light );
            light = new THREE.DirectionalLight( 0xffffff );
            light.position.set( 0, 200, 100 );
            light.castShadow = true;
            light.shadow.camera.top = 180;
            light.shadow.camera.bottom = - 100;
            light.shadow.camera.left = - 120;
            light.shadow.camera.right = 120;
            scene.add( light );
            // scene.add( new THREE.CameraHelper( light.shadow.camera ) );
            // ground
            let mesh = new THREE.Mesh( new THREE.PlaneBufferGeometry( 2000, 2000 ), new THREE.MeshPhongMaterial( { color: 0x999999, depthWrite: false } ) );
            mesh.rotation.x = - Math.PI / 2;
            mesh.receiveShadow = true;
            scene.add( mesh );
            let grid = new THREE.GridHelper( 2000, 20, 0x000000, 0x000000 );
            grid.material.opacity = 0.2;
            grid.material.transparent = true;
            scene.add( grid );
            // model
            let loader = new THREE.FBXLoader();
            loader.load( '/storage/modele/'+categorie_id+'/'+sub_name+'/'+identifier+'/'+identifier+'.fbx', function ( object ) {
                mixer = new THREE.AnimationMixer( object );
                let action = mixer.clipAction( object.animations[ 0 ] );
                action.play();
                object.traverse( function ( child ) {
                    if ( child.isMesh ) {
                        child.castShadow = true;
                        child.receiveShadow = true;
                    }
                } );
                scene.add( object );
            } );

            renderer = new THREE.WebGLRenderer( { antialias: true } );
            renderer.setPixelRatio( window.devicePixelRatio );
            renderer.setSize( window.innerWidth, window.innerHeight );
            renderer.shadowMap.enabled = true;
            container.appendChild( renderer.domElement );
            window.addEventListener( 'resize', onWindowResize, false );
            // stats
            stats = new Stats();
            container.appendChild( stats.dom );
        }
        function onWindowResize() {
            camera.aspect = window.innerWidth() / window.innerHeight();
            camera.updateProjectionMatrix();
            renderer.setSize( window.innerWidth, window.innerHeight);
        }
        //
        function animate() {
            requestAnimationFrame( animate );
            let delta = clock.getDelta();
            if ( mixer ) mixer.update( delta );
            renderer.render( scene, camera );
            stats.update();
        }
    </script>
@endsection
