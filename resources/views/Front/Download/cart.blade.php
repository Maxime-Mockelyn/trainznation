@extends("Front.Layout.app")
@section("title") {{ $asset->designation }} @endsection
@section("description") {{ substr($asset->short_description, 0, 120) }} @endsection

@section("css")
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
@endsection

@section("content")
    <section class="imagebg">
        <div class="background-image-holder">
            @if(env('APP_ENV') == 'production')
                <img src="{{ \Illuminate\Support\Facades\Storage::disk('s3')->url('download/'.$asset->id.'.png') }}" alt="">
            @else
                <img src="{{ \Illuminate\Support\Facades\Storage::disk('public')->url('download/'.$asset->id.'.png') }}" alt="">
            @endif
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Achat d'un Objet</h1>
                    <ol class="breadcrumbs">
                        <li>
                            <a href="#">{{ env("APP_NAME") }}</a>
                        </li>
                        <li>Achat d'un Objet</li>
                        <li>{{ $asset->designation }}</li>
                    </ol>
                    <hr>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section>
        <div class="container">
            <form action="{{ route('Front.Download.checkout', [$asset->asset_sub_categorie_id, $asset->id]) }}" class="cart-form" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-4">
                        <div class="product">
                            <span class="label">QTE: 1</span>
                            <img alt="Image" src="/storage/download/{{ $asset->id }}.png" />
                            <div>
                                <h5>{{ $asset->designation }}</h5>
                                <span> {{ $asset->short_description }}</span>
                            </div>
                            <div>
                                <span class="h4 inline-block">{{ formatCurrency($asset->price) }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt--2">
                    <div class="col-md-12">
                        <div class="boxed boxed--border cart-total">
                            <div class="row">
                                <div class="col-6">
                                    <span class="h5">Sous total:</span>
                                </div>
                                <div class="col-6 text-right">
                                    <span>{{ formatCurrency($asset->price) }}</span>
                                </div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="col-6">
                                    <span class="h5">Total:</span>
                                </div>
                                <div class="col-6 text-right">
                                    <span class="h5">{{ formatCurrency($asset->price) }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt--2">
                    <h2>Information de Paiement</h2>
                    <div class="col-md-12">
                        <label>Adresse Mail <span class="color--error">*</span> </label>
                        <input type="email" name="email" required>
                    </div>
                    <div class="col-md-6">
                        <label>Numéro de la carte <span class="color--error">*</span> </label>
                        <input type="text" id="num_cb" name="num_cb" data-inputmask="'mask': '9999 9999 9999 9999'" required>
                    </div>
                    <div class="col-md-2">
                        <label>Expiration <span class="color--error">*</span></label>
                        <input type="text" name="exp_month" data-inputmask="'mask': '99'" placeholder="MM" required>
                    </div>
                    <div class="col-md-2">
                        <label>&nbsp;</label>
                        <input type="text" name="exp_year" data-inputmask="'mask': '99'" placeholder="YY" required>
                    </div>
                    <div class="col-md-2">
                        <label>CVC <span class="color--error">*</span></label>
                        <input type="text" name="cvv_cb" data-inputmask="'mask': '999'" placeholder="999" required>
                    </div>
                    <button class="btn btn-primary btn-block"><i class="fa fa-credit-card color--white"></i> Payer</button>
                </div>
            </form>
        </div>
    </section>
@endsection

@section("script")
    <script src="/assets/custom/vendors/inputmask/inputmask.js"></script>
    <script src="/assets/custom/vendors/inputmask/inputmask.binding.js"></script>
    <script type="text/javascript">
        (function ($) {
            $(":input").inputmask();
        })(jQuery)
    </script>
@endsection
