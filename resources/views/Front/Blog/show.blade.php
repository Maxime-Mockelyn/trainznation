@extends("Front.Layout.app")
@section("title") {{ $blog->title }} @endsection
@section("description") {!! substr($blog->content, 0, 120) !!} @endsection

@section("css")

@endsection

@section("content")

    <section class="unpad">
        <article>
            <div class="imagebg text-center height-60" data-overlay="5">
                <div class="background-image-holder">
                    <img alt="background" src="{{ sourceImage('blog/'.$blog->id.'.png') }}" />
                </div>
                <div class="container pos-vertical-center">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="article__title">
                                <h1>{{ $blog->title }}</h1>
                                <span>{{ $blog->published_at->format("d/m/Y à H:i") }} dans </span>
                                <span>
                                            <a href="#">{{ $blog->categorie->name }}</a>
                                        </span>
                            </div>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
                <div class="pos-absolute pos-bottom col-12 text-center">
                    <div class="article__author">
                        <img alt="Image" src="/storage/avatar/trainznation.png" />
                        <h6 class="type--uppercase">TrainzNation</h6>
                    </div>
                </div>
            </div>
            <div>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-10 col-lg-8">
                            <div class="article__body">
                                <div id="description" data-id="{{ $blog->slug }}"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </section>
    <section class="space--sm">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <hr>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-10 col-lg-8">
                    <div class="comments">
                        <h3>{{ \App\Repository\Blog\BlogCommentRepository::getCountCommentFromBlog($blog->id) }} {{ formatPlural('Commentaire', \App\Repository\Blog\BlogCommentRepository::getCountCommentFromBlog($blog->id)) }}</h3>
                        <ul class="comments__list">
                            @foreach(\App\Repository\Blog\BlogCommentRepository::getComments($blog->id) as $comment)
                                @if($comment->state == 1)
                                    <li>
                                        <div class="comment">
                                            <div class="comment__avatar">
                                                @if($comment->user_id == 1)
                                                    <img alt="Image" src="/storage/avatar/trainznation.png" />
                                                @else
                                                    <img alt="Image" src="/storage/avatar/{{ $comment->user_id }}.png" />
                                                @endif
                                            </div>
                                            <div class="comment__body">
                                                <h5 class="type--fine-print">{{ $comment->user->name }}</h5>
                                                <div class="comment__meta">
                                                    <span>{{ $comment->updated_at->format("d/m/Y à H:i") }}</span>
                                                </div>
                                                <p>
                                                    {{ $comment->comment }}
                                                </p>
                                            </div>
                                        </div>
                                        <!--end comment-->
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                    <!--end comments-->
                    <div class="comments-form">
                        <h4>Laissez un commentaire</h4>
                        @auth()
                            <form class="row" action="" method="post">
                                @csrf
                                <div class="col-md-12">
                                    <label>Commentaire:</label>
                                    <textarea rows="4" name="comment" placeholder="Tapez votre commentaire Ici"></textarea>
                                </div>
                                <div class="col-md-3">
                                    <button class="btn btn--primary" type="submit">Soumettre mon commentaire</button>
                                </div>
                            </form>
                        @else
                            <div class="alert bg--error">
                                <div class="alert__body">
                                    <span><i class="material-icons color--warning">warning</i> <strong>Attention !</strong> Vous devez être connecter avant de poster un commentaire</span>
                                </div>
                                <div class="alert__close">×</div>
                            </div>
                        @endauth
                    </div>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section class="bg--secondary">
        <div class="container">
            <div class="row text-block">
                <div class="col-md-12">
                    <h3>Articles Similaire</h3>
                </div>
            </div>
            <!--end of row-->
            <div class="row">
                @foreach(\App\Repository\Blog\BlogRepository::getMostPostsFromPost($blog->categorie_id) as $post)
                <div class="col-md-4">
                    <article class="feature feature-1">
                        <a href="{{ route('Front.Blog.show', $post->slug) }}" class="block">
                            <img alt="Image" src="/storage/blog/{{ $post->id }}.png" />
                        </a>
                        <div class="feature__body boxed boxed--border">
                            <span>{{ $post->published_at->format("d/m/Y à H:i") }}</span>
                            <h5>{{ $post->title }}</h5>
                            <a href="{{ route('Front.Blog.show', $post->slug) }}">
                                En savoir plus...
                            </a>
                        </div>
                    </article>
                </div>
                @endforeach
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
@endsection

@section("script")
    <script src="/assets/custom/vendors/markdown-js/markdown.js"></script>
    <script type="text/javascript">
        function loadDescription()
        {
            let description = $("#description")

            $.ajax({
                url: '/blog/'+description.attr('data-id')+'/getContenue',
                success: function (data) {
                    let md_content = data[0]
                    description.html(markdown.toHTML(md_content))
                }
            })
        }

        (function ($) {
            loadDescription()
        })(jQuery)
    </script>
@endsection
