@extends("Front.Layout.app")
@section("title") Blog @endsection
@section("description") Liste des Articles @endsection

@section("css")

@endsection

@section("content")
    <section class="imagebg" data-gradient-bg='#4876BD,#5448BD,#8F48BD,#BD48B1'>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Blog</h1>
                    <ol class="breadcrumbs">
                        <li>
                            <a href="#">{{ env("APP_NAME") }}</a>
                        </li>
                        <li>Blog</li>
                    </ol>
                    <hr>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section class="space--sm">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="masonry">
                        <div class="masonry-filter-container d-flex align-items-center" id="tour_blog_category">
                            <span>Catégorie:</span>
                            <div class="masonry-filter-holder">
                                <div class="masonry__filters" data-filter-all-text="Toutes les Catégorie"></div>
                            </div>
                        </div>
                        <hr>
                        <div class="masonry__container row" id="tour_blog_content">
                            <div class="masonry__item col-md-6"></div>
                            @foreach($blogs as $blog)
                            <div class="masonry__item col-md-12" data-masonry-filter="{{ $blog->categorie->name }}">
                                <article class="feature feature-1">
                                    <a href="{{ route('Front.Blog.show', $blog->slug) }}" class="block">
                                        <img alt="Image" src="{{ sourceImage('blog/'.$blog->id.'.png') }}" />
                                    </a>
                                    <div class="feature__body boxed boxed--border">
                                        <span>{{ $blog->published_at->format("d/m/Y à H:i") }}</span>
                                        <h5>{{ $blog->title }}</h5>
                                        <span>{{ $blog->short_content }}</span>
                                        <a href="{{ route('Front.Blog.show', $blog->slug) }}">
                                            En savoir plus
                                        </a>
                                    </div>
                                </article>
                            </div>
                            @endforeach
                            <!--end item-->
                        </div>
                        <!--end of masonry container-->
                        {{ $blogs->links('Front.Include.paginate', ["results" => $blogs]) }}
                    </div>
                    <!--end masonry-->
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
@endsection

@section("script")

@endsection
