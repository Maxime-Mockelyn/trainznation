<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ env("APP_NAME") }} - Launcher</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
</head>
<body>
    <div class="pure-g">
        <div class="pure-u-1-3">
            <div class="row">
                <div class="col s12 m12">
                    <div class="card">
                        <div class="card-image">
                            <img src="/storage/other/register_wall.jpg">
                            <span class="card-title">Note de Mise à jour</span>
                        </div>
                        <div class="card-content">
                            <ul class="collapsible">
                                <li class="active">
                                    <div class="collapsible-header"><i class="material-icons">filter_drama</i>V7:27886 <span class="new badge" data-badge-caption="Final"></span></div>
                                    <div class="collapsible-body">
                                        <h3>Nouveauté</h3>
                                        <ul>
                                            NTS-RDN: / Création de la session: TER 858203 (Nantes-Redon) / 6h14
                                            RDN - NTS: / Création de la session: TER 858258 (Redon - nantes) / 7h27
                                            Nantes: / Correction de la version 1 (Nantes - Le Croisic) / Secteur Nantes
                                            Chantenay: / Correction de la version 1 (Nantes - Le Croisic) / Secteur Chantenay
                                            Basse Indre: / Correction de la version 1 (Nantes - Le Croisic) / Secteur Basse Indre
                                            Coueron: / Correction de la version 1 (Nantes - Le Croisic) / Secteur Coueron
                                            St Etienne de Montluc: / Correction de la version 1 (Nantes - Le Croisic) / Secteur St Etienne de Montluc
                                            Cordemais: / Correction de la version 1 (Nantes - Le Croisic) / Secteur Cordemais
                                            Savenay: / Correction de la version 1 (Nantes - Le Croisic) / Secteur Savenay
                                            Donges: / Correction de la version 1 (Nantes - Le Croisic) / Secteur Donges
                                            Montoir de Bretagne: / Correction de la version 1 (Nantes - Le Croisic) / Secteur Montoir de Bretagne
                                            La Croix de Méan: / Correction de la version 1 (Nantes - Le Croisic) / Secteur La Croix de Méan
                                            Penhoet: / Correction de la version 1 (Nantes - Le Croisic) / Secteur Penhoet
                                            Saint Nazaire: / Correction de la version 1 (Nantes - Le Croisic) / Secteur Saint Nazaire
                                            Pornichet: / Correction de la version 1 (Nantes - Le Croisic) / Secteur Pornichet
                                            La Baule - Les Pins: / Correction de la version 1 (Nantes - Le Croisic) / Secteur La Baule - Les Pins
                                            La Baule Escoublac: / Correction de la version 1 (Nantes - Le Croisic) / Secteur La Baule Escoublac
                                            Le Pouliguen: / Correction de la version 1 (Nantes - Le Croisic) / Secteur Le Pouliguen
                                            Batz sur Mer: / Correction de la version 1 (Nantes - Le Croisic) / Secteur Batz sur Mer
                                            Le Croisic: / Correction de la version 1 (Nantes - Le Croisic) / Secteur Le Croisic
                                            Thouaré: / Correction de la version 2 (Nantes - le Mans) / Secteur Thouaré
                                            Mauves sur Loire: / Correction de la version 2 (Nantes - le Mans) / Secteur Mauves sur Loire
                                            Le Cellier: / Correction de la version 2 (Nantes - le Mans) / Secteur Le Cellier
                                            Oudon: / Correction de la version 2 (Nantes - le Mans) / Secteur Oudon
                                            Ancenis: / Correction de la version 2 (Nantes - le Mans) / Secteur Ancenis
                                            Varades: / Correction de la version 2 (Nantes - le Mans) / Secteur Varades
                                            Ingrandes Sur Loire: / Correction de la version 2 (Nantes - le Mans) / Secteur Ingrandes sur Loire
                                            Champtocé sur Loire: / Correction de la version 2 (Nantes - le Mans) / Secteur Champtocé sur Loire
                                            La Possonnière: / Correction de la version 2 (Nantes - le Mans) / Secteur La Possonnière
                                            Savennière: / Correction de la version 2 (Nantes - le Mans) / Secteur Savenière
                                            Angers St Laud: / Correction de la version 2 (Nantes - le Mans) / Secteur Angers St Laud
                                            Ecouflant: / Correction de la version 2 (Nantes - le Mans) / Secteur Ecouflant
                                            Le Vieux Briolay: / Correction de la version 2 (Nantes - le Mans) / Secteur Le Vieux Briolay
                                            Tiercé: / Correction de la version 2 (Nantes - le Mans) / Secteur Tiercé
                                            Etriché: / Correction de la version 2 (Nantes - le Mans) / Secteur Etriché
                                            Morannes: / Correction de la version 2 (Nantes - le Mans) / Secteur Morannes
                                            Sablé-sur-Sarthe: / Correction de la version 2 (Nantes - le Mans) / Secteur Sablé sur Sarthe
                                            Noyen: / Correction de la version 2 (Nantes - le Mans) / Secteur Noyen
                                            La Suze: / Correction de la version 2 (Nantes - le Mans) / Secteur La Suze
                                            Voivre: / Correction de la version 2 (Nantes - le Mans) / Secteur Voivre
                                            Le Mans: / Correction de la version 2 (Nantes - le Mans) / Secteur Le Mans
                                            Champagné: / Correction de la version 3 (Le Mans - Nogent Le Rotrou) / Secteur Champagné
                                            St Mars la Brière: / Correction de la version 3 (Le Mans - Nogent Le Rotrou) / Secteur St Mars la Brière
                                            Montfort le Génois: / Correction de la version 3 (Le Mans - Nogent Le Rotrou) / Secteur Montfort le Genois
                                            Connerré: / Correction de la version 3 (Le Mans - Nogent Le Rotrou) / Secteur Connerré
                                            Sceaux - Boessé: / Correction de la version 3 (Le Mans - Nogent Le Rotrou) / Secteur Sceaux
                                            La Ferté Bernard: / Correction de la version 3 (Le Mans - Nogent Le Rotrou) / Secteur La Ferté Bernard
                                            Le Theil: / Correction de la version 3 (Le Mans - Nogent Le Rotrou) / Secteur Le Theil
                                            Nogent Le Rotrou: / Correction de la version 3 (Le Mans - Nogent Le Rotrou) / Secteur Nogent le Rotrou
                                            Chalonne sur Loire: / Correction de la version 4 (La Possonnière - Cholet - Clisson) / Secteur Chalonne sur Loire
                                            Chemillé: / Correction de la version 4 (La Possonnière - Cholet - Clisson) / Secteur Chemillé
                                            Cholet: / Correction de la version 4 (La Possonnière - Cholet - Clisson) / Secteur Cholet
                                            Torfou: / Correction de la version 4 (La Possonnière - Cholet - Clisson) / Secteur Torfou
                                            Boussay La Bruffière: / Correction de la version 4 (La Possonnière - Cholet - Clisson) / Secteur Boussay la Bruffière
                                            Cugand: / Correction de la version 4 (La Possonnière - Cholet - Clisson) / Secteur Cugand
                                            Clisson: / Correction de la version 4 (La Possonnière - Cholet - Clisson) / Secteur Clisson
                                            Gorges: / Correction de la version 5 (Clisson - Nantes) / Secteur Gorges
                                            Le Pallet: / Correction de la version 5 (Clisson - Nantes) / Secteur Le Pallet
                                            La Haye Fouassière: / Correction de la version 5 (Clisson - Nantes) / Secteur La Haye Fouassière
                                            Vertou: / Correction de la version 5 (Clisson - Nantes) / Secteur Vertou
                                            St Sébastien - Frène Rond: / Correction de la version 5 (Clisson - Nantes) / Secteur St Sébastien - Frène Rond
                                            St Sébastien - Pas Enchanté: / Correction de la version 5 (Clisson - Nantes) / Secteur St Sébastien - Pas Enchanté
                                            PCH-SAV:Restriction de vitesse mal placé / Déplacé ou supprimé le point de vitesse dans la direction Pontchateau -> Savenay
                                            JCT533502:Erreur de jonction: 533502 / Vérifier si un levier est existant après la jonction 533502
                                            DGS - LCR:Aucun Caténaire entre Donges et Le Croisic / Ajout de caténaire entre donges et Le Croisic
                                        </ul>
                                    </div>
                                </li>
                                <li class="active">
                                    <div class="collapsible-header"><i class="material-icons">filter_drama</i>V6:27886 <span class="new badge" data-badge-caption="Correctif"></span></div>
                                    <div class="collapsible-body">
                                        <h3>Nouveauté</h3>
                                        <ul>
                                            <li>- Pose des décors (St Gildas des Bois - Sévérac)</li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="">
                                    <div class="collapsible-header"><i class="material-icons">filter_drama</i>V6:27406 <span class="new badge" data-badge-caption="Correctif"></span></div>
                                    <div class="collapsible-body">
                                        <h3>Nouveauté</h3>
                                        <ul>
                                            <li>- Pose des décors (Dréfféac - St Gildas des Bois)</li>
                                            <li>- Gare de St Gildas des Bois modéliser</li>
                                            <li>- Système d'annonce</li>
                                        </ul>

                                        <h3>Correction</h3>
                                        <ul>
                                            <li>Réorganisation des triggers de toutes la map, il devront être installer lors de la création d'une nouvelle session.</li>
                                        </ul>

                                        <h3>Suppression</h3>
                                        <ul>
                                            <li>Suppression de tous les triggers de la Map</li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="">
                                    <div class="collapsible-header"><i class="material-icons">filter_drama</i>V6:27166 <span class="new badge" data-badge-caption="Correctif"></span></div>
                                    <div class="collapsible-body">
                                        <h3>Nouveauté</h3>
                                        <ul>
                                            <li>- Pose des décors (Pontchateau - Dréfféac)</li>
                                            <li>- Gare de Dréfféac modéliser</li>
                                            <li>- Certains décors des anciennes version ont été ajouté également</li>
                                            <li>- Le Test IA basique entre Savenay et Redon à été effectuer</li>
                                        </ul>

                                        <h3>Correction</h3>
                                        <ul>
                                            <li>- Changement du nombre de voyageur initial et par heure sur l'ensemble des gares du réseau</li>
                                            <li>- Fix mineur</li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script type="text/javascript">
        (function ($) {
            $('.collapsible').collapsible();
        })(jQuery)
    </script>
</body>
</html>
