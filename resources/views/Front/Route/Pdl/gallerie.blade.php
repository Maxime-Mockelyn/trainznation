@extends('Front.Route.Pdl.layout.app')

@section("content")
<section class="section no-pad-top">
    <div class="container js-filter">
        <div class="row">
            <div class="col s4">
                <div class="card">
                    <div class="collection js-filter-form">
                        <a class="collection-item" data-target="/api/route/pdl/gallery/listView">
                            <i class="material-icons right">sort</i>
                            <span class="title">Tous</span>
                        </a>
                        @foreach($categories as $category)
                            <a class="collection-item" data-target="/api/route/pdl/gallery/getGalleryFrom/{{ $category->id }}">
                                <i class="material-icons right">sort</i>
                                <span class="title">{{ $category->name }}</span>
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col s8">
                <div id="materialGallery">
                    <div class="card">
                        @foreach($categories as $category)
                            <div class="row js-filter-content"></div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('script')
    <script src="/assets/custom/vendors/blockui/blockui.js"></script>
<script type="text/javascript">
    (function ($) {
        $('.js-filter-form').on('click', '.collection-item', function (e) {
            e.preventDefault()
            let btn = $(this)
            let url = btn.attr('data-target')
            $(".js-filter-content").block({
                message: "<h1>Chargement</h1>",
                css: { border: '3px solid #a00'}
            })

            $.ajax({
                url: url,
                success: function (data) {
                    $(".js-filter-content").unblock()
                    $(".js-filter-content").html(data.content)
                },
                error: function () {
                    KTApp.unblock('#contentPrestation')
                }
            })

        })
    })(jQuery)
</script>
@endsection

