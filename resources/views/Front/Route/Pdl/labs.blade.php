@extends('Front.Route.Pdl.layout.app')

@section("style")
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
    </style>
@endsection

@section("content")
<section class="section">
    <div class="container">
        <div class="row">
            <div class="col s12">
                <div class="card">
                    <h2 class="card-content center-align">Avancement du projet</h2>
                </div>
            </div>
            <div class="col s12">
                <div class="card">
                    <div class="card-content">
                        <span class="card-title">Information sur l'avancement</span>
                        <div class="row">
                            <div class="col s3">
                                <div class="progress-pie-chart" data-percent="{{ $info['percent'] }}">
                                    <div class="ppc-progress">
                                        <div class="ppc-progress-fill"></div>
                                    </div>
                                    <div class="ppc-percents">
                                        <div class="pcc-percents-wrapper">
                                            <span>%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col s9">
                                <table class="table striped">
                                    <tbody>
                                        <tr>
                                            <td>Version</td>
                                            <td>{{ $info['build']['version'] }}</td>
                                        </tr>
                                        <tr>
                                            <td>Build Actuel</td>
                                            <td>{{ $info['build']['build'] }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="card">
                    <div class="card-content">
                        <span class="card-title">Liste des Taches</span>
                        <div class="kt-list-timeline">
                            <div class="kt-list-timeline__items">
                                @foreach($info['anomalies'] as $anomalie)
                                    <div class="kt-list-timeline__item">
                                        <span class="kt-list-timeline__badge {!! stateAn($anomalie['state']) !!}"></span>
                                        <span class="kt-list-timeline__text">{{ $anomalie['correction'] }}</span>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section("script")
    <script type="text/javascript">
        $(function(){
            var $ppc = $('.progress-pie-chart'),
                percent = parseInt($ppc.data('percent')),
                deg = 360*percent/100;
            if (percent > 50) {
                $ppc.addClass('gt-50');
            }
            $('.ppc-progress-fill').css('transform','rotate('+ deg +'deg)');
            $('.ppc-percents span').html(percent+'%');
        });
    </script>
@endsection

