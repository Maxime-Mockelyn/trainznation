@extends('Front.Route.Pdl.layout.app')

@section("content")
<section class="section no-pad-bot">
    <div class="container">
        <div class="col s12 m9">
            <div class="card horizontal">
                <div class="card-image">
                    <img src="/assets/images/cartographie.jpg">
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <h3 class="font-bold">Cartographie Etendue</h3>
                        <p>Conduisez votre train sur le réseau Pays de la Loire, Bretagne et même la LGV Atlantique jusqu'à Paris Montparnasse.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12 m9">
            <div class="card horizontal">
                <div class="card-stacked">
                    <div class="card-content">
                        <h3 class="font-bold">Plus de 200 batiments et structures reproduites</h3>
                        <p>Grâce à la cartographie de Google Earth, plus de 200 Batiments et de Structures Ferroviaires ont été reproduites le plus fidèlement possible.</p>
                    </div>
                </div>
                <div class="card-image">
                    <img src="/assets/images/structure.jpg">
                </div>
            </div>
        </div>
        <div class="col s12 m9">
            <div class="card horizontal">
                <div class="card-image">
                    <img src="/assets/images/sound.jpg">
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <h3 class="font-bold">Bibliothèque d'annonce</h3>
                        <p>Pour une plus grande immersion, nous avons créer une nouvelle règle "Système d'annonce SIVE" qui permet d'ajouter des sons d'annonce à bord.<br>C'est son sont paramétrable directement en session.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

