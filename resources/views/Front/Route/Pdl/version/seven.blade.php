@extends('Front.Route.Pdl.layout.app')

@section("style")
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
    </style>
@endsection

@section("content")
<section class="section">
    <div class="container">
        <div class="row">
            <div class="col s12">
                <div class="card">
                    <h2 class="card-content center-align">Version 7: Restructuration des lignes</h2>
                </div>
            </div>
            <div class="col s3">
                &nbsp;
            </div>
            <div class="col s9">
                <div class="card">
                    <div class="card-image">
                        <img src="/assets/images/version_seven.png">
                        <!--<span class="card-title">Card Title</span>-->
                    </div>
                    <div class="card-content">
                        <div class="video-container">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/s0NfE4ptvTE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <iframe width="100%" height="300px" frameBorder="0" allowfullscreen src="https://framacarte.org/fr/map/version-4_65335?scaleControl=false&miniMap=false&scrollWheelZoom=true&zoomControl=true&allowEdit=false&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=undefined&captionBar=false&datalayers=113168"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section("script")
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAM7Eilt9-Zti-fDYXe1dDYkSeaM49vlmY&callback=initMap">
    </script>
    <script type="text/javascript">
        var map;
        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: -34.397, lng: 150.644},
                zoom: 8
            });
        }
        (function ($) {
            $(document).ready(function(){
                $('.carousel').carousel();
                $('.tooltipped').tooltip();
            });
        })(jQuery)
    </script>
@endsection

