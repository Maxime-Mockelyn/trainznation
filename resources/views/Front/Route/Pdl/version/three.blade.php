@extends('Front.Route.Pdl.layout.app')

@section("style")
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
    </style>
@endsection

@section("content")
<section class="section">
    <div class="container">
        <div class="row">
            <div class="col s12">
                <div class="card">
                    <h2 class="card-content center-align">Version 3: Nantes <-> Angers St Laud <-> Le Mans</h2>
                </div>
            </div>
            <div class="col s3">
                <div class="card">
                    <div class="card-content">
                        <div class="kt-list-timeline">
                            <div class="kt-list-timeline__items">
                                <div class="kt-list-timeline__item">
                                    <span class="kt-list-timeline__badge kt-list-timeline__badge--success"></span>
                                    <span class="kt-list-timeline__text">
                                        <strong>Nantes</strong><br>
                                        <img src="/assets/images/pictogramme/ter.png" width="24" class="responsive-img tooltipped" alt="" data-position="top" data-tooltip="TER">
                                        <img src="/assets/images/pictogramme/tgv.png" width="24" class="responsive-img tooltipped" alt="" data-position="top" data-tooltip="TGV">
                                        <img src="/assets/images/pictogramme/tram.png" width="24" class="responsive-img tooltipped" alt="" data-position="top" data-tooltip="Reseau TAN">
                                        <img src="/assets/images/pictogramme/bus.png" width="24" class="responsive-img tooltipped" alt="" data-position="top" data-tooltip="Reseau TAN">
                                    </span>
                                </div>
                                <div class="kt-list-timeline__item">
                                    <span class="kt-list-timeline__badge kt-list-timeline__badge--dark"></span>
                                    <span class="kt-list-timeline__text">Thouaré-sur-Loire</span>
                                </div>
                                <div class="kt-list-timeline__item">
                                    <span class="kt-list-timeline__badge kt-list-timeline__badge--dark"></span>
                                    <span class="kt-list-timeline__text">Mauves-sur-Loire</span>
                                </div>
                                <div class="kt-list-timeline__item">
                                    <span class="kt-list-timeline__badge kt-list-timeline__badge--dark"></span>
                                    <span class="kt-list-timeline__text">Le Cellier</span>
                                </div>
                                <div class="kt-list-timeline__item">
                                    <span class="kt-list-timeline__badge kt-list-timeline__badge--dark"></span>
                                    <span class="kt-list-timeline__text">Oudon</span>
                                </div>
                                <div class="kt-list-timeline__item">
                                    <span class="kt-list-timeline__badge kt-list-timeline__badge--danger"></span>
                                    <span class="kt-list-timeline__text">
                                        <strong>Ancenis</strong><br>
                                        <img src="/assets/images/pictogramme/ter.png" width="24" class="responsive-img tooltipped" alt="" data-position="top" data-tooltip="TER">
                                        <img src="/assets/images/pictogramme/tgv.png" width="24" class="responsive-img tooltipped" alt="" data-position="top" data-tooltip="TGV">
                                    </span>
                                </div>
                                <div class="kt-list-timeline__item">
                                    <span class="kt-list-timeline__badge kt-list-timeline__badge--dark"></span>
                                    <span class="kt-list-timeline__text">Varades</span>
                                </div>
                                <div class="kt-list-timeline__item">
                                    <span class="kt-list-timeline__badge kt-list-timeline__badge--dark"></span>
                                    <span class="kt-list-timeline__text">Ingrandes-sur-Loire</span>
                                </div>
                                <div class="kt-list-timeline__item">
                                    <span class="kt-list-timeline__badge kt-list-timeline__badge--dark"></span>
                                    <span class="kt-list-timeline__text">Champtocé-sur-Loire</span>
                                </div>
                                <div class="kt-list-timeline__item">
                                    <span class="kt-list-timeline__badge kt-list-timeline__badge--danger"></span>
                                    <span class="kt-list-timeline__text">
                                        <strong>La Possonière</strong><br>
                                        <img src="/assets/images/pictogramme/ter.png" width="24" class="responsive-img tooltipped" alt="" data-position="top" data-tooltip="TER">
                                    </span>
                                </div>
                                <div class="kt-list-timeline__item">
                                    <span class="kt-list-timeline__badge kt-list-timeline__badge--dark"></span>
                                    <span class="kt-list-timeline__text">Savennières Behuard</span>
                                </div>
                                <div class="kt-list-timeline__item">
                                    <span class="kt-list-timeline__badge kt-list-timeline__badge--success"></span>
                                    <span class="kt-list-timeline__text">
                                        <strong>Angers St Laud</strong><br>
                                        <img src="/assets/images/pictogramme/ter.png" width="24" class="responsive-img tooltipped" alt="" data-position="top" data-tooltip="TER">
                                        <img src="/assets/images/pictogramme/tgv.png" width="24" class="responsive-img tooltipped" alt="" data-position="top" data-tooltip="TGV">
                                        <img src="/assets/images/pictogramme/tram.png" width="24" class="responsive-img tooltipped" alt="" data-position="top" data-tooltip="Reseau Irigo">
                                        <img src="/assets/images/pictogramme/bus.png" width="24" class="responsive-img tooltipped" alt="" data-position="top" data-tooltip="Reseau Irigo">
                                    </span>
                                </div>
                                <div class="kt-list-timeline__item">
                                    <span class="kt-list-timeline__badge kt-list-timeline__badge--dark"></span>
                                    <span class="kt-list-timeline__text">Angers - Maitre Ecole</span>
                                </div>
                                <div class="kt-list-timeline__item">
                                    <span class="kt-list-timeline__badge kt-list-timeline__badge--dark"></span>
                                    <span class="kt-list-timeline__text">Le Vieux-Briolay</span>
                                </div>
                                <div class="kt-list-timeline__item">
                                    <span class="kt-list-timeline__badge kt-list-timeline__badge--dark"></span>
                                    <span class="kt-list-timeline__text">Tiercé</span>
                                </div>
                                <div class="kt-list-timeline__item">
                                    <span class="kt-list-timeline__badge kt-list-timeline__badge--dark"></span>
                                    <span class="kt-list-timeline__text">Etriché - Chateauneuf</span>
                                </div>
                                <div class="kt-list-timeline__item">
                                    <span class="kt-list-timeline__badge kt-list-timeline__badge--dark"></span>
                                    <span class="kt-list-timeline__text">Morannes</span>
                                </div>
                                <div class="kt-list-timeline__item">
                                    <span class="kt-list-timeline__badge kt-list-timeline__badge--danger"></span>
                                    <span class="kt-list-timeline__text">
                                        <strong>Sablé-sur Sarthe</strong><br>
                                        <img src="/assets/images/pictogramme/ter.png" width="24" class="responsive-img tooltipped" alt="" data-position="top" data-tooltip="TER">
                                        <img src="/assets/images/pictogramme/tgv.png" width="24" class="responsive-img tooltipped" alt="" data-position="top" data-tooltip="TGV">
                                        <img src="/assets/images/pictogramme/bus.png" width="24" class="responsive-img tooltipped" alt="" data-position="top" data-tooltip="Reseau RESO">
                                    </span>
                                </div>
                                <div class="kt-list-timeline__item">
                                    <span class="kt-list-timeline__badge kt-list-timeline__badge--dark"></span>
                                    <span class="kt-list-timeline__text">Noyen</span>
                                </div>
                                <div class="kt-list-timeline__item">
                                    <span class="kt-list-timeline__badge kt-list-timeline__badge--dark"></span>
                                    <span class="kt-list-timeline__text">La Suze</span>
                                </div>
                                <div class="kt-list-timeline__item">
                                    <span class="kt-list-timeline__badge kt-list-timeline__badge--dark"></span>
                                    <span class="kt-list-timeline__text">Voivres</span>
                                </div>
                                <div class="kt-list-timeline__item">
                                    <span class="kt-list-timeline__badge kt-list-timeline__badge--success"></span>
                                    <span class="kt-list-timeline__text">
                                        <strong>Le Mans</strong><br>
                                        <img src="/assets/images/pictogramme/ter.png" width="24" class="responsive-img tooltipped" alt="" data-position="top" data-tooltip="TER">
                                        <img src="/assets/images/pictogramme/tgv.png" width="24" class="responsive-img tooltipped" alt="" data-position="top" data-tooltip="TGV">
                                        <img src="/assets/images/pictogramme/tram.png" width="24" class="responsive-img tooltipped" alt="" data-position="top" data-tooltip="Reseau SETRAM">
                                        <img src="/assets/images/pictogramme/bus.png" width="24" class="responsive-img tooltipped" alt="" data-position="top" data-tooltip="Reseau SETRAM">
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="collection">
                    <li class="collection-item avatar">
                        <img src="/assets/images/km.png" alt="" class="circle">
                        <span class="title text-accent-1"><strong>Distance</strong></span>
                        <p>184,2 km</p>
                    </li>
                    <li class="collection-item avatar">
                        <img src="/assets/images/icons/start.png" alt="" class="circle">
                        <span class="title"><strong>Point de Départ</strong></span>
                        <p>Nantes</p>
                    </li>
                    <li class="collection-item avatar">
                        <img src="/assets/images/icons/stop.png" alt="" class="circle">
                        <span class="title"><strong>Point d'arrivée</strong></span>
                        <p>Le Mans</p>
                    </li>
                </ul>
            </div>
            <div class="col s9">
                <div class="card">
                    <div class="card-image">
                        <img src="/assets/images/version_three.png">
                        <!--<span class="card-title">Card Title</span>-->
                    </div>
                    <div class="card-content">
                        <div class="video-container">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/s0NfE4ptvTE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <iframe width="100%" height="300px" frameBorder="0" allowfullscreen src="https://framacarte.org/fr/map/version-3_65316?scaleControl=true&miniMap=false&scrollWheelZoom=true&zoomControl=true&allowEdit=false&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=undefined&captionBar=false&datalayers=113136&fullscreenControl=true#9/47.6423/-0.9256"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section("script")
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAM7Eilt9-Zti-fDYXe1dDYkSeaM49vlmY&callback=initMap">
    </script>
    <script type="text/javascript">
        var map;
        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: -34.397, lng: 150.644},
                zoom: 8
            });
        }
        (function ($) {
            $(document).ready(function(){
                $('.carousel').carousel();
                $('.tooltipped').tooltip();
            });
        })(jQuery)
    </script>
@endsection

