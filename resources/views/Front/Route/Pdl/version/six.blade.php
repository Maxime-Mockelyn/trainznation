@extends('Front.Route.Pdl.layout.app')

@section("style")
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
    </style>
@endsection

@section("content")
<section class="section">
    <div class="container">
        <div class="row">
            <div class="col s12">
                <div class="card">
                    <h2 class="card-content center-align">Version 6: Savenay <-> Redon</h2>
                </div>
            </div>
            <div class="col s3">
                <div class="card">
                    <div class="card-content">
                        <div class="kt-list-timeline">
                            <div class="kt-list-timeline__items">
                                <div class="kt-list-timeline__item">
                                    <span class="kt-list-timeline__badge kt-list-timeline__badge--success"></span>
                                    <span class="kt-list-timeline__text">
                                        <strong>Savenay</strong><br>
                                        <img src="/assets/images/pictogramme/ter.png" width="24" class="responsive-img tooltipped" alt="" data-position="top" data-tooltip="TER">
                                    </span>
                                </div>
                                <div class="kt-list-timeline__item">
                                    <span class="kt-list-timeline__badge kt-list-timeline__badge--dark"></span>
                                    <span class="kt-list-timeline__text">Pontchateau</span>
                                </div>
                                <div class="kt-list-timeline__item">
                                    <span class="kt-list-timeline__badge kt-list-timeline__badge--dark"></span>
                                    <span class="kt-list-timeline__text">Dréfféac</span>
                                </div>
                                <div class="kt-list-timeline__item">
                                    <span class="kt-list-timeline__badge kt-list-timeline__badge--dark"></span>
                                    <span class="kt-list-timeline__text">St-Gildas-des-Bois</span>
                                </div>
                                <div class="kt-list-timeline__item">
                                    <span class="kt-list-timeline__badge kt-list-timeline__badge--dark"></span>
                                    <span class="kt-list-timeline__text">Sévérac</span>
                                </div>
                                <div class="kt-list-timeline__item">
                                    <span class="kt-list-timeline__badge kt-list-timeline__badge--success"></span>
                                    <span class="kt-list-timeline__text">
                                        <strong>Redon</strong><br>
                                        <img src="/assets/images/pictogramme/ter.png" width="24" class="responsive-img tooltipped" alt="" data-position="top" data-tooltip="TER">
                                        <img src="/assets/images/pictogramme/tgv.png" width="24" class="responsive-img tooltipped" alt="" data-position="top" data-tooltip="TGV">
                                        <img src="/assets/images/pictogramme/bus.png" width="24" class="responsive-img tooltipped" alt="" data-position="top" data-tooltip="BUS URBAIN">
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="collection">
                    <li class="collection-item avatar">
                        <img src="/assets/images/km.png" alt="" class="circle">
                        <span class="title text-accent-1"><strong>Distance</strong></span>
                        <p>42,33 km</p>
                    </li>
                    <li class="collection-item avatar">
                        <img src="/assets/images/icons/start.png" alt="" class="circle">
                        <span class="title"><strong>Point de Départ</strong></span>
                        <p>Savenay</p>
                    </li>
                    <li class="collection-item avatar">
                        <img src="/assets/images/icons/stop.png" alt="" class="circle">
                        <span class="title"><strong>Point d'arrivée</strong></span>
                        <p>Redon</p>
                    </li>
                </ul>
            </div>
            <div class="col s9">
                <div class="card">
                    <div class="card-image">
                        <img src="/assets/images/version_six.jpg">
                        <!--<span class="card-title">Card Title</span>-->
                    </div>
                    <div class="card-content">
                        <div class="video-container">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/s0NfE4ptvTE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <iframe width="100%" height="300px" frameBorder="0" allowfullscreen src="https://framacarte.org/fr/map/version-4_65335?scaleControl=false&miniMap=false&scrollWheelZoom=true&zoomControl=true&allowEdit=false&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=undefined&captionBar=false&datalayers=113168"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section("script")
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAM7Eilt9-Zti-fDYXe1dDYkSeaM49vlmY&callback=initMap">
    </script>
    <script type="text/javascript">
        var map;
        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: -34.397, lng: 150.644},
                zoom: 8
            });
        }
        (function ($) {
            $(document).ready(function(){
                $('.carousel').carousel();
                $('.tooltipped').tooltip();
            });
        })(jQuery)
    </script>
@endsection

