<div class="container">
    <nav class="red lighten-1">
        <div class="nav-wrapper">
            <a href="{{ route('home') }}" class="brand-logo valign-wrapper" style="left: 15px;">
                LIGNE PAYS DE LA LOIRE
            </a>
            <ul class="right hide-on-med-and-down">
                <li class="{{ currentRoute(route('Route.Pdl.index')) }}"><a href="{{ route('Route.Pdl.index') }}">Accueil</a></li>
                <li><a class="dropdown-trigger" href="#!" data-target="dropdown1"><i class="material-icons right">arrow_drop_down</i> Version</a></li>
                <li class="{{ currentRoute(route('Route.Pdl.gallerie')) }}"><a href="{{ route('Route.Pdl.gallerie') }}">Gallerie</a></li>
                <li class="{{ currentRoute(route('Route.Pdl.labs')) }}"><a href="{{ route('Route.Pdl.labs') }}">Avancement</a></li>
                <li class="{{ currentRoute(route('Route.Pdl.download')) }}"><a href="{{ route('Route.Pdl.download') }}">Téléchargement</a></li>
                <li><a href="//{{ env('APP_URL') }}"><i class="material-icons right">laptop</i> Retour a trainznation</a></li>

            </ul>
        </div>
        <ul id="dropdown1" class="dropdown-content">
            <li class="{{ currentRoute(route('Pdl.Version.one')) }}"><a href="{{ route('Pdl.Version.one') }}">Version 1</a></li>
            <li class="{{ currentRoute(route('Pdl.Version.two')) }}"><a href="{{ route('Pdl.Version.two') }}">Version 2</a></li>
            <li class="{{ currentRoute(route('Pdl.Version.three')) }}"><a href="{{ route('Pdl.Version.three') }}">Version 3</a></li>
            <li class="{{ currentRoute(route('Pdl.Version.four')) }}"><a href="{{ route('Pdl.Version.four') }}">Version 4</a></li>
            <li class="{{ currentRoute(route('Pdl.Version.five')) }}"><a href="{{ route('Pdl.Version.five') }}">Version 5</a></li>
            <li class="{{ currentRoute(route('Pdl.Version.six')) }}"><a href="{{ route('Pdl.Version.six') }}">Version 6</a></li>
            <li class="{{ currentRoute(route('Pdl.Version.seven')) }}"><a href="{{ route('Pdl.Version.seven') }}">Version 7</a></li>
            <li class="{{ currentRoute(route('Pdl.Version.huit')) }}"><a href="{{ route('Pdl.Version.huit') }}">Version 8</a></li>
        </ul>
    </nav>
</div>
