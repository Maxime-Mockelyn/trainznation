@extends("Front.Layout.app")

@section("css")

@endsection

@section("content")
    <section class="imagebg" data-gradient-bg='#4876BD,#5448BD,#8F48BD,#BD48B1'>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Route</h1>
                    <ol class="breadcrumbs">
                        <li>
                            <a href="#">{{ env("APP_NAME") }}</a>
                        </li>
                        <li>Route</li>
                    </ol>
                    <hr>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="card card-2 text-center">
                        <div class="card__top">
                            <span class="label">{{ \App\HelperClass\Route::latestBuildForRoute() }}</span>
                            <a href="{{ route('Route.Pdl.index') }}">
                                <img alt="Image" src="{{ \App\HelperClass\Route::latestImageContentForRoute('pdl') }}">
                            </a>
                        </div>
                        <div class="card__body">
                            <h4>Ligne Pays de la Loire</h4>
                            <!--<span class="type--fade">Video Tutorial Series</span>-->
                            <p>
                                Retranscription réel de la région pays de la loire ainsi que la LGV Atlantique
                            </p>
                        </div>
                        <div class="card__bottom text-center">
                            <div class="card__action">
                                <span class="h6 type--uppercase">En savoir plus</span>
                                <a href="{{ route('Route.Pdl.index') }}">
                                    <i class="material-icons">flip_to_front</i>
                                </a>
                            </div>
                            <div class="card__action">
                                <span class="h6 type--uppercase">Télécharger</span>
                                <a href="{{ route('Route.Pdl.download') }}">
                                    <i class="material-icons">save</i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-2 text-center">
                        <div class="card__top">
                            <span class="label bg-danger">Bientôt</span>
                            <a href="#">
                                <img alt="Image" src="/storage/route/rtp/ligne1/vignette.png">
                            </a>
                        </div>
                        <div class="card__body">
                            <h4>Réseau Transport Parisien (Métro) - Ligne 01</h4>
                            <!--<span class="type--fade">Video Tutorial Series</span>-->
                            <p>
                                Ligne 01 du métro parisien
                            </p>
                        </div>
                        <!--<div class="card__bottom text-center">
                            <div class="card__action">
                                <span class="h6 type--uppercase">En savoir plus</span>
                                <a href="{{ route('Route.Pdl.index') }}">
                                    <i class="material-icons">flip_to_front</i>
                                </a>
                            </div>
                            <div class="card__action">
                                <span class="h6 type--uppercase">Télécharger</span>
                                <a href="{{ route('Route.Pdl.download') }}">
                                    <i class="material-icons">save</i>
                                </a>
                            </div>
                        </div>-->
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-2 text-center">
                        <div class="card__top">
                            <span class="label bg-danger">Bientôt</span>
                            <a href="#">
                                <img alt="Image" src="/storage/route/rtp/ligne2/vignette.png">
                            </a>
                        </div>
                        <div class="card__body">
                            <h4>Réseau Transport Parisien (Métro) - Ligne 02</h4>
                            <!--<span class="type--fade">Video Tutorial Series</span>-->
                            <p>
                                Ligne 02 du métro parisien
                            </p>
                        </div>
                    <!--<div class="card__bottom text-center">
                            <div class="card__action">
                                <span class="h6 type--uppercase">En savoir plus</span>
                                <a href="{{ route('Route.Pdl.index') }}">
                                    <i class="material-icons">flip_to_front</i>
                                </a>
                            </div>
                            <div class="card__action">
                                <span class="h6 type--uppercase">Télécharger</span>
                                <a href="{{ route('Route.Pdl.download') }}">
                                    <i class="material-icons">save</i>
                                </a>
                            </div>
                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section("script")

@endsection
