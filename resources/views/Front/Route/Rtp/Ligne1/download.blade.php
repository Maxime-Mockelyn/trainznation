@extends('Front.Route.Rtp.Ligne1.layout.app')

@section("content")
<section class="section no-pad-bot">
    <div class="container">
        <div class="col s12 m9">
            <div class="card horizontal">
                <div class="card-image">
                    <img src="/assets/images/cartographie.jpg">
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <h3 class="font-bold">Dernière Version</h3>
                        <table>
                            <tr>
                                <td>Version:</td>
                                <td>{{ $latest->version }}</td>
                            </tr>
                            <tr>
                                <td>Build:</td>
                                <td>{{ $latest->build }}</td>
                            </tr>
                            <tr>
                                <td>Type:</td>
                                <td>{{ \App\HelperClass\PdlDownload::formatReleaseDownload($latest->typeRelease) }}</td>
                            </tr>
                        </table>
                        <div class="center-align">
                            <a href="{{ $latest->linkDownload }}" class="waves-effect waves-light btn-large tooltipped" data-position="left" data-tooltip="Télécharger"><i class="material-icons">cloud</i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section no-pad-bot">
    <div class="container-fluid">
        <div class="col s12">
            <div class="card">
                <div class="card-stacked">
                    <div class="card-content">
                        <ul class="collapsible popout">
                            @foreach($downloads as $download)
                            <li>
                                <div class="collapsible-header">
                                    V{{ $download->version }}:{{ $download->build }}
                                    <span class="badge">{{ \App\HelperClass\PdlDownload::formatReleaseDownload($download->typeRelease) }}</span>
                                </div>
                                <div class="collapsible-body">
                                    {!! \Michelf\Markdown::defaultTransform($download->note) !!}
                                    <div class="center-align">
                                        <a href="{{ $download->linkDownload }}" class="waves-effect waves-light btn-large"><i class="material-icons right">cloud</i> Téléchargement</a>
                                    </div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('script')
    <script type="text/javascript">
        (function ($) {
            $('.tooltipped').tooltip();
            $('.collapsible').collapsible();
        })(jQuery)
    </script>
@endsection

