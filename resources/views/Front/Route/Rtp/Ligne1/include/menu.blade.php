<div class="container">
    <nav class="red lighten-1">
        <div class="nav-wrapper">
            <a href="{{ route('home') }}" class="brand-logo valign-wrapper" style="left: 15px;">
                RTP - Ligne 01
            </a>
            <ul class="right hide-on-med-and-down">
                <li class="{{ currentRoute(route('Rtp.Ligne1.index')) }}"><a href="{{ route('Route.Pdl.index') }}">Accueil</a></li>
                <li class="{{ currentRoute(route('Rtp.Ligne1.gallerie')) }}"><a href="{{ route('Route.Pdl.gallerie') }}">Gallerie</a></li>
                <li class="{{ currentRoute(route('Rtp.Ligne1.download')) }}"><a href="{{ route('Route.Pdl.download') }}">Téléchargement</a></li>
                <li><a href="//{{ env('APP_URL') }}"><i class="material-icons right">laptop</i> Retour a trainznation</a></li>

            </ul>
        </div>
    </nav>
</div>
