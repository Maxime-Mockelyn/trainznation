@extends("Front.Layout.app")

@section("css")

@endsection

@section("content")
        <?php
            $first = \App\Repository\Blog\BlogRepository::staticGetFirstPost();
        ?>
        <section class="space--xs imagebg" data-gradient-bg='#4876BD,#5448BD,#8F48BD,#BD48B1'>
            <div class="container">
                <div class="cta cta--horizontal text-center-xs row">
                    <div class="col-md-4">
                        <h4>Nouveauté</h4>
                    </div>
                    <div class="col-md-5">
                        <p class="lead">
                            Découvrez les nouveautés du site trainznation
                        </p>
                    </div>
                    <div class="col-md-3 text-right text-center-xs">
                        <a class="btn btn--primary type--uppercase" href="#" id="start_tour">
                                                <span class="btn__text">
                                                    Découvrir
                                                </span>
                        </a>
                    </div>
                </div>
            </div>
            <!--end of container-->
        </section>
    <section class="height-80 imagebg" data-overlay="1">
        <div class="background-image-holder">
            <img alt="background" src="/storage/blog/{{ $first->id }}.png" />
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="typed-headline">
                        <span class="h1 inline-block">{{ $first->title }}</span><br>
                        <p class="inline-block">{{ $first->short_content }}</p>
                    </div>
                    <a class="btn btn--primary type--uppercase" href="{{ route('Front.Blog.show', $first->slug) }}">
                                <span class="btn__text">
                                    Voir l'article
                                </span>
                    </a>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section class="space--md">
        <div class="container" id="tour_tutoriel">
            <h1>Derniers Tutoriels</h1>
            <div class="row">
                @foreach($tutoriels as $tutoriel)
                <div class="col-md-3">
                    <div class="card card-1 boxed boxed--sm boxed--border {!! stateTutoriel($tutoriel->published) !!}">
                        <div class="card__top">
                            <div class="card__avatar">
                                <span>
                                    <strong>{{ $tutoriel->category->name }}</strong>
                                </span>
                            </div>
                            <div class="card__meta">
                                <span>
                                    <i class="icon-Timer"></i>
                                    @if($tutoriel->time != null || $tutoriel->time != 0)
                                        {{ $tutoriel->time }}
                                    @else
                                        NC
                                    @endif
                                </span>
                            </div>
                        </div>
                        <div class="card__body">
                            <a href="{{ route("Front.Tutoriel.show", [$tutoriel->subcategory->id, $tutoriel->slug]) }}">
                                <img src="{{ sourceImage('learning/'.$tutoriel->id.'.png') }}" alt="Image" class="border--round">
                            </a>
                            <p>{{ $tutoriel->title }}</p>
                        </div>
                        <div class="card__bottom">
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                    <div class="card__action">
                                        <a href="{{ route("Front.Tutoriel.show", [$tutoriel->subcategory->id, $tutoriel->slug]) }}#comment">
                                            <i class="material-icons">comment</i>
                                            <span>{{ \App\Repository\Tutoriel\TutorielCommentRepository::getCountFromTuto($tutoriel->id) }}</span>
                                        </a>
                                    </div>
                                </li>
                                @if($tutoriel->published == 2)
                                    <li class="list-inline-item h6 color--warning">
                                        <span class="countdown" data-date="{{ \Carbon\Carbon::createFromTimestamp(strtotime($tutoriel->published_at))->format('m/d/Y H:i:s') }}"></span>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <section class="space--sm">
        <div class="container" id="tour_news">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="masonry">
                        <div class="masonry-filter-container d-flex align-items-center">
                            <span>Catégorie:</span>
                            <div class="masonry-filter-holder">
                                <div class="masonry__filters" data-filter-all-text="Toutes les Catégorie"></div>
                            </div>
                        </div>
                        <hr>
                        <div class="masonry__container row">
                            <div class="masonry__item col-md-6"></div>
                            @foreach($blogs as $blog)
                            <div class="masonry__item col-md-12" data-masonry-filter="{{ $blog->categorie->name }}">
                                <article class="feature feature-1">
                                    <a href="{{ route('Front.Blog.show', $blog->slug) }}" class="block">
                                        <img alt="Image" src="{{ sourceImage('blog/'.$blog->id.'.png') }}" />
                                    </a>
                                    <div class="feature__body boxed boxed--border">
                                        <span>{{ $blog->published_at->format("d/m/Y à H:i") }}</span>
                                        <h5>{{ $blog->title }}</h5>
                                        <span>{{ $blog->short_content }}</span>
                                        <a href="{{ route('Front.Blog.show', $blog->slug) }}">
                                            En savoir plus
                                        </a>
                                    </div>
                                </article>
                            </div>
                            @endforeach
                            <!--end item-->
                        </div>
                        <!--end of masonry container-->
                    </div>
                    <!--end masonry-->
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
@endsection

@section("script")

@endsection
