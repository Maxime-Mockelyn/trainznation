@extends("Front.Layout.app")

@section("css")

@endsection

@section("content")
    <section class="imagebg" data-gradient-bg='#4876BD,#5448BD,#8F48BD,#BD48B1'>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Tutoriel - {{ $subcategorie->name }}</h1>
                    <ol class="breadcrumbs">
                        <li>
                            <a href="#">{{ env("APP_NAME") }}</a>
                        </li>
                        <li>Téléchargement</li>
                        <li>{{ $subcategorie->category->name }}</li>
                        <li>{{ $subcategorie->name }}</li>
                    </ol>
                    <hr>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section class="space--sm">
        <div class="container">
            <div class="row">
                @foreach($tutoriels as $tutoriel)
                    <div class="col-md-3">
                        <div class="card card-1 boxed boxed--sm boxed--border {!! stateTutoriel($tutoriel->published) !!}">
                            <div class="card__top">
                                <div class="card__avatar">
                                <span>
                                    <strong>{{ $tutoriel->category->name }}</strong>
                                </span>
                                </div>
                                <div class="card__meta">
                                <span>
                                    <i class="icon-Timer"></i>
                                    @if($tutoriel->time != null || $tutoriel->time != 0)
                                        {{ $tutoriel->time }}
                                    @else
                                        NC
                                    @endif
                                </span>
                                </div>
                            </div>
                            <div class="card__body">
                                <a href="{{ route('Front.Tutoriel.show', [$tutoriel->subcategory->id, $tutoriel->slug]) }}">
                                    <img src="{{ sourceImage('learning/'.$tutoriel->id.'.png') }}" alt="Image" class="border--round">
                                </a>
                                <p><strong>{{ $tutoriel->title }}</strong></p>
                                {{ $tutoriel->short_content }}
                            </div>
                            <div class="card__bottom">
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <div class="card__action">
                                            <a href="#">
                                                <i class="material-icons">comment</i>
                                                <span>{{ \App\Repository\Tutoriel\TutorielCommentRepository::getCountFromTuto($tutoriel->id) }}</span>
                                            </a>
                                        </div>
                                    </li>
                                    @if($tutoriel->published == 2)
                                        <li class="list-inline-item h6 color--warning">
                                            <span class="countdown" data-date="{{ \Carbon\Carbon::createFromTimestamp(strtotime($tutoriel->published_at))->format('m/d/Y H:i:s') }}"></span>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection

@section("script")

@endsection
