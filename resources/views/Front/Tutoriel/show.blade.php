@extends("Front.Layout.app")
@section("title") {{ $tutoriel->title }} @endsection
@section("description") {{ substr($tutoriel->content, 0, 120) }} @endsection

@section("css")

@endsection

@section("content")
    <section class="text-center imagebg" data-overlay='4'>
        <div class="background-image-holder">
            <img alt="background" src="{{ sourceImage('learning/'.$tutoriel->id.'_cover.png') }}" />
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-10">
                    <div class="video-cover border--round">
                        <div class="background-image-holder">
                            <img alt="image" src="{{ sourceImage('learning/'.$tutoriel->id.'.png') }}" />
                        </div>
                        @if($tutoriel->published == 2)
                            @auth()
                                @if(auth()->user()->account->premium == 1)
                                    <div class="video-play-icon"></div>
                                @else
                                    <span class="countdown h3" data-date="{{ \Carbon\Carbon::createFromTimestamp(strtotime($tutoriel->published_at))->format('m/d/Y H:i:s') }}"></span>
                                @endif
                            @else
                                <span class="countdown h3" data-date="{{ \Carbon\Carbon::createFromTimestamp(strtotime($tutoriel->published_at))->format('m/d/Y H:i:s') }}"></span>
                            @endauth
                        @endif
                        <iframe data-src="https://www.youtube.com/embed/{{ $tutoriel->linkVideo }}?autoplay=1" allowfullscreen="allowfullscreen"></iframe>
                    </div>
                    <!--end video cover-->
                    <span class="h2 type--bold">{{ $tutoriel->title }}</span>
                    <span class="h5">{{ $tutoriel->category->name }} - {{ $tutoriel->subcategory->name }}</span>
                    @if($tutoriel->source == 1)
                        @auth()
                            @if(auth()->user()->account->premium == 1)
                                <a class="btn btn--primary btn--icon" href="https://download.{{ env('APP_DOMAIN') }}/{{ $tutoriel->sources->pathSource }}">
                                <span class="btn__text">
                                    <i class="icon-upload"></i> Télécharger les sources</span>
                                </a>
                            @else
                                <div class="modal-instance">
                                    <a class="btn btn--primary btn--icon modal-trigger" href="https://download.{{ env('APP_DOMAIN') }}/{{ $tutoriel->sources->pathSource }}">
                                <span class="btn__text">
                                    <i class="icon-Download"></i> Télécharger les sources</span>
                                    </a>
                                    <div class="modal-container">
                                        <div class="modal-content">
                                            <section class="imageblock feature-large bg--white border--round ">
                                                <div class="imageblock__content col-lg-5 col-md-3 pos-left">
                                                    <div class="background-image-holder">
                                                        <img alt="image" src="{{ sourceImage('other/membre-premium.jpg') }}" />
                                                    </div>
                                                </div>
                                                <div class="container">
                                                    <div class="row justify-content-end">
                                                        <div class="col-lg-7">
                                                            <div class="row justify-content-center">
                                                                <div class="col-lg-10 col-md-11">
                                                                    <h2>OOPS !!!</h2>
                                                                    <p>Vous n'êtes pas premium, vous n'avez donc pas la possibilité de télécharger les sources de ce tutoriel !</p>
                                                                    <h3>Veuillez vous rendre dans votre espace et souscrire à une offre premium !</h3>
                                                                </div>
                                                            </div>
                                                            <!--end of row-->
                                                        </div>
                                                        <!--end of col-->
                                                    </div>
                                                    <!--end of row-->
                                                </div>
                                                <!--end of container-->
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @else
                            <a class="btn btn--primary btn--icon" href="https://download.{{ env('APP_DOMAIN') }}/{{ $tutoriel->sources->pathSource }}">
                                <span class="btn__text">
                                    <i class="icon-upload"></i> Télécharger les sources</span>
                            </a>
                        @endauth
                    @endif
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section class="space--xxs">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <span>Tutoriel / {{ $tutoriel->category->name }} / {{ $tutoriel->subcategory->name }} / {{ $tutoriel->title }}</span>
                </div>
                <div class="col-md-4 text-right">Il y a {{ \Carbon\Carbon::createFromTimestamp(strtotime($tutoriel->published_at))->diffForHumans(['syntax' => \Carbon\CarbonInterface::DIFF_ABSOLUTE]) }}</div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-1">
                    <ul>
                        <li><a href="#comment"><i class="icon-Speach-Bubble"></i> </a></li>
                    </ul>
                </div>
                <div class="col-md-7">
                    <div id="description" data-id="{{ $tutoriel->slug }}" data-category="{{ $tutoriel->tutoriel_sub_category_id }}"></div>
                </div>
                <div class="col-md-4">
                    <span class="h4 type--bold">De quoi sa parle ?</span>
                    <table>
                        @foreach($tutoriel->technos as $techno)
                            <tr>
                                <td>{{ $techno->techno }}</td>
                            </tr>
                        @endforeach
                    </table>
                    <span class="h4 type--bold">Ce que vous devez connaitre ?</span>
                    <table>
                        @foreach($tutoriel->experiences as $experience)
                            <tr>
                                <td>{{ $experience->experience }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </section>
    <section class="space--sm">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <hr>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-10 col-lg-8">
                    <div class="comments">
                        <h3>{{ \App\Repository\Tutoriel\TutorielCommentRepository::getCountFromTuto($tutoriel->id) }} {{ formatPlural('Commentaire', \App\Repository\Tutoriel\TutorielCommentRepository::getCountFromTuto($tutoriel->id)) }}</h3>
                        <ul class="comments__list">
                            @foreach(\App\Repository\Tutoriel\TutorielCommentRepository::getComments($tutoriel->id) as $comment)
                                @if($comment->published == 1)
                                    <li>
                                        <div class="comment">
                                            <div class="comment__avatar">
                                                @if($comment->user_id == 1)
                                                    <img alt="Image" src="/storage/avatar/trainznation.png" />
                                                @else
                                                    <img alt="Image" src="/storage/avatar/{{ $comment->user_id }}.png" />
                                                @endif
                                            </div>
                                            <div class="comment__body">
                                                <h5 class="type--fine-print">{{ $comment->user->name }}</h5>
                                                <div class="comment__meta">
                                                    <span>{{ $comment->published_at->format("d/m/Y à H:i") }}</span>
                                                </div>

                                            </div>
                                        </div>
                                        <!--end comment-->
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                    <!--end comments-->
                    <div class="comments-form">
                        <h4>Laissez un commentaire</h4>
                        @auth()
                            <form class="row" action="{{ route('Tutoriel.Comment.store', [$tutoriel->subcategory->id, $tutoriel->id]) }}" method="post">
                                @csrf
                                <div class="col-md-12">
                                    <label>Commentaire:</label>
                                    <textarea rows="4" name="comment" placeholder="Tapez votre commentaire Ici"></textarea>
                                </div>
                                <div class="col-md-3">
                                    <button class="btn btn--primary" type="submit">Envoyer</button>
                                </div>
                            </form>
                        @else
                            <div class="alert bg--error">
                                <div class="alert__body">
                                    <span><i class="material-icons color--warning">warning</i> <strong>Attention !</strong> Vous devez être connecter avant de poster un commentaire</span>
                                </div>
                                <div class="alert__close">×</div>
                            </div>
                        @endauth
                    </div>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
@endsection

@section("script")
<script src="/assets/custom/vendors/markdown-js/markdown.js"></script>
    <script type="text/javascript">
        function loadDescription()
        {
            let description = $("#description")

            $.ajax({
                url: '/tutoriel/'+description.attr('data-category')+'/'+description.attr('data-id')+'/getContenue',
                success: function (data) {
                    let md_content = data[0]
                    description.html(markdown.toHTML(md_content))
                }
            })
        }

        (function ($) {
            loadDescription()
        })(jQuery)
    </script>
@endsection
