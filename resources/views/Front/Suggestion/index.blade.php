@extends("Front.Layout.app")
@section("title") Suggestion @endsection
@section("description") Liste des Suggestions @endsection

@section("css")

@endsection

@section("content")
    <section class="imagebg" data-gradient-bg='#4876BD,#5448BD,#8F48BD,#BD48B1'>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Suggestions</h1>
                    <ol class="breadcrumbs">
                        <li>
                            <a href="#">{{ env("APP_NAME") }}</a>
                        </li>
                        <li>Suggestions</li>
                    </ol>
                    <hr>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section class="space--sm">
        <div class="container">
            <div class="modal-instance block">
                <a href="" class="btn modal-trigger">
                    <span class="btn__text">Nouvelle Suggestion</span>
                </a>
                <div class="modal-container">
                    <div class="modal-content">
                        <section class="imageblock feature-large bg--white border--round ">
                            <div class="imageblock__content col-lg-5 col-md-3 pos-left">
                                <div class="background-image-holder">
                                    <img alt="image" src="/storage/other/idea.png" />
                                </div>
                            </div>
                            <div class="container">
                                <div class="row justify-content-end">
                                    <div class="col-lg-6 col-md-7">
                                        <div class="row">
                                            <div class="col-md-11 col-lg-10">
                                                <h1>Nouvelle suggestion</h1>
                                                <p class="lead">
                                                    Vous avez une idée où vous avez décelé un problème avec nos créations, poster une suggestion afin que nous corrigions le problème.
                                                </p>
                                                <hr class="short">
                                                <form action="{{ route('Suggestion.store') }}" method="post">
                                                    @csrf
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <input type="text" name="name" placeholder="Votre pseudo" required/>
                                                        </div>
                                                        <div class="col-12">
                                                            <input type="text" name="email" placeholder="Votre email" required/>
                                                        </div>
                                                        <div class="col-12">
                                                            <select name="type" required>
                                                                <option value="0">Materiel</option>
                                                                <option value="1">Route</option>
                                                                <option value="2">Autre</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-12">
                                                            <input type="text" name="sujet" placeholder="Nom de la route, matériel ou sujet à aborder" required/>
                                                        </div>
                                                        <div class="col-12">
                                                            <textarea name="suggestion" id="" cols="30"
                                                                      rows="10" required></textarea>
                                                        </div>
                                                        <div class="col-12">
                                                            <button type="submit" class="btn btn--primary type--uppercase">Soumettre la suggestion</button>
                                                        </div>
                                                    </div>
                                                    <!--end row-->
                                                </form>
                                            </div>
                                            <!--end of col-->
                                        </div>
                                        <!--end of row-->
                                    </div>
                                </div>
                                <!--end of row-->
                            </div>
                            <!--end of container-->
                        </section>
                    </div>
                </div>
            </div>
            <table class="border--round table--alternate-row">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Pseudo</th>
                    <th>Infos</th>
                    <th>Statut</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($suggestions as $suggestion)
                    <tr>
                        <td>{{ $suggestion->id }}</td>
                        <td>@ {{ $suggestion->name }}</td>
                        <td>{!! \App\HelperClass\Suggestion::typeSuggestion($suggestion->type) !!} - {{ $suggestion->sujet }}</td>
                        <td>{!! \App\HelperClass\Suggestion::stateSuggestion($suggestion->state) !!}</td>
                        <td>
                            <a href="{{ route('Suggestion.show', $suggestion->id) }}" class="btn">Voir la suggestion </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
@endsection

@section("script")

@endsection
