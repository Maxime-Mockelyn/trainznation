@extends("Front.Layout.app")
@section("title") Suggestion @endsection
@section("description") {{ $suggestion->sujet }} @endsection

@section("css")

@endsection

@section("content")
    <section class="imagebg">
        <div class="background-image-holder" style="background: url('/storage/other/idea.png'); opacity: 1;">
            <img alt="background" src="/storage/other/idea.png">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>{{ $suggestion->sujet }}</h1>
                    <ol class="breadcrumbs">
                        <li>
                            <a href="#">{{ env("APP_NAME") }}</a>
                        </li>
                        <li>Suggestions</li>
                        <li>{{ $suggestion->sujet }}</li>
                    </ol>
                    <hr>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section class="space--sm">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="boxed boxed--lg boxed--border">
                        <div class="text-block text-center">
                            <img alt="{{ \App\HelperClass\Suggestion::typeSuggestion($suggestion->type) }}" src="{!! \App\HelperClass\Suggestion::typeImageSuggestion($suggestion->type) !!}" class="image--sm" data-tooltip="{{ \App\HelperClass\Suggestion::typeSuggestion($suggestion->type) }}">
                            <span class="h5">{{ $suggestion->sujet }}</span>
                            <span>par {{ $suggestion->name }}</span>
                            {!! \App\HelperClass\Suggestion::stateLabelSuggestion($suggestion->state) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="boxed boxed--lg boxed--border">
                        <h4>{{ $suggestion->sujet }}</h4>
                        <p class="lead">{{ $suggestion->suggestion }}</p>
                    </div>
                    <div class="boxed boxed--lg boxed--border">
                        <h4>Actualité de la suggestion</h4>
                        <ul class="process-stepper clearfix" data-process-steps="{{ \App\HelperClass\Suggestion::countTimelineForSuggestion($suggestion->id) }}">
                            @foreach($suggestion->timelines as $timeline)
                                <li class="active">
                                    <h4>{{ $timeline->title }}</h4>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
@endsection

@section("script")

@endsection
