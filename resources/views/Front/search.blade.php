@extends("Front.Layout.app")

@section("css")

@endsection

@section("content")
    <section class="text-center">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-10 col-lg-8">
                    <h2>Affichage des résultat pour &ldquo;{{ $search }}&rdquo;</h2>
                    <p class="lead">
                        <span>{{ $counts }}</span> {{ formatPlural('Résultat', $counts) }} {{ formatPlural('trouvé', $counts) }}
                    </p>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section>
        <div class="container">
            <div class="row">
                <h2>Objets ({{ $count['countAsset'] }} {{ formatPlural('Résultat', $count['countAsset']) }})</h2>
                <div class="col-md-12">
                    <div class="masonry masonry--tiles">
                        <div class="masonry__container row">
                            <div class="masonry__item col-md-6"></div>
                            @foreach($assets as $asset)
                                <div class="masonry__item col-md-4">
                                    <div class="product product--tile bg--secondary text-center">
                                        @if($asset->pricing == 0)
                                            <span class="label bg--success">Gratuit</span>
                                        @else
                                            <span class="label bg--error">Payant</span>
                                        @endif
                                        <a href="{{ route('Front.Download.show', [$asset->asset_sub_categorie_id, $asset->id]) }}">
                                            <img alt="Image" src="/storage/download/{{ $asset->id }}.png" />
                                        </a>
                                        <a class="block" href="{{ route('Front.Download.show', [$asset->asset_sub_categorie_id, $asset->id]) }}">
                                            <div>
                                                <h5>{{ $asset->designation }}</h5>
                                                <span> {{ $asset->short_description }}</span>
                                            </div>
                                            <div>
                                                @if($asset->pricing == 1)
                                                    <span class="h4 inline-block">{{ formatCurrency($asset->price) }}</span>
                                                @endif
                                            </div>
                                        </a>
                                    </div>
                                </div>
                        @endforeach
                        <!--end item-->
                        </div>
                        <!--end masonry container-->
                    </div>
                </div>
            </div>
            <div class="row">
                <h2>Articles ({{ $count['countBlog'] }} {{ formatPlural('Résultat', $count['countBlog']) }})</h2>
                <div class="col-md-12">
                    <div class="masonry">
                        <div class="masonry__container row">
                            <div class="masonry__item col-lg-4 col-md-6"></div>
                            @foreach($blogs as $blog)
                            <div class="masonry__item col-lg-4 col-md-6" data-masonry-filter="{{ $blog->categorie->name }}">
                                <article class="feature feature-1">
                                    <a href="{{ route('Front.Blog.show', $blog->slug) }}" class="block">
                                        <img alt="Image" src="/storage/blog/{{ $blog->id }}.png" />
                                    </a>
                                    <div class="feature__body boxed boxed--border">
                                        <span>{{ $blog->published_at->format('d/m/Y à H:i') }}</span>
                                        <h5>{{ $blog->title }}</h5>
                                        <a href="{{ route('Front.Blog.show', $blog->slug) }}">
                                            Lire l'article
                                        </a>
                                    </div>
                                </article>
                            </div>
                            @endforeach
                        </div>
                        <!--end of masonry container-->
                    </div>
                </div>
            </div>
            <div class="row">
                <h2>Suggestions ({{ $count['countSuggest'] }} {{ formatPlural('Résultat', $count['countSuggest']) }})</h2>
                @foreach($suggests as $suggest)
                    <div class="col-md-12">
                        <div class="feature {{ \App\HelperClass\Suggestion::colorFeaturedStateOfFront($suggest->state) }} feature-1 boxed boxed--border">
                            <h5>{{ $suggest->sujet }}</h5>
                            <p>
                                {{ \Illuminate\Support\Str::limit($suggest->suggestion, '100', '...') }}
                            </p>
                            <a href="{{ route('Suggestion.show', $suggest->id) }}">
                                En savoir plus
                            </a>
                            <span class="label">{{ \App\HelperClass\Suggestion::typeSuggestion($suggest->type) }}</span>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="row">
                <h2>Tutoriels ({{ $count['countTutoriel'] }} {{ formatPlural('Résultat', $count['countTutoriel']) }})</h2>
                <div class="col-md-12">
                    @foreach($tutoriels as $tutoriel)
                        <div class="col-md-3">
                            <div class="card card-1 boxed boxed--sm boxed--border {!! stateTutoriel($tutoriel->published) !!}">
                                <div class="card__top">
                                    <div class="card__avatar">
                                <span>
                                    <strong>{{ $tutoriel->category->name }}</strong>
                                </span>
                                    </div>
                                    <div class="card__meta">
                                <span>
                                    <i class="icon-Timer"></i>
                                    @if($tutoriel->time != null || $tutoriel->time != 0)
                                        {{ $tutoriel->time }}
                                    @else
                                        NC
                                    @endif
                                </span>
                                    </div>
                                </div>
                                <div class="card__body">
                                    <a href="{{ route("Front.Tutoriel.show", [$tutoriel->subcategory->id, $tutoriel->slug]) }}">
                                        <img src="{{ sourceImage('learning/'.$tutoriel->id.'.png') }}" alt="Image" class="border--round">
                                    </a>
                                    <p>{{ $tutoriel->title }}</p>
                                </div>
                                <div class="card__bottom">
                                    <ul class="list-inline">
                                        <li class="list-inline-item">
                                            <div class="card__action">
                                                <a href="{{ route("Front.Tutoriel.show", [$tutoriel->subcategory->id, $tutoriel->slug]) }}#comment">
                                                    <i class="material-icons">comment</i>
                                                    <span>{{ \App\Repository\Tutoriel\TutorielCommentRepository::getCountFromTuto($tutoriel->id) }}</span>
                                                </a>
                                            </div>
                                        </li>
                                        @if($tutoriel->published == 2)
                                            <li class="list-inline-item h6 color--warning">
                                                <span class="countdown" data-date="{{ \Carbon\Carbon::createFromTimestamp(strtotime($tutoriel->published_at))->format('m/d/Y H:i:s') }}"></span>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="row">
                <h2>Article de Wiki ({{ $count['countWiki'] }} {{ formatPlural('Résultat', $count['countWiki']) }})</h2>
                <div class="col-md-12">
                    @foreach($wikis as $wiki)
                        <div class="col-md-12">
                            <div class="feature feature--featured feature-1 boxed boxed--border">
                                <h5>{{ $wiki->title }}</h5>
                                <p>
                                    {{ \Illuminate\Support\Str::limit($wiki->content, '100', '...') }}
                                </p>
                                <a href="{{ route('Front.Wiki.show', [$wiki->wiki_category_id, $wiki->id]) }}">
                                    En savoir plus
                                </a>
                                <span class="label">{{ $wiki->category->name }}</span>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section class="text-center bg--secondary">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-6">
                    <div class="cta">
                        <h2>Didn't find what you were after?</h2>
                        <a class="btn btn--primary type--uppercase" href="#" data-notification-link="search-box">
                                    <span class="btn__text">
                                        Try Another Search
                                    </span>
                        </a>
                        <p class="type--fine-print">or contact us at
                            <a href="#">hello@oursite.net</a>
                        </p>
                    </div>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
@endsection

@section("script")

@endsection
