<section class="bar bar-3 bar--sm bg--secondary">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="bar__module">
                    <span class="type--fade" id="tour_info"><strong>Info:</strong> {{ \App\Repository\Other\BreakingRepository::getLatest() }}</span>
                </div>
            </div>
            <div class="col-lg-6 text-right text-left-xs text-left-sm">
                <div class="bar__module">
                    <ul class="menu-horizontal">
                        @auth

                        @else
                            <li>
                                <div class="modal-instance">
                                    <a href="#" class="modal-trigger" id="tour_connexion">Connexion</a>
                                    <div class="modal-container">
                                        <div class="modal-content section-modal">
                                            <section class="unpad ">
                                                <div class="container">
                                                    <div class="row justify-content-center">
                                                        <div class="col-md-6">
                                                            <div class="boxed boxed--lg bg--white text-center feature">
                                                                <div class="modal-close modal-close-cross"></div>
                                                                <h3>Connexion à votre compte</h3>
                                                                <!--<a class="btn block btn--icon bg--facebook type--uppercase" href="#">
                                                                            <span class="btn__text">
                                                                                <i class="socicon-facebook"></i>
                                                                                Login with Facebook
                                                                            </span>
                                                                </a>
                                                                <a class="btn block btn--icon bg--twitter type--uppercase" href="#">
                                                                            <span class="btn__text">
                                                                                <i class="socicon-twitter"></i>
                                                                                Login with Twitter
                                                                            </span>
                                                                </a>
                                                                <hr data-title="OR">-->
                                                                <div class="feature__body">
                                                                    <form action="{{ route('login') }}" method="POST">
                                                                        @csrf
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <input type="text" placeholder="Adresse Mail" name="email"/>
                                                                            </div>
                                                                            <div class="col-md-12">
                                                                                <input type="password" placeholder="Mot de passe" name="password"/>
                                                                            </div>
                                                                            <div class="col-md-12">
                                                                                <button class="btn btn--primary type--uppercase" type="submit">Connexion</button>
                                                                            </div>
                                                                        </div>
                                                                        <!--end of row-->
                                                                    </form>
                                                                    <span class="type--fine-print block">Je n'ai pas de compte?
                                                                            <a href="#">Créer un compte</a>
                                                                        </span>
                                                                    <!--<span class="type--fine-print block">Forgot your username or password?
                                                                                <a href="#">Recover account</a>
                                                                            </span>-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--end of row-->
                                                </div>
                                                <!--end of container-->
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="modal-instance">
                                    <a href="#" class="modal-trigger" id="tour_register">Créer un compte</a>
                                    <div class="modal-container">
                                        <div class="modal-content">
                                            <section class="imageblock feature-large bg--white border--round ">
                                                <div class="imageblock__content col-lg-5 col-md-3 pos-left">
                                                    <div class="background-image-holder">
                                                        <img alt="image" src="/storage/other/register_wall.jpg" />
                                                    </div>
                                                </div>
                                                <div class="container">
                                                    <div class="row justify-content-end">
                                                        <div class="col-lg-7">
                                                            <div class="row justify-content-center">
                                                                <div class="col-lg-10 col-md-11">
                                                                    <h2>Créer un compte</h2>
                                                                    <!--<a class="btn block btn--icon bg--facebook type--uppercase" href="#">
                                                                                <span class="btn__text">
                                                                                    <i class="socicon-facebook"></i>
                                                                                    Sign up with Facebook
                                                                                </span>
                                                                    </a>
                                                                    <a class="btn block btn--icon bg--twitter type--uppercase" href="#">
                                                                                <span class="btn__text">
                                                                                    <i class="socicon-twitter"></i>
                                                                                    Sign up with Twitter
                                                                                </span>
                                                                    </a>
                                                                    <hr data-title="OR">-->
                                                                    <form action="{{ route('register') }}" method="POST">
                                                                        @csrf
                                                                        <div class="row">
                                                                            <div class="col-12">
                                                                                <input type="text" name="name" placeholder="Votre pseudo" />
                                                                            </div>
                                                                            <div class="col-12">
                                                                                <input type="email" name="email" placeholder="Votre adresse mail" />
                                                                            </div>
                                                                            <div class="col-12">
                                                                                <input type="password" name="password" placeholder="Mot de passe" />
                                                                            </div>
                                                                            <div class="col-12">
                                                                                <button type="submit" class="btn btn--primary type--uppercase">Créer mon compte</button>
                                                                            </div>
                                                                            <!--<div class="col-12">
                                                                                        <span class="type--fine-print">By signing up, you agree to the
                                                                                            <a href="#">Terms of Service</a>
                                                                                        </span>
                                                                            </div>-->
                                                                        </div>
                                                                        <!--end row-->
                                                                    </form>
                                                                </div>
                                                            </div>
                                                            <!--end of row-->
                                                        </div>
                                                        <!--end of col-->
                                                    </div>
                                                    <!--end of row-->
                                                </div>
                                                <!--end of container-->
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endauth
                        <li>
                            <a href="#" data-notification-link="search-box" id="tour_search">
                                <i class="stack-search"></i>
                            </a>
                        </li>
                        @auth()
                                @if(auth()->user()->group == 1)
                                    <li class=""><a href="{{ route('Back.dashboard') }}"><i class="icon-Dashboard"></i> </a></li>
                                @endif
                            <li class="dropdown dropdown--absolute">
                                        <a href="#" class="dropdown-toggle" data-toogle="dropdown" role="button" aria-expanded="false" id="tour_notification">
                                            <span class="icon-Bell"></span>
                                            <span class="badge badge-danger">{{ auth()->user()->unreadNotifications->count() }}</span>
                                        </a>
                                        <div class="dropdown__container">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-md-10 dropdown__content">
                                                        <ul class="menu-vertical">
                                                            @if(auth()->user()->unreadNotifications->count() == 0)
                                                                <li class="text-center font-weight-bolder">Aucune Notification</li>
                                                            @else
                                                                @foreach(auth()->user()->unreadNotifications as $notification)
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-2">
                                                                                <img src="{{ $notification->data['images'] }}" alt="{{ $notification->data['name'] }}" width="100">
                                                                            </div>
                                                                            <div class="col-md-10">
                                                                                <strong>{{ $notification->data['name'] }}</strong><br>
                                                                                <p>{!! $notification->data['description'] !!}</p>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                @endforeach
                                                            @endif
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                            <li class="dropdown dropdown--absolute">
                                    <span class="dropdown__trigger" id="tour_account">
                                        <img alt="Image" class="flag" src="/storage/avatar/placeholder.png" />
                                    </span>
                                <div class="dropdown__container">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-2 dropdown__content">
                                                <ul class="menu-vertical text-left">
                                                    <li>
                                                        <strong>{{ auth()->user()->name }}</strong>
                                                    </li>
                                                    <li>
                                                        <a href="{{ route('Account.index') }}">Mon Compte</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ route('logout') }}">Déconnexion</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endauth
                            <li>
                                <a href="{{ route('Suggestion.index') }}" id="tour_suggestion">
                                    <i class="icon-Information"></i>
                                </a>
                            </li>
                    </ul>
                    <div class="notification pos-top pos-right search-box bg--white border--bottom" data-animation="from-top" data-notification-link="search-box">
                        <form action="{{ route('searchPost') }}" method="POST">
                            @csrf
                            <div class="row justify-content-center">
                                <div class="col-lg-6 col-md-8">
                                    <input type="text" name="search" placeholder="Rechercher un objet, un article, un tutoriel, etc..." />
                                </div>
                            </div>
                            <!--end of row-->
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
<div class="nav-container ">
    <div class="bar bar--sm visible-xs">
        <div class="container">
            <div class="row">
                <div class="col-3 col-md-2">
                    <a href="index.html">
                        <img class="logo logo-dark" alt="logo" src="/assets/front/img/logo-dark.png" />
                        <img class="logo logo-light" alt="logo" src="/assets/front/img/logo-light.png" />
                    </a>
                </div>
                <div class="col-9 col-md-10 text-right">
                    <a href="#" class="hamburger-toggle" data-toggle-class="#menu1;hidden-xs">
                        <i class="icon icon--sm stack-interface stack-menu"></i>
                    </a>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </div>
    <!--end bar-->
    <nav id="menu1" class="bar bar-2 hidden-xs">
        <div class="container">
            <div class="row">
                <div class="col-lg-1 col-md-2 hidden-xs">
                    <div class="bar__module">
                        <a href="{{ route('home') }}">
                            <img class="logo logo-dark" alt="logo" src="/assets/front/img/logo-dark.png" />
                            <img class="logo logo-light" alt="logo" src="/assets/front/img/logo-light.png" />
                        </a>
                    </div>
                    <!--end module-->
                </div>
                <div class="col-lg-11 col-md-12 text-right text-left-xs text-left-sm">
                    <div class="bar__module">
                        <ul class="menu-horizontal text-left" id="tour_menu">
                            <li class="active"><a href="{{ route('home') }}">Accueil</a></li>
                            <li class=""><a href="{{ route('Front.Blog.index') }}">Blog</a></li>
                            <li class=""><a href="{{ route('Front.Route.index') }}">Routes</a></li>
                            <li class="dropdown">
                                <span class="dropdown__trigger">Téléchargement</span>
                                <div class="dropdown__container">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12 dropdown__content dropdown__content--lg">
                                                @foreach(\App\Repository\Asset\AssetRepository::getCoverMenu() as $cover)
                                                    <div class="pos-absolute col-lg-5 imagebg hidden-sm hidden-xs" data-overlay="4">
                                                        <div class="background-image-holder">
                                                            @if($cover->images == 1)
                                                                @if(env('APP_ENV') == 'production')
                                                                    <img alt="background" src="{{ \Illuminate\Support\Facades\Storage::disk('s3')->url('download/'.$cover->id.'.png') }}" />
                                                                @else
                                                                    <img alt="background" src="{{ \Illuminate\Support\Facades\Storage::disk('public')->url('download/'.$cover->id.'.png') }}" />
                                                                @endif
                                                            @else
                                                                <img src="https://via.placeholder.com/500">
                                                            @endif
                                                        </div>
                                                        <div class="container pos-vertical-center pl-5">
                                                            <div class="row">
                                                                <div class="col-lg-8">
                                                                    <h2 class="color--white">{{ $cover->designation }}</h2>
                                                                    <span>{{ $cover->short_description }}</span>
                                                                    <a href="{{ route('Download.redirect', $cover->id) }}" class="btn btn--primary type--uppercase">
                                                                            <span class="btn__text">
                                                                                Télécharger
                                                                            </span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                                <div class="row justify-content-end">
                                                    @foreach(\App\Repository\Asset\AssetCategorieRepository::getCategories() as $category)
                                                        <div class="col-lg-3 col-md-3">
                                                            <strong>{{ $category->name }}</strong>
                                                            <hr>
                                                            <ul class="menu-vertical">
                                                                @foreach(\App\Repository\Asset\AssetSubCategorieRepository::getSubCategories($category->id) as $subCategory)
                                                                    <li>
                                                                        <a href="{{ route('Front.Download.list', $subCategory->id) }}">
                                                                            {{ $subCategory->name }}
                                                                        </a>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    @endforeach
                                                </div>
                                                <!--end of row-->
                                            </div>
                                            <!--end dropdown content-->
                                        </div>
                                    </div>
                                </div>
                                <!--end dropdown container-->
                            </li>
                            <li class="dropdown">
                                <span class="dropdown__trigger">Tutoriels</span>
                                <div class="dropdown__container">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12 dropdown__content dropdown__content--lg" style="height: 600px">
                                                @foreach(\App\Repository\Tutoriel\TutorielRepository::getCoverMenu() as $cover)
                                                    <div class="pos-absolute col-lg-5 imagebg hidden-sm hidden-xs" data-overlay="4">
                                                        <div class="background-image-holder">
                                                            <img alt="background" src="{{ sourceImage('learning/'.$cover->id.'.png') }}" />
                                                        </div>
                                                        <div class="container pos-vertical-center pl-5">
                                                            <div class="row">
                                                                <div class="col-lg-8">
                                                                    <h2 class="color--white">{{ $cover->title }}</h2>
                                                                    <span>{{ $cover->short_content }}</span>
                                                                    <a href="{{ route("Front.Tutoriel.show", [$cover->subcategory->id, $cover->slug]) }}" class="btn btn--primary type--uppercase">
                                                                            <span class="btn__text">
                                                                                Voir le Tutoriel
                                                                            </span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <!--end of row-->
                                                        </div>
                                                    </div>
                                                @endforeach
                                                <div class="row justify-content-end">
                                                    @foreach(\App\Repository\Tutoriel\TutorielCategorieRepository::getCategories() as $category)
                                                        <div class="col-lg-3 col-md-3">
                                                            <strong>{{ $category->name }}</strong>
                                                            <hr>
                                                            <ul class="menu-vertical">
                                                                @foreach(\App\Repository\Tutoriel\TutorielSubCategorieRepository::getSubCategories($category->id) as $subCategory)
                                                                    <li>
                                                                        <a href="{{ route('Front.Tutoriel.list', $subCategory->id) }}">
                                                                            {{ $subCategory->name }}
                                                                        </a>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    @endforeach
                                                </div>
                                                <!--end of row-->
                                            </div>
                                            <!--end dropdown content-->
                                        </div>
                                    </div>
                                </div>
                                <!--end dropdown container-->
                            </li>
                            <li class=""><a href="{{ route('Front.Wiki.index') }}">Wiki</a></li>
                            <li><a href="https://api.trainznation.eu">API SCRIPT</a></li>
                        </ul>
                    </div>
                    <!--end module-->
                    <!--end module-->
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </nav>
    <!--end bar-->
</div>
