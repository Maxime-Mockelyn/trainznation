@if($results->hasPages())
    <div class="pagination">
        @if($results->onFirstPage())

        @else
            <a class="pagination__prev disabled" aria-disabled="true" href="{{ $results->previousPageUrl() }}" title="Previous Page"><i class="icon-Arrow-LeftinCircle"></i></a>
        @endif
            <ol>
                @foreach($elements as $element)
                    @if (is_string($element))
                    <li class="disabled" aria-disabled="true"><a href="#">{{ $element }}</a></li>
                    @endif

                    @if(is_array($element))
                        @foreach($element as $page => $url)
                            @if($page == $results->currentpage())
                                <li class="pagination__current">{{ $page }}</li>
                            @else
                                <li><a href="{{ $url }}">{{ $page }}</a></li>
                            @endif
                        @endforeach
                    @endif
                @endforeach
            </ol>
        @if($results->hasMorePages())
            <a class="pagination__next" href="{{ $results->nextPageUrl() }}" title="Next Page"><i class="icon-Arrow-RightinCircle"></i> </a>
        @else

        @endif
    </div>
@endif
