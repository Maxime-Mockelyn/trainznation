<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{ env("APP_NAME") }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="@yield('title') - @yield('description')">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="Content-Security-Policy" content="default-src *;
   img-src * 'self' q.stripe.com data: https:; script-src 'self' 'unsafe-inline' 'unsafe-eval' *;
   style-src  'self' 'unsafe-inline' * checkout.stripe.com q.stripe.com">
    <link href="/assets/front/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/assets/front/css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/assets/front/css/socicon.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/assets/front/css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/assets/front/css/flickity.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/assets/front/css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/assets/front/css/jquery.steps.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/assets/front/css/theme.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/assets/front/css/custom.css" rel="stylesheet" type="text/css" media="all" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="/assets/custom/vendors/bootstrap-tour/css/bootstrap-tour-standalone.min.css">

    <link rel="shortcut icon" href="/favicon.ico" />
    @yield("css")
</head>
<body class=" ">
<a id="start"></a>
@include("Front.Include.navbar")
<div class="main-container">
    @yield("content")
    <section class="text-center imagebg" data-gradient-bg='#4876BD,#5448BD,#8F48BD,#BD48B1'>
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-lg-7">
                    <div class="tweets-feed slider text-center" data-feed-name="trainznation" data-amount="5" data-paging="true"></div>
                    <a class="btn btn--icon bg--twitter" href="https://twitter.com/trainznation">
                                                <span class="btn__text">
                                                    <i class="socicon socicon-twitter"></i>
                                                    Visitez @trainznation sur twitter
                                                </span>
                    </a>
                </div>
            </div>
            <!--end row-->
        </div>
        <!--end of container-->
    </section>
    @include("Front.Include.footer")
</div>
<!--<div class="loader"></div>-->
<a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
    <i class="stack-interface stack-up-open-big"></i>
</a>
<script src="/assets/front/js/jquery-3.1.1.min.js"></script>
<script src="/assets/front/js/flickity.min.js"></script>
<script src="/assets/front/js/easypiechart.min.js"></script>
<script src="/assets/front/js/parallax.js"></script>
<script src="/assets/front/js/typed.min.js"></script>
<script src="/assets/front/js/datepicker.js"></script>
<script src="/assets/front/js/isotope.min.js"></script>
<script src="/assets/front/js/ytplayer.min.js"></script>
<script src="/assets/front/js/lightbox.min.js"></script>
<script src="/assets/front/js/granim.min.js"></script>
<script src="/assets/front/js/jquery.steps.min.js"></script>
<script src="/assets/front/js/countdown.min.js"></script>
<script src="/assets/front/js/twitterfetcher.min.js"></script>
<script src="/assets/front/js/spectragram.min.js"></script>
<script src="/assets/front/js/smooth-scroll.min.js"></script>
<script src="/assets/front/js/scripts.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="/assets/custom/vendors/bootstrap-tour/js/bootstrap-tour-standalone.min.js" type="text/javascript"></script>
<!-- Start of Async Drift Code -->
<script>
    "use strict";

    !function() {
        var t = window.driftt = window.drift = window.driftt || [];
        if (!t.init) {
            if (t.invoked) return void (window.console && console.error && console.error("Drift snippet included twice."));
            t.invoked = !0, t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ],
                t.factory = function(e) {
                    return function() {
                        var n = Array.prototype.slice.call(arguments);
                        return n.unshift(e), t.push(n), t;
                    };
                }, t.methods.forEach(function(e) {
                t[e] = t.factory(e);
            }), t.load = function(t) {
                var e = 3e5, n = Math.ceil(new Date() / e) * e, o = document.createElement("script");
                o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + n + "/" + t + ".js";
                var i = document.getElementsByTagName("script")[0];
                i.parentNode.insertBefore(o, i);
            };
        }
    }();
    drift.SNIPPET_VERSION = '0.3.1';
    drift.load('dx8gtca3kmt2');
</script>
<!-- End of Async Drift Code -->
<script type="text/javascript">
    let tour = new Tour({
        template: '<div class="popover tour">' +
            '<div class="arrow"></div>' +
            '<h3 class="popover-title"></h3>' +
            '<div class="popover-content"></div>' +
            '<div class="popover-navigation">' +
            '<button class="btn btn--primary" data-role="prev">< Retour</button>' +
            '<span data-role="separator">|</span>' +
            '<button class="btn btn--primary" data-role="next">Suivant ></button>' +
            '</div>' +
            '<button class="btn btn--primary-1" data-role="end">Fin de la démonstation</button>' +
            '</div>',
        steps: [
            {
                element: "#tour_info",
                title: "Dernière Infos",
                content: "Retrouver nos dernières informations dans cette emplacement",
                placement: 'bottom'
            },
            {
                element: "#tour_connexion",
                title: "Connexion",
                content: "Connectez-vous à votre compte et entrer dans votre espace, poster des commentaires ou des suggestions",
                placement: 'bottom'
            },
            {
                element: "#tour_register",
                title: "Enregistrer",
                content: "Enregistrez-vous afin d'acceder à votre espace, poster des commentaires et participer à la communauté",
                placement: 'bottom'
            },
            {
                element: "#tour_search",
                title: "Rechercher",
                content: "Au lieu de naviger à travers les différentes parges, recherchez directement ce dont vous avez besoins (Tutoriel, Objets, Nouvelles, etc...)",
                placement: 'bottom'
            },
            {
                element: "#tour_notification",
                title: "Vos notifications",
                content: "Suivez en temps réel vos agissements sur le site trainznation",
                placement: 'bottom'
            },
            {
                element: "#tour_account",
                title: "Votre compte",
                content: "Accedez à votre compte ou déconnectez-vous de trainznation",
                placement: 'bottom'
            },
            {
                element: "#tour_suggestion",
                title: "Suggestion",
                content: "Vous rencontrez un bugs ou souhaitez emettre une suggestion, cliquez sur ce bouton",
                placement: 'bottom'
            },
            {
                element: "#tour_menu",
                title: "Menu",
                content: "Un nouveau menu pour ci retrouver plus facilement",
                placement: 'bottom'
            },
            {
                element: "#tour_tutoriel",
                title: "Derniers Tutoriels",
                content: "Retrouver ici les derniers tutoriels de trainznation",
                placement: 'bottom'
            },
            {
                element: "#tour_news",
                title: "Dernière News",
                content: "Retrouver nos dernières Nouvelles de trainznation",
                placement: 'bottom'
            },

        ]
    })

</script>


{!! Toastr::render() !!}
<script type="text/javascript">
    (function ($) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("#start_tour").on('click', function (e) {
            e.preventDefault()
            tour.init()
            tour.restart(true);
        })
    })(jQuery)
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-141285622-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-141285622-1');
</script>
<script>
    function initFreshChat() {
        window.fcWidget.init({
            token: "150be44c-09f0-4d24-80fe-04ff1bae178a",
            host: "https://wchat.freshchat.com"
        });
    }
    function initialize(i,t){var e;i.getElementById(t)?initFreshChat():((e=i.createElement("script")).id=t,e.async=!0,e.src="https://wchat.freshchat.com/js/widget.js",e.onload=initFreshChat,i.head.appendChild(e))}function initiateCall(){initialize(document,"freshchat-js-sdk")}window.addEventListener?window.addEventListener("load",initiateCall,!1):window.attachEvent("load",initiateCall,!1);
</script>

@yield("script")
</body>
</html>
