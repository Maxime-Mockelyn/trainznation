<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{ env("APP_NAME") }} | Vérification de compte</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Site Description Here">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="/assets/front/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/assets/front/css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/assets/front/css/socicon.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/assets/front/css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/assets/front/css/flickity.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/assets/front/css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/assets/front/css/jquery.steps.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/assets/front/css/theme.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/assets/front/css/custom.css" rel="stylesheet" type="text/css" media="all" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body class=" ">
<a id="start"></a>
<div class="nav-container ">
    <nav class="bar bar-4 bar--transparent bar--absolute" data-fixed-at="200">
        <div class="container">
            <div class="row">
                <div class="col-lg-1 col-md-2 col-md-offset-0 col-4">
                    <div class="bar__module">
                        <a href="{{ route('home') }}">
                            <img class="logo logo-dark" alt="logo" src="/assets/front/img/logo-dark.png" />
                            <img class="logo logo-light" alt="logo" src="/assets/front/img/logo-light.png" />
                        </a>
                    </div>
                    <!--end module-->
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </nav>
    <!--end bar-->
</div>
<div class="main-container">
    <section class="imageblock switchable feature-large height-100">
        <div class="imageblock__content col-lg-6 col-md-4 pos-right">
            <div class="background-image-holder">
                <img alt="image" src="{{ sourceImage("other/register_wall.jpg") }}" />
            </div>
        </div>
        <div class="container pos-vertical-center">
            <div class="row">
                <div class="col-lg-5 col-md-7">
                    <h2>{{ __('Verify Your Email Address') }}</h2>
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
</div>
<!--<div class="loader"></div>-->
<a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
    <i class="stack-interface stack-up-open-big"></i>
</a>
<script src="/assets/front/js/jquery-3.1.1.min.js"></script>
<script src="/assets/front/js/flickity.min.js"></script>
<script src="/assets/front/js/easypiechart.min.js"></script>
<script src="/assets/front/js/parallax.js"></script>
<script src="/assets/front/js/typed.min.js"></script>
<script src="/assets/front/js/datepicker.js"></script>
<script src="/assets/front/js/isotope.min.js"></script>
<script src="/assets/front/js/ytplayer.min.js"></script>
<script src="/assets/front/js/lightbox.min.js"></script>
<script src="/assets/front/js/granim.min.js"></script>
<script src="/assets/front/js/jquery.steps.min.js"></script>
<script src="/assets/front/js/countdown.min.js"></script>
<script src="/assets/front/js/twitterfetcher.min.js"></script>
<script src="/assets/front/js/spectragram.min.js"></script>
<script src="/assets/front/js/smooth-scroll.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src='https://www.google.com/recaptcha/api.js?render={{ env("GOOGLE_RECAPTCHA_KEY") }}"'></script>
<script src="/assets/front/js/scripts.js"></script>
<script type="text/javascript">

    (function ($) {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("#formSignup").on('submit', function (e) {
            e.preventDefault()
            let form = $(this)
            let url = form.attr('action')
            let btn = $("#btnForm")
            let data = form.serializeArray()

            btn.attr('disabled', true)

            $.ajax({
                url: url,
                method: "POST",
                data: data,
                success: function(){
                    window.location='/'
                    btn.removeAttr('disabled')
                },
                error: function(data){
                    //console.log(data)
                    if(data.errors) {toastr.error("Erreur !", data.errors)}
                    btn.removeAttr('disabled')
                }
            })
        })
    })(jQuery)
</script>
</body>
</html>
