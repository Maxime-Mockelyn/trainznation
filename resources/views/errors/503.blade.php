@extends("Front.Layout.app")

@section("css")

@endsection

@section("content")
    <section class="cover height-100 imagebg text-center" data-overlay="7">
        <div class="background-image-holder">
            <img alt="background" src="{{ sourceImage('other/503.jpg') }}" />
        </div>
        <div class="container pos-vertical-center">
            <div class="row">
                <div class="col-md-12">
                    <h2>
                        Site actuellement en maintenance !
                    </h2>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
@endsection

@section("script")

@endsection
