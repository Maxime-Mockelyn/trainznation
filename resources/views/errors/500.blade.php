@extends("Front.Layout.app")

@section("css")

@endsection

@section("content")
    <section class="cover height-100 imagebg text-center" data-overlay="7">
        <div class="background-image-holder">
            <img alt="background" src="{{ sourceImage('other/500.jpg') }}" />
        </div>
        <div class="container pos-vertical-center">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="h1--large">500</h1>
                    <p class="lead">
                        Une erreur inattendue s'est produite empêchant le chargement de la page.
                    </p>
                    <a href="{{ route('home') }}">Retournez à la page d'accueil</a>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
@endsection

@section("script")

@endsection
