@extends("Back.Layout.appTwo")

@section("css")

@endsection

@section("sub")
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Bienvenue {{ auth()->user()->name }}</h3>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('home') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ route('Account.index') }}" class="kt-subheader__breadcrumbs-link">
                    Tableau de Bord </a>
            </div>
        </div>
    </div>
@endsection

@section("content")
<div class="row">
    <div class="col-md-12">
        <div class="row row-full-height">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Activité Récente du jour
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body" id="activities">
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-6"></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="row row-full-height">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                <i class="la la-facebook-f"></i> Connexion Social
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body" id="socialite">
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-6"></div>
        </div>
    </div>
</div>
@endsection

@section("script")
    <script src="/assets/custom/back/account/index.js" type="text/javascript"></script>
@endsection

