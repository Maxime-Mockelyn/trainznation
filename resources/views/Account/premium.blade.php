@extends("Back.Layout.appTwo")

@section("css")
    <link href="/assets/back/css/demo1/pages/pricing/pricing-1.css" rel="stylesheet" type="text/css" />
@endsection

@section("sub")
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title"><i class="la la-certificate"></i> Offre Premium</h3>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('home') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ route('Account.badge') }}" class="kt-subheader__breadcrumbs-link">
                    Offre Premium </a>
            </div>
        </div>
    </div>
@endsection

@section("content")
    <div class="row">
        <div class="col-md-3">
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <table class="table">
                        <tbody>
                        <tr>
                            <td style="font-weight: bold;">Premium</td>
                            <td>
                                @if($user->account->premium == 1)
                                    <i class="la la-check-circle text-success la-lg" data-toggle="kt-tooltip" title="Vous êtes premium"></i>
                                @else
                                    <i class="la la-times-circle text-danger la-lg" data-toggle="kt-tooltip" title="Vous n'êtes pas premium"></i>
                                @endif
                            </td>
                        </tr>
                        @if($user->account->premium == 1)
                            <tr>
                                <td style="font-weight: bold;">Date de souscription</td>
                                <td>{{ $user->account->premium_start->format("d/m/Y") }}</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>

                    @if($user->account->premium == 1)
                        <form action="{{ route('Account.Premium.delete') }}" method="POST">
                            @csrf
                            <button type="submit" class="btn btn-block btn-danger">Annuler l'abonnement</button>
                        </form>
                    @endif
                </div>
            </div>

            @if($user->account->premium == 1)
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="la la-file-pdf-o"></i>
                        </span>
                            <h3 class="kt-portlet__head-title">
                                Factures
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <table class="table table-bordered table-striped">
                            <tbody>
                            @foreach($user->invoices() as $invoice)
                                <tr>
                                    <td>{{ $invoice->date()->format("d/m/Y") }}</td>
                                    <td>{{ $invoice->total() }}</td>
                                    <td><a href="{{ route("Account.Premium.invoice", $invoice->id) }}"><i class="flaticon2-download"></i> </a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
        </div>
        <div class="col-md-9">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="la la-certificate"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            Souscrire à une offre Premium
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-portlet kt-iconbox kt-iconbox--brand kt-iconbox--animate-slower">
                        <div class="kt-portlet__body">
                            <div class="kt-iconbox__body">
                                <div class="kt-iconbox__desc">
                                    <h3 class="kt-iconbox__title">
                                        <a class="kt-link" href="#">OK, mais quel sont les avantages</a>
                                    </h3>
                                    <div class="kt-iconbox__content">
                                        <ul>
                                            <li>Télécharger les vidéos des tutoriels</li>
                                            <li>Télécharger les ressources des tutoriels</li>
                                            <li>Acceder en avant première au tutoriel</li>
                                            <li>Acceder au contenue de téléchargement payant</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet kt-iconbox kt-iconbox--brand kt-iconbox--animate-slower">
                        <div class="kt-portlet__body">
                            <div class="kt-iconbox__body">
                                <div class="kt-iconbox__desc">
                                    <h3 class="kt-iconbox__title">
                                        <a class="kt-link" href="#">Pourquoi ?</a>
                                    </h3>
                                    <div class="kt-iconbox__content">
                                        <p>Mon but à travers trainznation.eu est de partager mes connaissances et mon contenue avec le plus grand nombre, c'est pourquoi j'essaie de rendre un maximum de contenu gratuit et public.</p>
                                        <p>Malgré tout, la préparation, l'enregistrement et le montage des tutoriels, la création de contenue et la maintenabilité des routes prend un temps considérable (20 à 45 heures par semaine). Du coup proposer des options payantes, comme le téléchargement des sources, me permet d'amortir une partie du temps passé et de continuer à faire vivre le site.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                        <!--begin::Portlet-->
                        <div class="kt-portlet">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
			<i class="la la-puzzle-piece"></i>
			</span>
                                    <h3 class="kt-portlet__head-title">
                                        Plan Premium
                                    </h3>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div class="kt-pricing-1 kt-pricing-1--fixed">
                                    <div class="kt-pricing-1__items row">
                                        <div class="kt-pricing-1__item col-lg-4">
                                            <div class="kt-pricing-1__visual">
                                                <div class="kt-pricing-1__hexagon1"></div>
                                                <div class="kt-pricing-1__hexagon2"></div>
                                                <span class="kt-pricing-1__icon kt-font-brand"><i class="fa flaticon-rocket"></i></span>
                                            </div>
                                            <span class="kt-pricing-1__price">3,50<span class="kt-pricing-1__label">€</span></span>
                                            <h2 class="kt-pricing-1__subtitle">1 mois</h2>
                                            <span class="kt-pricing-1__description">
                                                <span>Vidéos des tutoriel</span>
                                                <span>Sources des tutoriels</span>
                                                <span>Avant première</span>
                                                <span>Contenue Payant</span>
                                            </span>
                                            <div class="kt-pricing-1__btn">
                                                <form action="{{ route("Account.Premium.charge") }}" method="POST">
                                                    @csrf
                                                    <input type="hidden" name="plan" value="monthly-plan">
                                                    <script
                                                        src="https://checkout.stripe.com/checkout.js"
                                                        data-key="{{ env("STRIPE_PUB_KEY") }}"
                                                        class="stripe-button"
                                                        data-amount="350"
                                                        data-name="{{ env("APP_DOMAIN") }} - Souscription"
                                                        data-description="Souscription à l'offre premium de Trainznation"
                                                        data-image="/storage/other/logo-carre.png"
                                                        data-label="Souscrire"
                                                        data-locale="fr"
                                                        data-currency="eur">
                                                    </script>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="kt-pricing-1__item col-lg-4">
                                            <div class="kt-pricing-1__visual">
                                                <div class="kt-pricing-1__hexagon1"></div>
                                                <div class="kt-pricing-1__hexagon2"></div>
                                                <span class="kt-pricing-1__icon kt-font-brand"><i class="fa flaticon-piggy-bank"></i></span>
                                            </div>
                                            <span class="kt-pricing-1__price">10<span class="kt-pricing-1__label">€</span></span>
                                            <h2 class="kt-pricing-1__subtitle">3 mois</h2>
                                            <span class="kt-pricing-1__description">
                                                <span>Vidéos des tutoriel</span>
                                                <span>Sources des tutoriels</span>
                                                <span>Avant première</span>
                                                <span>Contenue Payant</span>
                                            </span>
                                            <div class="kt-pricing-1__btn">
                                                <form action="{{ route("Account.Premium.charge") }}" method="POST">
                                                    @csrf
                                                    <input type="hidden" name="plan" value="trim-plan">
                                                    <script
                                                            src="https://checkout.stripe.com/checkout.js"
                                                            data-key="{{ env("STRIPE_PUB_KEY") }}"
                                                            class="stripe-button"
                                                            data-amount="1000"
                                                            data-name="{{ env("APP_DOMAIN") }} - Souscription"
                                                            data-description="Souscription à l'offre premium de Trainznation"
                                                            data-image="/storage/other/logo-carre.png"
                                                            data-locale="fr"
                                                            data-label="Souscrire"
                                                            data-currency="eur">
                                                    </script>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="kt-pricing-1__item col-lg-4">
                                            <div class="kt-pricing-1__visual">
                                                <div class="kt-pricing-1__hexagon1"></div>
                                                <div class="kt-pricing-1__hexagon2"></div>
                                                <span class="kt-pricing-1__icon kt-font-brand"><i class="fa flaticon-gift"></i></span>
                                            </div>
                                            <span class="kt-pricing-1__price">40<span class="kt-pricing-1__label">€</span></span>
                                            <h2 class="kt-pricing-1__subtitle">12 mois</h2>
                                            <span class="kt-pricing-1__description">
                                                <span>Vidéos des tutoriel</span>
                                                <span>Sources des tutoriels</span>
                                                <span>Avant première</span>
                                                <span>Contenue Payant</span>
                                            </span>
                                            <div class="kt-pricing-1__btn">
                                                <form action="{{ route("Account.Premium.charge") }}" method="POST">
                                                    @csrf
                                                    <input type="hidden" name="plan" value="annual-plan">
                                                    <script
                                                            src="https://checkout.stripe.com/checkout.js"
                                                            data-key="{{ env("STRIPE_PUB_KEY") }}"
                                                            class="stripe-button"
                                                            data-amount="4000"
                                                            data-name="{{ env("APP_DOMAIN") }} - Souscription"
                                                            data-description="Souscription à l'offre premium de Trainznation"
                                                            data-image="/storage/other/logo-carre.png"
                                                            data-locale="fr"
                                                            data-label="Souscrire"
                                                            data-currency="eur">
                                                    </script>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Portlet-->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <!--<script src="/assets/custom/back/account/badge.js" type="text/javascript"></script>-->
@endsection

