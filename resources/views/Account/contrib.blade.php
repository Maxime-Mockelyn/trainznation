@extends("Back.Layout.appTwo")

@section("css")

@endsection

@section("sub")
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title"><i class="la la-comments"></i> Mes Contributions</h3>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('home') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ route('Account.contrib') }}" class="kt-subheader__breadcrumbs-link">
                    Mes Contribution </a>
            </div>
        </div>
    </div>
@endsection

@section("content")
    <div class="row">
        <div class="col-md-6">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Mes Notifications
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body" id="tableNotif">

                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Mes Activités
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body" id="tableActivity">

                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Mes Commentaires (Blog)
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body" id="tableBlog">

                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Mes Commentaires (Tutoriel)
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body" id="tableTutoriel">

                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="/assets/custom/back/account/contrib.js" type="text/javascript"></script>
@endsection

