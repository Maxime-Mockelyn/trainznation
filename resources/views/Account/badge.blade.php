@extends("Back.Layout.appTwo")

@section("css")

@endsection

@section("sub")
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title"><i class="la la-certificate"></i> Mes Badges</h3>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('home') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ route('Account.badge') }}" class="kt-subheader__breadcrumbs-link">
                    Mes Badges </a>
            </div>
        </div>
    </div>
@endsection

@section("content")
    <div class="row">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Mes Badges
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="row">
                    @foreach($badges as $badge)
                    <div class="col-md-3">
                        <div class="kt-portlet kt-portlet--height-fluid">
                            <div class="kt-portlet__head kt-portlet__head--noborder">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">

                                    </h3>
                                </div>
                            </div>
                            <div class="kt-portlet__body kt-portlet__body--fit-y">
                                <!--begin::Widget -->
                                <div class="kt-widget kt-widget--user-profile-4">
                                    <div class="kt-widget__head">
                                        <div class="kt-widget__media">
                                            @if(\App\Repository\Badge\BadgeUserRepository::badgeIsExists(auth()->user()->id, $badge->id) == true)
                                                <img class="kt-widget__img kt-hidden-" src="{{ sourceImage('other/badge/'.$badge->id.'.png') }}" alt="image">
                                            @else
                                                <img class="kt-widget__img kt-hidden-" src="{{ sourceImage('other/badge/0.png') }}" alt="image">
                                            @endif
                                            <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-hidden">
                                                JB
                                            </div>
                                        </div>
                                        <div class="kt-widget__content">
                                            <div class="kt-widget__section">
                                                <a href="" class="kt-widget__username">
                                                    {{ $badge->name }}<br>
                                                    <h6 class="text-muted">{{ $badge->description }}</h6>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Widget -->
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <!--<script src="/assets/custom/back/account/badge.js" type="text/javascript"></script>-->
@endsection

