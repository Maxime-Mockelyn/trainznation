@extends("Back.Layout.appTwo")

@section("css")

@endsection

@section("sub")
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title"><i class="la la-user"></i> Mon Compte</h3>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('home') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ route('Account.info') }}" class="kt-subheader__breadcrumbs-link">
                    Mon Compte </a>
            </div>
        </div>
    </div>
@endsection

@section("content")
    <div class="row">
        <div class="col-md-5">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Mon Compte
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--begin::Section-->
                    <div class="kt-section">
                        <form action="{{ route('Account.update') }}" id="form">
                            @method("PUT")
                            @csrf

                            <div class="form-group">
                                <label>Pseudo *</label>
                                <input type="text" class="form-control" name="name" value="{{ $user->name }}">
                            </div>

                            <div class="form-group">
                                <label>Adresse Mail *</label>
                                <input type="text" class="form-control" name="email" value="{{ $user->email }}">
                            </div>

                            <div class="kt-form__actions">
                                <button type="submit" id="btnSubmit" class="btn btn-primary">Mettre à jour</button>
                            </div>
                        </form>
                    </div>
                    <!--end::Section-->

                    <div class="kt-separator kt-separator--dashed"></div>
                </div>
            </div>
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Mon mot de passe
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--begin::Section-->
                    <div class="kt-section">
                        <form action="{{ route('Account.updatePassword') }}" id="formPassword">
                            @method("PUT")
                            @csrf

                            <div class="form-group">
                                <label>Mot de passe</label>
                                <input type="password" class="form-control" name="password" required>
                            </div>

                            <div class="form-group">
                                <label>Confirmation du mot de passe</label>
                                <input type="password" class="form-control" name="password_confirmation" required>
                            </div>

                            <div class="kt-form__actions">
                                <button type="submit" id="btnSubmitPassword" class="btn btn-primary">Mettre à jour</button>
                            </div>
                        </form>
                    </div>
                    <!--end::Section-->

                    <div class="kt-separator kt-separator--dashed"></div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Informations secondaires
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--begin::Section-->
                    <div class="kt-section">
                        <form action="{{ route('Account.second') }}" method="POST" enctype="multipart/form-data">
                            @method("PUT")
                            @csrf
                            <div class="text-center">
                                @if(auth()->user()->account->avatar == 0)
                                    <img src="{{ sourceImage('avatar/placeholder.png') }}" width="100" class="kt-img-rounded">
                                @else
                                    <img src="{{ sourceImage('avatar/'.$user->id.'.png') }}" width="100" class="kt-img-rounded">
                                @endif
                            </div>

                            <div class="form-group">
                                <label>Avatar</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="customFile" name="avatar">
                                    <label class="custom-file-label" for="customFile">Choix du fichier</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Site Web</label>
                                <input type="url" class="form-control" name="site_web" value="{{ $user->account->site_web }}">
                            </div>

                            <div class="form-group">
                                <label>Pseudo Twitter</label>
                                <input type="text" class="form-control" name="pseudo_twitter" value="{{ $user->account->pseudo_twitter }}">
                            </div>

                            <div class="form-group">
                                <label>Pseudo Facebook</label>
                                <input type="text" class="form-control" name="pseudo_facebook" value="{{ $user->account->pseudo_facebook }}">
                            </div>

                            <div class="form-group">
                                <label>Pseudo Discord</label>
                                <input type="text" class="form-control" name="discord_id" value="{{ $user->account->discord_id }}">
                            </div>

                            <div class="form-group">
                                <label>Identifiant Trainz (KUID)</label>
                                <input type="text" class="form-control" name="trainz_id" value="{{ $user->account->trainz_id }}">
                            </div>

                            <div class="kt-form__actions">
                                <button type="submit" id="btnSubmit" class="btn btn-primary">Mettre à jour</button>
                            </div>
                        </form>
                    </div>
                    <!--end::Section-->

                    <div class="kt-separator kt-separator--dashed"></div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Supprimer mon compte
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <p>
                        Vous n'êtes pas satisfait du contenu du site ?<br>
                        ou vous souhaitez supprimer toutes les informations associées à ce compte ?
                    </p>

                    <button id="btnDeleteAccount" class="btn btn-block btn-danger"><i class="la la-trash"></i> Supprimer</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="/assets/custom/back/account/info.js" type="text/javascript"></script>
@endsection

