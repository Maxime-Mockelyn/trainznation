<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{ env("APP_NAME") }} - Connexion</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Site Description Here">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="/assets/front/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/assets/front/css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/assets/front/css/socicon.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/assets/front/css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/assets/front/css/flickity.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/assets/front/css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/assets/front/css/jquery.steps.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/assets/front/css/theme.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/assets/front/css/custom.css" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body class=" ">
<a id="start"></a>
<div class="nav-container ">
    <div class="bar bar--sm visible-xs">
        <div class="container">
            <div class="row">
                <div class="col-3 col-md-2">
                    <a href="{{ route('home') }}">
                        <img class="logo logo-dark" alt="logo" src="/assets/front/img/logo-dark.png" />
                        <img class="logo logo-light" alt="logo" src="/assets/front/img/logo-light.png" />
                    </a>
                </div>
                <div class="col-9 col-md-10 text-right">
                    <a href="#" class="hamburger-toggle" data-toggle-class="#menu1;hidden-xs">
                        <i class="icon icon--sm stack-interface stack-menu"></i>
                    </a>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </div>
    <!--end bar-->
    <nav id="menu1" class="bar bar--sm bar-1 hidden-xs bar--transparent bar--absolute">
        <div class="container">
            <div class="row">
                <div class="col-lg-1 col-md-2 hidden-xs">
                    <div class="bar__module">
                        <a href="{{ route('home') }}">
                            <img class="logo logo-dark" alt="logo" src="/assets/front/img/logo-dark.png" />
                            <img class="logo logo-light" alt="logo" src="/assets/front/img/logo-light.png" />
                        </a>
                    </div>
                    <!--end module-->
                </div>
                <div class="col-lg-11 col-md-12 text-right text-left-xs text-left-sm">
                    <ul class="menu-horizontal text-left">
                        <li class="active"><a href="{{ route('home') }}">Accueil</a></li>
                        <li class=""><a href="{{ route('Front.Blog.index') }}">Blog</a></li>
                        <li class=""><a href="{{ route('home') }}">Routes</a></li>
                        <li class="dropdown">
                            <span class="dropdown__trigger">Téléchargement</span>
                            <div class="dropdown__container">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12 dropdown__content dropdown__content--lg">
                                            @foreach(\App\Repository\Asset\AssetRepository::getCoverMenu() as $cover)
                                                <div class="pos-absolute col-lg-5 imagebg hidden-sm hidden-xs" data-overlay="4">
                                                    <div class="background-image-holder">
                                                        <img alt="background" src="{{ \Illuminate\Support\Facades\Storage::disk('s3')->url('download/'.$cover->id.'.png') }}" />
                                                    </div>
                                                    <div class="container pos-vertical-center pl-5">
                                                        <div class="row">
                                                            <div class="col-lg-8">
                                                                <h2 class="color--white">{{ $cover->designation }}</h2>
                                                                <a href="{{ $cover->downloadLink }}" class="btn btn--primary type--uppercase">
                                                                            <span class="btn__text">
                                                                                Télécharger
                                                                            </span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                            <div class="row justify-content-end">
                                                @foreach(\App\Repository\Asset\AssetCategorieRepository::getCategories() as $category)
                                                    <div class="col-lg-2 col-md-4">
                                                        <strong>{{ $category->name }}</strong>
                                                        <hr>
                                                        <ul class="menu-vertical">
                                                            @foreach(\App\Repository\Asset\AssetSubCategorieRepository::getSubCategories($category->id) as $subCategory)
                                                                <li>
                                                                    <a href="elements-accordions.html">
                                                                        {{ $subCategory->name }}
                                                                    </a>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <!--end of row-->
                                        </div>
                                        <!--end dropdown content-->
                                    </div>
                                </div>
                            </div>
                            <!--end dropdown container-->
                        </li>
                        <li class="dropdown">
                            <span class="dropdown__trigger">Tutoriels</span>
                            <div class="dropdown__container">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12 dropdown__content dropdown__content--lg">
                                            @foreach(\App\Repository\Tutoriel\TutorielRepository::getCoverMenu() as $cover)
                                                <div class="pos-absolute col-lg-5 imagebg hidden-sm hidden-xs" data-overlay="4">
                                                    <div class="background-image-holder">
                                                        <img alt="background" src="{{ \Illuminate\Support\Facades\Storage::disk('s3')->url('learning/'.$cover->id.'.png') }}" />
                                                    </div>
                                                    <div class="container pos-vertical-center pl-5">
                                                        <div class="row">
                                                            <div class="col-lg-8">
                                                                <h2 class="color--white">{{ $cover->title }}</h2>
                                                                <span class="h3 color--white">{!! str_limit($cover->content, 50, '...') !!}</span>
                                                                <a href="{{ $cover->slug }}" class="btn btn--primary type--uppercase">
                                                                            <span class="btn__text">
                                                                                Voir le Tutoriel
                                                                            </span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <!--end of row-->
                                                    </div>
                                                </div>
                                            @endforeach
                                            <div class="row justify-content-end">
                                                @foreach(\App\Repository\Tutoriel\TutorielCategorieRepository::getCategories() as $category)
                                                    <div class="col-lg-2 col-md-4">
                                                        <strong>{{ $category->name }}</strong>
                                                        <hr>
                                                        <ul class="menu-vertical">
                                                            @foreach(\App\Repository\Tutoriel\TutorielSubCategorieRepository::getSubCategories($category->id) as $subCategory)
                                                                <li>
                                                                    <a href="elements-accordions.html">
                                                                        {{ $subCategory->name }}
                                                                    </a>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <!--end of row-->
                                        </div>
                                        <!--end dropdown content-->
                                    </div>
                                </div>
                            </div>
                            <!--end dropdown container-->
                        </li>
                        <li class=""><a href="{{ route('home') }}">Wiki</a></li>
                    </ul>
                    <div class="bar__module">
                        @auth()
                            <a class="btn btn--sm btn--primary type--uppercase" href="">
                               <span class="btn__text">
                                   <i class="material-icons">account_circle</i> Mon Compte
                               </span>
                            </a>
                        @else
                            <a class="btn btn--sm btn--primary type--uppercase" href="{{ route('login') }}">
                               <span class="btn__text">
                                   Connexion
                               </span>
                            </a>
                            <a class="btn btn--sm btn--primary-2 type--uppercase" href="{{ route('register') }}">
                               <span class="btn__text">
                                   S'enregistrer
                               </span>
                            </a>
                        @endauth
                    </div>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </nav>
    <!--end bar-->
</div>
<div class="main-container">
    <section class="height-100 imagebg text-center" data-overlay="4">
        <div class="background-image-holder">
            <img alt="background" src="{{ sourceImage("other/register_wall.jpg") }}" />
        </div>
        <div class="container pos-vertical-center">
            <div class="row">
                <div class="col-md-7 col-lg-5">
                    <h2>Connectez-vous</h2>
                    <p class="lead">
                        Bienvenue, connectez-vous grace à vos identifiant Trainznation !
                    </p>
                    @if($errors->has("email"))
                        <div class="alert bg--error">
                            <div class="alert__body">
                                <span class="color--black">{{ $errors->first("email") }}</span>
                            </div>
                            <div class="alert__close">×</div>
                        </div>
                    @endif
                    <form action="{{ route('login') }}" method="post" id="">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <input type="email" name="email" placeholder="Adresse Mail" class="{{ $errors->has('email') ? ' field-error' : '' }}" required/>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-12">
                                <input type="password" name="password" placeholder="Mot de passe" class="{{ $errors->has('email') ? ' field-error' : '' }}" required/>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-12">
                                <button id="btnForm" class="btn btn--primary type--uppercase" type="submit">Connexion</button>
                            </div>
                        </div>
                        <!--end of row-->
                    </form>
                    <span class="type--fine-print block">Vous n'avez pas de compte ?
                                <a href="{{ route('register') }}">Créer mon compte</a>
                            </span>
                    <span class="type--fine-print block">Je ne me souvient plus de mon mot de passe ?
                                <a href="#">Reinitialiser mon mot de passe</a>
                            </span>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-7 col-lg-5">
                    <p class="lead">
                        Bienvenue, connectez-vous grace à un réseau social !
                    </p>
                    <a class="btn block btn--icon bg--googleplus type--uppercase" href="{{ route('Auth.Google.redirect') }}">
                                <span class="btn__text">
                                    <i class="socicon-google"></i>
                                    Me connecter avec Google
                                </span>
                    </a>

                    <a class="btn block btn--icon bg--purple type--uppercase" href="{{ route('Auth.Discord.redirect') }}">
                                <span class="btn__text">
                                    <i class="socicon-discord"></i>
                                    Me connecter avec Discord
                                </span>
                    </a>

                    <a class="btn block btn--icon bg--facebook type--uppercase" href="{{ route('Auth.Facebook.redirect') }}">
                                <span class="btn__text">
                                    <i class="socicon-facebook"></i>
                                    Me connecter avec Facebook
                                </span>
                    </a>

                    <a class="btn block btn--icon bg--twitter type--uppercase" href="{{ route('Auth.Twitter.redirect') }}">
                                <span class="btn__text">
                                    <i class="socicon-twitter"></i>
                                    Me connecter avec Twitter
                                </span>
                    </a>

                    <a class="btn block btn--icon bg--purple type--uppercase" href="{{ route('Auth.Twitch.redirect') }}">
                                <span class="btn__text">
                                    <i class="socicon-twitch"></i>
                                    Me connecter avec Twitch
                                </span>
                    </a>

                    <a class="btn block btn--icon bg--googleplus type--uppercase" href="{{ route('Auth.Youtube.redirect') }}">
                                <span class="btn__text">
                                    <i class="socicon-youtube"></i>
                                    Me connecter avec Youtube
                                </span>
                    </a>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
</div>
<!--<div class="loader"></div>-->
<a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
    <i class="stack-interface stack-up-open-big"></i>
</a>
<script src="/assets/front/js/jquery-3.1.1.min.js"></script>
<script src="/assets/front/js/flickity.min.js"></script>
<script src="/assets/front/js/easypiechart.min.js"></script>
<script src="/assets/front/js/parallax.js"></script>
<script src="/assets/front/js/typed.min.js"></script>
<script src="/assets/front/js/datepicker.js"></script>
<script src="/assets/front/js/isotope.min.js"></script>
<script src="/assets/front/js/ytplayer.min.js"></script>
<script src="/assets/front/js/lightbox.min.js"></script>
<script src="/assets/front/js/granim.min.js"></script>
<script src="/assets/front/js/jquery.steps.min.js"></script>
<script src="/assets/front/js/countdown.min.js"></script>
<script src="/assets/front/js/twitterfetcher.min.js"></script>
<script src="/assets/front/js/spectragram.min.js"></script>
<script src="/assets/front/js/smooth-scroll.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="/assets/front/js/scripts.js"></script>
<script type="text/javascript">
    (function ($) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("#formLogin").on('submit', function (e) {
            e.preventDefault()
            let form = $(this)
            let url = form.attr('action')
            let btn = $("#btnForm")
            let data = form.serializeArray()

            btn.attr('disabled', true)

            $.ajax({
                url: url,
                method: "POST",
                data: data,
                success: function(){
                    window.location='/'
                    btn.removeAttr('disabled')
                },
                error: function(jqxhr, error){
                    console.log("data",data)
                    console.log("error", error)
                    if(data.errors) {toastr.error("Erreur !", data.errors)}
                    btn.removeAttr('disabled')
                }
            })
        })
    })(jQuery)
</script>
</body>
</html>
