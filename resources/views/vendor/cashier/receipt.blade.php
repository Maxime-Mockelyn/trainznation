<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Invoice</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body {
            background: #fff no-repeat;
            background-image: url("/storage/invoice/fond.png");
            font-size: 12px;
            font-family: "Lato", "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
        }
        address{
            margin-top:15px;
        }
        h2 {
            font-size:28px;
            color:#cccccc;
        }
        .container {
            padding-top:200px;
        }
        .table th {
            vertical-align: bottom;
            font-weight: bold;
            padding: 8px;
            line-height: 20px;
            text-align: left;
        }
        .table td {
            padding: 8px;
            line-height: 20px;
            text-align: left;
            vertical-align: top;
            border-top: 1px solid #dddddd;
        }
    </style>
</head>

<body>
<div class="container">
    <table>
        <tr>
            <td style="width: 250px; border-right: #F0F0F0 solid 2px;">
                <table style="padding-bottom: 20px;">
                    <tr>
                        <td style="font-weight: bold; font-size: 15px;">Votre Facture</td>
                    </tr>
                    <tr>
                        <td>N° {{ $id ?? $invoice->id }}</td>
                    </tr>
                </table>
                <table style="padding-bottom: 20px;">
                    <tr>
                        <td style="font-weight: bold; font-size: 15px;">Référence</td>
                    </tr>
                    <tr>
                        <td>{{ $product }}</td>
                    </tr>
                </table>
                <table style="padding-bottom: 20px;">
                    <tr>
                        <td style="font-weight: bold; font-size: 15px;">Date de la facture</td>
                    </tr>
                    <tr>
                        <td>{{ $invoice->date()->format("d/m/Y") }}</td>
                    </tr>
                </table>
            </td>
            <td style="width: 200px;">&nbsp;</td>
            <td style="width: 200px;">
                <table style="padding: 35px; border: solid 1px; border-radius: 10px;">
                    <tr>
                        <td style="font-weight: bold; font-size: 18px;">Identifiant</td>
                    </tr>
                    <tr>
                        <td>{{ $owner->email ?: $owner->name }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table style="margin-top: 40px;">
        <tr>
            <td style="">
                <table style="">
                    <tr>
                        <td style="font-weight: bold;font-size: 24px; text-decoration: underline;">Récapitulatif de la facture N°{{ $id ?? $invoice->id }}</td>
                    </tr>
                </table>
                <table style="margin-top: 15px;">
                    <tr>
                        <td>
                            <table style="border: solid 1px; width: 650px; border-radius: 5px;">
                                <thead>
                                    <tr>
                                        <th style="padding: 10px; border-bottom: solid 2px; background-color: #316fa2; color: white">Description</th>
                                        <th style="padding: 10px; border-bottom: solid 2px; background-color: #316fa2; color: white">Date</th>
                                        <th style="padding: 10px; border-bottom: solid 2px; background-color: #316fa2; color: white">Montant</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="padding: 5px; border-bottom: 1px solid;">Balance de Départ</td>
                                        <td style="padding: 5px; border-bottom: 1px solid;">&nbsp;</td>
                                        <td style="padding: 5px; border-bottom: 1px solid;">{{ $invoice->startingBalance() }}</td>
                                    </tr>
                                    @foreach ($invoice->invoiceItems() as $item)
                                        <tr>
                                            <td colspan="2" style="padding: 5px; border-bottom: 1px solid;">{{ $item->description }}</td>
                                            <td style="padding: 5px; border-bottom: 1px solid;">{{ $item->total() }}</td>
                                        </tr>
                                    @endforeach
                                    @foreach ($invoice->subscriptions() as $subscription)
                                        <tr>
                                            <td style="padding: 5px;">Subscription ({{ $subscription->quantity }})</td>
                                            <td style="padding: 5px;">
                                                {{ $subscription->startDateAsCarbon()->formatLocalized('%B %e, %Y') }} -
                                                {{ $subscription->endDateAsCarbon()->formatLocalized('%B %e, %Y') }}
                                            </td>
                                            <td style="padding: 5px;">{{ $subscription->total() }}</td>
                                        </tr>
                                    @endforeach
                                    @if ($invoice->hasDiscount())
                                        <tr>
                                            @if ($invoice->discountIsPercentage())
                                                <td>{{ $invoice->coupon() }} ({{ $invoice->percentOff() }}% Off)</td>
                                            @else
                                                <td>{{ $invoice->coupon() }} ({{ $invoice->amountOff() }} Off)</td>
                                            @endif
                                            <td>&nbsp;</td>
                                            <td>-{{ $invoice->discount() }}</td>
                                        </tr>
                                    @endif
                                    @if ($invoice->tax_percent)
                                        <tr>
                                            <td>Tax ({{ $invoice->tax_percent }}%)</td>
                                            <td>&nbsp;</td>
                                            <td>{{ Laravel\Cashier\Cashier::formatAmount($invoice->tax) }}</td>
                                        </tr>
                                    @endif
                                    <tr style="border-top:2px solid #fff;">
                                        <td>&nbsp;</td>
                                        <td style="text-align: right; font-size: 15px; padding-top: 5px; padding-bottom: 5px; background-color: #316fa2; color: white"><strong>Total de la facture</strong></td>
                                        <td style="font-size: 15px; padding-top: 5px; padding-bottom: 5px; background-color: #316fa2; color: white"><strong>{{ $invoice->total() }}</strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
