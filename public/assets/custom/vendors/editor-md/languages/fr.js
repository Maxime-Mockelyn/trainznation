(function(){
    let factory = function (exports) {
        let lang = {
            name : "fr",
            description : "Editeur Markdown",
            tocTitle    : "Table de contenue",
            toolbar : {
                undo             : "Annuler(Ctrl+Z)",
                redo             : "Refaire(Ctrl+Y)",
                bold             : "Gras",
                del              : "Barré",
                italic           : "Italique",
                quote            : "Citation",
                ucwords          : "La Première lettre est convertie en majuscule",
                uppercase        : "Selection de texte à convertir en majuscule",
                lowercase        : "Selection de texte à convertir en minuscule",
                h1               : "Heading 1",
                h2               : "Heading 2",
                h3               : "Heading 3",
                h4               : "Heading 4",
                h5               : "Heading 5",
                h6               : "Heading 6",
                "list-ul"        : "Unordered list",
                "list-ol"        : "Ordered list",
                hr               : "Règle horizontal",
                link             : "Lien",
                "reference-link" : "Référence du lien",
                image            : "Image",
                code             : "Code",
                "preformatted-text" : "Texte Préformaté /Block de Code (Tab indent)",
                "code-block"     : "Block de code (Multi-languages)",
                table            : "Tables",
                datetime         : "Datetime",
                emoji            : "Emoji",
                "html-entities"  : "HTML Entities",
                pagebreak        : "Saut de Page",
                watch            : "Ne Pas Voir",
                unwatch          : "Voir",
                preview          : "Prévisualisation HTML (Press Shift + ESC exit)",
                fullscreen       : "Plein Ecran (Press ESC exit)",
                clear            : "Nettoyer",
                search           : "Rechercher",
                help             : "Aide",
                info             : "A Propos " + exports.title
            },
            buttons : {
                enter  : "Entrer",
                cancel : "Annuler",
                close  : "Fermé"
            },
            dialog : {
                link : {
                    title    : "Lien",
                    url      : "Adresse",
                    urlTitle : "Titre",
                    urlEmpty : "Error: Veuillez remplir l'adresse du lien"
                },
                referenceLink : {
                    title    : "Lien de référence",
                    name     : "Nom",
                    url      : "Adresse",
                    urlId    : "ID",
                    urlTitle : "Titre",
                    nameEmpty: "Error: Le nom de la référence ne peut être vide",
                    idEmpty  : "Error: Veuillez renseigner l'identifiant du lien de référence.",
                    urlEmpty : "Error: Veuillez renseigner l'adresse du lien de référence."
                },
                image : {
                    title    : "Image",
                    url      : "Adresse",
                    link     : "Lien",
                    alt      : "Titre",
                    uploadButton     : "Envoyer",
                    imageURLEmpty    : "Error: l'adresse de l'image ne peut pas être vide.",
                    uploadFileEmpty  : "Error: L'image ne peut être vide",
                    formatNotAllowed : "Error: permet uniquement de télécharger des fichiers image, télécharger le format de fichier image autorisé:"
                },
                preformattedText : {
                    title             : "Preformatted text / Codes",
                    emptyAlert        : "Error: Please fill in the Preformatted text or content of the codes.",
                    placeholder       : "coding now...."
                },
                codeBlock : {
                    title             : "Code block",
                    selectLabel       : "Languages: ",
                    selectDefaultText : "select a code language...",
                    otherLanguage     : "Other languages",
                    unselectedLanguageAlert : "Error: Please select the code language.",
                    codeEmptyAlert    : "Error: Please fill in the code content.",
                    placeholder       : "coding now...."
                },
                htmlEntities : {
                    title : "HTML Entities"
                },
                help : {
                    title : "Aide"
                }
            }
        };

        exports.defaults.lang = lang;
    };

    // CommonJS/Node.js
    if (typeof require === "function" && typeof exports === "object" && typeof module === "object")
    {
        module.exports = factory;
    }
    else if (typeof define === "function")  // AMD/CMD/Sea.js
    {
        if (define.amd) { // for Require.js

            define(["editormd"], function(editormd) {
                factory(editormd);
            });

        } else { // for Sea.js
            define(function(require) {
                var editormd = require("../editormd");
                factory(editormd);
            });
        }
    }
    else
    {
        factory(window.editormd);
    }

})();