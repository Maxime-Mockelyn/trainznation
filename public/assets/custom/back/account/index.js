function loadActivity()
{
    let block = $("#activities")

    KTApp.block(block, {
        overlayColor: '#000000',
        type: 'v2',
        state: 'success',
        message: 'Chargement en cours...'
    })

    $.ajax({
        url: '/account/loadActivities',
        success: function (data) {
            KTApp.unblock(block)
            block.html(data)
        }
    })
}

function loadSocialite()
{
    let block = $("#socialite")

    KTApp.block(block, {
        overlayColor: '#000000',
        type: 'v2',
        state: 'success',
        message: 'Chargement en cours...'
    })

    $.ajax({
        url: '/account/loadSocialite',
        success: function (data) {
            KTApp.unblock(block)
            block.html(data)
        }
    })
}

(function ($) {
    loadActivity()
    loadSocialite()
})(jQuery)
