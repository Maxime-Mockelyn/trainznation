(function ($) {
    $("#codePassword").inputmask({
        "mask": "9999"
    });

    $("#form").on('submit', function (e) {
        e.preventDefault()
        let form = $(this)
        let url = form.attr('action')
        let btn = $("#btnForm")
        let data = form.serializeArray()

        KTApp.progress(btn)

        $.ajax({
            url: url,
            method: 'PUT',
            data: data,
            success: function (data) {
                KTApp.unprogress(btn)
                toastr.success("Vos informations principal ont été mis à jour !", "Edition de compte");
            }
        })
    })

    $("#formPassword").on('submit', function (e) {
        e.preventDefault()
        let form = $(this)
        let url = form.attr('action')
        let btn = $("#btnSubmitPassword")
        let data = form.serializeArray()

        KTApp.progress(btn)

        $.ajax({
            url: url,
            method: "PUT",
            data: data,
            success: function (data) {
                KTApp.unprogress(btn)
                toastr.success("Votre mot de passe à été modifier !", "Edition de compte")
            }
        })
    })

    $("#btnDeleteAccount").on('click', function (e) {
        e.preventDefault()
        let btn = $(this)

        KTApp.progress(btn)

        swal.fire({
            title: "Êtes-vous sur ?",
            text: "La suppression de votre compte est définitif.",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'Oui, je veux le supprimer !'
        }).then(function (result) {
            if(result.value)
            {
                $.ajax({
                    url: '/account/delete',
                    success: function (data) {
                        KTApp.unprogress(btn)
                        swal.fire(
                            'Suppression de compte!',
                            'Votre compte à été supprimer !',
                            'success'
                        )

                        setTimeout(function () {
                            window.location='/'
                        }, 1500)
                    }
                })
            }
        })
    })
})(jQuery)
