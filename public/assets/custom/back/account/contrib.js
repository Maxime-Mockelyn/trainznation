function loadTableNotif()
{
    let block = $("#tableNotif");

    KTApp.block(block)

    $.ajax({
        url: '/account/contrib/loadNotif',
        success: function (data) {
            KTApp.unblock(block)
            block.html(data)
        }
    })
}

function loadTableActivity()
{
    let block = $("#tableActivity");

    KTApp.block(block)

    $.ajax({
        url: '/account/contrib/loadActivity',
        success: function (data) {
            KTApp.unblock(block)
            block.html(data)
        }
    })
}

function loadTableBlog()
{
    let block = $("#tableBlog");

    KTApp.block(block)

    $.ajax({
        url: '/account/contrib/loadBlog',
        success: function (data) {
            KTApp.unblock(block)
            block.html(data)
        }
    })
}

(function ($) {
    loadTableNotif()
    loadTableActivity()
    loadTableBlog()
})(jQuery)
