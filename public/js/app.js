/**
 * Ajoute et formate le spinner de progression
 * @param btn
 * @returns {*}
 */
function progressBtn(btn)
{
    return btn.attr('disabled', true)
            .html('<span class="kt-spinner kt-spinner--v2 kt-spinner--sm kt-spinner--brand"></span> En attente...')
}

/**
 * Enlève le spinner de progression
 * @param btn
 * @returns {*}
 */
function unprogressBtn(btn)
{
    return btn.removeAttr('disabled')
            .html(btn.innerHTML)
}

function createCkeditor(block)
{
    ClassicEditor
        .create(document.querySelector(block))
        .then(editor => {
            console.log(editor)
        })
        .catch(error => {
            console.log(error)
        })
}

function toastrResult(data, title){
    if(data.responseStatus === 'error') {
        return toastr.error(data.responseText, title)
    } else if(data.responseStatus === 'warning') {
        return toastr.warning(data.responseText, title)
    } else if(data.responseStatus === 'success') {
        return toastr.success(data.responseText, title)
    } else {
        return toastr.info(data.responseText, title)
    }
}


function formatErrorJson(dataMessage, div)
{
    div.html('<div class="alert alert-danger" role="alert">\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class="alert-text">\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h4 class="alert-heading">Erreur 500</h4>\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p>'+dataMessage+'</p>\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>')
}
