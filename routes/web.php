<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ["as" => "home", "uses" => "HomeController@index"]);
Route::get('/r/{download_id}', ["as" => "Download.redirect", "uses" => "HomeController@redirect"]);
Route::post('/newsletter', ["as" => "Newsletter.subscribe", "uses" => "HomeController@newsletter"]);
Route::get('/launcher', 'HomeController@launcher');


Route::group(["prefix" => "auth", "namespace" => "Auth"], function (){
    Route::group(["prefix" => "google"], function (){
        Route::get('/redirect', ["as" => "Auth.Google.redirect", "uses" => "GoogleController@redirect"]);
        Route::get('/callback', ["as" => "Auth.Google.callback", "uses" => "GoogleController@callback"]);
    });
    Route::group(["prefix" => "discord"], function (){
        Route::get('/redirect', ["as" => "Auth.Discord.redirect", "uses" => "DiscordController@redirect"]);
        Route::get('/callback', ["as" => "Auth.Discord.callback", "uses" => "DiscordController@callback"]);
    });

    Route::group(["prefix" => "facebook"], function (){
        Route::get('/redirect', ["as" => "Auth.Facebook.redirect", "uses" => "FacebookController@redirect"]);
        Route::get('/callback', ["as" => "Auth.Facebook.callback", "uses" => "FacebookController@callback"]);
    });

    Route::group(["prefix" => "twitter"], function (){
        Route::get('/redirect', ["as" => "Auth.Twitter.redirect", "uses" => "TwitterController@redirect"]);
        Route::get('/callback', ["as" => "Auth.Twitter.callback", "uses" => "TwitterController@callback"]);
    });

    Route::group(["prefix" => "twitch"], function (){
        Route::get('/redirect', ["as" => "Auth.Twitch.redirect", "uses" => "TwitchController@redirect"]);
        Route::get('/callback', ["as" => "Auth.Twitch.callback", "uses" => "TwitchController@callback"]);
    });

    Route::group(["prefix" => "youtube"], function (){
        Route::get('/redirect', ["as" => "Auth.Youtube.redirect", "uses" => "YoutubeController@redirect"]);
        Route::get('/callback', ["as" => "Auth.Youtube.callback", "uses" => "YoutubeController@callback"]);
    });
});

Route::group(["prefix" => "youtube"], function (){
    Route::get('/authorize/{uri}', ["as" => "Youtube.authorize", "uses" => "YoutubeController@authorized"]);
    Route::get('/callback', ["as" => "Youtube.callbacks", "uses" => "ToutubeController@callbacks"]);
});

Route::get('/youtube/callback', 'HomeController@callback');

Route::get('/support/increasedPassword/{user_id}', 'SupportController@increasedPassword');

Route::group(["prefix" => "account", "namespace" => "Account", "middleware" => ["verified"]], function (){
    Route::get('/', ["as" => "Account.index", "uses" => "AccountController@index"]);
    Route::get('/info', ["as" => "Account.info", "uses" => "AccountController@info"]);
    Route::get('/badge', ["as" => "Account.badge", "uses" => "AccountController@badge"]);
    Route::get('/contrib', ["as" => "Account.contrib", "uses" => "AccountController@contrib"]);
    Route::put('second', ["as" => "Account.second", "uses" => 'AccountController@second']);
    //Route::get('premium', ["as" => "Account.premium", "uses" => "AccountController@premium"]);

    Route::group(["prefix" => "invoices", "namespace" => "Invoice"], function (){
        Route::get('/', ["as" => "Account.Invoice.index", "uses" => "InvoiceController@index"]);
    });

    Route::group(["prefix" => "premium", "namespace" => "Premium"], function (){
        Route::get('/', ["as" => "Account.Premium.index", "uses" => "PremiumController@index"]);
        Route::post('charge', ["as" => "Account.Premium.charge", "uses" => "PremiumController@charge"]);
        Route::get('/invoice/{invoice_id}', ["as" => "Account.Premium.invoice", "uses" => "PremiumController@invoice"]);
        Route::post('delete', ["as" => "Account.Premium.delete", "uses" => "PremiumController@delete"]);
    });

    Route::group(["prefix" => "formation", "namespace" => "Formation"], function (){
        Route::get('');
    });

    // Ajax Method
    Route::get('loadActivities', 'AccountAjaxController@loadActivities');
    Route::get('loadSocialite', 'AccountAjaxController@loadSocialite');

    Route::put('update', ["as" => "Account.update", "uses" => 'AccountAjaxController@update']);
    Route::put('updatePassword', ["as" => "Account.updatePassword", "uses" => 'AccountAjaxController@updatePassword']);
    Route::put('confirmUpdatePassword', ["as" => "Account.confirmUpdatePassword", "uses" => "AccountAjaxController@confirmUpdatePassword"]);
    Route::get('delete', 'AccountAjaxController@delete');

    Route::get('contrib/loadNotif', 'AccountAjaxController@loadNotif');
    Route::get('contrib/loadActivity', 'AccountAjaxController@loadActivity');
    Route::get('contrib/loadBlog', 'AccountAjaxController@loadBlog');

    Route::get('notification/{notification_id}/read', 'AccountAjaxController@notificationRead');

});

Route::group(["namespace" => "Front"], function (){
    Route::group(["prefix" => "blog", "namespace" => "Blog"], function (){
        Route::get('/', ["as" => "Front.Blog.index", "uses" => "BlogController@index"]);
        Route::get('/{slug}', ["as" => "Front.Blog.show", "uses" => "BlogController@show"]);
        Route::post('/{slug}', ["as" => "Front.Blog.postComment", "uses" => "BlogController@postComment"]);

        Route::get('{slug}/getContenue', "BlogController@getContenue");
    });

    Route::group(["prefix" => "route", "namespace" => "Route"], function (){
        Route::get('/', ["as" => "Front.Route.index", "uses" => "RouteController@index"]);
        Route::group(["prefix" => "pays-de-la-loire"], function (){
            Route::get('/', ["as" => "Route.Pdl.index", "uses" => "PdlController@index"]);

            Route::group(["prefix" => "version"], function (){
                Route::get('one', ["as" => "Pdl.Version.one", "uses" => "PdlController@versionOne"]);
                Route::get('two', ["as" => "Pdl.Version.two", "uses" => "PdlController@versionTwo"]);
                Route::get('three', ["as" => "Pdl.Version.three", "uses" => "PdlController@versionThree"]);
                Route::get('four', ["as" => "Pdl.Version.four", "uses" => "PdlController@versionFour"]);
                Route::get('five', ["as" => "Pdl.Version.five", "uses" => "PdlController@versionFive"]);
                Route::get('six', ["as" => "Pdl.Version.six", "uses" => "PdlController@versionSix"]);
                Route::get('seven', ["as" => "Pdl.Version.seven", "uses" => "PdlController@versionSeven"]);
                Route::get('huit', ["as" => "Pdl.Version.huit", "uses" => "PdlController@versionHuit"]);
            });

            Route::get('gallerie', ["as" => "Route.Pdl.gallerie", "uses" => "PdlController@gallerie"]);
            Route::get('labs', ["as" => "Route.Pdl.labs", "uses" => "PdlController@labs"]);
            Route::get('download', ["as" => "Route.Pdl.download", "uses" => "PdlController@download"]);
        });
        Route::group(["prefix" => "rtp", "namespace" => "Rtp"], function (){
            Route::get('/', ["as" => "Route.Rtp.index", "uses" => "RtpController@index"]);
            Route::group(["prefix" => "ligne1"], function (){
                Route::get('/', ["as" => "Rtp.Ligne1.index", "uses" => "Ligne1Controller@index"]);
                Route::get('gallerie', ["as" => "Rtp.Ligne1.gallerie", "uses" => "Ligne1Controller@gallerie"]);
                Route::get('download', ["as" => "Rtp.Ligne1.download", "uses" => "Ligne1Controller@download"]);
            });
        });
    });

    Route::group(["prefix" => "download", "namespace" => "Download"], function (){
        Route::get('/', ["as" => "Front.Download.index", "uses" => "DownloadController@index"]);
        Route::get('/{subcategory_id}', ["as" => "Front.Download.list", "uses" => "DownloadController@list"]);
        Route::get('{subcategory_id}/{asset_id}', ["as" => "Front.Download.show", "uses" => "DownloadController@show"]);
        Route::get('{subcategory_id}/{asset_id}/tree', ["as" => "Front.Download.tree", "uses" => "DownloadController@tree"]);

        Route::get('{subcategory_id}/{asset_id}/cart', ["as" => "Front.Download.cart", "uses" => "DownloadController@cart"]);
        Route::post('{subcategory_id}/{asset_id}/checkout', ["as" => "Front.Download.checkout", "uses" => "DownloadController@checkout"]);
        Route::get('{subcategory_id}/{asset_id}/{invoice_id}/success', ["as" => "Front.Download.success", "uses" => "DownloadController@success"]);

        Route::get('{subcategory_id}/{asset_id}/getContenue', "DownloadController@getContenue");
    });

    Route::group(["prefix" => "tutoriel", "namespace" => "Tutoriel"], function (){
        Route::get('/', ["as" => "Front.Tutoriel.index", "uses" => "TutorielController@index"]);
        Route::get('/{subcategory_id}', ["as" => "Front.Tutoriel.list", "uses" => "TutorielController@list"]);
        Route::get('/{subcategory_id}/{tutoriel_slug}', ["as" => "Front.Tutoriel.show", "uses" => "TutorielController@show"]);

        Route::post('{subcategory_id}/{tutoriel_slug}', ["as" => "Tutoriel.Comment.store", "uses" => "TutorielController@storeComment"]);

        Route::get('{subcategory_id}/{tutoriel_slug}/getContenue', "TutorielController@getContenue");
    });

    Route::group(["prefix" => "wiki", "namespace" => "Wiki"], function (){
        Route::get('/', ["as" => "Front.Wiki.index", "uses" => "WikiController@index"]);
        Route::get('{category_id}', ["as" => "Front.Wiki.showCategory", "uses" => "WikiController@showCategory"]);
        Route::get('{category_id}/{wiki_id}', ["as" => "Front.Wiki.show", "uses" => "WikiController@show"]);

        Route::get('{category_id}/{wiki_id}/getContenue', 'WikiController@getContenue');
    });

    Route::group(["prefix" => "course", "namespace" => "Course"], function (){
        Route::get('/', ["as" => "Front.Course.dashboard", "uses" => "CourseController@dashboard"]);
        Route::get('/course', ["as" => "Front.Course.index", "uses" => "CourseController@index"]);
        Route::get('/course/{course_slug}', ["as" => "Front.Course.show", "uses" => "CourseController@show"]);
    });

    Route::group(["prefix" => "contact", "namespace" => "Contact"], function (){
        Route::get('/', ["as" => "Front.Contact.index", "uses" => "ContactController@index"]);
    });

    Route::group(["prefix" => "suggestion", "namespace" => "Suggestion"], function (){
        Route::get('/', ["as" => "Suggestion.index", "uses" => "SuggestionController@index"]);
        Route::post('/', ["as" => "Suggestion.store", "uses" => "SuggestionController@store"]);
        Route::get('{suggestion_id}', ["as" => "Suggestion.show", "uses" => "SuggestionController@show"]);
    });
});

Route::group(["prefix" => "back", "namespace" => "Back", "middleware" => ["verified"]], function (){
    Route::get('/', ["as" => "Back.dashboard", "uses" => "BackController@dashboard"]);

    Route::group(["prefix" => "blog", "namespace" => "Blog"], function (){
        Route::get('/', ["as" => "Back.Blog.index", "uses" => "BlogController@index"]);
        Route::get('/create', ["as" => "Back.Blog.create", "uses" => "BlogController@create"]);
        Route::post('/create', ["as" => "Back.Blog.store", "uses" => "BlogController@store"]);
        Route::get('/{slug}', ["as" => "Back.Blog.show", "uses" => "BlogController@show"]);
        Route::get('/{id}/edit', ["as" => "Back.Blog.edit", "uses" => "BlogController@edit"]);
        Route::put('/{id}/edit', ["as" => "Back.Blog.update", "uses" => "BlogController@update"]);
        Route::put('/{id}/editContent', ["as" => "Back.Blog.updateContent", "uses" => "BlogController@updateContent"]);
        Route::put('/{id}/editContenu', ["as" => "Back.Blog.updateContenu", "uses" => "BlogController@updateContenu"]);
        Route::delete('/{slug}', ["as" => "Back.Blog.delete", "uses" => "BlogController@delete"]);
        Route::get('/{id}/delete', ["as" => "Back.Blog.getDelete", "uses" => "BlogController@getDelete"]);

        Route::get('/{id}/publish', ["as" => "Back.Blog.publish", "uses" => "BlogController@publish"]);
        Route::get('/{id}/unPublish', ["as" => "Back.Blog.unPublish", "uses" => "BlogController@unPublish"]);
        Route::get('/{id}/getPublish', ["as" => "Back.Blog.getPublish", "uses" => "BlogController@getPublish"]);
        Route::get('/{id}/getUnPublish', ["as" => "Back.Blog.getUnpublish", "uses" => "BlogController@getUnPublish"]);
        Route::get('/{id}/getContenue', ["as" => "Back.Blog.getContenue", "uses" => "BlogController@getContenue"]);

        Route::group(["prefix" => "category", "namespace" => "Category"], function (){
            Route::get('/create', ["as" => "Blog.Category.create", "uses" => "CategoryController@create"]);
            Route::post('/create', ["as" => "Blog.Category.store", "uses" => "CategoryController@store"]);
            Route::get('/{category_id}/edit', ["as" => "Blog.Category.edit", "uses" => "CategoryController@edit"]);
            Route::put('/{category_id}/edit', ["as" => "Blog.Category.update", "uses" => "CategoryController@update"]);
            Route::delete('/{category_id}', ["as" => "Blog.Category.delete", "uses" => "CategoryController@delete"]);
        });

        Route::group(["prefix" => "{blog_id}/comment", "namespace" => "Comment"], function (){
            Route::get('{comment_id}/approuved', ["as" => "Blog.Comment.approuved", "uses" => "CommentController@approuved"]);
            Route::get('{comment_id}/desapprouved', ["as" => "Blog.Comment.desapprouved", "uses" => "CommentController@desapprouved"]);
        });
    });

    Route::group(["prefix" => "route", "namespace" => "Route"], function (){
        Route::group(["prefix" => "pdl", "namespace" => "Pdl"], function (){

            Route::get('/', ["as" => "Back.Route.Pdl.index", "uses" => "PdlController@index"]);
            Route::get('create', ["as" => "Back.Route.Pdl.create", "uses" => "PdlController@create"]);
            Route::post('create', ["as" => "Back.Route.Pdl.store", "uses" => "PdlController@store"]);
            Route::get('{id}/edit', ["as" => "Back.Route.Pdl.edit", "uses" => "PdlController@edit"]);
            Route::put('{id}/edit', ["as" => "Back.Route.Pdl.update", "uses" => "PdlController@update"]);
            Route::get('{id}/delete', ["as" => "Back.Route.Pdl.delete", "uses" => "PdlController@delete"]);

            Route::get('{id}/getContenue', "PdlController@getContenue");

            Route::group(["prefix" => "anomalie", "namespace" => "PdlAnomalie"], function (){
               Route::get('/', ["as" => "Route.Pdl.Anomalie.index", "uses" => "PdlAnomalieController@index"]);
               Route::post('/', ["as" => "Route.Pdl.Anomalie.store", "uses" => "PdlAnomalieController@store"]);
                Route::get('{anomalie_id}/edit', ["as" => "Route.Pdl.Anomalie.edit", "uses" => "PdlAnomalieController@edit"]);
                Route::put('{anomalie_id}/edit', ["as" => "Route.Pdl.Anomalie.update", "uses" => "PdlAnomalieController@update"]);
                Route::delete('{anomalie_id}', ["as" => "Route.Pdl.Anomalie.delete", "uses" => "PdlAnomalieController@delete"]);
            });

            Route::group(["prefix" => "gallery", "namespace" => "Gallery"], function (){
                Route::get('/', ["as" => "Route.Pdl.Gallery.index", "uses" => "GalleryController@index"]);
                Route::post('/', ["as" => "Route.Pdl.Gallery.store", "uses" => "GalleryController@store"]);
                Route::get('{category_id}', ["as" => "Pdl.Gallery.show", "uses" => "GalleryController@show"]);
                Route::post('{category_id}', ["as" => "Pdl.Gallery.upload", "uses" => "GalleryController@upload"]);
                Route::post('{category_id}/image/delete', ["as" => "Pdl.Gallery.fileDelete", "uses" => "GalleryController@fileDelete"]);

                Route::delete('{categorie_id}', ["as" => "Route.Pdl.Gallery.delete", "uses" => "GalleryController@delete"]);
            });

            Route::group(["prefix" => "download", "namespace" => "Download"], function (){
                Route::get('/', ["as" => "Route.Pdl.Download.index", "uses" => "DownloadController@index"]);
                Route::get('create', ["as" => "Route.Pdl.Download.create", "uses" => "DownloadController@create"]);
                Route::post('create', ["as" => "Route.Pdl.Download.store", "uses" => "DownloadController@store"]);
                Route::get('{download_id}', ["as" => "Route.Pdl.Download.delete", "uses" => "DownloadController@delete"]);

                Route::get('{download_id}/activate', ["as" => "Route.Pdl.Download.activate", "uses" => "DownloadController@activate"]);
                Route::get('{download_id}/desactivate', ["as" => "Route.Pdl.Download.desactivate", "uses" => "DownloadController@desactivate"]);
            });

            Route::post('/upVersion/{next}', ["as" => "Route.Pdl.upVersion", "uses" => "PdlController@upVersion"]);
        });
    });

    Route::group(["prefix" => "download", "namespace" => "Download"], function (){
        Route::get('/', ["as" => "Back.Download.index", "uses" => "DownloadController@index"]);
        Route::get('create', ["as" => "Back.Download.create", "uses" => "DownloadController@create"]);
        Route::post('create', ["as" => "Back.Download.store", "uses" => "DownloadController@store"]);
        Route::get('/{download_id}', ["as" => "Back.Download.show", "uses" => "DownloadController@show"]);
        Route::get('/{download_id}/edit', ["as" => "Back.Download.edit", "uses" => "DownloadController@edit"]);
        Route::put('/{download_id}/edit', ["as" => "Back.Download.update", "uses" => "DownloadController@update"]);
        Route::put('/{download_id}/editContent', ["as" => "Back.Download.updateContent", "uses" => "DownloadController@updateContent"]);
        Route::put('/{download_id}/editContenu', ["as" => "Back.Download.updateContenu", "uses" => "DownloadController@updateContenu"]);
        Route::put('/{download_id}/editPrice', ["as" => "Back.Download.updatePrice", "uses" => "DownloadController@updatePrice"]);
        Route::get('/{download_id}/delete', ["as" => "Back.Download.delete", "uses" => "DownloadController@delete"]);
        Route::get('{download_id}/3d', ["as" => "Back.Download.3d", "uses" => "DownloadController@tree_d"]);

        Route::get('/{download_id}/publish', ["as" => "Back.Download.publish", "uses" => "DownloadController@publish"]);
        Route::get('/{download_id}/unpublish', ["as" => "Back.Download.unpublish", "uses" => "DownloadController@unpublish"]);

        Route::get('/r/{download_id}', ["as" => "Back.Download.redirect", "uses" => "DownloadController@redirect"]);
        Route::get('{download_id}/getContenue', "DownloadController@getContenue");
        Route::get('/listSubCategorie/{categorie_id}', 'DownloadController@listSubcategorie');

        Route::group(["prefix" => "category", "namespace" => "Category"], function (){
            Route::get('/list', ["as" => "Download.Category.index", "uses" => "CategoryController@index"]);
            Route::post('/create', ["as" => "Download.Category.store", "uses" => "CategoryController@store"]);
            Route::get('/{category_id}/edit', ["as" => "Download.Category.edit", "uses" => "CategoryController@edit"]);
            Route::put('/{category_id}/edit', ["as" => "Download.Category.update", "uses" => "CategoryController@update"]);
            Route::delete('/{category_id}', ["as" => "Download.Category.delete", "uses" => "CategoryController@delete"]);
        });

        Route::group(["prefix" => "subcategory", "namespace" => "Subcategory"], function (){
            Route::post('/create', ["as" => "Download.Subcategory.store", "uses" => "SubcategoryController@store"]);
            Route::get('/{subcategory_id}/edit', ["as" => "Download.Subcategory.edit", "uses" => "SubcategoryController@edit"]);
            Route::put('/{subcategory_id}/edit', ["as" => "Download.Subcategory.update", "uses" => "SubcategoryController@update"]);
            Route::delete('/{subcategory_id}', ["as" => "Download.Subcategory.delete", "uses" => "SubcategoryController@delete"]);
        });

        Route::group(["prefix" => "{asset_id}/compatibility"], function (){
            Route::get('/', ["as" => "Back.Download.Compatibility.create", "uses" => "CompatibilityController@create"]);
            Route::post('/', ["as" => "Back.Download.Compatibility.store", "uses" => "CompatibilityController@store"]);
            Route::delete('{compatibility_id}/delete', 'CompatibilityController@delete')->name("Back.Download.Compatibility.delete");
            Route::get('{compatibility_id}', 'CompatibilityController@edit')->name("Back.Download.Compatibility.edit");
            Route::put('{compatibility_id}', 'CompatibilityController@update')->name("Back.Download.Compatibility.update");
        });

        Route::group(["prefix" => "{asset_id}/version"], function (){
            Route::get('/', ["as" => "Download.Version.create", "uses" => "VersionController@create"]);
            Route::post('/', ["as" => "Download.Version.store", "uses" => "VersionController@store"]);
            Route::get('{version_id}/delete', 'VersionController@delete')->name("Download.Version.delete");
            Route::get('{version_id}', 'VersionController@edit')->name("Download.Version.edit");
            Route::put('{version_id}', 'VersionController@update')->name("Download.Version.update");

            Route::post('getAll', 'VersionController@getAll');
            Route::get('{version_id}/getInfo', 'VersionController@getInfo');
        });

        Route::get('/search/{terme}', 'DownloadController@search');
    });

    Route::group(["prefix" => "tutoriel", "namespace" => "Tutoriel"], function (){
        Route::get('/', ["as" => "Back.Tutoriel.index", "uses" => "TutorielController@index"]);
        Route::get('create', ["as" => "Back.Tutoriel.create", "uses" => "TutorielController@create"]);
        Route::post('create', ["as" => "Back.Tutoriel.store", "uses" => "TutorielController@store"]);
        Route::get('{slug}', ["as" => "Back.Tutoriel.show", "uses" => "TutorielController@show"]);
        Route::get('{slug}/edit', ["as" => "Back.Tutoriel.edit", "uses" => "TutorielController@edit"]);
        Route::put('{slug}/edit', ["as" => "Back.Tutoriel.update", "uses" => "TutorielController@update"]);
        Route::put('{slug}/editContent', ["as" => "Back.Tutoriel.updateContent", "uses" => "TutorielController@updateContent"]);
        Route::put('{slug}/editContenu', ["as" => "Back.Tutoriel.updateContenu", "uses" => "TutorielController@updateContenu"]);
        Route::delete('{slug}', ["as" => "Back.Tutoriel.delete", "uses" => "TutorielController@delete"]);
        Route::get('{slug}/delete', ["as" => "Back.Tutoriel.getDelete", "uses" => "TutorielController@getDelete"]);

        Route::get('{tutoriel_id}/publish', ["as" => "Back.Tutoriel.publish", "uses" => "TutorielController@publish"]);
        Route::get('{tutoriel_id}/unpublish', ["as" => "Back.Tutoriel.unpublish", "uses" => "TutorielController@unpublish"]);
        Route::get('{tutoriel_id}/getPublish', ["as" => "Back.Tutoriel.getPublish", "uses" => "TutorielController@getPublish"]);
        Route::get('{tutoriel_id}/getUnpublish', ["as" => "Back.Tutoriel.getUnpublish", "uses" => "TutorielController@getUnpublish"]);

        Route::get('{tutoriel_id}/getContenue', 'TutorielController@getContenue');

        Route::get('/listSubCategorie/{categorie_id}', 'TutorielController@listSubcategorie');

        Route::group(["prefix" => "{tutoriel_id}/video", "namespace" => "Video"], function (){
            Route::post('videoUpload', ["as" => "Tutoriel.Video.upload", "uses" => "VideoController@upload"]);
        });

        Route::group(["prefix" => "{tutoriel_id}/comment", "namespace" => "Comment"], function (){
            Route::get('{comment_id}/getComment', 'CommentController@getComment');
            Route::get('{comment_id}/publish', 'CommentController@publish')->name("Tutoriel.Comment.publish");
            Route::get('{comment_id}/unpublish', 'CommentController@unpublish')->name("Tutoriel.Comment.unpublish");
        });

        Route::group(["prefix"  => "{tutoriel_id}/experience"], function (){
            Route::post('/', ["as" => "Tutoriel.Experience.store", "uses" => "ExperienceController@store"]);
            Route::delete('{experience_id}', ["as" => "Tutoriel.Experience.delete", "uses" => "ExperienceController@delete"]);
        });

        Route::group(["prefix"  => "{tutoriel_id}/technologie"], function (){
            Route::post('/', ["as" => "Tutoriel.Technologie.store", "uses" => "TechnologieController@store"]);
            Route::delete('{technologie_id}', ["as" => "Tutoriel.Technologie.delete", "uses" => "TechnologieController@delete"]);
        });

        Route::group(["prefix" => "category", "namespace" => "Category"], function (){
            Route::get('/list', ["as" => "Tutoriel.Category.index", "uses" => "CategoryController@index"]);
            Route::post('/create', ["as" => "Tutoriel.Category.store", "uses" => "CategoryController@store"]);
            Route::get('/{id}/edit', ["as" => "Tutoriel.Category.edit", "uses" => "CategoryController@edit"]);
            Route::put('/{id}/edit', ["as" => "Tutoriel.Category.update", "uses" => "CategoryController@update"]);
            Route::delete('/{id}', ["as" => "Tutoriel.Category.delete", "uses" => "CategoryController@delete"]);
        });

        Route::group(["prefix" => "subcategory", "namespace" => "Subcategory"], function (){
            Route::get('/list', ["as" => "Tutoriel.Subcategory.index", "uses" => "SubcategoryController@index"]);
            Route::post('/create', ["as" => "Tutoriel.Subcategory.store", "uses" => "SubcategoryController@store"]);
            Route::get('/{id}/edit', ["as" => "Tutoriel.Subcategory.edit", "uses" => "SubcategoryController@edit"]);
            Route::put('/{id}/edit', ["as" => "Tutoriel.Subcategory.update", "uses" => "SubcategoryController@update"]);
            Route::delete('/{id}', ["as" => "Tutoriel.Subcategory.delete", "uses" => "SubcategoryController@delete"]);
        });
    });

    Route::group(["prefix" => "wiki", "namespace" => "Wiki"], function (){
        Route::get('/', ["as" => "Back.Wiki.index", "uses" => "WikiController@index"]);
        Route::get('create', ["as" => "Back.Wiki.create", "uses" => "WikiController@create"]);
        Route::post('store', ["as" => "Back.Wiki.store", "uses" => "WikiController@store"]);
        Route::get('{id}', ["as" => "Back.Wiki.show", "uses" => "WikiController@show"]);
        Route::get('{id}/edit', ["as" => "Back.Wiki.edit", "uses" => "WikiController@edit"]);
        Route::put('{id}/edit', ["as" => "Back.Wiki.update", "uses" => "WikiController@update"]);
        Route::delete('{id}', ["as" => "Back.Wiki.delete", "uses" => "WikiController@delete"]);

        Route::get('{id}/publish', ["as" => "Back.Wiki.publish", "uses" => "WikiController@publish"]);
        Route::get('{id}/unpublish', ["as" => "Back.Wiki.unpublish", "uses" => "WikiController@unpublish"]);

        Route::get('{id}/getPublish', ["as" => "Back.Wiki.getPublish", "uses" => "WikiController@getPublish"]);
        Route::get('{id}/getUnpublish', ["as" => "Back.Wiki.getUnpublish", "uses" => "WikiController@getUnpublish"]);
        Route::get('{id}/getDelete', ["as" => "Back.Wiki.getDelete", "uses" => "WikiController@getDelete"]);

        Route::group(["prefix" => "category"], function (){
            Route::post('create', ["as" => "Back.Wiki.Category.store", "uses" => "CategoryController@store"]);
            Route::get('{id}/edit', ["as" => "Back.Wiki.Category.edit", "uses" => "CategoryController@edit"]);
            Route::put('{id}/edit', ["as" => "Back.Wiki.Category.update", "uses" => "CategoryController@update"]);
            Route::delete('{id}', ["as" => "Back.Wiki.Category.delete", "uses" => "CategoryController@delete"]);
        });

    });

    Route::group(["prefix" => "user", "namespace" => "User"], function (){
        Route::get('/', ["as" => "Back.User.index", "uses" => "UserController@index"]);
    });

    Route::group(["prefix" => "mailbox", "namespace" => "Mailbox"], function (){
        Route::get('/', ["as" => "Back.Mailbox.index", "uses" => "MailboxController@index"]);
        Route::get('/deleteAllMail', ["as" => "Back.Mailbox.deleteAll", "uses" => "MailboxController@deleteAll"]);
        Route::post('/loadMail', ["as" => "Back.Mailbox.loadMail", "uses" => "MailboxController@loadMail"]);
        Route::get('/{id}/delete', ["as" => "Back.Mailbox.deleteMail", "uses" => "MailboxController@deleteMail"]);
    });

    Route::group(["prefix" => "suggestion", "namespace" => "Suggestion"], function (){
        Route::get('/', ["as" => "Back.Suggestion.index", "uses" => "SuggestionController@index"]);
        Route::get('{suggestion_id}', ["as" => "Back.Suggestion.show", "uses" => "SuggestionController@show"]);

        Route::post('{suggestion_id}/rejected', ["as" => "Back.Suggestion.rejected", "uses" => "SuggestionController@rejected"]);
        Route::post('{suggestion_id}/accepted', ["as" => "Back.Suggestion.accepted", "uses" => "SuggestionController@accepted"]);
    });

    Route::group(["prefix" => "notifications", "namespace" => "Notifications"], function (){
        Route::get('create', ["as" => "Back.Notification.create", "uses" => "NotificationController@create"]);
        Route::post('create', ["as" => "Back.Notification.store", "uses" => "NotificationController@store"]);
    });

    Route::group(["prefix" => "course", "namespace" => "Course"], function (){
        Route::group(["prefix" => "category", "namespace" => "Category"], function (){
            Route::get('/', ["as" => "Back.Course.Category.index", "uses" => "CategoryController@index"]);
        });


    });


});

Auth::routes(["verify" => true]);

Route::get('/code', 'TestController@code');
Route::get('/nessie', 'HomeController@nessie');
Route::get('/logout', "Auth\LoginController@logout");
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->name('logs');
Route::get('/newsletter', 'HomeController@viewNewsletter');
Route::match(["get", "post"],'/botman/telegram', 'Botman\BotmanTelegramController@handle');
Route::match(["get", "post"],'/botman/chat', 'Botman\BotmanWebController@handle');
Route::match(["get", "post"],'/botman/slack', 'Botman\BotmanSlackController@handle');

Route::post('search', ["as" => "searchPost", "uses" => "SearchController@searchPost"]);

Route::any('/ckfinder/examples/{example?}', 'CKSource\CKFinderBridge\Controller\CKFinderController@examplesAction')
    ->name('ckfinder_examples');
