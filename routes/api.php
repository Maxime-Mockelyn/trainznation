<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'API\Auth\RegisterController@register');

Route::middleware('auth:api')->group( function () {
    Route::group(["prefix" => "user", "namespace" => "API\User"], function (){
        Route::get('/list', 'UserController@list');
    });

});

Route::group(["prefix" => "blog", "namespace" => "API\Blog"], function (){
    Route::get('/', 'BlogController@all');
    Route::get('/first', 'BlogController@first');
});

Route::group(["prefix" => "suggestion", "namespace" => "API\Suggestion"], function (){
    Route::get('/all', 'SuggestionController@all');
    Route::post('/', 'SuggestionController@create');
});

Route::group(["prefix" => "route", "namespace" => "Api\Route"], function () {
    Route::get('getInfo', 'RouteController@getInfo');
    Route::get('update', 'RouteController@update');
    Route::post('update', 'RouteController@update');
    Route::get('latestDownload', 'RouteController@latestDownload');

    Route::group(["prefix" => "pdl"], function (){
        Route::group(["prefix" => "gallery"], function (){
            Route::get('listView', 'PdlGalleryController@listView');
            Route::get('getGalleryFrom/{category_id}', 'PdlGalleryController@getGalleryFrom');
        });
    });

});

Route::group(["prefix" => "assets", "namespace" => "API\Assets"], function (){

});
