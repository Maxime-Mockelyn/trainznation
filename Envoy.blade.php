@servers(['web' => 'ubuntu@ec2-52-47-182-141.eu-west-3.compute.amazonaws.com'])

@setup
    $dir = '/var/www/trainznation.eu';
    $releases = 3;

    $dirlinks = ["storage/app/public", "storage/framework", "storage/logs"];

    $current = $dir.'/current';
    $shared = $dir.'/shared';
    $repo = $dir.'/repo';
    $release = $dir."/releases/".date('YmdHis');
@endsetup

@macro("deploy")
    createRelease
    composer
    links
    laravel
    currentRelease
    siteUp
@endmacro


@task("prepare")
    echo "Préparation des dossiers en vue du déploiement";
    mkdir -p {{ $repo }};
    mkdir -p {{ $shared }};
    cd {{ $repo }};
    git init --bare;
@endtask

@task("createRelease")
    echo "Création de la release courante";
    mkdir -p {{ $release }};
    cd {{ $repo }};
    git archive master | tar -x -C {{ $release }};
    echo "Création de la release: {{ $release }}";
@endtask

@task("composer")
    echo "Execution de composer";
    mkdir -p {{ $shared }}/vendor;
    ln -s {{ $shared }}/vendor {{ $release }}/vendor;
    cd {{ $release }};
    composer install --no-dev --no-progress;
@endtask

@task("links")
    echo "Création des liens symboliques";
    @foreach ($dirlinks as $link)
        mkdir -p {{ $shared }}/{{ $link }};
        @if(strpos($link, '/'))
            mkdir -p {{ $release }}/{{ dirname($link) }};
        @endif
        chmod 777 {{ $shared }}/{{ $link }};
        ln -s {{ $shared }}/{{ $link }} {{ $release }}/{{ $link }};
    @endforeach
@endtask

@task("laravel")
    echo "Execution des taches LARAVEL";

    cd {{ $release }};
    rm -rf .env;
    cp .env.prod .env;
    php artisan key:generate;

    chmod -R 777 storage/ bootstrap/;

    php artisan clear;
    php artisan route:clear;
    php artisan view:clear;
    php artisan cache:clear;

    yes | php artisan migrate --force;
@endtask

@task("currentRelease")
    rm -rf -f {{ $current }};
    ln -s {{ $release }} {{ $current }};
    ls {{ $dir }}/releases | sort -r | tail -n +{{ $releases + 1 }} |xargs -I{} -r rm -rf {{ $dir }}/releases/{};
    echo "{{ $current }} => {{ $release }}";
@endtask

@task("siteUp")
cd {{ $current }}
php artisan up
@endtask

@finished
    @slack("https://hooks.slack.com/services/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", '#trainznation')
@endfinished


