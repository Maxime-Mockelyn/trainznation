# 20/08/2019
## Automate
- Ajout d'un automatisme de création d'un article récapitulatif sur la semaine

## Téléchargement
- Correction de la route de la gestion des catégories
- Correction des controllers

## Tutoriel
- Correction de la route de la gestion des catégories
- Correction des controllers 

# 22/08/2019
## Général
- Correction de la configuration SMTP

##Auth

- Correction de l'inscription en mode auto et non ajax
- Correction de la distribution du badge de création de compte
- Ajout de la vérification Mail lors de l'inscription

# 28/08/2019
#Download

- Ajout du système d'asset payant

# 01/09/2019

##Front
- Incrustation d'un menu en haut pour la connexion et autre info
- Incrustation d'un Tour

# 03/09/2019

## Front
- Ajout de la fonction de recherche

## Suggestion
- Ajout de la Timeline