<?php

namespace App\Mail\Checkout;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CheckoutDownload extends Mailable
{
    use Queueable, SerializesModels;
    public $route;
    public $asset;

    /**
     * Create a new message instance.
     *
     * @param $route
     * @param $asset
     */
    public function __construct($route, $asset)
    {
        $this->route = $route;
        $this->asset = $asset;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Mail.Checkout.download')->subject('Trainznation - Votre téléchargement');
    }
}
