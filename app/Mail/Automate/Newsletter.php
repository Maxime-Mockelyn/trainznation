<?php

namespace App\Mail\Automate;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Newsletter extends Mailable
{
    use Queueable, SerializesModels;
    public $tutoriel_count;
    public $download_count;
    public $article_count;
    public $wiki_count;
    public $tutoriels;
    public $downloads;
    public $articles;
    public $wikis;

    /**
     * Create a new message instance.
     *
     * @param $tutoriel_count
     * @param $download_count
     * @param $article_count
     * @param $wiki_count
     * @param $tutoriels
     * @param $downloads
     * @param $articles
     * @param $wikis
     */
    public function __construct($tutoriel_count,$download_count,$article_count,$wiki_count, $tutoriels,$downloads,$articles,$wikis)
    {
        //
        $this->tutoriel_count = $tutoriel_count;
        $this->download_count = $download_count;
        $this->article_count = $article_count;
        $this->wiki_count = $wiki_count;
        $this->tutoriels = $tutoriels;
        $this->downloads = $downloads;
        $this->articles = $articles;
        $this->wikis = $wikis;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Mail.Automate.newsletter')
            ->subject("Newsletter du ".Carbon::now()->format('d/m/Y'));
    }
}
