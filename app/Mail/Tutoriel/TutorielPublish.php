<?php

namespace App\Mail\Tutoriel;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TutorielPublish extends Mailable
{
    use Queueable, SerializesModels;
    public $id = 2;
    public $content = "No";

    /**
     * Create a new message instance.
     *
     * @param $tutoriel_id
     * @param $content
     */
    public function __construct($tutoriel_id, $content)
    {

        $this->id = $tutoriel_id;
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('maileclipse::templates.tutorielPublished')
            ->from('trainznation@gmail.com', 'TrainzNation')
            ->subject('Nouveau Tutoriel publier !');
    }
}
