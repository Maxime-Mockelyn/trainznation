<?php

namespace App\Mail\Suggestion;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Rejected extends Mailable
{
    use Queueable, SerializesModels;
    public $rejected;
    public $suggestion;

    /**
     * Create a new message instance.
     *
     * @param $rejected
     * @param $suggestion
     */
    public function __construct($rejected, $suggestion)
    {
        //
        $this->rejected = $rejected;
        $this->suggestion = $suggestion;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Mail.Suggestion.rejected')
            ->subject("Trainznation - Suggestion N°".$this->suggestion->id." - Rejeter");
    }
}
