<?php

namespace App\Mail\Suggestion;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Accepted extends Mailable
{
    use Queueable, SerializesModels;
    public $accepted;
    public $suggestion;

    /**
     * Create a new message instance.
     *
     * @param $accepted
     * @param $suggestion
     */
    public function __construct($accepted, $suggestion)
    {
        //
        $this->accepted = $accepted;
        $this->suggestion = $suggestion;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Mail.Suggestion.accepted')
            ->subject("Trainznation - Suggestion N°".$this->suggestion->id." - Accepter");
    }
}
