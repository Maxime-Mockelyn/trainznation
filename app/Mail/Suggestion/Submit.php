<?php

namespace App\Mail\Suggestion;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Submit extends Mailable
{
    use Queueable, SerializesModels;
    public $suggestion;

    /**
     * Create a new message instance.
     *
     * @param $suggestion
     */
    public function __construct($suggestion)
    {
        //
        $this->suggestion = $suggestion;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Mail.Suggestion.submit')
            ->subject("Trainznation - Soumission de la requete N°".$this->suggestion->id);
    }
}
