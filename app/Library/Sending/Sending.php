<?php
/**
 * Created by PhpStorm.
 * User: Sylth
 * Date: 21/03/2019
 * Time: 19:39
 */

namespace App\Library\Sending;


use SendinBlue\Client\Configuration;

class Sending
{
    public $config;

    public function __construct()
    {
        $config = Configuration::getDefaultConfiguration()->setApiKey('api-key', env("SENDING_APIKEY"));
        $this->config = $config;
    }
}
