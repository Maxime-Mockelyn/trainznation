<?php


namespace App\Library\Twitter;


use Thujohn\Twitter\Facades\Twitter;

class TwitterAccessor
{
    public function postTweet(string $tweet)
    {
        try {
            $newTweet = ["status" => $tweet];

            $twitter = Twitter::postTweet($newTweet);

            return null;
        }catch (\Exception $exception){
            \Log::error(Twitter::logs(), ["code" => $exception->getCode()]);
            \Toastr::error("Erreur Twitter, consulter les logs", "Post Twitter");
            return null;
        }
    }

    public function postTweetWithImage($tweet, $sector, $id){
        try{
            $newTweet = ["status" => $tweet];

            switch ($sector){
                case 'blog':
                    $uploadMedia = Twitter::uploadMedia(["media" => file_get_contents(env('APP_URL').'/storage/blog/'.$id.'.png')]);
                    $newTweet['media_ids'][$uploadMedia->media_id_string] = $uploadMedia->media_id_string;
                    break;

                case 'download':
                    $uploadMedia = Twitter::uploadMedia(["media" => file_get_contents(env('APP_URL').'/storage/download/'.$id.'.png')]);
                    $newTweet['media_ids'][$uploadMedia->media_id_string] = $uploadMedia->media_id_string;
                    break;

                case 'tutoriel':
                    $uploadMedia = Twitter::uploadMedia(["media" => file_get_contents(env('APP_URL').'/storage/learning/'.$id.'.png')]);
                    $newTweet['media_ids'][$uploadMedia->media_id_string] = $uploadMedia->media_id_string;
                    break;
            }

            $twitter = Twitter::postTweet($newTweet);

            return null;
        }catch (\Exception $exception) {
            \Log::error(Twitter::logs(), ["code" => $exception->getCode()]);
            \Toastr::error("Erreur Twitter, consulter les logs", "Post Twitter");
            return null;
        }
    }

    public static function twitterUserTimeline(){
        return Twitter::getUserTimeline(["count" => 5, "format" => "array"]);
    }
}