<?php
/**
 * Created by PhpStorm.
 * User: Sylth
 * Date: 10/05/2019
 * Time: 16:46
 */

namespace App\Library\Youtube;


use Carbon\Carbon;
use Google_Service_YouTube;
use Google_Service_YouTube_Video;
use Google_Service_YouTube_VideoSnippet;
use Google_Service_YouTube_VideoStatus;
use Illuminate\Support\Facades\DB;

class Uploader
{
    private $client;
    public $client_id;
    public $client_secret;

    private $youtube;

    private $accessToken;
    private $devKey;


    /**
     * Uploader constructor.
     *
     */
    public function __construct()
    {
        $this->client_id = env("YOUTUBE_CLIENT_ID");
        $this->client_secret = env("YOUTUBE_CLIENT_SECRET");
        $this->devKey = env("YOUTUBE_DEV_KEY");

        $this->client = new \Google_Client();
        $this->client->setApplicationName(env("APP_NAME"));
        $this->client->setScopes([
            'https://www.googleapis.com/auth/youtube.upload',
        ]);
        try {
            $this->client->setAuthConfig(public_path('storage/client_secret.json'));
        } catch (\Google_Exception $e) {
            return $e->getMessage();
        }
        $this->client->setAccessType('offline');

        if(!$this->client->getAccessToken())
        {
            define('STDIN',fopen("php://stdin","r"));
            $authUrl = $this->client->createAuthUrl();
            printf("Open this link in your browser:\n%s\n", $authUrl);
            print('Enter verification code: ');
            $this->verifAccessToken();
        }

        $this->youtube = new \Google_Service_YouTube($this->client);

    }

    public function verifAccessToken()
    {
        if(session()->exists('youtubeAccessToken') == false)
        {
            print_r("Error Token !");
        }else{
            $this->client->fetchAccessTokenWithAuthCode(session("youtube_code"));
            session(["youtubeAccessToken" => $this->client->getAccessToken()]);
            $this->accessToken = session()->get('youtubeAccessToken');
            return null;
        }
    }


    public function uploads($tutorielId, $title, $description, $tags, $pathFile, $privacy = 'public', $categoryId = 20)
    {
        //dd();
        $client = new \Google_Client();
        $client->setApplicationName("Trainznation");
        $client->setScopes([
            "https://www.googleapis.com/auth/youtube.upload"
        ]);

        try {
            $client->setAuthConfig(public_path('storage/client_secret.json'));
        } catch (\Google_Exception $e) {
            echo $e->getCode().':'.$e->getMessage();
        }
        $client->setAccessType('offline');

        $authUrl = $client->createAuthUrl();
        printf("Open this link in your browser:\n%s\n", $authUrl);
        print('Enter verification code: ');
        $authCode = trim(fgets(STDIN));

        // Exchange authorization code for an access token.
        $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
        $client->setAccessToken($accessToken);

// Define service object for making API requests.
        $service = new Google_Service_YouTube($client);

// Define the $video object, which will be uploaded as the request body.
        $video = new Google_Service_YouTube_Video();

// Add 'snippet' object to the $video object.
        $videoSnippet = new Google_Service_YouTube_VideoSnippet();
        $videoSnippet->setCategoryId('22');
        $videoSnippet->setDescription('Description of uploaded video.');
        $videoSnippet->setTitle('Test video upload.');
        $video->setSnippet($videoSnippet);

// Add 'status' object to the $video object.
        $videoStatus = new Google_Service_YouTube_VideoStatus();
        $videoStatus->setPrivacyStatus('private');
        $video->setStatus($videoStatus);

        $queryParams = [
            'autoLevels' => false,
            'notifySubscribers' => true
        ];

// TODO: For this request to work, you must replace "YOUR_FILE"
//       with a pointer to the actual file you are uploading.
//       The maximum file size for this operation is 64GB.
        $response = $service->videos->insert(
            'snippet,status',
            $video,
            $queryParams,
            array(
                'data' => file_get_contents("YOUR_FILE"),
                'mimeType' => 'video/*',
                'uploadType' => 'multipart'
            )
        );
        print_r($response);

    }

    private function setSnippet($title, $categoryId, $description, $tags)
    {
        $snippet = new \Google_Service_YouTube_VideoSnippet();
        $snippet->setTitle($title);
        $snippet->setCategoryId($categoryId);
        $snippet->setDescription($description);
        $snippet->setTags($tags);

        return $snippet;
    }

    private function setStatus($privacy = 'public')
    {
        $status = new \Google_Service_YouTube_VideoStatus();
        $status->setPrivacyStatus($privacy);

        return $status;
    }



}
