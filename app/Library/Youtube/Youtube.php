<?php
/**
 * Created by PhpStorm.
 * User: Sylth
 * Date: 26/03/2019
 * Time: 14:24
 */

namespace App\Library\Youtube;


use Google_Client;
use Google_Service_YouTube;

class Youtube
{
    private $key_dev;

    private $youtube;

    /**
     * Youtube constructor.
     */
    public function __construct()
    {
        $this->key_dev = env('YOUTUBE_DEV_KEY');

        $client = new Google_Client();
        $client->setDeveloperKey($this->key_dev);

        $this->youtube = new Google_Service_YouTube($client);
    }

    public function videoInfo($video_id)
    {
        $video = $this->youtube->videos->listVideos('id,snippet,contentDetails', ["id" => $video_id]);
        $items = $video->getItems();
        return $items[0]->getContentDetails();
    }

    public function getTimeVideo($video_id)
    {
        $video = $this->youtube->videos->listVideos('id,snippet,contentDetails', ["id" => $video_id]);
        $items = $video->getItems();
        $item = $items[0]->getContentDetails();
        $time = $item->duration;
        return formatYoutubeTime($time);
    }

    public function videoSnippet($video_id)
    {
        $video = $this->youtube->videos->listVideos('id,snippet,contentDetails', ["id" => $video_id]);
        $items = $video->getItems()->getStatus();
        return $items[0]->getSnippet();
    }

    public function videoStatus($video_id)
    {
        $video = $this->youtube->videos->listVideos('id,snippet,contentDetails', ["id" => $video_id]);
        $items = $video->getItems();
        return $items;
    }

    public static function getViewVideo($videos_id)
    {
        $key = env("YOUTUBE_DEV_KEY");
        $client = new Google_Client();
        $client->setDeveloperKey($key);

        $youtube = new Google_Service_YouTube($client);

        $video = $youtube->videos->listVideos('id,snippet,statistics', ["id" => $videos_id]);
        $items = $video->getItems();
        $item = $items[0]->getStatistics();
        return $item->viewCount;
    }
}
