<?php
/**
 * Created by PhpStorm.
 * User: Sylth
 * Date: 20/04/2019
 * Time: 14:44
 */

namespace App\Library\ImapConnector\src;


use Kamaln7\Toastr\Facades\Toastr;
use PhpImap\Exception;
use PhpImap\Mailbox;

class ImapSource
{
    private $mailbox;
    private $inbox_bound;
    private $server;
    private $protocol;
    private $port;
    private $cert;
    private $login;
    private $password;


    public function __construct()
    {

        $this->inbox_bound = config("imap.bound");
        $this->server = config("imap.server");
        $this->protocol = config("imap.protocol");
        $this->port = config('imap.port');
        $this->cert = config('imap.cert');
        $this->login = config('imap.login');
        $this->password = config('imap.password');


    }


    public function MailList()
    {

        $array = [
            "data" => []
        ];

        try {
            $mailbox = $this->mailbox = new Mailbox('{'.$this->server.':'.$this->port.'/imap/'.$this->protocol.'/'.$this->cert.'}INBOX', $this->login, $this->password);
        } catch (Exception $e) {
            return Toastr::error("Erreur lors de la connexion au service Mail", "Module: ImapConnector");
        }

        $mailIds = $mailbox->searchMailbox('ALL');
        $mailbox->sortMails(SORTARRIVAL, false);

        for($i=0; $i < $mailbox->countMails(); $i++)
        {
            $mail = $mailbox->getMail($mailIds[$i]);
            array_push($array['data'], [
                "id"            => $mail->id,
                "object"        => $mail->subject,
                "fromName"      => $mail->fromName,
                "fromAddress"   => $mail->fromAddress,
                "date"          => $mail->date
            ]);
        }

        return (object)$array;

    }

    public function countMail()
    {
        try {
            $mailbox = $this->mailbox = new Mailbox('{'.$this->server.':'.$this->port.'/imap/'.$this->protocol.'/'.$this->cert.'}INBOX', $this->login, $this->password);
        } catch (Exception $e) {
            return Toastr::error("Erreur lors de la connexion au service Mail", "Module: ImapConnector");
        }

        return $mailbox->countMails();
    }

    public function deleteMails()
    {
        try {
            $mailbox = $this->mailbox = new Mailbox('{'.$this->server.':'.$this->port.'/imap/'.$this->protocol.'/'.$this->cert.'}INBOX', $this->login, $this->password);
        } catch (Exception $e) {
            return Toastr::error("Erreur lors de la connexion au service Mail", "Module: ImapConnector");
        }

        $count = 0;

        $mailIds = $mailbox->searchMailbox('ALL');

        for($i=1; $i < $mailbox->countMails(); $i++)
        {
            $mailbox->deleteMail($mailIds[$i]);
            $count++;
        }

        if($count >= 1)
        {
            return $count.' Message !';
        }else{
            return formatPlural('Message', $count);
        }
    }


}
