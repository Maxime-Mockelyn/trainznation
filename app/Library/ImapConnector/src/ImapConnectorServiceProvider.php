<?php

namespace Sylth\ImapConnector;

use Illuminate\Support\ServiceProvider;

class ImapConnectorServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/imap.php' => config_path('imap.php')
        ]);
    }
}
