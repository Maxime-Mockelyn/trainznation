<?php


return [
    'bound'     => env("INBOX_BOUND", 'gmail'),
    'server'    => env("INBOX_SERVER", 'imap.gmail.com'),
    'protocol'  => env("INBOX_PROTOCOL", 'ssl'),
    'port'      => env("INBOX_PORT", '993'),
    'cert'      => env("INBOX_CERT", 'novalidate-cert'),
    'login'     => env("INBOX_LOGIN", ''),
    'password'  => env("INBOX_PASSWORD", "")
];
