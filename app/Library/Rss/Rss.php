<?php
/**
 * Created by PhpStorm.
 * User: Sylth
 * Date: 21/03/2019
 * Time: 03:47
 */

namespace App\Library\Rss;


class Rss
{
    public function lit_rss($fichier, array $objets)
    {
        if($chaine = @implode("", @file($fichier)))
        {
            $tmp = preg_split("/<\/?"."item".">/",$chaine);

            // pour chaque item
            for($i=1;$i<sizeof($tmp)-1;$i+=2)

                // on lit chaque objet de l'item
                foreach($objets as $objet) {

                    // on découpe la chaine pour obtenir le contenu de l'objet
                    $tmp2 = preg_split("/<\/?".$objet.">/",$tmp[$i]);

                    // on ajoute le contenu de l'objet au tableau resultat
                    $resultat[$i-1][] = @$tmp2[1];
                }

            // on retourne le tableau resultat
            return $resultat;
        }
    }
}
