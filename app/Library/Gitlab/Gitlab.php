<?php


namespace App\Library\Gitlab;


class Gitlab
{
    protected $api_access_token;
    protected $endpoint = 'https://gitlab.example.com/api/v4/';

    /**
     * Gitlab constructor.
     * @param $api_access_token
     */
    public function __construct($api_access_token)
    {
        $this->api_access_token = $api_access_token;
    }

    public function boot($uri, array $option = [])
    {
        $ch = curl_init();

        $query = http_build_url($uri, $option);

        return $query;
    }
}