<?php
/**
 * Created by PhpStorm.
 * User: Sylth
 * Date: 30/04/2019
 * Time: 19:19
 */

namespace App\Library\ZipExtractor;


use Illuminate\Support\Facades\Storage;

class ZipExtractor
{
    public function extract($pathExtractDirectory, $zipFileDirectory)
    {
        $verbose = [];
        $zip = zip_open($zipFileDirectory);

        if(!is_resource($zip)){
            trigger_error("Impossible d'ouvrir le fichier ZIP", E_USER_NOTICE);
            return false;
        }

        // Création du dossier d'extraction si il n'existe pas

        if(!is_dir($pathExtractDirectory)){
            if(!mkdir($pathExtractDirectory, 0777, true)){
                trigger_error("Impossible de créer le dossier de destination", E_USER_NOTICE);
            }
            chmod($pathExtractDirectory, 0777);
        }


        // Test du premier élément : si c'est un dossier on extrait uniquement le contenu de ce dossier, si c'est un fichier on on extrait tous les fichiers à la racine du zip
        // Si le premier élément est vide, le zip est vide, et on sort de la fonction
        if( ($zip_entry = zip_read($zip)) === false ){
            trigger_error("Le fichier Zip est vide !", E_USER_NOTICE);
            return false;
        }

        // Dossier conteneur (c'est un dossier si son nom se termine pas \ ou /
        if( preg_match('#[\\/]$#', zip_entry_name($zip_entry)) ){
            $zipRoot = zip_entry_name($zip_entry);
        }
        // Fichiers directement à la racine du zip
        else {
            $zipRoot = '';
            file_put_contents($pathExtractDirectory.'/'.zip_entry_name($zip_entry), zip_entry_read($zip_entry, zip_entry_filesize($zip_entry))); // Extraction du premier fichier
        }

        $verbose[] = 'zipRoot: '.$zipRoot;
        $verbose[] = 'first zip entry: '.zip_entry_name($zip_entry);

        // Parcours du zip (éléments suivants)
        while( ($zip_entry = zip_read($zip)) !== false ){
            $currentEntryName = zip_entry_name($zip_entry);

            // Si l'entrée est un dossier, on l'ignore
            if( preg_match('#[\\/]$#', $currentEntryName) ){
                $verbose[] = "$currentEntryName ignored (is folder)";
                continue;
            }

            // Si l'entrée n'est pas dans le dossier qui nous intéresse ($zipRoot), on l'ignore
            if( !preg_match('#^'.$zipRoot.'[^\\/]+$#', $currentEntryName) ){
                $verbose[] = "$currentEntryName ignored (not in zipRoot)";
                continue;
            }

            $verbose[] = "Extracting ".$currentEntryName;
            $extractFilenameFullpath = $pathExtractDirectory.'/'.basename($currentEntryName);
            file_put_contents($extractFilenameFullpath, zip_entry_read($zip_entry, zip_entry_filesize($zip_entry))); // Extraction du premier fichier
            chmod($extractFilenameFullpath, 0777);
        }

        // debug
        echo '<pre>'.implode("\n", $verbose).'</pre>';
    }

    public function whileDoss($asset_id, $localDrive)
    {
        if($dossier = opendir(public_path('/tmp/'.$asset_id))){
            while(false !== ($fichier = readdir($dossier)))
            {
                if($fichier != '.' && $fichier != '..')
                {
                    if($localDrive == 's3'){

                    }
                }
            }
            closedir($dossier);
        }
        else{
            echo "Le dossier n'a pas pus être ouvert !";
        }
    }
}
