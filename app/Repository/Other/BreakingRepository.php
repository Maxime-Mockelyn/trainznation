<?php
namespace App\Repository\Other;

use App\Model\Other\Breaking;

class BreakingRepository
{
    /**
     * @var Breaking
     */
    private $breaking;

    /**
     * BreakingRepository constructor.
     * @param Breaking $breaking
     */

    public function __construct(Breaking $breaking)
    {
        $this->breaking = $breaking;
    }

    public static function getLatest()
    {
        $breaking = new Breaking();
        $data =  $breaking->newQuery()->limit(1)->orderBy('id', 'desc')->first();
        return $data->content;
    }

}

        