<?php
namespace App\Repository\Badge;

use App\Model\User\BadgeUser;

class BadgeUserRepository
{
    /**
     * @var BadgeUser
     */
    private $badgeUser;

    /**
     * BadgeUserRepository constructor.
     * @param BadgeUser $badgeUser
     */

    public function __construct(BadgeUser $badgeUser)
    {
        $this->badgeUser = $badgeUser;
    }

    public static function badgeIsExists($user_id, $badge_id)
    {
        $badge = new BadgeUser();

        $arg = $badge->newQuery()
            ->where('user_id', $user_id)
            ->where('badge_id', $badge_id)
            ->exists();

        return $arg;
    }

}

