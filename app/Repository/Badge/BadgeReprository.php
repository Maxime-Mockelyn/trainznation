<?php
namespace App\Repository\Badge;

use App\Model\Badge\Badge;
use App\Notifications\Badge\BadgeUnlock;
use App\User;
use Kamaln7\Toastr\Facades\Toastr;

class BadgeReprository
{
    /**
     * @var Badge
     */
    private $badge;

    /**
     * BadgeReprository constructor.
     * @param Badge $badge
     */

    public function __construct(Badge $badge)
    {
        $this->badge = $badge;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function isUnlockedFor(User $user)
    {
        return $this->badge->unlocks()
            ->where('user_id', $user->id)
            ->exists();
    }

    /**
     * @param $user_id
     * @param string $action
     * @param int $count
     * @return null
     */
    public function unlockActionFor($user_id, $action, $count = 0)
    {
        $badge =  $this->badge->newQuery()
            ->where('action', $action)
            ->where('action_count', $count)
            ->first();

        if($badge && !BadgeUserRepository::badgeIsExists($user_id, $badge->id))
        {
            $user = User::find($user_id);
            $user->badges()->attach($badge);
            return $badge;
        }

        return null;
    }

    public function getAllBadges()
    {
        return $this->badge->newQuery()
            ->get();
    }

    public static function isStaticUnlockedFor(User $user, $badge_id)
    {
        $badge = new Badge();
        $arg = $badge->unlocks()->where('user_id', $user->id)
            ->where('badge_id', $badge_id)
            ->exists();

        return $arg;
    }

    public function notifyBadgeUnlocked($user, $badge)
    {
        if($badge)
        {
            $user->notify(new BadgeUnlock($badge));
            Toastr::success("Badge Débloqué !", "Vous avez débloqué le badge ".$badge->name);
        }
    }

}

