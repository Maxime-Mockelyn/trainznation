<?php
namespace App\Repository\Suggestion;

use App\Model\Suggestion\Suggestion;

class SuggestionRepository
{
    /**
     * @var Suggestion
     */
    private $suggestion;

    /**
     * SuggestionRepository constructor.
     * @param Suggestion $suggestion
     */

    public function __construct(Suggestion $suggestion)
    {
        $this->suggestion = $suggestion;
    }

    public function all()
    {
        return $this->suggestion->newQuery()
            ->get();
    }

    public function allForLimit($limit)
    {
        return $this->suggestion->newQuery()
            ->limit($limit)
            ->get();
    }

    public function create($name, $suggestion)
    {
        return $this->suggestion->newQuery()
            ->create([
                "name"  => $name,
                "suggestion" => $suggestion
            ]);
    }

    public function createForAll($name, $type, $sujet, $suggestion, $email)
    {
        return $this->suggestion->newQuery()
            ->create([
                "name"  => $name,
                "type"  => $type,
                "sujet" => $sujet,
                "suggestion" => $suggestion,
                "email" => $email
            ]);
    }

    public function get($suggestion_id)
    {
        return $this->suggestion->newQuery()
            ->find($suggestion_id)
            ->load('timelines');
    }

    public function rejected($suggestion_id)
    {
        $this->suggestion->newQuery()
            ->find($suggestion_id)
            ->update([
                "state" => 2,
                "updated_at" => now()
            ]);
    }

    public function accepted($suggestion_id)
    {
        $this->suggestion->newQuery()
            ->find($suggestion_id)
            ->update([
                "state" => 1,
                "updated_at" => now()
            ]);
    }

    public function searchByLike($search)
    {
        $suggests = $this->suggestion->newQuery()
            ->where('sujet', 'like', '%'.$search.'%')
            ->orWhere('suggestion', 'like', '%'.$search.'%')
            ->get();

        $counts = $suggests->count();

        return [
            "suggests"  => $suggests,
            "counts"    => $counts
        ];

    }

}

        