<?php
namespace App\Repository\Suggestion;

use App\Model\Suggestion\SuggestionTimeline;

class SuggestionTimelineRepository
{
    /**
     * @var SuggestionTimeline
     */
    private $suggestionTimeline;

    /**
     * SuggestionTimelineRepository constructor.
     * @param SuggestionTimeline $suggestionTimeline
     */

    public function __construct(SuggestionTimeline $suggestionTimeline)
    {
        $this->suggestionTimeline = $suggestionTimeline;
    }

    public function rejected($suggestion_id, $sujet, $text)
    {
        $suggest = $this->suggestionTimeline->newQuery()
            ->create([
                "suggestion_id" => $suggestion_id,
                "user_id"       => auth()->user()->id,
                "title"         => "Requete N°".$suggestion_id.': '.$sujet.' (Rejeter)',
                "text"          => "Cette requete à été rejeter pour la cause ci-dessous:<br>".$text
            ]);

        return $suggest;
    }

    public function accepted($suggestion_id, $sujet, $text)
    {
        $suggest = $this->suggestionTimeline->newQuery()
            ->create([
                "suggestion_id" => $suggestion_id,
                "user_id"       => auth()->user()->id,
                "title"         => "Requete N°".$suggestion_id.': '.$sujet.' (Accepter)',
                "text"          => $text.'<br> La requete est désormais dans la roadmap'
            ]);

        return $suggest;
    }

    public function store($suggestion_id, int $user_id, string $title, string $text)
    {
        $suggest = $this->suggestionTimeline->newQuery()
            ->create([
                "suggestion_id" => $suggestion_id,
                "user_id"       => $user_id,
                "title"         => $title,
                "text"          => $text
            ]);

        return $suggest;
    }

}

        