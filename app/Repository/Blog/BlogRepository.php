<?php
namespace App\Repository\Blog;

use App\Model\Blog\Blog;

class BlogRepository
{
    /**
     * @var Blog
     */
    private $blog;

    /**
     * BlogRepository constructor.
     * @param Blog $blog
     */

    public function __construct(Blog $blog)
    {
        $this->blog = $blog;
    }

    public function getLatestPost()
    {
        return $this->blog->newQuery()
            ->where('published', 1)
            ->orderByDesc('published_at')
            ->limit(3)
            ->get()
            ->load('categorie');
    }

    public function getPosts()
    {
        return $this->blog->newQuery()
            ->where('published', 1)
            ->orderByDesc('published_at')
            ->paginate(5);
    }

    public function getPost($slug)
    {
        return $this->blog->newQuery()
            ->where('slug', $slug)
            ->first()->load('categorie', 'comments');
    }

    public static function getMostPostsFromPost($categorie_id)
    {
        $blog = new Blog();
        return $blog->newQuery()
            ->where('categorie_id', $categorie_id)
            ->where('published', 1)
            ->orderByDesc('published_at')
            ->limit(3)
            ->get()
            ->load('categorie');
    }

    public function getAllPosts()
    {
        return $this->blog->newQuery()
            ->get()
            ->load('categorie');
    }

    public function create($category_id, $title, $slug, $published, $twitter, $short_content)
    {
        return $this->blog->newQuery()
            ->create([
                "categorie_id"  => $category_id,
                "title"         => $title,
                "slug"          => $slug,
                "content"       => "#Content",
                "published"     => $published,
                "twitter"       => $twitter,
                "short_content" => $short_content
            ]);
    }

    public function getPostById($blog_id)
    {
        return $this->blog->newQuery()
            ->find($blog_id)
            ->load('categorie');
    }

    public function publish($id)
    {
        return $this->blog->newQuery()
            ->find($id)
            ->update([
                "published"     => 1,
                "published_at"  => now()
            ]);
    }

    public function unPublish($id)
    {
        return $this->blog->newQuery()
            ->find($id)
            ->update([
                "published"     => 0
            ]);
    }

    public function delete($id)
    {
        return $this->blog->newQuery()
            ->find($id)
            ->delete();
    }



    public function getFirstPost()
    {
        return $this->blog->newQuery()
            ->where('published', 1)
            ->orderByDesc('published_at')
            ->limit(1)
            ->first()->load('categorie');
    }

    public static function staticGetFirstPost()
    {
        $blog = new Blog();
        return $blog->newQuery()
            ->where('published', 1)
            ->orderByDesc('published_at')
            ->limit(1)
            ->first()->load('categorie');
    }

    public function update($id, $title, $slug, $category_id, $short_content)
    {
        $this->blog->newQuery()
            ->find($id)
            ->update([
                "categorie_id"  => $category_id,
                "title"         => $title,
                "slug"          => $slug,
                "short_content" => $short_content
            ]);

        return $this->blog->newQuery()
            ->find($id)
            ->load('categorie');
    }

    public function updateContent($id, $twitterText, $published_at)
    {
        $this->blog->newQuery()
            ->find($id)
            ->update([
                "twitterText"   => $twitterText,
                "published_at"  => $published_at
            ]);

        return $this->blog->newQuery()
            ->find($id)
            ->load('categorie');
    }

    public function updateContenu($id, $description)
    {
        $this->blog->newQuery()
            ->find($id)
            ->update([
                "content"   => $description
            ]);

        return $this->blog->newQuery()
            ->find($id)
            ->load('categorie');
    }

    public function searchByLike($search)
    {
        $blog = $this->blog->newQuery()
            ->where('title', 'like', '%'.$search.'%')
            ->get()
            ->load('categorie');

        $count = $blog->count();

        return [
            "blogs" => $blog,
            "counts"    => $count
        ];
    }

    public function countBlogFromWeek()
    {
        return $this->blog->newQuery()
            ->whereBetween('published_at', [now()->startOfWeek(), now()->endOfWeek()])
            ->count();
    }

    public function blogFromWeek()
    {
        return $this->blog->newQuery()
            ->whereBetween('published_at', [now()->startOfWeek(), now()->endOfWeek()])
            ->limit(5)
            ->get()
            ->load('categorie');
    }

}

