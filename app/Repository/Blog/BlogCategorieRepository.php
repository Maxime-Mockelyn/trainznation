<?php
namespace App\Repository\Blog;

use App\Model\Blog\Category;

class BlogCategorieRepository
{
    /**
     * @var Category
     */
    private $category;

    /**
     * BlogCategorieRepository constructor.
     * @param Category $category
     */

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function getAll()
    {
        return $this->category->newQuery()
            ->get();
    }

    public function create($name)
    {
        return $this->category->newQuery()
            ->create([
                "name"  => $name
            ]);
    }

    public function get($category_id)
    {
        return $this->category->newQuery()
            ->find($category_id);
    }

    public function update($id, $name)
    {
        return $this->category->newQuery()
            ->find($id)
            ->update([
                "name"  => $name
            ]);
    }

    public function delete($category_id)
    {
        return $this->category->newQuery()
            ->find($category_id)
            ->delete();
    }

}

