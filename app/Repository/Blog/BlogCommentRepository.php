<?php
namespace App\Repository\Blog;

use App\Model\Blog\BlogComment;

class BlogCommentRepository
{
    /**
     * @var BlogComment
     */
    private $blogComment;

    /**
     * BlogCommentRepository constructor.
     * @param BlogComment $blogComment
     */

    public function __construct(BlogComment $blogComment)
    {
        $this->blogComment = $blogComment;
    }

    public static function getComments($blog_id)
    {

        $blogComment = new BlogComment();
        return $blogComment->newQuery()
            ->where('blog_id', $blog_id)
            ->where('state', 1)
            ->get()
            ->load('user');
    }

    public static function getCountCommentFromBlog($blog_id)
    {
        $blog = new BlogComment();
        return $blog->newQuery()
            ->where('blog_id', $blog_id)
            ->where('state', 1)
            ->get()
            ->count();
    }

    public function addComment($blog_id, $user_id, $comment)
    {
        return $this->blogComment->newQuery()
            ->create([
                "blog_id"   => $blog_id,
                "user_id"   => $user_id,
                "comment"   => $comment
            ]);
    }

    public function approuved($comment_id)
    {
        return $this->blogComment->newQuery()
            ->find($comment_id)
            ->update([
                "state" => 1
            ]);
    }

    public function desapprouved($comment_id)
    {
        return $this->blogComment->newQuery()
            ->find($comment_id)
            ->update([
                "state" => 0
            ]);
    }


}

