<?php
namespace App\Repository\Tutoriel;

use App\Model\Tutoriel\TutorielTechno;

class TutorielTechnoRepository
{
    /**
     * @var TutorielTechno
     */
    private $tutorielTechno;

    /**
     * TutorielTechnoRepository constructor.
     * @param TutorielTechno $tutorielTechno
     */

    public function __construct(TutorielTechno $tutorielTechno)
    {
        $this->tutorielTechno = $tutorielTechno;
    }

    public function list()
    {
        return $this->tutorielTechno->newQuery()
            ->get();
    }

    public function store($tutoriel_id, $technologie)
    {
        return $this->tutorielTechno->newQuery()
            ->create([
                "tutoriel_id"   => $tutoriel_id,
                "techno"        => $technologie,
                "slug"          => str_slug($technologie)
            ]);
    }

    public function delete($techno_id)
    {
        return $this->tutorielTechno->newQuery()
            ->find($techno_id)
            ->delete();
    }

}

