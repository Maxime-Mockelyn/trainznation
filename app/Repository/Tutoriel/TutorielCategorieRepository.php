<?php
namespace App\Repository\Tutoriel;

use App\Model\Tutoriel\TutorielCategory;

class TutorielCategorieRepository
{
    /**
     * @var TutorielCategory
     */
    private $tutorielCategory;

    /**
     * TutorielCategorieRepository constructor.
     * @param TutorielCategory $tutorielCategory
     */

    public function __construct(TutorielCategory $tutorielCategory)
    {
        $this->tutorielCategory = $tutorielCategory;
    }

    public static function getCategories()
    {
        $categories = new TutorielCategory();

        return $categories->newQuery()
            ->get();
    }

    public function list()
    {
        return $this->tutorielCategory->newQuery()
            ->get()
            ->load( 'subcategories');
    }

    public function create($name)
    {
        return $this->tutorielCategory->newQuery()
            ->create([
                "name"  => $name
            ]);
    }

    public function delete($category_id)
    {
        return $this->tutorielCategory->newQuery()
            ->find($category_id)
            ->delete();
    }

    public static function countSubFromCategory($category_id)
    {
        $category = new TutorielCategory();

        return $category->newQuery()
            ->where('tutoriel_category_id', $category_id)
            ->get()
            ->count();
    }

}

