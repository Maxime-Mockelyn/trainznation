<?php
namespace App\Repository\Tutoriel;

use App\Model\Tutoriel\TutorielExperience;

class TutorielExperienceRepository
{
    /**
     * @var TutorielExperience
     */
    private $tutorielExperience;

    /**
     * TutorielExperienceRepository constructor.
     * @param TutorielExperience $tutorielExperience
     */

    public function __construct(TutorielExperience $tutorielExperience)
    {
        $this->tutorielExperience = $tutorielExperience;
    }

    public function store($tutoriel_id, $experience)
    {
        return $this->tutorielExperience->newQuery()
            ->create([
                "tutoriel_id"   => $tutoriel_id,
                "experience"    => $experience
            ]);
    }

    public function delete($experience_id)
    {
        return $this->tutorielExperience->newQuery()
            ->find($experience_id)
            ->delete();
    }

}

