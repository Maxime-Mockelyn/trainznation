<?php
namespace App\Repository\Tutoriel;

use App\Model\Tutoriel\TutorielComment;

class TutorielCommentRepository
{
    /**
     * @var TutorielComment
     */
    private $tutorielComment;

    /**
     * TutorielCommentRepository constructor.
     * @param TutorielComment $tutorielComment
     */

    public function __construct(TutorielComment $tutorielComment)
    {
        $this->tutorielComment = $tutorielComment;
    }

    public static function getCountFromTuto($tuto_id)
    {
        $comment = new TutorielComment();

        return $comment->newQuery()
            ->where('tutoriel_id', $tuto_id)
            ->get()
            ->count();
    }

    public static function getComments($tutoriel_id)
    {
        $comment = new TutorielComment();

        return $comment->newQuery()
            ->where('tutoriel_id', $tutoriel_id)
            ->get()
            ->load('user');
    }

    public function store($tutoriel_id, $id, $comment)
    {
        return $this->tutorielComment->newQuery()
            ->create([
                "tutoriel_id"   => $tutoriel_id,
                "user_id"       => $id,
                "content"       => $comment,
                "published"     => 1,
                "published_at"  => now()
            ]);
    }

    public function get($comment_id)
    {
        return $this->tutorielComment->newQuery()
            ->find($comment_id)
            ->load("user");
    }

    public function publish($comment_id)
    {
        return $this->tutorielComment->newQuery()
            ->find($comment_id)
            ->update([
                "published"   => 1
            ]);
    }

    public function unpublish($comment_id)
    {
        return $this->tutorielComment->newQuery()
            ->find($comment_id)
            ->update([
                "published" => 0
            ]);
    }

}

