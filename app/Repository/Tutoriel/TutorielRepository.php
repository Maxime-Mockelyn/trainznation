<?php
namespace App\Repository\Tutoriel;

use App\Model\Tutoriel\Tutoriel;

class TutorielRepository
{
    /**
     * @var Tutoriel
     */
    private $tutoriel;

    /**
     * TutorielRepository constructor.
     * @param Tutoriel $tutoriel
     */

    public function __construct(Tutoriel $tutoriel)
    {
        $this->tutoriel = $tutoriel;
    }

    public static function getCoverMenu()
    {
        $tutoriel = new Tutoriel();
        return $tutoriel->newQuery()
            ->where('published', 1)
            ->orderBy('published_at', 'desc')
            ->limit(1)
            ->get();
    }

    public function getLatestTuto()
    {
        return $this->tutoriel->newQuery()
            ->where('published', 1)
            ->Orwhere('published', 2)
            ->orderBy('published_at', 'desc')
            ->limit(4)
            ->get()
            ->load('category', 'subcategory');
    }

    public function list()
    {
        return $this->tutoriel->newQuery()
            ->paginate(25);
    }

    public function create($tutoriel_category_id, $tutoriel_sub_category_id, $uuid, $title, $user_id, $slug, $short_content, $published = 0, $published_at = null, $source = 0, $premium = 0)
    {
        return $this->tutoriel->newQuery()
            ->create([
                "tutoriel_category_id"          => $tutoriel_category_id,
                "tutoriel_sub_category_id"      => $tutoriel_sub_category_id,
                "users_id"                      => $user_id,
                "title"                         => $title,
                "slug"                          => $slug,
                "published"                     => $published,
                "published_at"                  => $published_at,
                "source"                        => $source,
                "premium"                       => $premium,
                "uuid"                          => $uuid,
                "short_content"                 => $short_content
            ]);


    }


    public function getTuto($slug)
    {
        return $this->tutoriel->newQuery()
            ->where('slug', $slug)
            ->first()
            ->load('category', 'subcategory', 'user', 'experiences', 'technos', 'sources');
    }

    public function getTutoWithoutLoad($slug)
    {
        return $this->tutoriel->newQuery()
            ->where('slug', $slug)
            ->first();
    }

    public function delete($slug)
    {
        return $this->tutoriel->newQuery()
            ->where('slug', $slug)
            ->first()
            ->delete();
    }

    public function upTime($id, $duration)
    {
        return $this->tutoriel->newQuery()
            ->find($id)
            ->update([
                "time"  => $duration
            ]);
    }

    public function listWithCategory($subcategories_id)
    {
        return $this->tutoriel->newQuery()
            ->where('tutoriel_sub_category_id', $subcategories_id)
            ->where('published', 1)
            ->orWhere('published', 2)
            ->orderBy('published_at', 'desc')
            ->get();
    }

    public function getTutoForId($tutoriel_id)
    {
        return $this->tutoriel->newQuery()
            ->find($tutoriel_id)
            ->load('category', 'subcategory', 'user', 'experiences', 'technos', 'sources');
    }

    public function publish($id)
    {
        return $this->tutoriel->newQuery()
            ->find($id)
            ->update([
                "published"     => 2,
                "published_at"  => now()
            ]);
    }

    public function unpublish($tutoriel_id)
    {
        return $this->tutoriel->newQuery()
            ->find($tutoriel_id)
            ->update([
                "published"     => 0,
                "published_at"  => null
            ]);
    }

    public static function countTutoriel()
    {
        $tutoriel = new Tutoriel();

        return $tutoriel->newQuery()->count();
    }

    public function upLinkVideo($tutoriel_id, string $video_id)
    {
        return $this->tutoriel->newQuery()->find($tutoriel_id)->update(["linkVideo" => $video_id]);
    }

    public function update($slug, $title, $category_id, $subcategory_id, $user_id, $published, $published_at, $sources, $premium, $short_content)
    {
        return $this->tutoriel->newQuery()
            ->where('slug', $slug)
            ->first()
            ->update([
                "title"                     => $title,
                "slug"                      => $slug,
                "tutoriel_category_id"      => $category_id,
                "tutoriel_sub_category_id"  => $subcategory_id,
                "user_id"                   => $user_id,
                "published"                 => $published,
                "published_at"              => $published_at,
                "sources"                   => $sources,
                "premium"                   => $premium,
                "short_content"             => $short_content
            ]);
    }

    public function upYoutubeId($id, $youtube_id)
    {
        return $this->tutoriel->newQuery()->find($id)->update(["youtube_id" => $youtube_id]);
    }

    public function upContenu($slug, $description)
    {
        return $this->tutoriel->newQuery()
            ->where('slug', $slug)
            ->first()
            ->update([
                "content"   => $description
            ]);
    }

    public function searchByLike($search)
    {
        $tutoriels = $this->tutoriel->newQuery()
            ->where('title', 'like', '%'.$search.'%')
            ->get()
            ->load('category', 'subcategory');

        $counts = $tutoriels->count();

        return [
            "tutoriels" => $tutoriels,
            "counts"    => $counts
        ];
    }

    public function countTutorielFromWeek()
    {
        return $this->tutoriel->newQuery()
            ->whereBetween('published_at', [now()->startOfWeek(), now()->endOfWeek()])
            ->count();
    }

    public function tutorielFromWeek()
    {
        return $this->tutoriel->newQuery()
            ->whereBetween('published_at', [now()->startOfWeek(), now()->endOfWeek()])
            ->limit(5)
            ->get()
            ->load('category');
    }


}

