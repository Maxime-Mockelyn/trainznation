<?php
namespace App\Repository\Tutoriel;

use App\Model\Tutoriel\TutorielSource;

class TutorielSourceRepository
{
    /**
     * @var TutorielSource
     */
    private $tutorielSource;

    /**
     * TutorielSourceRepository constructor.
     * @param TutorielSource $tutorielSource
     */

    public function __construct(TutorielSource $tutorielSource)
    {
        $this->tutorielSource = $tutorielSource;
    }

    public function store($id, $sources)
    {
        return $this->tutorielSource->newQuery()
            ->create([
                "tutoriel_id"   => $id,
                "pathSource"    => $sources
            ]);
    }

    public function update($id, $sourceFile)
    {
        return $this->tutorielSource->newQuery()
            ->where('tutoriel_id', $id)
            ->first()
            ->update([
                "pathSource"    => $sourceFile
            ]);
    }

}

        