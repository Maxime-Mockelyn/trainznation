<?php
namespace App\Repository\Tutoriel;

use App\Model\Tutoriel\Tutoriel;
use App\Model\Tutoriel\TutorielSubCategory;

class TutorielSubCategorieRepository
{
    /**
     * @var TutorielSubCategory
     */
    private $tutorielSubCategory;
    /**
     * @var Tutoriel
     */
    private $tutoriel;

    /**
     * TutorielSubCategorieRepository constructor.
     * @param TutorielSubCategory $tutorielSubCategory
     * @param Tutoriel $tutoriel
     */

    public function __construct(TutorielSubCategory $tutorielSubCategory, Tutoriel $tutoriel)
    {
        $this->tutorielSubCategory = $tutorielSubCategory;
        $this->tutoriel = $tutoriel;
    }

    public static function getSubCategories($categorie_id)
    {
        $categories = new TutorielSubCategory();
        return $categories->newQuery()
            ->where('tutoriel_category_id', $categorie_id)
            ->orderBy('name', 'asc')
            ->get();
    }

    public function list()
    {
        return $this->tutorielSubCategory->newQuery()
            ->get()
            ->load('category', 'tutoriels');
    }

    public function create($categorie_id, $name)
    {
        return $this->tutorielSubCategory->newQuery()
            ->create([
                "tutoriel_category_id"  => $categorie_id,
                "name"                  => $name
            ]);
    }

    public function delete($subcategory_id)
    {
        return $this->tutorielSubCategory->newQuery()
            ->find($subcategory_id)
            ->delete();
    }

    public function getForCategory($category_id)
    {
        return $this->tutorielSubCategory->newQuery()
            ->where('tutoriel_category_id', $category_id)
            ->get();
    }

    public function get($subcategories_id)
    {
        return $this->tutorielSubCategory->newQuery()
            ->find($subcategories_id)
            ->load('category');
    }

    public static function countTutorielFromSub($subcategory_id)
    {
        $tutoriel = new Tutoriel();
        return $tutoriel->newQuery()
            ->where('tutoriel_sub_category_id', $subcategory_id)
            ->get()
            ->count();

    }

}

