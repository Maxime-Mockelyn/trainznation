<?php
namespace App\Repository\Checkout;

use App\Model\Invoice\Invoice;

class InvoiceRepository
{
    /**
     * @var Invoice
     */
    private $invoice;

    /**
     * InvoiceRepository constructor.
     * @param Invoice $invoice
     */

    public function __construct(Invoice $invoice)
    {
        $this->invoice = $invoice;
    }

    public function create($email, $asset_id, $price, $user_id = null)
    {
        return $this->invoice->newQuery()
            ->create([
                "email"     => $email,
                "asset_id"  => $asset_id,
                "total"     => $price,
                "user_id"   => $user_id
            ])->load('asset');
    }

    public function get($invoice_id)
    {
        return $this->invoice->newQuery()
            ->find($invoice_id)
            ->load('asset');
    }

    public function listForUser($id)
    {
        return $this->invoice->newQuery()
            ->where('user_id', $id)
            ->get()
            ->load('asset');
    }

}

