<?php
namespace App\Repository\Wiki;

use App\Model\Wiki\WikiCategory;

class WikiCategorieRepository
{
    /**
     * @var WikiCategory
     */
    private $wikiCategory;

    /**
     * WikiCategorieRepository constructor.
     * @param WikiCategory $wikiCategory
     */

    public function __construct(WikiCategory $wikiCategory)
    {
        $this->wikiCategory = $wikiCategory;
    }

    public function all()
    {
        return $this->wikiCategory->newQuery()
            ->get();
    }

    public function create($name)
    {
        return $this->wikiCategory->newQuery()
            ->create([
                "name"  => $name
            ]);
    }

    public function delete($id)
    {
        try {
            return $this->wikiCategory->newQuery()
                ->find($id)
                ->delete();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public static function staticAll()
    {
        $category = new WikiCategory();

        return $category->newQuery()->get();
    }

    public function get($category_id)
    {
        return $this->wikiCategory->newQuery()
            ->find($category_id);
    }

}

