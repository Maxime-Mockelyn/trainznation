<?php
namespace App\Repository\Wiki;

use App\Model\Wiki\Wiki;

class WikiRepository
{
    /**
     * @var Wiki
     */
    private $wiki;

    /**
     * WikiRepository constructor.
     * @param Wiki $wiki
     */

    public function __construct(Wiki $wiki)
    {
        $this->wiki = $wiki;
    }

    public function lastWiki()
    {
        return $this->wiki->newQuery()
            ->where('published', 1)
            ->orderBy('published_at', 'desc')
            ->limit(5)
            ->get()
            ->load('category');
    }

    public function all()
    {
        return $this->wiki->newQuery()
            ->get()
            ->load('category', 'user');
    }

    public function create($title, $category_id, $contents)
    {
        return $this->wiki->newQuery()
            ->create([
                "title"         => $title,
                "content"       => $contents,
                "wiki_category_id"   => $category_id,
                "user_id"       => auth()->user()->id
            ]);
    }

    public function get($id)
    {
        return $this->wiki->newQuery()
            ->find($id)
            ->load('category', 'user');
    }

    public function update($id, $title, $category_id, $contents)
    {
        return $this->wiki->newQuery()
            ->find($id)
            ->update([
                "title" => $title,
                "wiki_category_id"  => $category_id,
                "content"       => $contents
            ]);
    }

    public function delete($id)
    {
        return $this->wiki->newQuery()
            ->find($id)
            ->delete();
    }

    public function publish($id)
    {
        return $this->wiki->newQuery()
            ->find($id)
            ->update([
                "published"     => 1,
                "published_at"  => now()
            ]);
    }

    public function unpublish($id)
    {
        return $this->wiki->newQuery()
            ->find($id)
            ->update([
                "published"     => 0,
                "published_at"  => null
            ]);
    }

    public static function countArticleFromCategory(int $category_id)
    {
        $wiki = new Wiki();

        return $wiki->newQuery()->where('wiki_category_id', $category_id)->get()->count();
    }

    public function getForCategory($category_id)
    {
        return $this->wiki->newQuery()
            ->where('wiki_category_id', $category_id)
            ->where('published', 1)
            ->orderByDesc('published_at')
            ->get()
            ->load('user', 'category');
    }

    public function searchByLike($search)
    {
        $wikis = $this->wiki->newQuery()
            ->where('title', 'like', '%'.$search.'%')
            ->orWhere('content', 'like', '%'.$search.'%')
            ->get()
            ->load('category');

        $count = $wikis->count();

        return [
            "wikis" => $wikis,
            "counts" => $count
        ];
    }

    public function countWikiFromWeek()
    {
        return $this->wiki->newQuery()
            ->whereBetween('published_at', [now()->startOfWeek(), now()->endOfWeek()])
            ->count();
    }

    public function wikiFromWeek()
    {
        return $this->wiki->newQuery()
            ->whereBetween('published_at', [now()->startOfWeek(), now()->endOfWeek()])
            ->limit(5)
            ->get()
            ->load('category');
    }
}

