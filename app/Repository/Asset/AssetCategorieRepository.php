<?php
namespace App\Repository\Asset;

use App\Model\Asset\AssetCategory;

class AssetCategorieRepository
{
    /**
     * @var AssetCategory
     */
    private $assetCategory;

    /**
     * AssetCategorieRepository constructor.
     * @param AssetCategory $assetCategory
     */

    public function __construct(AssetCategory $assetCategory)
    {
        $this->assetCategory = $assetCategory;
    }

    public static function getCategories()
    {
        $categories = new AssetCategory();

        return $categories->newQuery()
            ->get();
    }

    public function getAll()
    {
        return $this->assetCategory->newQuery()
            ->get();
    }

    public function create($name)
    {
        return $this->assetCategory->newQuery()
            ->create([
                "name"  => $name
            ]);
    }

    public function delete($category_id)
    {
        return $this->assetCategory->newQuery()
            ->find($category_id)
            ->delete();
    }

}

