<?php
namespace App\Repository\Asset;

use App\Model\Asset\AssetCompatibility;

class AssetCompatibilityRepository
{
    /**
     * @var AssetCompatibility
     */
    private $assetCompatibility;

    /**
     * AssetCompatibilityRepository constructor.
     * @param AssetCompatibility $assetCompatibility
     */

    public function __construct(AssetCompatibility $assetCompatibility)
    {
        $this->assetCompatibility = $assetCompatibility;
    }

    public function delete($compatibility_id)
    {
        $this->assetCompatibility->newQuery()
            ->find($compatibility_id)
            ->delete();

        return null;
    }

    public function store($asset_id, $trainz_build, $state)
    {
        return $this->assetCompatibility->newQuery()
            ->create([
                "asset_id"      => $asset_id,
                "trainz_build"  => $trainz_build,
                "state"         => $state
            ]);
    }

    public function get($compatibility_id)
    {
        return $this->assetCompatibility->newQuery()
            ->find($compatibility_id);
    }

    public function update($asset_id, $compatibility_id, $trainz_build, $state)
    {
        return $this->assetCompatibility->newQuery()
            ->find($compatibility_id)
            ->update([
                "asset_id"  => $asset_id,
                "trainz_build"  => $trainz_build,
                "state"     => $state
            ]);
    }

}

