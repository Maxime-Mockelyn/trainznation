<?php
namespace App\Repository\Asset;

use App\Model\Asset\AssetSubCategory;

class AssetSubCategorieRepository
{
    /**
     * @var AssetSubCategory
     */
    private $assetSubCategory;

    /**
     * AssetSubCategorieRepository constructor.
     * @param AssetSubCategory $assetSubCategory
     */

    public function __construct(AssetSubCategory $assetSubCategory)
    {
        $this->assetSubCategory = $assetSubCategory;
    }

    public static function getSubCategories($categorie_id)
    {
        $categories = new AssetSubCategory();
        return $categories->newQuery()
            ->where('asset_categorie_id', $categorie_id)
            ->orderBy('name', 'asc')
            ->get();
    }

    public function getAll()
    {
        return $this->assetSubCategory->newQuery()
            ->get()
            ->load('categorie');
    }

    public function create($categorie_id, $name)
    {
        return $this->assetSubCategory->newQuery()
            ->create([
                "asset_categorie_id"    => $categorie_id,
                "name"                  => $name
            ]);
    }

    public function delete($subcategory_id)
    {
        return $this->assetSubCategory->newQuery()
            ->find($subcategory_id)
            ->delete();
    }

    public function getForCategorie($categorie_id)
    {
        return $this->assetSubCategory->newQuery()
            ->where('asset_categorie_id', $categorie_id)
            ->get();
    }

    public function get($subcategory_id)
    {
        return $this->assetSubCategory->newQuery()
            ->find($subcategory_id)
            ->load('categorie');
    }

}

