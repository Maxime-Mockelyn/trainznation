<?php
namespace App\Repository\Asset;

use App\Model\Asset\AssetVersion;

class AssetVersionRepository
{
    /**
     * @var AssetVersion
     */
    private $assetVersion;

    /**
     * AssetVersionRepository constructor.
     * @param AssetVersion $assetVersion
     */

    public function __construct(AssetVersion $assetVersion)
    {
        $this->assetVersion = $assetVersion;
    }

    public function getAllFromAsset($asset_id)
    {
        return $this->assetVersion->newQuery()
            ->where('asset_id', $asset_id)
            ->orderByDesc('created_at')
            ->get();
    }

    public function get($version_id)
    {
        return $this->assetVersion->newQuery()
            ->find($version_id);
    }

    public function store($asset_id, $version_text, $version_log, $version_link)
    {
        return $this->assetVersion->newQuery()
            ->create([
                "asset_id"      => $asset_id,
                "version_text"  => $version_text,
                "version_log"   => $version_log,
                "version_link"  => $version_link
            ]);
    }

}

