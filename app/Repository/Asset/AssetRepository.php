<?php
namespace App\Repository\Asset;

use App\Model\Asset\Asset;

class AssetRepository
{
    /**
     * @var Asset
     */
    private $asset;

    /**
     * AssetRepository constructor.
     * @param Asset $asset
     */

    public function __construct(Asset $asset)
    {
        $this->asset = $asset;
    }

    public static function getCoverMenu()
    {
        $asset = new Asset();

        return $asset->newQuery()
            ->where('published', 1)
            ->orderBy('id', 'desc')
            ->limit(1)
            ->get();
    }

    protected function queryGetAll()
    {
        return $this->asset->newQuery()->orderByDesc('id');
    }

    public function getAll($nbrpages)
    {
        return $this->queryGetAll()->paginate($nbrpages);
    }

    public function get($download_id)
    {
        return $this->asset->newQuery()
            ->find($download_id)
            ->load('categorie', 'subcategorie');
    }



    public function getForSubs($subcategory_id)
    {
        return $this->asset->newQuery()
            ->where('asset_sub_categorie_id', $subcategory_id)
            ->where('published', 1)
            ->orderBy('published_at', 'desc')
            ->get();
    }

    public function publish($download_id)
    {
        return $this->asset->newQuery()
            ->find($download_id)
            ->update([
                "published" => 1,
                "published_at"  => now()
            ]);
    }

    public function unpublish($download_id)
    {
        return $this->asset->newQuery()
            ->find($download_id)
            ->update([
                "published" => 0,
                "published_at"  => null
            ]);
    }

    public function addCountDownload($download_id, $count)
    {
        $newCount = $count+1;

        return $this->asset->newQuery()
            ->find($download_id)
            ->update([
                "count" => $newCount
            ]);
    }

    public function delete($download_id)
    {
        return $this->asset->newQuery()
            ->find($download_id)
            ->delete();
    }


    public function updateLink($asset_id, $version_link)
    {
        return $this->asset->newQuery()
            ->find($asset_id)
            ->update([
                "downloadLink"  => $version_link
            ]);
    }

    public static function countAll()
    {
        $asset = new Asset();

        return $asset->newQuery()->get()->count();
    }

    public function getSearch($terme)
    {
        return $this->asset->newQuery()
            ->where('designation', 'like', '%'.$terme.'%')
            ->get();
    }

    public function getSearchNull()
    {
        return $this->asset->newQuery()
            ->get();
    }

    public function create($category_id, $subcategory_id, $designation, $downloadLink, $kuid, $twitter, $twitterText, $published, ?\Illuminate\Support\Carbon $published_at, $tree, $config, $short_description)
    {
        return $this->asset->newQuery()->create([
            "asset_categorie_id"    => $category_id,
            "asset_sub_categorie_id" => $subcategory_id,
            "designation"           => $designation,
            "short_description"     => $short_description,
            "downloadLink"          => $downloadLink,
            "kuid"                  => $kuid,
            "twitter"               => $twitter,
            "twitterText"           => $twitterText,
            "published"             => $published,
            "published_at"          => $published_at,
            "tree"                  => $tree,
            "config"                => $config
        ]);
    }

    public function updateContent($asset_id, $percent)
    {
        $this->asset->newQuery()
            ->find($asset_id)
            ->update([
                "percent" => $percent
            ]);

        return $this->get($asset_id);
    }

    public function update($asset_id, $category_id, $subcategory_id, $designation, $downloadLink, $kuid, $twitter, $twitterText, $published, ?\Illuminate\Support\Carbon $published_at, $tree, $config, $short_description)
    {
        $this->asset->newQuery()
            ->find($asset_id)
            ->update([
                "asset_categorie_id"    => $category_id,
                "asset_sub_categorie_id"=> $subcategory_id,
                "designation"           => $designation,
                "short_description"     => $short_description,
                "downloadLink"          => $downloadLink,
                "kuid"                  => $kuid,
                "twitter"               => $twitter,
                "twitterText"           => $twitterText,
                "published"             => $published,
                "published_at"          => $published_at,
                "tree"                  => $tree,
                "config"                => $config
            ]);

        return $this->get($asset_id);
    }

    public function updateContenu($asset_id, $description, $short_description)
    {
        $this->asset->newQuery()
            ->find($asset_id)
            ->update([
                "description"       => $description,
                "short_description" => $short_description
            ]);

        return $this->get($asset_id);
    }

    public function updateImage($asset_id)
    {
        $this->asset->newQuery()
            ->find($asset_id)
            ->update([
                "images"    => 1
            ]);

        return $this->get($asset_id);
    }

    public function updatePrice($asset_id, $pricing, $price)
    {
        $this->asset->newQuery()
            ->find($asset_id)
            ->update([
                "pricing"   => $pricing,
                "price"     => $price
            ]);

        return $this->get($asset_id);
    }

    public function searchByLike($search)
    {
        $assets = $this->asset->newQuery()
            ->where('designation', 'like', '%'.$search.'%')
            ->get()
            ->load('categorie', 'subcategorie');

        $count = $assets->count();
        return [
            "assets"    => $assets,
            "counts"    => $count
        ];
    }

    public function countAssetFromWeek()
    {
        return $this->asset->newQuery()
            ->whereBetween('published_at', [now()->startOfWeek(), now()->endOfWeek()])
            ->count();
    }

    public function assetFromWeek()
    {
        return $this->asset->newQuery()
            ->whereBetween('published_at', [now()->startOfWeek(), now()->endOfWeek()])
            ->limit(5)
            ->get()
            ->load('subcategorie');
    }

}

