<?php
namespace App\Repository\User;

use App\User;

class UserRepository
{
    /**
     * @var User
     */
    private $user;

    /**
     * UserRepository constructor.
     * @param User $user
     */

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user->newQuery()
            ->find(auth()->user()->id);
    }

    public function getUserLoad()
    {
        return $this->user->newQuery()
            ->find(auth()->user()->id)
            ->load("account", "blogcomments", "tutoriel", "tutorielcomment", "activities");
    }

    public function getUserbyId($user_id)
    {
        return $this->user->newQuery()
            ->find($user_id);
    }

    public function updateInfo($name, $email)
    {
        return $this->user->newQuery()
            ->find(auth()->user()->id)
            ->update([
                "name"  => $name,
                "email" => $email
            ]);
    }

    public function delete()
    {
        $this->user->newQuery()
            ->find(auth()->user()->id)
            ->delete();
    }

    public function getAll()
    {
        return $this->user->newQuery()
            ->get();
    }

    public function getAllAdministrator()
    {
        return $this->user->newQuery()
            ->where('group', 1)
            ->get();
    }

    public function updatePassword($user_id, string $password)
    {
        return $this->user->newQuery()
            ->find($user_id)
            ->update([
                "password"  => $password
            ]);
    }

    public function createForkGoogle($name, $email, string $password, $id)
    {
        return $this->user->newQuery()
            ->create([
                "name"      => $name,
                "email"     => $email,
                "password"  => $password,
                "google_id" => $id
            ]);
    }

    public function getUserbyEmail($email)
    {
        return $this->user->newQuery()->where('email', $email)->first();
    }

    public function create($name, $email, string $password)
    {
        return $this->user->newQuery()
            ->create([
                "name"      => $name,
                "email"     => $email,
                "password"  => $password,
            ]);
    }


}

