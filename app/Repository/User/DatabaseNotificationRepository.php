<?php
namespace App\Repository\User;

use Illuminate\Notifications\DatabaseNotification;

class DatabaseNotificationRepository
{
    /**
     * @var DatabaseNotification
     */
    private $databaseNotification;

    /**
     * DatabaseNotificationRepository constructor.
     * @param DatabaseNotification $databaseNotification
     */

    public function __construct(DatabaseNotification $databaseNotification)
    {
        $this->databaseNotification = $databaseNotification;
    }

    public function hasRead($notifications_id)
    {
        return $this->databaseNotification->newQuery()
                    ->find($notifications_id)
                    ->update([
                        "read_at"   => now(),
                        "updated_at"=> now()
                    ]);
    }

}

