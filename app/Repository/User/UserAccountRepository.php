<?php
namespace App\Repository\User;


use App\Model\User\UserAccount;

class UserAccountRepository
{
    /**
     * @var UserAccount
     */
    private $userAccount;

    /**
     * UserAccountRepository constructor.
     * @param UserAccount $userAccount
     */

    public function __construct(UserAccount $userAccount)
    {
        $this->userAccount = $userAccount;
    }

    public function login()
    {
        $this->userAccount->newQuery()
            ->where('user_id', auth()->user()->id)
            ->update([
                "last_login" => now()
            ]);
    }

    public function register($user)
    {
        $this->userAccount->newQuery()
            ->insert([
                "user_id"   => $user->id,
                "last_login"=> now()
            ]);
    }

    public function updateInfo($site_web, $pseudo_twitter, $pseudo_facebook, $discord_id, $channel, $trainz_id)
    {
        $this->userAccount->newQuery()
            ->where('user_id', auth()->user()->id)
            ->update([
                "site_web"          => $site_web,
                "pseudo_twitter"    => $pseudo_twitter,
                "pseudo_facebook"   => $pseudo_facebook,
                "discord_id"        => $discord_id,
                "discord_channel"   => $channel,
                "trainz_id"         => $trainz_id
            ]);

        return null;
    }

    public function delete()
    {
        $this->userAccount->newQuery()
            ->where('user_id', auth()->user()->id)
            ->delete();

        return null;
    }

    public function setCodePassword($id, string $code)
    {
        return $this->userAccount->newQuery()
            ->where('user_id', $id)
            ->first()
            ->update([
                "code"  => $code
            ]);
    }

    public function addAvatar()
    {
        return $this->userAccount->newQuery()
            ->where('user_id', auth()->user()->id)
            ->first()
            ->update([
                "avatar"    => 1
            ]);
    }

    public function getAccountFromUser($id)
    {
        return $this->userAccount->newQuery()->where('user_id', $id)->first();
    }

    public function setGooglePseudo($id, $name)
    {
        return $this->userAccount->newQuery()
            ->where('user_id', $id)
            ->first()
            ->update([
                "pseudo_google" => $name
            ]);
    }

    public function addindPseudoByProvider(string $provider, $pseudo, $user_id)
    {
        switch ($provider)
        {
            case 'twitter':
                $this->userAccount->newQuery()->where('user_id', $user_id)
                    ->first()
                    ->update(["pseudo_twitter" => $pseudo]);
                break;

            case 'facebook':
                $this->userAccount->newQuery()->where('user_id', $user_id)
                    ->first()
                    ->update(["pseudo_facebook" => $pseudo]);
                break;

            case 'discord':
                $this->userAccount->newQuery()->where('user_id', $user_id)
                    ->first()
                    ->update(["pseudo_discord" => $pseudo]);
                break;

            case 'google':
                $this->userAccount->newQuery()->where('user_id', $user_id)
                    ->first()
                    ->update(["pseudo_google" => $pseudo]);
                break;

            case 'microsoft':
                $this->userAccount->newQuery()->where('user_id', $user_id)
                    ->first()
                    ->update(["pseudo_microsoft" => $pseudo]);
                break;

            case 'twitch':
                $this->userAccount->newQuery()->where('user_id', $user_id)
                    ->first()
                    ->update(["pseudo_twitch" => $pseudo]);
                break;

            case 'youtube':
                $this->userAccount->newQuery()->where('user_id', $user_id)
                    ->first()
                    ->update(["pseudo_youtube" => $pseudo]);
                break;
        }
    }

    public function subscribePremium($id)
    {
        return $this->userAccount->newQuery()
            ->where('user_id', $id)
            ->first()
            ->update([
                "premium"   => 1,
                "premium_start" => now()
            ]);
    }

    public function deletePremium($id)
    {
        return $this->userAccount->newQuery()
            ->where('user_id', $id)
            ->first()
            ->update([
                "premium"   => 0
            ]);
    }

}

