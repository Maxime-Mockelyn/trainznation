<?php
namespace App\Repository\User;

use App\Model\User\UserActivity;

class UserActivityRepository
{
    /**
     * @var UserActivity
     */
    private $userActivity;

    /**
     * UserActivityRepository constructor.
     * @param UserActivity $userActivity
     */

    public function __construct(UserActivity $userActivity)
    {
        $this->userActivity = $userActivity;
    }

    public function getLatestActivity()
    {
        return $this->userActivity->newQuery()
            ->where('user_id', auth()->user()->id)
            ->whereBetween('created_at', [now()->format('Y-m-d 00:00:00'), now()->format('Y-m-d 23:59:59')])
            ->get();
    }

    public function getActivities()
    {
        return $this->userActivity->newQuery()
            ->where('user_id', auth()->user()->id)
            ->get();
    }

    public function getCountLatestActivity()
    {
        return $this->userActivity->newQuery()
            ->where('user_id', auth()->user()->id)
            ->whereBetween('created_at', [now()->format('Y-m-d 00:00:00'), now()->format('Y-m-d 23:59:59')])
            ->get()
            ->count();
    }

}

