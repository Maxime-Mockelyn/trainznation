<?php
namespace App\Repository\Route\Rtp;

use App\Model\Route\Rtp\RtpLigne1GalleryCategory;

class RtpLigne1GalleryCategoryRepository
{
    /**
     * @var RtpLigne1GalleryCategory
     */
    private $galleryCategory;

    /**
     * RtpLigne1GalleryCategoryRepository constructor.
     * @param RtpLigne1GalleryCategory $galleryCategory
     */

    public function __construct(RtpLigne1GalleryCategory $galleryCategory)
    {
        $this->galleryCategory = $galleryCategory;
    }

}

