<?php
namespace App\Repository\Route\Rtp;

use App\Model\Route\Rtp\RtpLigne1Download;

class RtpLigne1DownloadRepository
{
    /**
     * @var RtpLigne1Download
     */
    private $download;

    /**
     * RtpLigne1DownloadRepository constructor.
     * @param RtpLigne1Download $download
     */

    public function __construct(RtpLigne1Download $download)
    {
        $this->download = $download;
    }

}

