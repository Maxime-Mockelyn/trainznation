<?php
namespace App\Repository\Route\Rtp;
use App\Model\Route\Rtp\RtpLigne1Gallery;

class RtpLigne1GalleryRepository
{
    /**
     * @var RtpLigne1Gallery
     */
    private $gallery;

    /**
     * RtpLigne1GalleryRepository constructor.
     * @param RtpLigne1Gallery $gallery
     */

    public function __construct(RtpLigne1Gallery $gallery)
    {
        $this->gallery = $gallery;
    }

}



