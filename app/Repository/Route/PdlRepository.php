<?php
namespace App\Repository\Route;

use App\Model\Route\Pdl;

class PdlRepository
{
    /**
     * @var Pdl
     */
    private $pdl;

    /**
     * PdlRepository constructor.
     * @param Pdl $pdl
     */

    public function __construct(Pdl $pdl)
    {
        $this->pdl = $pdl;
    }

    public function all()
    {
        return $this->pdl->newQuery()
            ->orderByDesc('id')
            ->get();
    }

    public function create($title, $contents, int $right, int $left)
    {
        return $this->pdl->newQuery()
            ->create([
                "title" => $title,
                "content"  => $contents,
                "right" => $right,
                "left"  => $left
            ]);
    }

    public function get($pdl_id)
    {
        return $this->pdl->newQuery()
            ->find($pdl_id);
    }

    public function update($pdl_id, $title, $contents, int $right, int $left)
    {
        return $this->pdl->newQuery()
            ->find($pdl_id)
            ->update([
                "title" => $title,
                "content"   => $contents,
                "right" => $right,
                "left"  => $left
            ]);
    }

    public function delete($pdl_id)
    {
        return $this->pdl->newQuery()
            ->find($pdl_id)
            ->delete();
    }

}

        