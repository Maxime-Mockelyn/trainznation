<?php
namespace App\Repository\Route;

use App\Model\Route\PdlGallery;

class PdlGalleryRepository
{
    /**
     * @var PdlGallery
     */
    private $pdlGallery;

    /**
     * PdlGalleryRepository constructor.
     * @param PdlGallery $pdlGallery
     */

    public function __construct(PdlGallery $pdlGallery)
    {
        $this->pdlGallery = $pdlGallery;
    }

    /**
     * Retourne le nombre d'image de la catégorie
     * @param $category_id
     * @return int
     */
    public static function countPictFromCategory($category_id)
    {
        $gallery = new PdlGallery();

        return $gallery->newQuery()->where('pdl_gallery_category_id', $category_id)->get()->count();
    }

    /**
     * Insert une nouvelle image en base de donnée
     * @param $category_id
     * @param string $imageName
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function store($category_id, string $imageName)
    {
        return $this->pdlGallery->newQuery()
            ->create([
                "pdl_gallery_category_id"   => $category_id,
                "filename"                  => $imageName
            ]);
    }

    /**
     * Supprime une image en base de donnée
     * @param string $filename
     * @return mixed
     */
    public function delete(string $filename)
    {
        return $this->pdlGallery->newQuery()
            ->where('filename', $filename)
            ->delete();
    }

    public function getForCategory($category_id)
    {
        return $this->pdlGallery->newQuery()
            ->where('pdl_gallery_category_id', $category_id)
            ->orderByDesc('created_at')
            ->get();
    }

    public function getAll()
    {
        return $this->pdlGallery->newQuery()
            ->orderByDesc('created_at')
            ->get()
            ->load('categorie');
    }

    public function getLast()
    {
        return $this->pdlGallery->newQuery()
            ->orderByDesc('created_at')
            ->limit(1)
            ->first();
    }

}

