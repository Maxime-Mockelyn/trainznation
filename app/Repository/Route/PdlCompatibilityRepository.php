<?php
namespace App\Repository\Route;

use App\Model\Route\PdlCompatibility;

class PdlCompatibilityRepository
{
    /**
     * @var PdlCompatibility
     */
    private $pdlCompatibility;

    /**
     * PdlCompatibilityRepository constructor.
     * @param PdlCompatibility $pdlCompatibility
     */

    public function __construct(PdlCompatibility $pdlCompatibility)
    {
        $this->pdlCompatibility = $pdlCompatibility;
    }

    public function create($nextVersion, $state_37, $state_45, $state_46)
    {
        $this->pdlCompatibility->newQuery()->create(["version" => $nextVersion, "trainz_build" => "3.7", "state" => $state_37]);
        $this->pdlCompatibility->newQuery()->create(["version" => $nextVersion, "trainz_build" => "4.5", "state" => $state_45]);
        $this->pdlCompatibility->newQuery()->create(["version" => $nextVersion, "trainz_build" => "4.6", "state" => $state_46]);

        return null;
    }

    public static function getCompatibilityVersion($version)
    {
        $comp = new PdlCompatibility();

        return $comp->newQuery()
            ->where('version', $version)
            ->get();

    }

}

