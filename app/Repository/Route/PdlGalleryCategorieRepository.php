<?php
namespace App\Repository\Route;

use App\Model\Route\PdlGalleryCategory;

class PdlGalleryCategorieRepository
{
    /**
     * @var PdlGalleryCategory
     */
    private $pdlGalleryCategory;

    /**
     * PdlGalleryCategorieRepository constructor.
     * @param PdlGalleryCategory $pdlGalleryCategory
     */

    public function __construct(PdlGalleryCategory $pdlGalleryCategory)
    {
        $this->pdlGalleryCategory = $pdlGalleryCategory;
    }

    public function all()
    {
        return $this->pdlGalleryCategory->newQuery()->get()->load('galleries');
    }

    public function get($category_id)
    {
        return $this->pdlGalleryCategory->newQuery()
            ->find($category_id)
            ->load('galleries');
    }

    public function store($name)
    {
        return $this->pdlGalleryCategory->newQuery()
            ->create([
                "name"  => $name
            ]);
    }

    public function delete($category_id)
    {
        return $this->pdlGalleryCategory->newQuery()
            ->find($category_id)
            ->delete();
    }

}

