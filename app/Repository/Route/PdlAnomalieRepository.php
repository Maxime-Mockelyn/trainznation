<?php
namespace App\Repository\Route;

use App\Model\Route\PdlAnomalie;

class PdlAnomalieRepository
{
    /**
     * @var PdlAnomalie
     */
    private $pdlAnomalie;

    /**
     * PdlAnomalieRepository constructor.
     * @param PdlAnomalie $pdlAnomalie
     */

    public function __construct(PdlAnomalie $pdlAnomalie)
    {
        $this->pdlAnomalie = $pdlAnomalie;
    }

    public function getAllAnomalie()
    {
        return $this->pdlAnomalie->newQuery()
            ->get();
    }

    public function count()
    {
        return $this->pdlAnomalie->newQuery()
            ->get()
            ->count();
    }

    public function sumStateSuccess()
    {
        return $this->pdlAnomalie->newQuery()
            ->where('state', 2)
            ->get()
            ->count();
    }

    public function percent()
    {
        $count = $this->count();
        $success = $this->sumStateSuccess();

        if($count == 0){
            return 0;
        }else{
            return round($success*100/$count, '2');
        }
    }

    public function create($anomalie, $correction, $lieu, $state = 0)
    {
        return $this->pdlAnomalie->newQuery()
            ->create([
                "anomalie"      => $anomalie,
                "correction"    => $correction,
                "lieu"          => $lieu,
                "state"         => $state
            ]);
    }

    public function get($anomalie_id)
    {
        return $this->pdlAnomalie->newQuery()
            ->find($anomalie_id);
    }

    public function delete($anomalie_id)
    {
        return $this->pdlAnomalie->newQuery()
            ->find($anomalie_id)
            ->delete();
    }

    public function update($anomalie_id, $anomalie, $correction, $lieu, $state = 0)
    {
        return $this->pdlAnomalie->newQuery()
            ->find($anomalie_id)
            ->update([
                "anomalie"  => $anomalie,
                "correction"=> $correction,
                "lieu"      => $lieu,
                "state"     => $state
            ]);
    }

    public function getAnomaliesDraft()
    {
        return $this->pdlAnomalie->newQuery()
            ->where('state', 0)
            ->orWhere('state', 1)
            ->get();
    }

    public function trashTerminatedAnomalie()
    {
        return $this->pdlAnomalie->newQuery()
            ->where('state', 2)
            ->delete();
    }

    public function getTerminatedAnomaly()
    {
        return $this->pdlAnomalie->newQuery()
            ->where('state', 2)
            ->get();
    }

}

