<?php
namespace App\Repository\Route;

use App\Model\Route\PdlTimeline;

class PdlTimelineRepository
{
    /**
     * @var PdlTimeline
     */
    private $pdlTimeline;

    /**
     * PdlTimelineRepository constructor.
     * @param PdlTimeline $pdlTimeline
     */

    public function __construct(PdlTimeline $pdlTimeline)
    {
        $this->pdlTimeline = $pdlTimeline;
    }

    public function create($version, $description)
    {
        return $this->pdlTimeline->newQuery()
            ->create([
                "version"       => $version,
                "description"   => $description,
                "release_at"    => now()
            ]);
    }

    public function getAllTimeline()
    {
        return $this->pdlTimeline->newQuery()
            ->orderBy('id', 'desc')
            ->get();
    }

}

