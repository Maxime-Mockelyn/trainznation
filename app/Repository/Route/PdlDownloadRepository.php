<?php
namespace App\Repository\Route;

use App\Model\Route\PdlDownload;

class PdlDownloadRepository
{
    /**
     * @var PdlDownload
     */
    private $pdlDownload;

    /**
     * PdlDownloadRepository constructor.
     * @param PdlDownload $pdlDownload
     */

    public function __construct(PdlDownload $pdlDownload)
    {
        $this->pdlDownload = $pdlDownload;
    }

    public function getAll()
    {
        return $this->pdlDownload->newQuery()
            ->get();
    }

    public function store($version, $build, $typeDownload, $typeRelease, $linkDownload, $note, int $published)
    {
        return $this->pdlDownload->newQuery()
            ->create([
                "version"   => $version,
                "build"     => $build,
                "typeDownload" => $typeDownload,
                "typeRelease" => $typeRelease,
                "linkDownload" => $linkDownload,
                "note"      => $note,
                "published" => $published
            ]);
    }

    public function activate($download_id)
    {
        return $this->pdlDownload->newQuery()
            ->find($download_id)
            ->update([
                "published" => 1
            ]);
    }

    public function desactivate($download_id)
    {
        return $this->pdlDownload->newQuery()
            ->find($download_id)
            ->update([
                "published" => 0
            ]);
    }

    public function delete($download_id)
    {
        return $this->pdlDownload->newQuery()
            ->find($download_id)
            ->delete();
    }

    public function getAllForPublish()
    {
        return $this->pdlDownload->newQuery()
            ->where('published', 1)
            ->orderBy('created_at', 'desc')
            ->get();
    }

    public function getForBuild($build)
    {
        return $this->pdlDownload->newQuery()
            ->where('build', $build)
            ->where('typeDownload', 1)
            ->first();
    }

    public function getLatestBuild()
    {
        return $this->pdlDownload->newQuery()
            ->orderByDesc('build')
            ->first();
    }

    public function getLatestBuildActivate()
    {
        return $this->pdlDownload->newQuery()
            ->where('published', 1)
            ->orderByDesc('build')
            ->first();
    }

}

