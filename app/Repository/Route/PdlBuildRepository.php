<?php
namespace App\Repository\Route;

use App\Model\Route\PdlBuild;

class PdlBuildRepository
{
    /**
     * @var PdlBuild
     */
    private $pdlBuild;

    /**
     * PdlBuildRepository constructor.
     * @param PdlBuild $pdlBuild
     */

    public function __construct(PdlBuild $pdlBuild)
    {
        $this->pdlBuild = $pdlBuild;
    }

    public static function getBuildFromVersion($version)
    {
        $build = new PdlBuild();
        $v = $build->newQuery()
            ->where('version', $version)
            ->first();

        if($v)
        {
            return $v->build;
        }else{
            return "Aucun Build actuellement !";
        }
    }

    public function getActualBuild()
    {
        return $this->pdlBuild->newQuery()
            ->orderBy('id', 'desc')
            ->first();
    }

    public function getFirstBuild()
    {
        return $this->pdlBuild->newQuery()
            ->first();
    }

    public function update(float $newBuild)
    {
        return $this->pdlBuild->newQuery()
            ->orderBy('id', 'desc')
            ->first()
            ->update([
                "build" => $newBuild
            ]);
    }

    public function create($nextVersion, $latestBuild)
    {
        return $this->pdlBuild->newQuery()
            ->create([
                "version"   => $nextVersion,
                "build"     => $latestBuild
            ]);
    }

}

