<?php


namespace App\HelperClass;


class Images
{
    public function __construct()
    {
    }

    public function images()
    {
        $images = [
            0 => [
                "id"    => 1,
                "icon" => "other/icons/auth_login.png",
                "text" => "Connexion de l'utilisateur"
            ],

            1 => [
                "id"    => 2,
                "icon" => "other/icons/auth_register.png",
                "text" => "Enregistrement de l'utilisateur"
            ],

            2 => [
                "id"    => 3,
                "icon" => "other/icons/discord.png",
                "text" => "Discord"
            ],

            3 => [
                "id"    => 4,
                "icon" => "other/icons/facebook.png",
                "text" => "Facebook"
            ],

            4 => [
                "id"    => 5,
                "icon" => "other/icons/google.png",
                "text" => "Google"
            ],

            5 => [
                "id"    => 6,
                "icon" => "other/icons/microsoft.png",
                "text" => "Microsoft"
            ],

            6 => [
                "id"    => 7,
                "icon" => "other/icons/other.png",
                "text" => "Autre"
            ],

            7 => [
                "id"    => 8,
                "icon" => "other/icons/question.png",
                "text" => "Question"
            ],

            8 => [
                "id"    => 9,
                "icon" => "other/icons/road.png",
                "text" => "Route"
            ],

            9 => [
                "id"    => 10,
                "icon" => "other/icons/train.png",
                "text" => "Train & Consist"
            ],

            10 => [
                "id"    => 111,
                "icon" => "other/icons/twitch.png",
                "text" => "Twitch"
            ],

            11 => [
                "id"    => 12,
                "icon" => "other/icons/twitter.png",
                "text" => "Enregistrement de l'utilisateur"
            ],

            12 => [
                "id"    => 13,
                "icon" => "other/icons/video_timer.png",
                "text" => "Timer"
            ],

            13 => [
                "id"    => 14,
                "icon" => "other/icons/youtube.png",
                "text" => "Youtube"
            ],

            14 => [
                "id"    => 15,
                "icon" => "other/icons/exclamation-mark.png",
                "text" => "Information"
            ],

            15 => [
                "id"    => 16,
                "icon" => "other/icons/triangle.png",
                "text" => "Attention"
            ]
        ];

        return (object) $images;
    }
}