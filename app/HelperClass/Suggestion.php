<?php


namespace App\HelperClass;


use App\Model\Suggestion\SuggestionTimeline;

class Suggestion
{
    public static function typeSuggestion($type)
    {
        switch ($type)
        {
            case 0: return "Matériel";break;
            case 1: return "Route"; break;
            case 2: return "Autre"; break;
            default: return "Inconnue"; break;
        }
    }

    public static function typeImageSuggestion($type)
    {
        switch ($type)
        {
            case 0: return "/storage/other/icons/train.png";break;
            case 1: return "/storage/other/icons/road.png"; break;
            case 2: return "/storage/other/icons/other.png"; break;
            default: return "/storage/other/icons/question.png"; break;
        }
    }

    public static function stateSuggestion($state, $view = 'front')
    {
        if($view == 'back')
        {
            switch ($state)
            {
                case 0: return "<i class=\"flaticon2-time kt-font-brand\" data-toggle='kt-tooltip' data-original-title='Soumis'></i>";
                case 1: return "<i class=\"flaticon2-correct kt-font-success\" data-toggle='kt-tooltip' data-original-title='Valider'></i>";
                case 2: return "<i class=\"flaticon2-close-cross kt-font-danger\" data-toggle='kt-tooltip' data-original-title='Rejeter'></i>";
                default: return "<i class=\"flaticon2-information kt-font-dark\" data-toggle='kt-tooltip' data-original-title='Inconnue'></i>";
            }
        }else{
            switch ($state)
            {
                case 0: return "<span class='color--primary'>Soumis</span>";
                case 1: return "<span class='color--success'>Valider</span>";
                case 2: return "<span class='color--error'>Rejeter</span>";
                default: return "<span class='color--dark'>Statut Inconnue</span>";
            }
        }
    }

    public static function stateBackSuggestion($state, $dot = false)
    {
        if($dot == false)
        {
            switch ($state)
            {
                case 0: return "<span class='kt-badge kt-badge--primary kt-badge--inline'>Soumis</span>";
                case 1: return "<span class='kt-badge kt-badge--success kt-badge--inline'>Valider</span>";
                case 2: return "<span class='kt-badge kt-badge--danger kt-badge--inline'>Rejeter</span>";
                default: return "<span class='kt-badge kt-badge--dark kt-badge--inline'>Statut Inconnue</span>";
            }
        }else{
            switch ($state)
            {
                case 0: return "kt-bg-primary";
                case 1: return "kt-bg-success";
                case 2: return "kt-bg-danger";
                default: return "kt-bg-dark";
            }
        }
    }

    public static function stateLabelSuggestion($state)
    {
        switch ($state)
        {
            case 0: return "<span class='label'>Soumis</span>";
            case 1: return "<span class='label bg--success'>Valider</span>";
            case 2: return "<span class='label bg--error'>Rejeter</span>";
            default: return "<span class='label bg--dark'>Statut Inconnue</span>";
        }
    }

    public static function stateBackProgress($state)
    {
        if($state == 0)
        {
            ob_start();
            ?>
            <div class="kt-widget__progress d-flex  align-items-center">
                <div class="progress" style="height: 5px;width: 100%;">
                    <div class="progress-bar kt-bg-info" role="progressbar" style="width: 50%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <span class="kt-widget__stat">
                                        50%
                                    </span>
            </div>
            <?php
            $content = ob_get_clean();
        }elseif($state == 1)
        {
            ob_start();
            ?>
            <div class="kt-widget__progress d-flex  align-items-center">
                <div class="progress" style="height: 5px;width: 100%;">
                    <div class="progress-bar kt-bg-success" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <span class="kt-widget__stat">
                                        100%
                                    </span>
            </div>
            <?php
            $content = ob_get_clean();
        }else{
            ob_start();
            ?>
            <div class="kt-widget__progress d-flex  align-items-center">
                <div class="progress" style="height: 5px;width: 100%;">
                    <div class="progress-bar kt-bg-danger" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <span class="kt-widget__stat">
                                        100%
                                    </span>
            </div>
            <?php
            $content = ob_get_clean();
        }

        return $content;
    }

    public static function countSubmitSuggestion()
    {
        $suggestion = new \App\Model\Suggestion\Suggestion();

        return $suggestion->newQuery()
            ->where('state', 0)
            ->count();
    }

    public static function getSubmitSuggestion()
    {
        $suggestion = new \App\Model\Suggestion\Suggestion();

        return $suggestion->newQuery()
            ->where('state', 0)
            ->get();
    }

    public static function countTimelineForSuggestion($suggestion_id)
    {
        $timeline = new SuggestionTimeline();

        return $timeline->newQuery()
            ->where('suggestion_id', $suggestion_id)
            ->count();
    }

    public static function colorFeaturedStateOfFront($state)
    {
        switch ($state)
        {
            case '0': return 'feature--featured'; break;
            case '1': return 'feature--featured-success'; break;
            case '2': return 'feature--featured-error'; break;
            case '3': return 'feature--featured-dark'; break;
            default: return 'feature--featured'; break;
        }
    }
}