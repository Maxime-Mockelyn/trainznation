<?php


namespace App\HelperClass;


class ErrorClass
{
    public function error(string $responseText) {
        return [
            "responseStatus" => "error",
            "responseText"   => $responseText
        ];
    }

    public function warning(string $responseText) {
        return [
            "responseStatus"    => "warning",
            "responseText"      => $responseText
        ];
    }

    public function success(string $responseText) {
        return [
            "responseStatus"    => "success",
            "responseText"      => $responseText
        ];
    }

    public function info(string $responseText) {
        return [
            "responseStatus"    => "info",
            "responseText"      => $responseText
        ];
    }
}