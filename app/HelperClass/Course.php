<?php


namespace App\HelperClass;


class Course
{
    public static function levelCourse($level)
    {
        switch ($level)
        {
            case 1: return "Débutant"; break;
            case 2: return "Intermédiaire"; break;
            case 3: return "Confirmé"; break;
            default: return "Inconnu"; break;
        }
    }
}