<?php


namespace App\HelperClass;


use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Download
{
    /**
     * @var ErrorClass
     */
    private $errorClass;

    /**
     * Download constructor.
     * @param ErrorClass $errorClass
     */
    public function __construct(ErrorClass $errorClass)
    {
        $this->errorClass = $errorClass;
    }

    public function deleteConfigFile($asset)
    {
        try {
            if(Storage::disk('public')->exists('config/'.Str::slug($asset->categorie->name).'/'.Str::slug($asset->subcategorie->name).'/'.pregReplaceKuid($asset->kuid).'.txt')){
                Storage::disk('public')->delete('config/'.Str::slug($asset->categorie->name).'/'.Str::slug($asset->subcategorie->name).'/'.pregReplaceKuid($asset->kuid).'.txt');
                return $this->errorClass->success("Le fichier de configuration à été supprimer");
            }else{
                return $this->errorClass->warning("Le fichier de configuration n'existe pas, il ne peut donc pas être supprimer");
            }
        } catch(\Exception $exception) {
            Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
            return $this->errorClass->error("Erreur lors de la suppression du fichier de configuration, consulter les logs");
        }
    }

    public function deleteModeleFile($asset)
    {
        try {
            if(Storage::disk('public')->exists('modele/'.Str::slug($asset->categorie->name).'/'.Str::slug($asset->subcategorie->name).'/'.$asset->id.'/'.$asset->id.'.fbx')){
                Storage::disk('public')->delete('modele/'.Str::slug($asset->categorie->name).'/'.Str::slug($asset->subcategorie->name).'/'.$asset->id.'/'.$asset->id.'.fbx');
                return $this->errorClass->success("Le fichier de modèle 3D à été supprimer");
            }else{
                return $this->errorClass->warning("Le fichier de modèle 3D n'existe pas, il ne peut donc pas être supprimer");
            }
        } catch(\Exception $exception) {
            Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
            return $this->errorClass->error("Erreur lors de la suppression du fichier de modèle 3D, consulter les logs");
        }
    }

    public function deleteImageFile($asset) {
        try {
            if(Storage::disk('public')->exists('download/'.$asset->id.'.png')){
                Storage::disk('public')->delete('download/'.$asset->id.'.png');
                return $this->errorClass->success("L'image de l'objet à été supprimer");
            }else{
                return $this->errorClass->warning("L'image de l'objet n'existe pas, il ne peut donc pas être supprimer");
            }
        } catch(\Exception $exception) {
            Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
            return $this->errorClass->error("Erreur lors de la suppression de l'image de l'objet, consulter les logs");
        }
    }
}