<?php


namespace App\HelperClass;


class PdlDownload
{
    public static function formatTypeDownload($type)
    {
        switch ($type)
        {
            case 1: return "/storage/route/map.png"; break;
            case 2: return "/storage/route/dependance.png"; break;
            case 3: return "/storage/route/session.png"; break;
            default: return null;
        }
    }

    public static function formatReleaseDownload($release)
    {
        switch ($release)
        {
            case 0: return "Correctif"; break;
            case 1: return "Alpha"; break;
            case 2: return "Beta"; break;
            case 3: return "RC"; break;
            case 4: return "Final"; break;
            default: return null;
        }
    }

    public static function formatReleaseDownloadColor($release)
    {
        switch ($release)
        {
            case 0: return ""; break;
            case 1: return "background-color: teal; color: white"; break;
            case 2: return "background-color: orange; color: black"; break;
            case 3: return "background-color: red; color: white"; break;
            case 4: return "background-color: green; color: white"; break;
            default: return null;
        }
    }

    public static function formatPublishDownload($state)
    {
        switch ($state)
        {
            case 0: return  '<span class="kt-badge kt-badge--danger kt-badge--inline">Non Publier</span>'; break;
            case 1: return  '<span class="kt-badge kt-badge--success kt-badge--inline">Publier</span>'; break;
            default: return null;
        }
    }
}