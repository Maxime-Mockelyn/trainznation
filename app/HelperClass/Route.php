<?php


namespace App\HelperClass;


use App\Model\Route\Pdl;
use App\Model\Route\PdlBuild;

class Route
{
    public static function latestImageContentForRoute($route)
    {
        switch ($route)
        {
            case 'pdl':
                $pdl = new Pdl();
                $convert = $pdl->newQuery()->orderByDesc('id')->first();
                return '/storage/route/pdl/'.$convert->id.'.png';
        }
    }

    public static function latestBuildForRoute()
    {
        $build = new PdlBuild();
        $data = $build->newQuery()->orderByDesc('id')->first();

        return $data->build;
    }
}
