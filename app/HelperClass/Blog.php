<?php


namespace App\HelperClass;


use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class Blog
{
    /**
     * @var ErrorClass
     */
    private $errorClass;

    /**
     * Blog constructor.
     * @param ErrorClass $errorClass
     */
    public function __construct(ErrorClass $errorClass)
    {
        $this->errorClass = $errorClass;
    }

    public function deleteImage($blog)
    {
        try {
            if(Storage::disk("public")->exists('blog/'.$blog->id.'.png'))
            {
                Storage::disk('public')->delete('blog/'.$blog->id.'.png');
                return $this->errorClass->success("L'image de l'article à été supprimer");
            }else{
                return $this->errorClass->warning("L'image n'existe pas, elle ne peut pas être supprimer");
            }
        } catch (FileException $exception) {
            Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
            return $this->errorClass->error("Erreur lors de la suppression de l'image, consulter les logs");
        }
    }


}