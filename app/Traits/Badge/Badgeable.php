<?php
/**
 * Created by PhpStorm.
 * User: Sylth
 * Date: 19/03/2019
 * Time: 16:28
 */

namespace App\Traits\Badge;


use App\Model\Badge\Badge;

trait Badgeable
{
    public function badges()
    {
        return $this->belongsToMany(Badge::class);
    }
}
