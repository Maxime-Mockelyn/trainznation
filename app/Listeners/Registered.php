<?php

namespace App\Listeners;

use App\Notifications\Auth\RegisteredNotification;
use App\Notifications\Badge\BadgeUnlock;
use App\Repository\Badge\BadgeReprository;
use App\Repository\User\UserAccountRepository;
use App\Repository\User\UserRepository;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Kamaln7\Toastr\Facades\Toastr;

class Registered
{
    /**
     * @var BadgeReprository
     */
    private $badgeReprository;
    /**
     * @var UserAccountRepository
     */
    private $userAccountRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * Create the event listener.
     *
     * @param BadgeReprository $badgeReprository
     * @param UserAccountRepository $userAccountRepository
     * @param UserRepository $userRepository
     */
    public function __construct(BadgeReprository $badgeReprository, UserAccountRepository $userAccountRepository, UserRepository $userRepository)
    {
        //
        $this->badgeReprository = $badgeReprository;
        $this->userAccountRepository = $userAccountRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * Handle the event.
     *
     * @param  object $event
     * @return \Illuminate\View\View
     * @throws \ReflectionException
     */
    public function handle($event)
    {
        $this->userAccountRepository->register($event->user);
        //dd($event);
        $user = $event->user;
        $badge = $this->badgeReprository->unlockActionFor($user->id, 'register');

        if($badge)
        {
            $user->notify(new BadgeUnlock($badge));
            Toastr::success("Badge Débloqué !", "Vous avez débloqué le badge ".$badge->name);
        }

        // Notification:Back::AlertRegisteredUser
        return (new \App\Mail\Auth\Registered($event->user))->render();
        //Mail::to($event->user)->send(new \App\Mail\Auth\Registered($event->user));

        // Notification:Back::AlertAdministrateur

        $users = $this->userRepository->getAllAdministrator();

        foreach ($users as $user)
        {
            Notification::send($user, new RegisteredNotification($user));
        }
    }
}
