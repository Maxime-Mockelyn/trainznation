<?php

namespace App\Listeners;

use App\Notifications\Auth\LoginNotification;
use App\Repository\Badge\BadgeReprository;
use App\Repository\User\UserAccountRepository;
use App\Repository\User\UserRepository;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class Login
{
    /**
     * @var UserAccountRepository
     */
    private $userAccountRepository;
    /**
     * @var BadgeReprository
     */
    private $badgeReprository;
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * Create the event listener.
     *
     * @param UserAccountRepository $userAccountRepository
     * @param BadgeReprository $badgeReprository
     * @param UserRepository $userRepository
     */
    public function __construct(UserAccountRepository $userAccountRepository, BadgeReprository $badgeReprository, UserRepository $userRepository)
    {
        //
        $this->userAccountRepository = $userAccountRepository;
        $this->badgeReprository = $badgeReprository;
        $this->userRepository = $userRepository;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $this->userAccountRepository->login();

        // On vérifie que l'age d'inscription déclenche ou non un badge
        $now = now();
        $dateInscription = $event->user->created_at;

        $diffInYear = Carbon::createFromTimestamp(strtotime($dateInscription))->diffInYears($now);

        if($diffInYear == 1)
        {
            $badge = $this->badgeReprository->unlockActionFor($event->user->id, 'annual', 1);
            $this->badgeReprository->notifyBadgeUnlocked($event->user, $badge);
        }elseif($diffInYear == 2){
            $badge = $this->badgeReprository->unlockActionFor($event->user->id, 'annual', 2);
            $this->badgeReprository->notifyBadgeUnlocked($event->user, $badge);
        }elseif($diffInYear == 3){
            $badge = $this->badgeReprository->unlockActionFor($event->user->id, 'annual', 3);
            $this->badgeReprository->notifyBadgeUnlocked($event->user, $badge);
        }

        // Notification:Back::AlertAdministrateur

        $users = $this->userRepository->getAllAdministrator();

        foreach ($users as $user)
        {
            Notification::send($user, new LoginNotification($user));
        }
    }
}
