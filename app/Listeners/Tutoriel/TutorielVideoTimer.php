<?php

namespace App\Listeners\Tutoriel;

use App\Library\Youtube\Youtube;
use App\Notifications\Tutoriel\TutoTimerNotification;
use App\Repository\Tutoriel\TutorielRepository;
use App\User;
use Carbon\CarbonInterval;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class TutorielVideoTimer
{
    /**
     * @var TutorielRepository
     */
    private $tutorielRepository;
    /**
     * @var Youtube
     */
    private $youtube;

    /**
     * Create the event listener.
     *
     * @param TutorielRepository $tutorielRepository
     * @param Youtube $youtube
     */
    public function __construct(TutorielRepository $tutorielRepository, Youtube $youtube)
    {
        $this->tutorielRepository = $tutorielRepository;
        $this->youtube = $youtube;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $tutoriels = $this->tutorielRepository->list();
        $arr = [];

        $user = new User();

        $admin = $user->newQuery()->find('1');

        foreach ($tutoriels as $tutoriel)
        {
            $video_id = $tutoriel->linkVideo;
            $video = $this->youtube->videoInfo($video_id);
            $time = CarbonInterval::make($video->duration);
            $this->tutorielRepository->upTime($tutoriel->id, $time->minutes."min ".$time->seconds.'s');

            $admin->notify(new TutoTimerNotification($tutoriel));
        }
    }
}
