<?php

namespace App\Model\Tutoriel;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Tutoriel extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function category()
    {
        return $this->belongsTo(TutorielCategory::class, 'tutoriel_category_id');
    }

    public function subcategory()
    {
        return $this->belongsTo(TutorielSubCategory::class, 'tutoriel_sub_category_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    public function experiences()
    {
        return $this->hasMany(TutorielExperience::class);
    }

    public function technos()
    {
        return $this->hasMany(TutorielTechno::class);
    }

    public function comments()
    {
        return $this->hasMany(TutorielComment::class);
    }

    public function sources()
    {
        return $this->hasOne(TutorielSource::class);
    }

    public function getDates()
    {
        return ["published_at"];
    }
}
