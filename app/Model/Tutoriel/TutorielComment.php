<?php

namespace App\Model\Tutoriel;

use App\User;
use Illuminate\Database\Eloquent\Model;

class TutorielComment extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function tutoriel()
    {
        return $this->belongsTo(Tutoriel::class, 'tutoriel_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getDates()
    {
        return ["published_at"];
    }
}
