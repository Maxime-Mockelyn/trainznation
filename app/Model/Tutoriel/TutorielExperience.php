<?php

namespace App\Model\Tutoriel;

use Illuminate\Database\Eloquent\Model;

class TutorielExperience extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function tutoriel()
    {
        $this->belongsTo(Tutoriel::class, 'tutoriel_id');
    }
}
