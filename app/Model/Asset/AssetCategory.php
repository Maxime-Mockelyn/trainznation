<?php

namespace App\Model\Asset;

use Illuminate\Database\Eloquent\Model;

class AssetCategory extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function asset()
    {
        return $this->hasMany(Asset::class);
    }

    public function subcategorie()
    {
        return $this->hasMany(AssetSubCategory::class);
    }
}
