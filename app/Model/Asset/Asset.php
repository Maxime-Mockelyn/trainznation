<?php

namespace App\Model\Asset;

use App\Model\Invoice\Invoice;
use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    protected $guarded = [];

    public function categorie()
    {
        return $this->belongsTo(AssetCategory::class, 'asset_categorie_id');
    }

    public function subcategorie()
    {
        return $this->belongsTo(AssetSubCategory::class, 'asset_sub_categorie_id');
    }

    public function compatibilities()
    {
        return $this->hasMany(AssetCompatibility::class);
    }

    public function versions()
    {
        return $this->hasMany(AssetVersion::class);
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function getDates()
    {
        return ["created_at", "updated_at", "published_at"];
    }
}
