<?php

namespace App\Model\Asset;

use Illuminate\Database\Eloquent\Model;

class AssetVersion extends Model
{
    protected $guarded = [];

    public function asset()
    {
        return $this->belongsTo(Asset::class, 'asset_id');
    }
}
