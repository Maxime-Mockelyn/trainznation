<?php

namespace App\Model\Asset;

use Illuminate\Database\Eloquent\Model;

class AssetSubCategory extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function asset()
    {
        return $this->hasMany(Asset::class);
    }

    public function categorie()
    {
        return $this->belongsTo(AssetCategory::class, 'asset_categorie_id');
    }
}
