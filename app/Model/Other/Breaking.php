<?php

namespace App\Model\Other;

use Illuminate\Database\Eloquent\Model;

class Breaking extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
