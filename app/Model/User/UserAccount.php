<?php

namespace App\Model\User;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserAccount extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getDates()
    {
        return ["last_login", "premium_start", "premium_end"];
    }
}
