<?php

namespace App\Model\Learning;

use Illuminate\Database\Eloquent\Model;

class CourseModule extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
