<?php

namespace App\Model\Learning;

use Illuminate\Database\Eloquent\Model;

class CourseCategory extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
