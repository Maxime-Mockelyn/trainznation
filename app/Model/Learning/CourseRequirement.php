<?php

namespace App\Model\Learning;

use Illuminate\Database\Eloquent\Model;

class CourseRequirement extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
