<?php

namespace App\Model\Learning;

use Illuminate\Database\Eloquent\Model;

class CourseComment extends Model
{
    protected $guarded = [];
}
