<?php

namespace App\Model\Learning;

use Illuminate\Database\Eloquent\Model;

class CourseSubcategory extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
