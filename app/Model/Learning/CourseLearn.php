<?php

namespace App\Model\Learning;

use Illuminate\Database\Eloquent\Model;

class CourseLearn extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
