<?php

namespace App\Model\Learning;

use Illuminate\Database\Eloquent\Model;

class CourseTag extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
