<?php

namespace App\Model\Learning;

use Illuminate\Database\Eloquent\Model;

class CourseLesson extends Model
{
    protected $guarded = [];

    public function getDates()
    {
        return ["published_at"];
    }
}
