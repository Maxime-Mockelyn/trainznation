<?php

namespace App\Model\Route;

use Illuminate\Database\Eloquent\Model;

class PdlAnomalie extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
