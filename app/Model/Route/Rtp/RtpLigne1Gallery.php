<?php

namespace App\Model\Route\Rtp;

use Illuminate\Database\Eloquent\Model;

class RtpLigne1Gallery extends Model
{
    protected $guarded = [];

    public function categorie() {
        return $this->belongsTo(RtpLigne1GalleryCategory::class, 'rtp_ligne1_gallery_category_id');
    }
}
