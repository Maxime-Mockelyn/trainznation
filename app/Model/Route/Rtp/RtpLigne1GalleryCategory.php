<?php

namespace App\Model\Route\Rtp;

use Illuminate\Database\Eloquent\Model;

class RtpLigne1GalleryCategory extends Model
{
    protected $guarded = [];

    public function galleries()
    {
        return $this->hasMany(RtpLigne1Gallery::class);
    }
}
