<?php

namespace App\Model\Route;

use Illuminate\Database\Eloquent\Model;

class PdlGallery extends Model
{
    protected $guarded = [];

    public function categorie()
    {
        return $this->belongsTo(PdlGalleryCategory::class, 'pdl_gallery_category_id');
    }
}
