<?php

namespace App\Model\Route;

use Illuminate\Database\Eloquent\Model;

class PdlBuild extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
