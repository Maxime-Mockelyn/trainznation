<?php

namespace App\Model\Route;

use Illuminate\Database\Eloquent\Model;

class PdlGalleryCategory extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function galleries()
    {
        return $this->hasMany(PdlGallery::class);
    }
}
