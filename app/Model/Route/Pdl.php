<?php

namespace App\Model\Route;

use Illuminate\Database\Eloquent\Model;

class Pdl extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
