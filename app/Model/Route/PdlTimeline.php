<?php

namespace App\Model\Route;

use Illuminate\Database\Eloquent\Model;

class PdlTimeline extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function getDates()
    {
        return ["release_at"];
    }
}
