<?php

namespace App\Model\Route;

use Illuminate\Database\Eloquent\Model;

class PdlCompatibility extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
