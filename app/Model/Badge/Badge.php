<?php

namespace App\Model\Badge;

use App\Model\User\BadgeUser;
use Illuminate\Database\Eloquent\Model;

class Badge extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function unlocks()
    {
        return $this->hasMany(BadgeUser::class);
    }
}
