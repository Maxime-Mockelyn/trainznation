<?php

namespace App\Model\Wiki;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Wiki extends Model
{
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(WikiCategory::class, 'wiki_category_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getDates()
    {
        return ["created_at", "updated_at", "published_at"];
    }
}
