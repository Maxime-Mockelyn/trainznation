<?php

namespace App\Model\Wiki;

use Illuminate\Database\Eloquent\Model;

class WikiCategory extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function wikis()
    {
        return $this->hasMany(Wiki::class);
    }

}
