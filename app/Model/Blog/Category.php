<?php

namespace App\Model\Blog;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function blogs()
    {
        return $this->hasMany(Blog::class);
    }
}
