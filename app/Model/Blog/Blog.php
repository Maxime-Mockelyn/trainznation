<?php

namespace App\Model\Blog;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Blog extends Model
{
    use Notifiable;

    protected $guarded = [];
    public $timestamps = false;

    public function categorie()
    {
        return $this->belongsTo(Category::class, 'categorie_id');
    }

    public function comments()
    {
        return $this->hasMany(BlogComment::class);
    }

    public function getDates()
    {
        return ["published_at"];
    }
}
