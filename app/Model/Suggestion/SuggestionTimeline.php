<?php

namespace App\Model\Suggestion;

use App\User;
use Illuminate\Database\Eloquent\Model;

class SuggestionTimeline extends Model
{
    protected $guarded = [];

    public function suggestion()
    {
        return $this->belongsTo(Suggestion::class, 'suggestion_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
