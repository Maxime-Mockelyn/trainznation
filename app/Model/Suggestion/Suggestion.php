<?php

namespace App\Model\Suggestion;

use Illuminate\Database\Eloquent\Model;

class Suggestion extends Model
{
    protected $guarded = [];

    public function timelines()
    {
        return $this->hasMany(SuggestionTimeline::class);
    }
}
