<?php

namespace App\Providers;

use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Redis::enableEvents();
        Schema::defaultStringLength(191);
        if (App::environment('production')) {
            URL::forceScheme('https');
        }

        Blade::component('Back.Components.back', 'back');
        Blade::component('Back.Components.edit', 'edit');
        Blade::component('Back.Components.delete', 'delete');
        Blade::component('Back.Components.publish', 'publish');
        Blade::component('Back.Components.unpublish', 'unpublish');

        Validator::extend(
            'recaptcha',
            'App\\Validators\\ReCaptcha@validate'
        );

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->isLocal()) {
            $this->app->register(TelescopeServiceProvider::class);
        }

        if ($this->app->environment() !== 'production') {
            $this->app->register(IdeHelperServiceProvider::class);
        }
    }
}
