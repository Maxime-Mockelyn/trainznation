<?php

namespace App\Notifications\Suggestion;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class Rejected extends Notification
{
    use Queueable;
    public $rejected;
    public $suggestion;

    /**
     * Create a new notification instance.
     *
     * @param $rejected
     * @param $suggestion
     */
    public function __construct($rejected, $suggestion)
    {
        //
        $this->rejected = $rejected;
        $this->suggestion = $suggestion;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            "title"     => $this->suggestion->sujet." à été rejeter",
            "sector"    => "SuggestionPost",
            "time"      => now(),
            "images"    => '/storage/other/icons/question.png',
            "name"      => "Suggestion",
            "description"=> $this->suggestion->sujet." à été rejeter",
            "optional"  => true,
        ];
    }
}
