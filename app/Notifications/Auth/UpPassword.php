<?php

namespace App\Notifications\Auth;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UpPassword extends Notification
{
    use Queueable;
    /**
     * @var User
     */
    private $user;

    /**
     * Create a new notification instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = '/support/increasedPassword/'.$this->user->id;
        return (new MailMessage)
                    ->subject("Modification de votre mot de passe")
                    ->line("Notre système nous indique que vous avez modifier une information importante dans votre espace à savoir:")
            ->line("<ul><li>Votre Mot de passe</li></ul>")
            ->line("Si vous êtes à l'origine de cette modification, vous pouvez ignorer ce mail.<br>Par contre si vous n'êtes pas à l'origine de cette modification, veuillez cliquer sur le bouton suivant afin de nous le signaler.")
            ->action("Je ne suis pas à l'origine de cette modification", $url);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
