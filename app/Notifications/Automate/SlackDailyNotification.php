<?php

namespace App\Notifications\Automate;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SlackDailyNotification extends Notification
{
    use Queueable;
    private $count;

    /**
     * Create a new notification instance.
     *
     * @param $count
     */
    public function __construct($count)
    {
        //
        $this->count = $count;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    public function toSlack($notifiable)
    {
        $url = env("APP_URL").'/back';

        return (new SlackMessage)
            ->info()
            ->to("#trainznation")
            ->image(env("APP_URL").'/storage/avatar/trainznation.png')
            ->content("Les Taches Journalière ont été executer !: ".$this->count." ".str_plural('Tache', $this->count))
            ->attachment(function ($attachment) use ($url){
                $attachment->title("Tache 1:", $url)
                    ->content("Suppression de toutes le notifications optionnel de l'administrateur.");
            })
            ->attachment(function ($attachment) use ($url){
                $attachment->title("Tache 2:", $url)
                    ->content("Publication des vidéos en mode planifié");
            });
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
