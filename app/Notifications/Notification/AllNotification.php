<?php

namespace App\Notifications\Notification;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AllNotification extends Notification
{
    use Queueable;
    private $title;
    private $content;
    private $image;
    private $link;

    /**
     * Create a new notification instance.
     *
     * @param $title
     * @param $content
     * @param $image
     * @param $link
     */
    public function __construct($title, $content, $image, $link)
    {
        //
        $this->title = $title;
        $this->content = $content;
        $this->image = $image;
        $this->link = $link;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            "name"          => $this->title,
            "description"   => $this->content,
            "images"        => sourceImage($this->image),
            "link"          => $this->link,
            "optional"      => true,
            "title"     => $this->title,
            "sector"    => "notifyUser",
            "time"      => now(),
        ];
    }
}
