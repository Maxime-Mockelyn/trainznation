<?php

namespace App\Notifications\Badge;

use App\Model\Badge\Badge;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Storage;

class BadgeUnlock extends Notification
{
    use Queueable;
    /**
     * @var Badge
     */
    private $badge;

    /**
     * Create a new notification instance.
     *
     * @param Badge $badge
     */
    public function __construct(Badge $badge)
    {
        $this->badge = $badge;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('Vous avez débloqué le badge '.$this->badge->name)
                    ->line('Bravo !');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            "name"  => $this->badge->name,
            "description" => $this->badge->description,
            "images"    => sourceImage('other/badge/'.$this->badge->id.'.png'),
            "title"     => $this->badge->user." à débloquer le badge: $this->badge->name",
            "sector"    => "badgeUnlock",
            "time"      => now(),
            "optional"  => true
        ];
    }

    public static function toText($data)
    {
        return "Vous Avez déploqué le badge ".$data['name'];
    }
}
