<?php

namespace App\Notifications\Tutoriel;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Storage;
use NotificationChannels\Twitter\TwitterChannel;
use NotificationChannels\Twitter\TwitterStatusUpdate;
use qoraiche\mailEclipse\Facades\mailEclipse;

class TutorielPublishNotification extends Notification
{
    use Queueable;
    private $tutoriel;

    /**
     * Create a new notification instance.
     *
     * @param $tutoriel
     */
    public function __construct($tutoriel)
    {
        //
        $this->tutoriel = $tutoriel;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [TwitterChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject("Nouveau Tutoriel publier !")
                    ->view('');
    }

    /**
     * @param $notifiable
     * @return TwitterStatusUpdate
     * @throws \NotificationChannels\Twitter\Exceptions\CouldNotSendNotification
     */
    public function toTwitter($notifiable) {
        $link = env("APP_URL").'/tutoriel/'.$this->tutoriel->subcategory->id.'/'.$this->tutoriel->slug;
        if(env("APP_ENV") == 'production')
        {
            return (new TwitterStatusUpdate($this->tutoriel->twitterText.' '.$link))->withImage([
                Storage::disk('s3')->url('learning/'.$this->tutoriel->id.'_cover.png')
            ]);
        }else{
            return (new TwitterStatusUpdate($this->tutoriel->twitterText.' '.$link))->withImage([
                Storage::disk('public')->url('learning/'.$this->tutoriel->id.'_cover.png')
            ]);
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [

        ];
    }
}
