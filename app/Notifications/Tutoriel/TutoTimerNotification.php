<?php

namespace App\Notifications\Tutoriel;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Storage;

class TutoTimerNotification extends Notification
{
    use Queueable;
    private $tutoriel;

    /**
     * Create a new notification instance.
     *
     * @param $tutoriel
     */
    public function __construct($tutoriel)
    {
        //
        $this->tutoriel = $tutoriel;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    public function toSlack($notifiable)
    {
        $url = route("Back.Tutoriel.show", $this->tutoriel->slug);

        return (new SlackMessage)
            ->to("#trainznation")
            ->content("Le Timer de la video '".$this->tutoriel->title." à été mis à jour'")
            ->image(sourceImage('learning/'.$this->tutoriel->id.'.png'))
            ->attachment(function ($attachment) use ($url){
                $attachment->title("Tutoriel N°".$this->tutoriel->id, $url)
                    ->fields([
                        "Titre" => $this->tutoriel->title,
                        "ID Video Youtube" => $this->tutoriel->linkVideo,
                        "Durée"            => $this->tutoriel->time
                    ]);
            });
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            "name"          => $this->tutoriel->title,
            "description"   => "Le Timer de la vidéo ".$this->tutoriel->title." à été mis à jour.",
            "images"        => sourceImage('other/icons/video_timer.png'),
            "link"          => route('Front.Tutoriel.show', [$this->tutoriel->tutoriel_sub_category_id, $this->tutoriel->slug]),
            "optional"      => true,

            "title"     => "Le Timer de la vidéo ".$this->tutoriel->title." à été mis à jour.",
            "sector"    => "VideoTimer",
            "time"      => now(),
        ];

    }
}
