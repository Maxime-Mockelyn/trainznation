<?php

namespace App\Notifications\Tutoriel;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Storage;

class TutorielPublishDatabaseNotification extends Notification
{
    use Queueable;
    private $tutoriel;

    /**
     * Create a new notification instance.
     *
     * @param $tutoriel
     */
    public function __construct($tutoriel)
    {
        //
        $this->tutoriel = $tutoriel;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            "name"          => $this->tutoriel->title,
            "description"   => "La Vidéo <strong>".$this->tutoriel->title."</strong> à été publier",
            "images"        => sourceImage('learning/'.$this->tutoriel->id.'.png'),
            "link"          => route('Front.Tutoriel.show', [$this->tutoriel->tutoriel_sub_category_id, $this->tutoriel->slug]),
            "optional"      => true,
            "title"     => "La Vidéo <strong>".$this->tutoriel->title."</strong> à été publier",
            "sector"    => "VideoPublish",
            "time"      => now(),
        ];
    }
}
