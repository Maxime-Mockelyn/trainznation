<?php

namespace App\Notifications\Blog;

use App\Model\Blog\Blog;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Storage;
use NotificationChannels\Discord\DiscordChannel;
use NotificationChannels\Discord\DiscordMessage;

class BlogPublishedDatabase extends Notification
{
    use Queueable;
    /**
     * @var Blog
     */
    private $blog;

    /**
     * Create a new notification instance.
     *
     * @param Blog $blog
     */
    public function __construct(Blog $blog)
    {
        //
        $this->blog = $blog;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast', DiscordChannel::class];
    }

    public function toDiscord()
    {
        return DiscordMessage::create("**{$this->blog->title}**", ["url" => "https://trainznation.io/blog/test"]);
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            "title" => "un article à été publier",
            "text"  => $this->blog->title
        ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            "name"          => $this->blog->title,
            "description"   => $this->blog->title." à été publier",
            "images"        => sourceImage('blog/'.$this->blog->id.'.png'),
            "link"          => route('Front.Blog.show', $this->blog->id),
            "optional"      => true,
            "title"     => $this->blog->title." à été publier",
            "sector"    => "BlogPublish",
            "time"      => now(),
        ];
    }
}
