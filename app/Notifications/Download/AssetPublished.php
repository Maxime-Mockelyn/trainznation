<?php

namespace App\Notifications\Download;

use App\Model\Asset\Asset;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Twitter\TwitterChannel;
use NotificationChannels\Twitter\TwitterStatusUpdate;

class AssetPublished extends Notification
{
    use Queueable;
    /**
     * @var Asset
     */
    private $asset;

    /**
     * Create a new notification instance.
     *
     * @param Asset $asset
     */
    public function __construct(Asset $asset)
    {
        //
        $this->asset = $asset;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [TwitterChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    /**
     * @param $notifiable
     * @return TwitterStatusUpdate
     * @throws \NotificationChannels\Twitter\Exceptions\CouldNotSendNotification
     */
    public function toTwitter($notifiable) {
        $link = env("APP_URL").'/download/'.$this->asset->subcategorie->id.'/'.$this->asset->id;
        return (new TwitterStatusUpdate($this->asset->twitterText.' '.$link));
    }
}
