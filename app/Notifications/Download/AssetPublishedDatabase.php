<?php

namespace App\Notifications\Download;

use App\Model\Asset\Asset;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Storage;

class AssetPublishedDatabase extends Notification
{
    use Queueable;
    /**
     * @var Asset
     */
    private $asset;

    /**
     * Create a new notification instance.
     *
     * @param Asset $asset
     */
    public function __construct(Asset $asset)
    {
        //
        $this->asset = $asset;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            "name"          => $this->asset->designation,
            "description"   => $this->asset->designation." à été publier",
            "images"        => sourceImage('download/'.$this->asset->id.'.png'),
            "link"          => "/download/".$this->asset->subcategorie->id."/".$this->asset->id,
            "optional"      => true,
            "title"     => $this->asset->designation." à été publier",
            "sector"    => "AssetPublish",
            "time"      => now(),
        ];
    }
}
