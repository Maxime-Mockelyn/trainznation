<?php

namespace App\Jobs\Tutoriel;

use App\Library\Youtube\Youtube;
use App\Model\Tutoriel\Tutoriel;
use App\Model\Tutoriel\TutorielVideo;
use App\Model\Tutoriel\TutorielVideoState;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UploadStepThree implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var Tutoriel
     */
    private $tutoriel;

    /**
     * Create a new job instance.
     *
     * @param Tutoriel $tutoriel
     */
    public function __construct(Tutoriel $tutoriel)
    {

        $this->tutoriel = $tutoriel;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $tutorielVideo = new TutorielVideo();
        $tutorielVideoState = new TutorielVideoState();
        $youtube = new Youtube();

        $video = $tutorielVideo->newQuery()->where('tutoriel_id', $this->tutoriel->id)->first();
        $video->update([
            "stateVideo" => 3
        ]);

        $this->tutoriel->newQuery()->find($this->tutoriel->id)->update([
            "linkVideo" => $video->youtube_id
        ]);

        $snippet = $youtube->videoInfo($video->linkVideo);

        $this->tutoriel->newQuery()->find($this->tutoriel->id)->update([
            "time"  => $snippet['contentDetails']["duration"]
        ]);

        $second = $tutorielVideoState->newQuery()->where('tutoriel_video_id', $video->id)->get()->last(function ($value, $key){
            return $key-2;
        });

        $tutorielVideoState->newQuery()->where('tutoriel_video_id', $video->id)->first()->update([
            "state" => 2
        ]);
        $tutorielVideoState->newQuery()->find($second->id)->update([
            "state" => 2
        ]);
    }
}
