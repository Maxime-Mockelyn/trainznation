<?php

namespace App\Jobs\Tutoriel;

use App\Model\Tutoriel\Tutoriel;
use App\Model\Tutoriel\TutorielVideo;
use App\Model\Tutoriel\TutorielVideoState;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class uploadStepTwo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var Tutoriel
     */
    private $tutoriel;

    /**
     * Create a new job instance.
     *
     * @param Tutoriel $tutoriel
     */
    public function __construct(Tutoriel $tutoriel)
    {
        $this->tutoriel = $tutoriel;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $tutorielVideo = new TutorielVideo();
        $tutorielVideoState = new TutorielVideoState();

        $video = $tutorielVideo->newQuery()->where('tutoriel_id', $this->tutoriel->id)->first();
        $video->update([
            "stateVideo"         => 2,
            "updated_at"    => now()
        ]);

        $videoState = $tutorielVideoState->newQuery()->where('tutoriel_video_id', $video->id)->first()
            ->update(["state" => 2]);

        dispatch(new UploadStepThree($this->tutoriel));
    }
}
