<?php

namespace App\Jobs\Tutoriel;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;

class TutorielImageStore implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $file;
    private $tutoriel;

    /**
     * Create a new job instance.
     *
     * @param $file
     * @param $tutoriel
     */
    public function __construct($file, $tutoriel)
    {
        //
        $this->file = $file;
        $this->tutoriel = $tutoriel;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Storage::put('storage/learning/'.$this->tutoriel->id.'.png', $this->file);
    }
}
