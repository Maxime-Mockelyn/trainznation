<?php

namespace App\Jobs\Tutoriel;

use Dawson\Youtube\Facades\Youtube;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Str;

class UploadVideo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $tutoriel;
    private $pathFile;

    /**
     * Create a new job instance.
     *
     * @param $tutoriel
     * @param $pathFile
     */
    public function __construct($tutoriel, $pathFile)
    {
        //
        $this->tutoriel = $tutoriel;
        $this->pathFile = $pathFile;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $thumbnail = sourceImage('learning/'.$this->tutoriel->id.'.png');
        $video = Youtube::upload($this->pathFile, [
            "title"         => $this->tutoriel->title,
            "description"   => Str::limit($this->tutoriel->content,150, '...'),
            "tags"          => ["TRS", "Trainz", "Tutoriel", "Trainznation", "Contenue", "Content Manager", "Video", "Tane", "TRS2019", "French", "Français", "3D", "Modélisation"]
        ], 'public');

        $video_id = $video->getVideoId();

    }
}
