<?php

namespace App\Jobs\Tutoriel;

use App\Model\Tutoriel\Tutoriel;
use App\Model\Tutoriel\TutorielVideo;
use App\Model\Tutoriel\TutorielVideoState;
use App\Repository\Tutoriel\TutorielRepository;
use Dawson\Youtube\Facades\Youtube;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UploadStepOne implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array
     */
    public $opts;
    /**
     * @var string
     */
    public $publicy;

    /**
     * @var Tutoriel
     */
    public $tutoriel_id;


    /**
     * Create a new job instance.
     *
     * @param array $opts // Liste des paramètres youtube
     * @param $tutoriel_id
     * @param string $publicy // Method available: 'public', 'private', 'unlisted'
     */
    public function __construct(array $opts, $tutoriel_id, string $publicy = 'public')
    {
        $this->opts = $opts;
        $this->publicy = $publicy;
        $this->tutoriel_id = $tutoriel_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $tutorielVideo = new TutorielVideo();
        $tutorielVideoState = new TutorielVideoState();
        $tutoriel = new Tutoriel();

        // Récupération des informations existantes

        $videoBase = $tutorielVideo->newQuery()->where('tutoriel_id', $this->tutoriel_id)->first();
        $thumbnailPath = sourceImage('learning/'.$this->tutoriel_id.'.png');



        // Mise à jour initial des informations de la vidéo
        $tutorielVideoState->newQuery()->where('tutoriel_video_id', $videoBase->id)->first()->update(["state" => 1]);

        // Envoie de la vidéo sur youtube


        $video = Youtube::upload(public_path('storage/uploads/'.$this->tutoriel_id.'.mp4'), [
            "title"         => $this->opts['title'],
            "description"   => $this->opts['description'],
            "tags"          => $this->opts['tags']
        ], $this->publicy);

        $video_id = $video->getVideoId();

        // Mise à jour des bases de données
        $tutoriel->newQuery()->find($this->tutoriel_id)->update(["linkVideo" => $video_id]);
        $tutorielVideo->newQuery()->where('tutoriel_id', $this->tutoriel_id)->first()->update(["youtube_id" => $video_id, "stateVideo" => 1]);
        $tutorielVideoState->newQuery()->where('tutoriel_video_id', $videoBase->id)->first()->update(["state" => 2]);

        dispatch(new uploadStepTwo($tutoriel));
    }
}
