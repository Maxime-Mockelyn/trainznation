<?php

namespace App\Jobs\Asset;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;

class AssetImageStore implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $file;
    private $asset;

    /**
     * Create a new job instance.
     *
     * @param $file
     * @param $asset
     */
    public function __construct($file, $asset)
    {
        //
        $this->file = $file;
        $this->asset = $asset;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->file->store('storage/download/'.$this->asset->id.'.png', 'public');
    }
}
