<?php

namespace App\Jobs\Blog;

use App\Model\Blog\Blog;
use App\Notifications\Blog\BlogPublished;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use NotificationChannels\Twitter\TwitterChannel;
use NotificationChannels\Twitter\TwitterStatusUpdate;

class PublishedTwitter implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var Blog
     */
    private $blog;

    /**
     * Create a new job instance.
     *
     * @param Blog $blog
     */
    public function __construct(Blog $blog)
    {
        //
        $this->blog = $blog;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->blog->notify(new BlogPublished($this->blog));
    }
}
