<?php

namespace App\Jobs\Blog;

use App\Model\Blog\Blog;
use App\Notifications\Blog\BlogPublishedDatabase;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class PublishedDatabase implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var array
     */
    private $users;
    /**
     * @var Blog
     */
    private $blog;

    /**
     * Create a new job instance.
     *
     * @param $users
     * @param Blog $blog
     */
    public function __construct($users, Blog $blog)
    {
        //
        $this->users = $users;
        $this->blog = $blog;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->users as $user)
        {
            $user->notify(new BlogPublishedDatabase($this->blog));
        }
    }
}
