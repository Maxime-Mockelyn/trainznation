<?php

namespace App\Jobs\Blog;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Intervention\Image\ImageManager;

class ImageBlogStore implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $file;
    private $blog_id;

    /**
     * Create a new job instance.
     *
     * @param $file
     * @param $blog_id
     */
    public function __construct($file, $blog_id)
    {
        //
        $this->file = $file;
        $this->blog_id = $blog_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $manager = new ImageManager();
        $manager->make($this->file)->save('/public/blog/'.$this->blog_id.'.png');
    }
}
