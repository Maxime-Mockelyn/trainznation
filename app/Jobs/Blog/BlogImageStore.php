<?php

namespace App\Jobs\Blog;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;

class BlogImageStore implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $file;
    private $blog;

    /**
     * Create a new job instance.
     *
     * @param $file // File GetOriginalPath()
     * @param $blog // Model Blog
     */
    public function __construct($file, $blog)
    {
        //
        $this->file = $file;
        $this->blog = $blog;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $local = Storage::disk('local');
        $local->put('blog', file_get_contents($this->file), 'local');
    }
}
