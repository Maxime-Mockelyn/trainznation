<?php

namespace App\Forms\Account;

use Kris\LaravelFormBuilder\Form;

class AccountForm extends Form
{
    public function buildForm()
    {
        $this->formOptions = [
            "method"    => "PUT",
            "url"     => route("Account.Update.info")
        ];

        $this->add('email', 'email', [
            "value" => $this->getData('email'),
            "label" => "Adresse Mail",
            "rules" => "required|email"
        ])
            ->add('name', 'text', [
                "value" => $this->getData('name'),
                "label" => "Pseudo",
                "rules" => "required|min:6"
            ]);

        $this->add('submit', 'submit', [
            "label" => "Mettre à jour",
            "class" => "btn btn--primary btn--icon"
        ]);
    }
}
