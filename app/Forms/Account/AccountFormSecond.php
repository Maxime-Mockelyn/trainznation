<?php

namespace App\Forms\Account;

use Kris\LaravelFormBuilder\Form;

class AccountFormSecond extends Form
{
    public function buildForm()
    {
        $this->formOptions = [
            "method"    => "PUT",
            "url"     => route("Account.Update.second")
        ];

        $this->add('avatar', 'file', [
            "label" => "Avatar",
            "rules" => "file"
        ])
            ->add('site_web', 'url', [
                "value" => $this->getData('site_web'),
                "label" => "Site Web",
                "rules" => "url"
            ])
            ->add('pseudo_twitter', 'text', [
                "value" => $this->getData('pseudo_twitter'),
                "label" => "Pseudo Twitter"
            ])
            ->add('pseudo_facebook', 'text', [
                "value" => $this->getData('pseudo_facebook'),
                "label" => "Pseudo Facebook"
            ])
            ->add('trainz_id', 'text', [
                "value" => $this->getData('trainz_id'),
                "label" => "Trainz Id"
            ]);

        $this->add('submit', 'submit', [
            "label" => "Mettre à jour"
        ]);
    }
}
