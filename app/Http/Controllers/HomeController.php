<?php

namespace App\Http\Controllers;

use App\Events\Badge\Nessie;
use App\Library\Youtube\Uploader;
use App\Repository\Asset\AssetRepository;
use App\Repository\Badge\BadgeReprository;
use App\Repository\Blog\BlogRepository;
use App\Repository\Tutoriel\TutorielRepository;
use App\Repository\User\UserRepository;
use App\Repository\Wiki\WikiRepository;
use Dawson\Youtube\Facades\Youtube;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Newsletter;

class HomeController extends Controller
{
    /**
     * @var BlogRepository
     */
    private $blogRepository;
    /**
     * @var TutorielRepository
     */
    private $tutorielRepository;
    /**
     * @var BadgeReprository
     */
    private $badgeReprository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var AssetRepository
     */
    private $assetRepository;
    /**
     * @var WikiRepository
     */
    private $wikiRepository;

    /**
     * Create a new controller instance.
     *
     * @param BlogRepository $blogRepository
     * @param TutorielRepository $tutorielRepository
     * @param BadgeReprository $badgeReprository
     * @param UserRepository $userRepository
     * @param AssetRepository $assetRepository
     * @param WikiRepository $wikiRepository
     */
    public function __construct(
        BlogRepository $blogRepository,
        TutorielRepository $tutorielRepository,
        BadgeReprository $badgeReprository,
        UserRepository $userRepository,
        AssetRepository $assetRepository,
        WikiRepository $wikiRepository)
    {
        //$this->middleware('auth');
        $this->blogRepository = $blogRepository;
        $this->tutorielRepository = $tutorielRepository;
        $this->badgeReprository = $badgeReprository;
        $this->userRepository = $userRepository;
        $this->assetRepository = $assetRepository;
        $this->wikiRepository = $wikiRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //dd(AssetRepository::getCoverMenu());
        return view('Front.home', [
            "blogs" => $this->blogRepository->getLatestPost(),
            "tutoriels" => $this->tutorielRepository->getLatestTuto()
        ]);
    }

    public function redirect($download_id)
    {
        $download = $this->assetRepository->get($download_id);

        $this->assetRepository->addCountDownload($download_id, $download->count);

        return Redirect::to($download->downloadLink);
    }

    /**
     * Trouvez nessie
     */
    public function nessie()
    {
        $user = $this->userRepository->getUser();
        $a = event(new Nessie($user));
        return redirect()->back();
    }

    public function newsletter(Request $request)
    {
        Newsletter::subscribeOrUpdate($request->email, [], 'Trainznation');

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws Exception
     */
    public function callback(Request $request)
    {
        if(!$request->has('code')) {
            throw new Exception('$_GET[\'code\'] is not set. Please re-authenticate.');
        }

        $token = Youtube::authenticate($request->get('code'));

        Youtube::saveAccessTokenToDB($token);

        return redirect()->route('Back.dashboard');
    }

    public function viewNewsletter()
    {
        return new \App\Mail\Automate\Newsletter(
            $this->tutorielRepository->countTutorielFromWeek(),
            $this->assetRepository->countAssetFromWeek(),
            $this->blogRepository->countBlogFromWeek(),
            $this->wikiRepository->countWikiFromWeek(),
            $this->tutorielRepository->tutorielFromWeek(),
            $this->assetRepository->assetFromWeek(),
            $this->blogRepository->blogFromWeek(),
            $this->wikiRepository->wikiFromWeek()
        );
    }

    public function launcher()
    {
        return view("Front.launcher");
    }
}
