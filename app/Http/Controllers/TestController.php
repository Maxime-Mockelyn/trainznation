<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Automate\AutomateController;
use App\Repository\Route\PdlDownloadRepository;

class TestController extends Controller
{
    public function code(PdlDownloadRepository $downloadRepository)
    {
        dd($downloadRepository->getLatestBuild());
    }
}
