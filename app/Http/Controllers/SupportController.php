<?php

namespace App\Http\Controllers;

use App\Notifications\Support\IncreasedPassword;
use App\Repository\User\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class SupportController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * SupportController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function increasedPassword($user_id)
    {
        $user = $this->userRepository->getUserbyId($user_id);
        $admin = $this->userRepository->getAllAdministrator();

        Notification::send($admin, new IncreasedPassword($user));

        return redirect()->route('Account.info');

    }
}
