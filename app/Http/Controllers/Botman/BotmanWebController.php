<?php

namespace App\Http\Controllers\Botman;

use BotMan\BotMan\BotMan;
use BotMan\BotMan\BotManFactory;
use BotMan\BotMan\Drivers\DriverManager;
use BotMan\Drivers\Web\WebDriver;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BotmanWebController extends BotmanController
{
    public function __construct()
    {
        parent::__construct();
        $this->config = [
            'web' => [
                'matchingData' => [
                    'driver' => 'web',
                ],
            ]
        ];
    }

    public function handle()
    {
        DriverManager::loadDriver(WebDriver::class);
        $this->botman = BotManFactory::create($this->config);

        $this->botman->hears('My First Message', function (Botman $bot) {
            $bot->reply('Your First Response');
        });

        $this->botman->listen();
    }
}
