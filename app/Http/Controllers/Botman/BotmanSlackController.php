<?php

namespace App\Http\Controllers\Botman;

use BotMan\BotMan\BotMan;
use BotMan\BotMan\BotManFactory;
use BotMan\BotMan\Drivers\DriverManager;
use BotMan\Drivers\Slack\SlackDriver;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BotmanSlackController extends BotmanController
{
    public function __construct()
    {
        parent::__construct();
        $this->config = [
            'slack' => [
                'token' => '969183840:AAGQoEGB3hDsFcCQFpF2G329F5hHKtoQc2U'
            ]
        ];
    }

    public function handle()
    {
        DriverManager::loadDriver(SlackDriver::class);
        $this->botman = BotManFactory::create($this->config);

        $this->botman->hears("hello", function (BotMan $bot){
            $bot->reply("Bonjour");
        });

        $this->botman->listen();
    }
}
