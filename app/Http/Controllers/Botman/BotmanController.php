<?php

namespace App\Http\Controllers\Botman;

use BotMan\BotMan\BotMan;
use BotMan\BotMan\BotManFactory;
use BotMan\BotMan\Drivers\DriverManager;
use BotMan\Drivers\Telegram\TelegramDriver;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BotmanController extends Controller
{
    public $config;
    public $botman;

    public function __construct()
    {

    }
}
