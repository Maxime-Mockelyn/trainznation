<?php

namespace App\Http\Controllers\Botman;

use BotMan\BotMan\BotMan;
use BotMan\BotMan\BotManFactory;
use BotMan\BotMan\Drivers\DriverManager;
use BotMan\Drivers\Telegram\TelegramDriver;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class BotmanTelegramController extends BotmanController
{
    public function __construct()
    {
        parent::__construct();
        $this->config = [
            'telegram' => [
                'token' => '969183840:AAGQoEGB3hDsFcCQFpF2G329F5hHKtoQc2U'
            ]
        ];
    }

    public function handle()
    {
        //dd($this->config);
        DriverManager::loadDriver(TelegramDriver::class);
        $this->botman = BotManFactory::create($this->config);

        $this->botman->hears("hello", function (BotMan $bot){
            $bot->reply("Bonjour");
        });

        $this->botman->hears('Bonjour, je m\'appelle {name}', function (BotMan $bot, $name){
            $bot->reply("Bonjour ".$name.", Je suis le bot autonome !<br>En quoi puis-je t'aider ?");
        });

        $this->botman->hears('Give me {currency} rates', function (BotMan $bot, $currency) {
            $bot->types();
            $results = $this->getCurrency($currency);
            $bot->reply($results);
            Log::info("BOT:".$results);
        });

        $this->botman->fallback(function (BotMan $bot){
            $bot->reply("Désolé, je n'ai pas compris ta question !");
        });
        $this->botman->listen();
    }

    private function getCurrency($currency)
    {
        $client = new Client();
        $uri = 'http://api.fixer.io/latest?base='.$currency;

        $response = $client->get($uri);
        $results = json_decode($response->getBody()->getContents());

        $date = date('d F Y', strtotime($results->date));
        $data = "Voici les taux de changes de la monnaie: ".$currency.".\nDate:".$date."\n";
        foreach ($results->rates as $k => $v)
        {
            $data .= $k." - ".$v."\n";
        }
        return $data;
    }
}
