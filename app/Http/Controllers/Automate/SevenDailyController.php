<?php

namespace App\Http\Controllers\Automate;

use App\Notifications\Automate\SevenNotification;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SevenDailyController extends Controller
{
    private static $countSuccess;

    public function __construct()
    {
    }

    public static function exec()
    {
        self::RevertAllNotification();
        self::NotificationSlackSevenal();
    }

    protected static function RevertAllNotification()
    {
        $user = new User();

        $users = $user->newQuery()->get();

        foreach ($users as $user)
        {
            foreach ($user->notifications as $notification)
            {
                $notification->delete();
            }
        }

        self::$countSuccess = self::$countSuccess+1;
    }

    protected static function NotificationSlackSevenal()
    {
        $user = new User();
        $admin = $user->newQuery()->find(1);

        $admin->notify(new SevenNotification(self::$countSuccess));
    }
}
