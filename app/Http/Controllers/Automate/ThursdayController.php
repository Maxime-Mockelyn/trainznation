<?php

namespace App\Http\Controllers\Automate;

use App\Mail\Automate\Newsletter;
use App\Model\Asset\Asset;
use App\Model\Blog\Blog;
use App\Model\Tutoriel\Tutoriel;
use App\Model\Wiki\Wiki;
use App\Notifications\Automate\ThursdayNotification;
use App\Repository\User\UserRepository;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Kamaln7\Toastr\Facades\Toastr;

class ThursdayController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * ThursdayController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {

        $this->userRepository = $userRepository;
    }

    public static function exec()
    {
        self::blogNews();
    }

    protected static function blogNews()
    {
        $user = new User();
        $users = $user->newQuery()->get();

        foreach ($users as $user)
        {
            \Mail::to($user->email)->send(new Newsletter(
                self::countTutoriel(),
                self::countDownload(),
                self::countArticle(),
                self::countWiki(),
                self::tutoriel(),
                self::download(),
                self::blog(),
                self::wiki()
            ));
        }
    }

    private static function countTutoriel()
    {
        $tutoriel = new Tutoriel();

        return $tutoriel->newQuery()->whereBetween('published_at', [now()->startOfWeek(), now()->endOfWeek()])->get()->count();
    }

    private static function countDownload()
    {
        $asset = new Asset();

        return $asset->newQuery()->whereBetween('published_at', [now()->startOfWeek(), now()->endOfWeek()])->get()->count();
    }
    private static function countArticle()
    {
        $blog = new Blog();

        return $blog->newQuery()->whereBetween('published_at', [now()->startOfWeek(), now()->endOfWeek()])->get()->count();
    }

    private static function countWiki()
    {
        $wiki = new Wiki();

        return $wiki->newQuery()->whereBetween('published_at', [now()->startOfWeek(), now()->endOfWeek()])->get()->count();
    }

    private static function tutoriel()
    {
        $tutoriel = new Tutoriel();

        return $tutoriel->newQuery()
            ->whereBetween('published_at', [now()->startOfWeek(), now()->endOfWeek()])
            ->where('published', 1)
            ->limit(5)
            ->get();
    }

    private static function download()
    {
        $download = new Asset();

        return $download->newQuery()
            ->whereBetween('published_at', [now()->startOfWeek(), now()->endOfWeek()])
            ->where('published', 1)
            ->limit(5)
            ->get();
    }

    private static function blog()
    {
        $blog = new Blog();

        return $blog->newQuery()
            ->whereBetween('published_at', [now()->startOfWeek(), now()->endOfWeek()])
            ->where('published', 1)
            ->limit(5)
            ->get();
    }

    private static function wiki()
    {
        $wiki = new Wiki();

        return $wiki->newQuery()
            ->whereBetween('published_at', [now()->startOfWeek(), now()->endOfWeek()])
            ->where('published', 1)
            ->limit(5)
            ->get();
    }
}
