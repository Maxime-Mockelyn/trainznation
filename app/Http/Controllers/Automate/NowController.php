<?php

namespace App\Http\Controllers\Automate;

use App\Library\Youtube\Youtube;
use App\Model\Asset\Asset;
use App\Model\Tutoriel\Tutoriel;
use App\Notifications\Download\AssetPublished;
use App\Notifications\Download\AssetPublishedDatabase;
use App\Notifications\Tutoriel\TutorielPublishDatabaseNotification;
use App\Notifications\Tutoriel\TutorielPublishNotification;
use App\Notifications\Tutoriel\TutoTimerNotification;
use App\Repository\Tutoriel\TutorielRepository;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NowController extends Controller
{

    /**
     * NowController constructor.
     */
    public function __construct()
    {

    }

    public static function exec()
    {
        self::publishAsset();
    }

    protected static function publishAsset()
    {
        $asset = new Asset();
        $user = new User();

        $querys = $asset->newQuery()->where('published', 2)->whereBetween('published_at', [now()->subMinute(), now()->addMinute()])->get();
        $users = $user->newQuery()->get();

        foreach ($querys as $data)
        {

            $asset->newQuery()->find($data->id)->update(["published" => 1, "published_at" => now()]);

            foreach ($users as $a)
            {
                if($data->twitter == 1)
                {
                    $a->notify(new AssetPublishedDatabase($data));
                    $a->notify(new AssetPublished($data));
                }else{
                    $a->notify(new AssetPublishedDatabase($data));
                }
            }
        }
    }

    protected static function planifiedTutorielWrapper()
    {
        $tutoriel = new Tutoriel();
        $user = new User();

        $users = $user->newQuery()->get();
        $tutoriels = $tutoriel->newQuery()->where('published', 2)->whereBetween('published_at', [now()->subMinute(), now()->addMinute()])->get();

        foreach ($tutoriels as $tuto)
        {
            $tutoriel->newQuery()->find($tuto->id)->update(["published" => 1, "published_at" => now()]);

            foreach ($users as $u)
            {
                if($tuto->twitter == 1)
                {
                    $u->notify(new TutorielPublishNotification($tuto));
                    $u->notify(new TutorielPublishDatabaseNotification($tuto));
                }else{
                    $u->notify(new TutorielPublishDatabaseNotification($tuto));
                }
            }
        }
    }
}
