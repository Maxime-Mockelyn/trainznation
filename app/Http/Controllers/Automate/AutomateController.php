<?php

namespace App\Http\Controllers\Automate;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AutomateController extends Controller
{
    public static function now()
    {
        NowController::exec();
    }

    public static function daily()
    {
        DailyController::exec();
    }

    public static function seven()
    {
        SevenDailyController::exec();
    }

    public static function thursday()
    {
        ThursdayController::exec();
    }
}
