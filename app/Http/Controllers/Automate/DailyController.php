<?php

namespace App\Http\Controllers\Automate;

use App\Mail\Tutoriel\TutorielPublish;
use App\Model\Tutoriel\Tutoriel;
use App\Notifications\Automate\SlackDailyNotification;
use App\Notifications\Tutoriel\TutorielPublishDatabaseNotification;
use App\Notifications\Tutoriel\TutorielPublishNotification;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DailyController extends Controller
{

    private static $countSuccess;

    public function __construct()
    {
    }

    public static function exec()
    {
        self::RevertOptionnalNotificationForAdmin();
        self::NotificationSlackDaily();
    }

    protected static function RevertOptionnalNotificationForAdmin()
    {
        $user = new User();

        $users = $user->newQuery()->where('group', 1)->get();

        foreach ($users as $user)
        {
            foreach ($user->notifications as $notification)
            {
                if($notification->data['optional'] == true)
                {
                    $notification->delete();
                }
            }
        }

        self::$countSuccess = self::$countSuccess+1;
    }



    protected static function NotificationSlackDaily()
    {
        $user = new User();
        $admin = $user->newQuery()->find(1);

        $admin->notify(new SlackDailyNotification(self::$countSuccess));
    }
}
