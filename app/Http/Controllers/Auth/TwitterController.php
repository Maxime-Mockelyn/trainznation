<?php

namespace App\Http\Controllers\Auth;

use App\Notifications\Auth\LoginNotification;
use App\Notifications\Auth\RegisteredNotification;
use App\Repository\User\UserAccountRepository;
use App\Repository\User\UserRepository;
use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;
use Laravel\Socialite\Facades\Socialite;

class TwitterController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var UserAccountRepository
     */
    private $userAccountRepository;

    /**
     * FacebookController constructor.
     * @param UserRepository $userRepository
     * @param UserAccountRepository $userAccountRepository
     */
    public function __construct(UserRepository $userRepository, UserAccountRepository $userAccountRepository)
    {
        $this->userRepository = $userRepository;
        $this->userAccountRepository = $userAccountRepository;
    }

    public function redirect()
    {
        return Socialite::with('twitter')->redirect();
    }

    public function callback(Request $request)
    {
        dd($request->all());

        try{
            $user = Socialite::driver('twitter')->user();
            //dd($user);
        }catch (\Exception $exception)
        {
            Toastr::error("Impossible de récupérer les informations de compte FACEBOOK", "Erreur");
            return redirect()->route('login');
        }

        $existingUser = $this->userRepository->getUserbyEmail($user->email);

        $adminUsers = $this->userRepository->getAllAdministrator();

        if($existingUser)
        {
            $existingAccount = $this->userAccountRepository->getAccountFromUser($existingUser->id);
            $this->userAccountRepository->addindPseudoByProvider('twitter', $user->name, $existingAccount->user_id);
            auth()->login($existingUser, true);
            foreach ($adminUsers as $user)
            {
                $user->notify(new LoginNotification($existingUser));
            }
        }else{
            $password = createPassword();

            $newUser = $this->userRepository->create(
                $user->name,
                $user->email,
                Hash::make($password)
            );

            $this->userAccountRepository->register($newUser);
            $this->userAccountRepository->addindPseudoByProvider('twitter', $user->name, $newUser->id);

            auth()->login($newUser, true);

            $newUser->notify(new RegisteredNotification($newUser));
            foreach ($adminUsers as $user)
            {
                $user->notify(new LoginNotification($newUser));
            }
        }

        return redirect()->to('/');
    }
}
