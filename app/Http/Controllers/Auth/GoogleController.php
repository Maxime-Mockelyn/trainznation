<?php

namespace App\Http\Controllers\Auth;

use App\Notifications\Auth\LoginNotification;
use App\Notifications\Auth\RegisteredNotification;
use App\Repository\User\UserAccountRepository;
use App\Repository\User\UserRepository;
use App\User;
use Exception;
use Hash;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Redirector;
use Kamaln7\Toastr\Facades\Toastr;
use Laravel\Socialite\Facades\Socialite;

class GoogleController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var UserAccountRepository
     */
    private $userAccountRepository;

    /**
     * GoogleController constructor.
     * @param UserRepository $userRepository
     * @param UserAccountRepository $userAccountRepository
     */
    public function __construct(UserRepository $userRepository, UserAccountRepository $userAccountRepository)
    {

        $this->userRepository = $userRepository;
        $this->userAccountRepository = $userAccountRepository;
    }

    public function redirect()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Redirector
     */
    public function callback(Request $request)
    {

        //dd($request->all());

        try{
            $user = Socialite::driver('google')->user();
        }catch (Exception $e)
        {
            Toastr::error("Impossible de récupérer les informations de compte GOOGLE", "Erreur");
            return redirect('login');
        }

        $existingUser = User::where('email', $user->email)->first();
        $adminUsers = $this->userRepository->getAllAdministrator();

        if($existingUser)
        {
            $existingAccount = $this->userAccountRepository->getAccountFromUser($existingUser->id);
            $this->userAccountRepository->addindPseudoByProvider('google', $user->name, $existingAccount->user_id);
            auth()->login($existingUser, true);
            foreach ($adminUsers as $user)
            {
                $user->notify(new LoginNotification($existingUser));
            }
        }else{
            $password = createPassword();

            $newUser = $this->userRepository->createForkGoogle(
                $user->name,
                $user->email,
                Hash::make($password),
                $user->id
            );

            $this->userAccountRepository->register($newUser);
            $this->userAccountRepository->addindPseudoByProvider('google', $user->name, $newUser->id);

            auth()->login($newUser, true);

            $newUser->notify(new RegisteredNotification($newUser));
            foreach ($adminUsers as $user)
            {
                $user->notify(new LoginNotification($newUser));
            }
        }

        return redirect()->to('/');
    }
}
