<?php

namespace App\Http\Controllers\Account;

use App\Events\Badge\Createur;
use App\Forms\Account\AccountForm;
use App\Forms\Account\AccountFormSecond;
use App\Repository\Badge\BadgeReprository;
use App\Repository\User\UserAccountRepository;
use App\Repository\User\UserActivityRepository;
use App\Repository\User\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Kamaln7\Toastr\Facades\Toastr;
use Kris\LaravelFormBuilder\FormBuilder;
use NotificationChannels\Discord\Discord;

class AccountController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var BadgeReprository
     */
    private $badgeReprository;
    /**
     * @var FormBuilder
     */
    private $formBuilder;
    /**
     * @var UserAccountRepository
     */
    private $userAccountRepository;
    /**
     * @var UserActivityRepository
     */
    private $userActivityRepository;

    /**
     * AccountController constructor.
     * @param UserRepository $userRepository
     * @param BadgeReprository $badgeReprository
     * @param FormBuilder $formBuilder
     * @param UserAccountRepository $userAccountRepository
     * @param UserActivityRepository $userActivityRepository
     */
    public function __construct(
        UserRepository $userRepository,
        BadgeReprository $badgeReprository,
        FormBuilder $formBuilder,
        UserAccountRepository $userAccountRepository,
        UserActivityRepository $userActivityRepository)
    {
        $this->userRepository = $userRepository;
        $this->badgeReprository = $badgeReprository;
        $this->formBuilder = $formBuilder;
        $this->userAccountRepository = $userAccountRepository;
        $this->userActivityRepository = $userActivityRepository;
    }

    public function index()
    {
        return view('Account.account', [
            "user"  => $this->userRepository->getUser()
        ]);
    }

    public function info()
    {
        return view("Account.info", [
            "user"  => $this->userRepository->getUser(),
        ]);
    }

    public function badge()
    {
        return view("Account.badge", [
            "user"  => $this->userRepository->getUser(),
            "badges"    => $this->badgeReprository->getAllBadges()
        ]);
    }

    public function contrib()
    {
        return view("Account.contrib", [
            "user"  => $this->userRepository->getUser()
        ]);
    }

    public function update(Request $request)
    {
        $request->validate([
            "pseudo"    => "required|min:6",
            "email"     => "required|email"
        ]);

        $this->userRepository->updateInfo($request->pseudo, $request->email);
        addingActivity("Vos informations principales ont été mis à jour", "la la-user", 2);

        Toastr::success("Vos informations principales ont été mis à jour", "Edition de compte");

        return redirect()->back();
    }

    public function second(Request $request)
    {
        $channel = app(Discord::class)->getPrivateChannel($request->discord_id);

        $this->userAccountRepository->updateInfo(
            $request->site_web,
            $request->pseudo_twitter,
            $request->pseudo_facebook,
            $request->discord_id,
            $channel,
            $request->trainz_id
        );



        if($request->file('avatar'))
        {
            if(env("APP_ENV") == "production")
            {
                $request->file('avatar')->storeAs('avatar/', auth()->user()->id.'.png', 's3');
                Storage::disk('s3')->setVisibility('avatar/'.auth()->user()->id.'.png', 'public');
            }else{
                $request->file('avatar')->storeAs('avatar/', auth()->user()->id.'.png', 'public');
                Storage::disk('public')->setVisibility('avatar/'.auth()->user()->id.'.png', 'public');
            }

            $this->userAccountRepository->addAvatar();
        }

        event(new Createur(auth()->user()));

        addingActivity('Mise à jour des informations secondaires', 'la la-user', 2);

        Toastr::success("Vos informations secondaire ont été mis à jour.", "Edition de compte");

        return redirect()->back();

    }
}
