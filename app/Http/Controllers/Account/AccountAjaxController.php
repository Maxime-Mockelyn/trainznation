<?php

namespace App\Http\Controllers\Account;

use App\Notifications\Auth\ConfirmPasswordCode;
use App\Repository\Blog\BlogCommentRepository;
use App\Repository\User\DatabaseNotificationRepository;
use App\Repository\User\UserAccountRepository;
use App\Repository\User\UserActivityRepository;
use App\Repository\User\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Support\Facades\Hash;

class AccountAjaxController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var UserAccountRepository
     */
    private $userAccountRepository;
    /**
     * @var UserActivityRepository
     */
    private $userActivityRepository;
    /**
     * @var DatabaseNotificationRepository
     */
    private $databaseNotificationRepository;
    /**
     * @var BlogCommentRepository
     */
    private $blogCommentRepository;

    /**
     * AccountAjaxController constructor.
     * @param UserRepository $userRepository
     * @param UserAccountRepository $userAccountRepository
     * @param UserActivityRepository $userActivityRepository
     * @param DatabaseNotificationRepository $databaseNotificationRepository
     * @param BlogCommentRepository $blogCommentRepository
     */
    public function __construct(
        UserRepository $userRepository,
        UserAccountRepository $userAccountRepository,
        UserActivityRepository $userActivityRepository,
        DatabaseNotificationRepository $databaseNotificationRepository,
        BlogCommentRepository $blogCommentRepository)
    {
        $this->userRepository = $userRepository;
        $this->userAccountRepository = $userAccountRepository;
        $this->userActivityRepository = $userActivityRepository;
        $this->databaseNotificationRepository = $databaseNotificationRepository;
        $this->blogCommentRepository = $blogCommentRepository;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function loadActivities()
    {
        $activities = $this->userActivityRepository->getLatestActivity();
        $countActivity = $this->userActivityRepository->getCountLatestActivity();

        ob_start();
        ?>
        <!--Begin::Timeline 3 -->
        <div class="kt-timeline-v2">
            <?php if($countActivity == 0): ?>
                <div class="kt-timeline-v2__items  kt-padding-top-25 kt-padding-bottom-30">
                    <div class="kt-timeline-v2__item">
                        <span class="text-center">Aucune Activité</span>
                    </div>
                </div>
            <?php else: ?>
                <div class="kt-timeline-v2__items  kt-padding-top-25 kt-padding-bottom-30">
                    <?php foreach ($activities as $activity): ?>
                        <div class="kt-timeline-v2__item">
                            <span class="kt-timeline-v2__item-time"><?= $activity->created_at->format("H:i"); ?></span>
                            <div class="kt-timeline-v2__item-cricle">
                                <i class="<?= $activity->icons; ?> <?= stateUserActivity($activity->state); ?>"></i>
                            </div>
                            <div class="kt-timeline-v2__item-text  kt-padding-top-5">
                                <?= $activity->activity; ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
        <!--End::Timeline 3 -->
        <?php
        $content = ob_get_clean();

        return response()->json([$content]);
    }

    public function loadSocialite()
    {
        $user = $this->userRepository->getUser();
        $account = $this->userAccountRepository->getAccountFromUser($user->id);
        //dd($account, $user);

        ob_start();
        ?>
        <div class="row">
            <div class="col-md-3">
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head kt-portlet__head--noborder">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">

                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body kt-portlet__body--fit-y">
                        <!--begin::Widget -->
                        <div class="kt-widget kt-widget--user-profile-4">
                            <div class="kt-widget__head">
                                <div class="kt-widget__media">
                                    <img class="kt-widget__img kt-hidden-" src="<?= sourceImage('other/icons/twitter.png') ?>" alt="image">
                                    <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-hidden">
                                        TW
                                    </div>
                                </div>
                                <div class="kt-widget__content">
                                    <div class="kt-widget__section">
                                        <a href="#" class="kt-widget__username">
                                            TWITTER <?php if($account->pseudo_twitter): ?>(<?= $account->pseudo_twitter; ?>)<?php endif; ?>
                                        </a>
                                        <div class="kt-widget__button">
                                            <?php if(empty($account->pseudo_twitter)): ?>
                                            <span class="btn btn-label-danger btn-sm">Non Connecter</span>
                                            <?php else: ?>
                                                <span class="btn btn-label-success btn-sm">Connecter</span>
                                            <?php endif; ?>
                                        </div>

                                        <div class="kt-widget__action">
                                            <?php if(empty($account->pseudo_twitter)): ?>
                                            <a id="btnTwitterConnect" href="#" class="btn btn-label-twitter">
                                                <i class="socicon-twitter"></i> Connection
                                            </a>
                                            <?php else: ?>
                                                <a href="#" class="btn btn-label-twitter">
                                                    <i class="socicon-twitter"></i> Deconnexion
                                                </a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Widget -->
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head kt-portlet__head--noborder">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">

                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body kt-portlet__body--fit-y">
                        <!--begin::Widget -->
                        <div class="kt-widget kt-widget--user-profile-4">
                            <div class="kt-widget__head">
                                <div class="kt-widget__media">
                                    <img class="kt-widget__img kt-hidden-" src="<?= sourceImage('other/icons/facebook.png') ?>" alt="image">
                                    <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-hidden">
                                        FB
                                    </div>
                                </div>
                                <div class="kt-widget__content">
                                    <div class="kt-widget__section">
                                        <a href="#" class="kt-widget__username">
                                            FACEBOOK <?php if($account->pseudo_facebook): ?>(<?= $account->pseudo_facebook; ?>)<?php endif; ?>
                                        </a>
                                        <div class="kt-widget__button">
                                            <?php if(empty($account->pseudo_facebook)): ?>
                                                <span class="btn btn-label-danger btn-sm">Non Connecter</span>
                                            <?php else: ?>
                                                <span class="btn btn-label-success btn-sm">Connecter</span>
                                            <?php endif; ?>
                                        </div>

                                        <div class="kt-widget__action">
                                            <?php if(empty($account->pseudo_facebook)): ?>
                                                <a href="#" class="btn btn-label-facebook">
                                                    <i class="socicon-facebook"></i> Connection
                                                </a>
                                            <?php else: ?>
                                                <a href="#" class="btn btn-label-facebook">
                                                    <i class="socicon-facebook"></i> Deconnexion
                                                </a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Widget -->
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head kt-portlet__head--noborder">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">

                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body kt-portlet__body--fit-y">
                        <!--begin::Widget -->
                        <div class="kt-widget kt-widget--user-profile-4">
                            <div class="kt-widget__head">
                                <div class="kt-widget__media">
                                    <img class="kt-widget__img kt-hidden-" src="<?= sourceImage('other/icons/discord.png') ?>" alt="image">
                                    <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-hidden">
                                        DD
                                    </div>
                                </div>
                                <div class="kt-widget__content">
                                    <div class="kt-widget__section">
                                        <a href="#" class="kt-widget__username">
                                            DISCORD <?php if($account->pseudo_discord): ?>(<?= $account->pseudo_discord; ?>)<?php endif; ?>
                                        </a>
                                        <div class="kt-widget__button">
                                            <?php if(empty($account->pseudo_discord)): ?>
                                                <span class="btn btn-label-danger btn-sm">Non Connecter</span>
                                            <?php else: ?>
                                                <span class="btn btn-label-success btn-sm">Connecter</span>
                                            <?php endif; ?>
                                        </div>

                                        <div class="kt-widget__action">
                                            <?php if(empty($account->pseudo_discord)): ?>
                                                <a href="#" class="btn btn-label-discord">
                                                    <i class="socicon-discord"></i> Connection
                                                </a>
                                            <?php else: ?>
                                                <a href="#" class="btn btn-label-discord">
                                                    <i class="socicon-discord"></i> Deconnexion
                                                </a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Widget -->
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head kt-portlet__head--noborder">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">

                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body kt-portlet__body--fit-y">
                        <!--begin::Widget -->
                        <div class="kt-widget kt-widget--user-profile-4">
                            <div class="kt-widget__head">
                                <div class="kt-widget__media">
                                    <img class="kt-widget__img kt-hidden-" src="<?= sourceImage('other/icons/google.png') ?>" alt="image">
                                    <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-hidden">
                                        GG
                                    </div>
                                </div>
                                <div class="kt-widget__content">
                                    <div class="kt-widget__section">
                                        <a href="#" class="kt-widget__username">
                                            GOOGLE <?php if($account->pseudo_google): ?>(<?= $account->pseudo_google; ?>)<?php endif; ?>
                                        </a>
                                        <div class="kt-widget__button">
                                            <?php if(empty($account->pseudo_google)): ?>
                                                <span class="btn btn-label-danger btn-sm">Non Connecter</span>
                                            <?php else: ?>
                                                <span class="btn btn-label-success btn-sm">Connecter</span>
                                            <?php endif; ?>
                                        </div>

                                        <div class="kt-widget__action">
                                            <?php if(empty($account->pseudo_google)): ?>
                                                <a href="#" class="btn btn-label-google">
                                                    <i class="socicon-google"></i> Connection
                                                </a>
                                            <?php else: ?>
                                                <a href="#" class="btn btn-label-google">
                                                    <i class="socicon-google"></i> Deconnexion
                                                </a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Widget -->
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head kt-portlet__head--noborder">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">

                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body kt-portlet__body--fit-y">
                        <!--begin::Widget -->
                        <div class="kt-widget kt-widget--user-profile-4">
                            <div class="kt-widget__head">
                                <div class="kt-widget__media">
                                    <img class="kt-widget__img kt-hidden-" src="<?= sourceImage('other/icons/microsoft.png') ?>" alt="image">
                                    <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-hidden">
                                        MC
                                    </div>
                                </div>
                                <div class="kt-widget__content">
                                    <div class="kt-widget__section">
                                        <a href="#" class="kt-widget__username">
                                            MICROSOFT <?php if($account->pseudo_microsoft): ?>(<?= $account->pseudo_microsoft; ?>)<?php endif; ?>
                                        </a>
                                        <div class="kt-widget__button">
                                            <?php if(empty($account->pseudo_microsoft)): ?>
                                                <span class="btn btn-label-danger btn-sm">Non Connecter</span>
                                            <?php else: ?>
                                                <span class="btn btn-label-success btn-sm">Connecter</span>
                                            <?php endif; ?>
                                        </div>

                                        <div class="kt-widget__action">
                                            <?php if(empty($account->pseudo_microsoft)): ?>
                                                <a href="#" class="btn btn-label-microsoft">
                                                    <i class="socicon-rss"></i> Connection
                                                </a>
                                            <?php else: ?>
                                                <a href="#" class="btn btn-label-microsoft">
                                                    <i class="socicon-rss"></i> Deconnexion
                                                </a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Widget -->
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head kt-portlet__head--noborder">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">

                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body kt-portlet__body--fit-y">
                        <!--begin::Widget -->
                        <div class="kt-widget kt-widget--user-profile-4">
                            <div class="kt-widget__head">
                                <div class="kt-widget__media">
                                    <img class="kt-widget__img kt-hidden-" src="<?= sourceImage('other/icons/twitch.png') ?>" alt="image">
                                    <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-hidden">
                                        TC
                                    </div>
                                </div>
                                <div class="kt-widget__content">
                                    <div class="kt-widget__section">
                                        <a href="#" class="kt-widget__username">
                                            TWITCH <?php if($account->pseudo_twitch): ?>(<?= $account->pseudo_twitch; ?>)<?php endif; ?>
                                        </a>
                                        <div class="kt-widget__button">
                                            <?php if(empty($account->pseudo_twitch)): ?>
                                                <span class="btn btn-label-danger btn-sm">Non Connecter</span>
                                            <?php else: ?>
                                                <span class="btn btn-label-success btn-sm">Connecter</span>
                                            <?php endif; ?>
                                        </div>

                                        <div class="kt-widget__action">
                                            <?php if(empty($account->pseudo_twitch)): ?>
                                                <a href="#" class="btn btn-label-discord">
                                                    <i class="socicon-twitch"></i> Connection
                                                </a>
                                            <?php else: ?>
                                                <a href="#" class="btn btn-label-discord">
                                                    <i class="socicon-twitch"></i> Deconnexion
                                                </a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Widget -->
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head kt-portlet__head--noborder">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">

                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body kt-portlet__body--fit-y">
                        <!--begin::Widget -->
                        <div class="kt-widget kt-widget--user-profile-4">
                            <div class="kt-widget__head">
                                <div class="kt-widget__media">
                                    <img class="kt-widget__img kt-hidden-" src="<?= sourceImage('other/icons/youtube.png') ?>" alt="image">
                                    <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-hidden">
                                        YT
                                    </div>
                                </div>
                                <div class="kt-widget__content">
                                    <div class="kt-widget__section">
                                        <a href="#" class="kt-widget__username">
                                            YOUTUBE <?php if($account->pseudo_youtube): ?>(<?= $account->pseudo_youtube; ?>)<?php endif; ?>
                                        </a>
                                        <div class="kt-widget__button">
                                            <?php if(empty($account->pseudo_youtube)): ?>
                                                <span class="btn btn-label-danger btn-sm">Non Connecter</span>
                                            <?php else: ?>
                                                <span class="btn btn-label-success btn-sm">Connecter</span>
                                            <?php endif; ?>
                                        </div>

                                        <div class="kt-widget__action">
                                            <?php if(empty($account->pseudo_youtube)): ?>
                                                <a href="#" class="btn btn-label-youtube">
                                                    <i class="socicon-youtube"></i> Connection
                                                </a>
                                            <?php else: ?>
                                                <a href="#" class="btn btn-label-youtube">
                                                    <i class="socicon-youtube"></i> Deconnexion
                                                </a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Widget -->
                    </div>
                </div>
            </div>
        </div>
        <?php
        $content = ob_get_clean();
        return response()->json([$content]);

    }

    /**
     * @param Request $request
     * @return null
     */
    public function updatePassword(Request $request)
    {
        $request->validate([
            "password"  => "required|string|min:6|confirmed"
        ]);

        $password = Hash::make($request->password);

        $this->userRepository->updatePassword($request->user_id, $password);

        addingActivity("Modification de votre mot de passe", "la la-key", 2);

        return null;
    }


    /**
     * @return null
     */
    public function delete()
    {
        $this->userRepository->delete();

        return null;
    }

    public function loadNotif()
    {
        ob_start();
        ?>
        <div class="kt-section">
            <div class="kt-section__content" id="notification">
                <?php foreach (auth()->user()->unreadNotifications as $notification): ?>
                <div class="kt-widget4">
                    <div class="kt-widget4__item">
                        <div class="kt-widget4__pic kt-widget4__pic--pic">
                            <img src="<?= $notification->data['images']; ?>" alt="">
                        </div>
                        <div class="kt-widget4__info">
                            <a href="#" class="kt-widget4__username">
                                <?= $notification->data["title"] ?>
                            </a>
                            <p class="kt-widget4__text">
                                <?= $notification->data["description"] ?>
                            </p>
                        </div>
                        <a href="#" id="btnHasRead" data-id="<?= $notification->id ?>" class="btn btn-sm btn-label-brand btn-bold"><i class="la la-check"></i> </a>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
        <script type="text/javascript">
            (function ($) {
                $("#notification").on('click', '#btnHasRead', function (e) {
                    e.preventDefault()
                    let btn = $(this)
                    let id = btn.attr('data-id')
                    let url = '/account/notification/'+id+'/read'

                    KTApp.progress(btn)

                    $.ajax({
                        url: url,
                        method: "GET",
                        success: function (data) {
                            KTApp.unprogress(btn)
                            btn.parents('.kt-widget4').fadeOut()
                        }
                    })
                })
            })(jQuery)
        </script>
        <?php
        $content = ob_get_clean();

        return response()->json($content);
    }

    public function loadActivity()
    {
        $activities = $this->userActivityRepository->getActivities();
        ob_start();
        ?>
        <div class="kt-section">
            <div class="kt-section__content" id="activities">
                <?php foreach ($activities as $activity): ?>
                    <div class="kt-widget4">
                        <div class="kt-widget4__item">
                            <div class="kt-widget4__pic kt-widget4__pic--pic">
                                <i class="<?= $activity->icons; ?>"></i>
                            </div>
                            <div class="kt-widget4__info">
                                <a href="#" class="kt-widget4__username">
                                    <?= $activity->activity; ?>
                                </a>
                                <p class="kt-widget4__text">
                                    <?= $activity->updated_at->format("d/m/Y à H:i") ?>
                                </p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <?php
        $content = ob_get_clean();

        return response()->json($content);
    }

    public function loadBlog()
    {
        $user = $this->userRepository->getUserLoad();
        ob_start();
        ?>
        <div class="kt-section">
            <div class="kt-section__content">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Sujets</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($user->blogcomments as $blogcomment): ?>
                        <tr>
                            <td><i class="la la-circle-o-notch"></i> </td>
                            <td>
                                <?= $blogcomment->blog->title; ?><br>
                                <h6 class="text-muted">Posté le <?= $blogcomment->updated_at->format('d/m/Y à H:i'); ?></h6>
                            </td>
                            <td></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php
        $content = ob_get_clean();

        return response()->json($content);
    }

    public function notificationRead($notification_id)
    {
        $this->databaseNotificationRepository->hasRead($notification_id);

        return null;
    }
}
