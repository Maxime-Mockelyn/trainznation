<?php

namespace App\Http\Controllers\Account\Invoice;

use App\Repository\Checkout\InvoiceRepository;
use App\Repository\User\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InvoiceController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var InvoiceRepository
     */
    private $invoiceRepository;

    /**
     * InvoiceController constructor.
     * @param UserRepository $userRepository
     * @param InvoiceRepository $invoiceRepository
     */
    public function __construct(UserRepository $userRepository, InvoiceRepository $invoiceRepository)
    {
        $this->userRepository = $userRepository;
        $this->invoiceRepository = $invoiceRepository;
    }

    public function index()
    {
        return view("Account.Invoice.index", [
            "invoices"  => $this->invoiceRepository->listForUser(auth()->user()->id)
        ]);
    }
}
