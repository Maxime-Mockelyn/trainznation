<?php

namespace App\Http\Controllers\Account\Premium;

use App\Repository\User\UserAccountRepository;
use App\Repository\User\UserRepository;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Laravel\Cashier\Cashier;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Stripe;

class PremiumController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var UserAccountRepository
     */
    private $userAccountRepository;

    /**
     * PremiumController constructor.
     * @param UserRepository $userRepository
     * @param UserAccountRepository $userAccountRepository
     * @throws \Exception
     */
    public function __construct(UserRepository $userRepository, UserAccountRepository $userAccountRepository)
    {
        $this->userRepository = $userRepository;
        $this->userAccountRepository = $userAccountRepository;
        Cashier::useCurrency('eur', '€');
        Stripe::setApiKey(env("STRIPE_SECRET_KEY"));
    }

    public function index()
    {
        return view("Account.premium", [
            "user"  => $this->userRepository->getUser()
        ]);
    }

    public function charge(Request $request)
    {
        //dd($request->all());
        try {
            $user = $this->userRepository->getUser();
            $user->newSubscription('main', $request->plan)->create($request->stripeToken, [
                "email" => $request->stripeEmail
            ]);

            $this->userAccountRepository->subscribePremium($user->id);

            \Toastr::success("Votre souscription à été valider", "Souscription Offre Premium");
            return redirect()->back();
        }catch (\Exception $ex){
            return $ex->getMessage();
        }
    }

    public function invoice($invoice_id)
    {
        try {
            $user = $this->userRepository->getUser();
            return $user->downloadInvoice($invoice_id, [
                "vendor"    => env("APP_NAME"),
                "product"   => "Souscription Premium"
            ]);
        }catch (\Exception $exception){
            \Toastr::error("Erreur lors de la génération de la facture PDF", "Facture PDF");
            Log::error($exception->getMessage());
            return redirect()->back();
        }
    }

    public function delete(Request $request)
    {
        try {
            $user = $this->userRepository->getUser();
            $user->subscription('main')->cancel();

            $this->userAccountRepository->deletePremium($user->id);

            \Toastr::success("L'abonnement à été annuler", "Annulation de l'abonnement");
            return redirect()->back();
        }catch (\Exception $exception)
        {
            \Toastr::error("Erreur lors de l'annulation de l'abonnement", "Annulation de l'abonnement");
            Log::error($exception->getMessage());
            return redirect()->back();
        }
    }
}
