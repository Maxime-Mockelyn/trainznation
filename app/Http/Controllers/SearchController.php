<?php

namespace App\Http\Controllers;

use App\Repository\Asset\AssetRepository;
use App\Repository\Blog\BlogRepository;
use App\Repository\Suggestion\SuggestionRepository;
use App\Repository\Tutoriel\TutorielRepository;
use App\Repository\Wiki\WikiRepository;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    /**
     * @var AssetRepository
     */
    private $assetRepository;
    /**
     * @var BlogRepository
     */
    private $blogRepository;
    /**
     * @var SuggestionRepository
     */
    private $suggestionRepository;
    /**
     * @var TutorielRepository
     */
    private $tutorielRepository;
    /**
     * @var WikiRepository
     */
    private $wikiRepository;

    /**
     * SearchController constructor.
     * @param AssetRepository $assetRepository
     * @param BlogRepository $blogRepository
     * @param SuggestionRepository $suggestionRepository
     * @param TutorielRepository $tutorielRepository
     * @param WikiRepository $wikiRepository
     */
    public function __construct(AssetRepository $assetRepository, BlogRepository $blogRepository, SuggestionRepository $suggestionRepository, TutorielRepository $tutorielRepository, WikiRepository $wikiRepository)
    {
        $this->assetRepository = $assetRepository;
        $this->blogRepository = $blogRepository;
        $this->suggestionRepository = $suggestionRepository;
        $this->tutorielRepository = $tutorielRepository;
        $this->wikiRepository = $wikiRepository;
    }

    public function searchPost(Request $request)
    {
        $assets = $this->assetRepository->searchByLike($request->search);
        $blogs = $this->blogRepository->searchByLike($request->search);
        $suggests = $this->suggestionRepository->searchByLike($request->search);
        $tutoriels = $this->tutorielRepository->searchByLike($request->search);
        $wikis = $this->wikiRepository->searchByLike($request->search);
        $countTerme = $assets["counts"]+$blogs['counts']+$suggests['counts']+$tutoriels['counts']+$wikis['counts'];

        $counts = [
            "countAsset" => $assets['counts'],
            "countBlog" => $blogs['counts'],
            "countSuggest" => $suggests['counts'],
            "countTutoriel" => $tutoriels['counts'],
            "countWiki" => $wikis['counts']
        ];


        return view("Front.search", [
            "assets"    => $assets['assets'],
            "blogs"     => $blogs['blogs'],
            "suggests"  => $suggests['suggests'],
            "tutoriels" => $tutoriels['tutoriels'],
            "wikis"     => $wikis['wikis'],
            "counts"    => $countTerme,
            "search"    => $request->search,
            "count"     => $counts
        ]);
    }
}
