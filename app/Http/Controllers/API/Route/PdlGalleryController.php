<?php

namespace App\Http\Controllers\Api\Route;

use App\Http\Controllers\API\BaseController;
use App\Repository\Route\PdlGalleryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdlGalleryController extends BaseController
{
    /**
     * @var PdlGalleryRepository
     */
    private $galleryRepository;

    /**
     * PdlGalleryController constructor.
     * @param PdlGalleryRepository $galleryRepository
     */
    public function __construct(PdlGalleryRepository $galleryRepository)
    {
        $this->galleryRepository = $galleryRepository;
    }

    public function listView()
    {
        $datas = $this->galleryRepository->getAll();

        ob_start();
        ?>
        <?php foreach($datas as $gallery): ?>
        <div class="col s4">
            <img src="<?= sourceImage('route/pdl/gallery/'.$gallery->filename); ?>" alt="" class="materialboxed" width="250">
        </div>
        <?php endforeach; ?>
        <script type="text/javascript">
            (function ($) {
                $('.materialboxed').materialbox();
            })(jQuery)
        </script>
        <?php
        $content = ob_get_clean();
        return response()->json(["content" => $content]);
    }

    public function getGalleryFrom($category_id)
    {
        $datas = $this->galleryRepository->getForCategory($category_id);
        ob_start();
        ?>
        <?php foreach($datas as $gallery): ?>
        <div class="col s4">
            <img src="<?= sourceImage('route/pdl/gallery/'.$gallery->filename); ?>" alt="" class="materialboxed" width="250">
        </div>
        <?php endforeach; ?>
        <script type="text/javascript">
            (function ($) {
                $('.materialboxed').materialbox();
            })(jQuery)
        </script>
        <?php
        $content = ob_get_clean();
        return response()->json(["content" => $content]);
    }
}
