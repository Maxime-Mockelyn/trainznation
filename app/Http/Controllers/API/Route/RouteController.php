<?php

namespace App\Http\Controllers\Api\Route;

use App\Http\Controllers\API\BaseController;
use App\Repository\Route\PdlAnomalieRepository;
use App\Repository\Route\PdlBuildRepository;
use App\Repository\Route\PdlDownloadRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RouteController extends BaseController
{
    /**
     * @var PdlDownloadRepository
     */
    private $downloadRepository;
    /**
     * @var PdlBuildRepository
     */
    private $buildRepository;
    /**
     * @var PdlAnomalieRepository
     */
    private $anomalieRepository;

    /**
     * RouteController constructor.
     * @param PdlDownloadRepository $downloadRepository
     * @param PdlBuildRepository $buildRepository
     * @param PdlAnomalieRepository $anomalieRepository
     */
    public function __construct(PdlDownloadRepository $downloadRepository, PdlBuildRepository $buildRepository, PdlAnomalieRepository $anomalieRepository)
    {
        $this->downloadRepository = $downloadRepository;
        $this->buildRepository = $buildRepository;
        $this->anomalieRepository = $anomalieRepository;
    }

    public function update(Request $request)
    {
        //dd($request->all());
        try {
            $info = $this->downloadRepository->getForBuild($request->build);
            return $this->sendResponse($info->toArray(), "Build");
        } catch (\Exception $exception) {
            return $this->sendError("Erreur", [
                "exception" => $exception->getMessage()
            ], 251);
        }
    }

    public function getInfo()
    {
        try {
            $info = [
                "build" => $this->buildRepository->getActualBuild(),
                "anomalies" => $this->anomalieRepository->getAllAnomalie(),
                "percent"   => $this->anomalieRepository->percent()
            ];

            return $this->sendResponse($info, "Information");
        }catch (\Exception $exception) {
            return $this->sendError("Erreur", [
                "exception" => $exception->getMessage()
            ], 251);
        }
    }

    public function latestDownload()
    {
        try {
            $info = $this->downloadRepository->getLatestBuild();
            return $this->sendResponse($info->toArray(), "Dernier Téléchargement");
        } catch (\Exception $exception) {
            return $this->sendError("Erreur", [
                "exception" => $exception->getMessage()
            ], 251);
        }
    }
}
