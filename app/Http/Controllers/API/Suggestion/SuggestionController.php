<?php

namespace App\Http\Controllers\API\Suggestion;

use App\Http\Controllers\API\BaseController;
use App\Repository\Suggestion\SuggestionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class SuggestionController extends BaseController
{
    /**
     * @var SuggestionRepository
     */
    private $suggestionRepository;

    /**
     * SuggestionController constructor.
     * @param SuggestionRepository $suggestionRepository
     */
    public function __construct(SuggestionRepository $suggestionRepository)
    {
        $this->suggestionRepository = $suggestionRepository;
    }

    public function all()
    {
        $suggestions = $this->suggestionRepository->all();

        return $this->sendResponse($suggestions->toArray(), "Les Suggestions ont été Récupéré");
    }

    public function create(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            "name"  => "required",
            "suggestion" => "required|min:2|max:2500"
        ]);

        if($validator->fails()){
            return $this->sendError("Erreur de Validation", [$validator->errors()]);
        }

        $suggestion = $this->suggestionRepository->create($request->name, $request->suggestion);

        return $this->sendResponse($suggestion->toArray(), "La Suggestion à été soumise avec succès");
    }
}
