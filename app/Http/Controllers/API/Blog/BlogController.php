<?php

namespace App\Http\Controllers\API\Blog;

use App\Http\Controllers\API\BaseController;
use App\Repository\Blog\BlogRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends BaseController
{
    /**
     * @var BlogRepository
     */
    private $blogRepository;

    /**
     * BlogController constructor.
     * @param BlogRepository $blogRepository
     */
    public function __construct(BlogRepository $blogRepository)
    {
        $this->blogRepository = $blogRepository;
    }

    public function all()
    {
        $blogs = $this->blogRepository->getAllPosts();

        return $this->sendResponse($blogs->toArray(), 'Blog List Succefully');
    }

    public function first()
    {
        $blog = $this->blogRepository->getFirstPost();

        return $this->sendResponse($blog->toArray(), "Blog First Post Success");
    }
}
