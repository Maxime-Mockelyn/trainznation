<?php

namespace App\Http\Controllers;

use App\Library\Youtube\Uploader;
use Illuminate\Http\Request;

class YoutubeController extends Controller
{
    public function authorized($uri)
    {
        return view("Back.Youtube.authorize", [
            "uri"   => $uri
        ]);
    }

    public function callbacks(Request $request)
    {
        session(["youtube_code" => $request->code]);
        dd(session()->all());
    }
}
