<?php

namespace App\Http\Controllers\Back\Notifications;

use App\HelperClass\Images;
use App\Http\Controllers\Back\BackController;
use App\Notifications\Notification\AllNotification;
use App\Repository\User\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use Kamaln7\Toastr\Facades\Toastr;

class NotificationController extends BackController
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var Images
     */
    private $images;

    /**
     * NotificationController constructor.
     * @param UserRepository $userRepository
     * @param Images $images
     */
    public function __construct(UserRepository $userRepository, Images $images)
    {
        parent::__construct();
        $this->userRepository = $userRepository;
        $this->images = $images;
    }

    public function create()
    {
        return view("Back.Notification.create", [
            "users" => $this->userRepository->getAll(),
            "images" => $this->images->images()
        ]);
    }

    public function store(Request $request)
    {
        //dd($request->all());

        $image = Arr::get((array)$this->images->images(), $request->icons-1);

        //dd($image);

        if($request->state)
        {
            $users = $this->userRepository->getAll();
            foreach ($users as $user)
            {
                $user->notify(new AllNotification(
                    $request->title,
                    $request->contents,
                    $image,
                    $request->link
                ));
            }

            Toastr::success("Les utilisateur ont été notifier", "Notification");
            return back();
        } else {
            $user = $this->userRepository->getUserbyId($request->user_id);
            $user->notify(new AllNotification(
                $request->title,
                $request->contents,
                $image,
                $request->link
            ));
            Toastr::success("L'utilisateur <strong>".$user->name."</strong> à été notifier", "Notification");
            return back();
        }
    }


}
