<?php

namespace App\Http\Controllers\Back\Suggestion;

use App\Http\Controllers\Back\BackController;
use App\Mail\Suggestion\Accepted;
use App\Mail\Suggestion\Rejected;
use App\Repository\Suggestion\SuggestionRepository;
use App\Repository\Suggestion\SuggestionTimelineRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Kamaln7\Toastr\Facades\Toastr;

class SuggestionController extends BackController
{
    /**
     * @var SuggestionRepository
     */
    private $suggestionRepository;
    /**
     * @var SuggestionTimelineRepository
     */
    private $suggestionTimelineRepository;

    /**
     * SuggestionController constructor.
     * @param SuggestionRepository $suggestionRepository
     * @param SuggestionTimelineRepository $suggestionTimelineRepository
     */
    public function __construct(SuggestionRepository $suggestionRepository, SuggestionTimelineRepository $suggestionTimelineRepository)
    {
        parent::__construct();
        $this->suggestionRepository = $suggestionRepository;
        $this->suggestionTimelineRepository = $suggestionTimelineRepository;
    }

    public function index()
    {
        return view("Back.Suggestion.index", [
            "suggestions"   => $this->suggestionRepository->all()
        ]);
    }

    public function show($suggestion_id)
    {
        return view("Back.Suggestion.show", [
            "suggestion" => $this->suggestionRepository->get($suggestion_id)
        ]);
    }

    public function rejected(Request $request, $suggestion_id)
    {
        try {
            // Impotation de la suggestion
            $suggestion = $this->suggestionRepository->get($suggestion_id);

            try {
                // Rejet de la suggestion dans la base
                $this->suggestionRepository->rejected($suggestion_id);

                try {
                    // Ajout du rejet dans la timeline
                    $reject = $this->suggestionTimelineRepository->rejected($suggestion_id, $request->title, $request->text);

                    try {
                        // Envoie de l'email à l'initiateur de la suggestion
                        Mail::to($suggestion->email)->send(new Rejected($reject, $suggestion));

                        try {
                            // Notification à l'administrateur du rejet de la suggestion
                            Notification::send(auth()->user(), new \App\Notifications\Suggestion\Rejected($reject, $suggestion));
                            Toastr::success("La suggestion à bien été rejeter", "Suggestion");
                            return back();
                        }catch (\Exception $exception)
                        {
                            Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
                            Toastr::error("Impossible de notifier l'administrateur, consulter les logs", "Erreur Serveur");
                            return back();
                        }
                    }catch (\Exception $exception)
                    {
                        Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
                        Toastr::error("Impossible de notifier l'initiateur de la suggestion, consulter les logs", "Erreur Serveur");
                        return back();
                    }
                }catch (\Exception $exception)
                {
                    Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
                    Toastr::error("Impossible d'ajouter la commande dans le timeline, consulter les logs", "Erreur Serveur");
                    return back();
                }
            }catch (\Exception $exception)
            {
                Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
                Toastr::error("Impossible de rejeter la suggestion, consulter les logs", "Erreur Serveur");
                return back();
            }
        }catch (\Exception $exception)
        {
            Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
            Toastr::error("Impossible d'impoter la suggestion, consulter les logs", "Erreur Serveur");
            return back();
        }
    }

    public function accepted(Request $request, $suggestion_id)
    {
        try {
            // Impotation de la suggestion
            $suggestion = $this->suggestionRepository->get($suggestion_id);

            try {
                // Rejet de la suggestion dans la base
                $this->suggestionRepository->accepted($suggestion_id);

                try {
                    // Ajout du rejet dans la timeline
                    $reject = $this->suggestionTimelineRepository->accepted($suggestion_id, $request->title, $request->text);

                    try {
                        // Envoie de l'email à l'initiateur de la suggestion
                        Mail::to($suggestion->email)->send(new Accepted($reject, $suggestion));

                        try {
                            // Notification à l'administrateur du rejet de la suggestion
                            Notification::send(auth()->user(), new \App\Notifications\Suggestion\Accepted($reject, $suggestion));
                            Toastr::success("La suggestion à bien été accepter", "Suggestion");
                            return back();
                        }catch (\Exception $exception)
                        {
                            Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
                            Toastr::error("Impossible de notifier l'administrateur, consulter les logs", "Erreur Serveur");
                            return back();
                        }
                    }catch (\Exception $exception)
                    {
                        Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
                        Toastr::error("Impossible de notifier l'initiateur de la suggestion, consulter les logs", "Erreur Serveur");
                        return back();
                    }
                }catch (\Exception $exception)
                {
                    Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
                    Toastr::error("Impossible d'ajouter la commande dans le timeline, consulter les logs", "Erreur Serveur");
                    return back();
                }
            }catch (\Exception $exception)
            {
                Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
                Toastr::error("Impossible de rejeter la suggestion, consulter les logs", "Erreur Serveur");
                return back();
            }
        }catch (\Exception $exception)
        {
            Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
            Toastr::error("Impossible d'impoter la suggestion, consulter les logs", "Erreur Serveur");
            return back();
        }
    }
}
