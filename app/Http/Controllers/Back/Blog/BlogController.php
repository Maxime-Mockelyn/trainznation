<?php

namespace App\Http\Controllers\Back\Blog;

use App\HelperClass\Blog;
use App\Http\Controllers\Back\BackController;
use App\Http\Requests\Blog\StorePost;
use App\Http\Requests\Blog\UpdatePost;
use App\Jobs\Blog\BlogImageStore;
use App\Jobs\Blog\ImageProcess;
use App\Jobs\Blog\PublishedDatabase;
use App\Jobs\Blog\PublishedTwitter;
use App\Library\Twitter\TwitterAccessor;
use App\Notifications\Blog\BlogPublished;
use App\Notifications\Blog\BlogPublishedDatabase;
use App\Repository\Blog\BlogCategorieRepository;
use App\Repository\Blog\BlogCommentRepository;
use App\Repository\Blog\BlogRepository;
use App\Repository\User\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;
use Kamaln7\Toastr\Facades\Toastr;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class BlogController extends BackController
{
    /**
     * @var BlogCategorieRepository
     */
    private $blogCategorieRepository;
    /**
     * @var BlogRepository
     */
    private $blogRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var BlogCommentRepository
     */
    private $blogCommentRepository;
    /**
     * @var TwitterAccessor
     */
    private $twitterAccessor;
    /**
     * @var Blog
     */
    private $blog;

    /**
     * BlogController constructor.
     * @param BlogCategorieRepository $blogCategorieRepository
     * @param BlogRepository $blogRepository
     * @param UserRepository $userRepository
     * @param BlogCommentRepository $blogCommentRepository
     * @param TwitterAccessor $twitterAccessor
     * @param Blog $blog
     */
    public function __construct(BlogCategorieRepository $blogCategorieRepository, BlogRepository $blogRepository, UserRepository $userRepository, BlogCommentRepository $blogCommentRepository, TwitterAccessor $twitterAccessor, Blog $blog)
    {
        parent::__construct();
        $this->blogCategorieRepository = $blogCategorieRepository;
        $this->blogRepository = $blogRepository;
        $this->userRepository = $userRepository;
        $this->blogCommentRepository = $blogCommentRepository;
        $this->twitterAccessor = $twitterAccessor;
        $this->blog = $blog;
    }

    public function index()
    {
        return view("Back.Blog.index", [
            "categories" => $this->blogCategorieRepository->getAll(),
            "blogs"      => $this->blogRepository->getAllPosts()
        ]);
    }

    public function create()
    {
        return view("Back.Blog.create", [
            "categories"    => $this->blogCategorieRepository->getAll()
        ]);
    }

    public function store(Request $request)
    {
        try {
            if($request->published){$published = 1;} else {$published = 0;}
            if($request->twitter){$twitter = 1;} else {$twitter = 0;}

            $blog = $this->blogRepository->create(
                $request->category_id,
                $request->title,
                $request->slug,
                $published,
                $twitter,
                $request->short_content
            );

            Toastr::success("L'article <strong>".$blog->title."</strong> à été créer avec succès", "Nouvelle article");
            return redirect()->route('Back.Blog.index');
        }catch (\Exception $exception){
            Log::error($exception->getMessage(), ["code" => $exception->getCode()]);
            Toastr::error("Erreur lors du traitement, vérifier le fichier de Log", "Erreur 500");
            return back();
        }
    }

    public function show($slug)
    {
        return view("Back.Blog.show", [
            "blog"  => $this->blogRepository->getPost($slug)
        ]);
    }

    public function edit($id)
    {

        //dd($this->blogRepository->getPostById($id));

        return view("Back.Blog.edit", [
            "categories"    => $this->blogCategorieRepository->getAll(),
            "blog"          => $this->blogRepository->getPostById($id)
        ]);
    }

    public function update(Request $request, $id)
    {
        try {
            $blog = $this->blogRepository->update(
                $id,
                $request->title,
                $request->slug,
                $request->category_id,
                $request->short_content
            );

            Toastr::success("L'article <strong>".$request->title."</strong> à été mis à jour", "Mise à jour d'un article");
            return redirect()->route("Back.Blog.show", $blog->slug);
        }catch (\Exception $exception){
            Log::error($exception->getMessage(), ["code" => $exception->getCode()]);
            Toastr::error("Erreur de traitement, consulter les logs", "Erreur 500");
            return redirect()->back();
        }
    }

    public function updateContent(Request $request, $id)
    {
        try {
            $blog = $this->blogRepository->getPostById($id);
            // Traitement du fichier si il existe
            if($request->file('file'))
            {
                $request->file('file')->storeAs('blog/', $blog->id.'.png', 'public');
                Storage::disk('public')->setVisibility('blog/'.$blog->id.'.png', 'public');
            }

            // Traitement des élements existants
            if($request->twitterText != null) {$twitterText = $request->twitterText;}else{$twitterText = null;}
            if($request->published_at) {$published_at = $request->published_at;}else{$published_at = null;}

            // Mise à jour de la base
            $blog = $this->blogRepository->updateContent(
                $id,
                $twitterText,
                $published_at
            );

            Toastr::success("L'article <strong>".$blog->title."</strong> à été mis à jour", "Mise à jour d'un article");
            return redirect()->route("Back.Blog.show", $blog->slug);
        }catch (\Exception $exception) {
            Log::error($exception->getMessage(), ["code" => $exception->getCode()]);
            Toastr::error("Erreur de traitement, consulter les logs", "Erreur 500");
            return redirect()->back();
        }
    }

    public function updateContenu(Request $request, $id)
    {
        //dd($request->all());
        try {
            $blog = $this->blogRepository->updateContenu($id, $request->description);

            Toastr::success("L'article <strong>".$blog->title."</strong> à été mis à jour", "Mise à jour d'un article");
            return redirect()->route("Back.Blog.show", $blog->slug);
        }catch (\Exception $exception) {
            Log::error($exception->getMessage(), ["code" => $exception->getCode()]);
            Toastr::error("Erreur de traitement, consulter les logs", "Erreur 500");
            return redirect()->back();
        }
    }

    public function delete($id)
    {
        try {
            $blog = $this->blogRepository->getPostById($id);
            $this->blogRepository->delete($id);

            try {
                $image = $this->blog->deleteImage($blog);
                Toastr::success($image["responseText"], "Suppression de l'image");
            }catch (FileException $exception){
                Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
                Toastr::error($exception->getMessage(), "Erreur Server");
            }

            Toastr::success("L'article à été supprimer", "Suppression d'un article");
            return redirect()->route("Back.Blog.index");

        } catch (\Exception $exception) {
            Log::error($exception->getMessage(), ["code" => $exception->getCode()]);
            Toastr::error("Erreur de traitement, consulter les logs", "Erreur 500");
            return redirect()->back();
        }
    }


    public function publish($blog_id)
    {
        try {
            $blog = $this->blogRepository->getPostById($blog_id);
            $users = $this->userRepository->getAll();
            dispatch(new PublishedDatabase($users, $blog));

            try {
                if($blog->twitter == 1)
                {
                    $this->twitterAccessor->postTweetWithImage($blog->twitterText, 'blog', $blog->id);
                    $this->blogRepository->publish($blog_id);
                    Toastr::success("L'article à été publier sur twitter", "Twitter");
                    Toastr::success("L'article à été publier", "Publication d'un article");
                }else{
                    $this->blogRepository->publish($blog->id);
                    Toastr::success("L'article à été publier", "Publication d'un article");
                }
            }catch (\Exception $exception){
                Log::error($exception->getMessage(), ["code" => $exception->getCode()]);
                Toastr::error("Erreur de traitement, consulter les logs", "Erreur 500");
            }

            Toastr::info("Un processus de publication à été lancer", "Processus");
            return back();
        }catch (\Exception $exception) {
            Log::error($exception->getMessage(), ["code" => $exception->getCode()]);
            Toastr::error("Erreur de traitement, consulter les logs", "Erreur 500");
            return redirect()->back();
        }
    }


    public function unPublish($blog_id)
    {
        try {
            $blog = $this->blogRepository->getPostById($blog_id);
            $this->blogRepository->unPublish($blog->id);

            Toastr::success("L'article à été dépublier", "Publication d'un article");
            return back();
        } catch (\Exception $exception) {
            Log::error($exception->getMessage(), ["code" => $exception->getCode()]);
            Toastr::error("Erreur de traitement, consulter les logs", "Erreur 500");
            return redirect()->back();
        }
    }


    public function getContenue($id)
    {
        $blog = $this->blogRepository->getPostById($id);
        return response()->json([$blog->content]);
    }

}
