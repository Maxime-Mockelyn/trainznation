<?php

namespace App\Http\Controllers\Back\Blog\Category;

use App\Http\Controllers\Back\BackController;
use App\Http\Requests\Blog\Category\StoreCategory;
use App\Http\Requests\Blog\Category\UpdateCategory;
use App\Repository\Blog\BlogCategorieRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends BackController
{
    /**
     * @var BlogCategorieRepository
     */
    private $blogCategorieRepository;

    /**
     * CategoryController constructor.
     * @param BlogCategorieRepository $blogCategorieRepository
     */
    public function __construct(BlogCategorieRepository $blogCategorieRepository)
    {
        parent::__construct();
        $this->blogCategorieRepository = $blogCategorieRepository;
    }

    public function create()
    {
        return view("Back.Blog.Category.create");
    }

    public function store(StoreCategory $request)
    {
        $category = $this->blogCategorieRepository->create($request->get('name'));

        if($category){
            return response()->json(["name" => $category->name]);
        }else{
            return null;
        }
    }

    public function edit($category_id)
    {
        return view("Back.Blog.Category.edit", [
            "category"  => $this->blogCategorieRepository->get($category_id)
        ]);
    }

    public function update(UpdateCategory $request)
    {
        $up = $this->blogCategorieRepository->update(
            $request->get('id'),
            $request->get('name')
        );

        $category = $this->blogCategorieRepository->get($request->get('id'));


        if($up){
            return response()->json(["name" => $category->name]);
        }else{
            return null;
        }

    }

    public function delete($category_id)
    {
        $this->blogCategorieRepository->delete($category_id);
    }
}
