<?php

namespace App\Http\Controllers\Back\Blog\Comment;

use App\Http\Controllers\Back\BackController;
use App\Repository\Blog\BlogCommentRepository;
use App\Repository\Blog\BlogRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentController extends BackController
{
    /**
     * @var BlogRepository
     */
    private $blogRepository;
    /**
     * @var BlogCommentRepository
     */
    private $blogCommentRepository;

    /**
     * CommentController constructor.
     * @param BlogRepository $blogRepository
     * @param BlogCommentRepository $blogCommentRepository
     */
    public function __construct(BlogRepository $blogRepository, BlogCommentRepository $blogCommentRepository)
    {
        parent::__construct();
        $this->blogRepository = $blogRepository;
        $this->blogCommentRepository = $blogCommentRepository;
    }

    public function approuved($blog_id, $comment_id)
    {
        $this->blogCommentRepository->approuved($comment_id);

        return null;
    }

    public function desapprouved($blog_id, $comment_id)
    {
        $this->blogCommentRepository->desapprouved($comment_id);

        return null;
    }
}
