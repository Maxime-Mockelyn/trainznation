<?php

namespace App\Http\Controllers\Back\Route\Pdl\Gallery;

use App\Http\Controllers\Back\BackController;
use App\Repository\Route\PdlGalleryCategorieRepository;
use App\Repository\Route\PdlGalleryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Kamaln7\Toastr\Facades\Toastr;

class GalleryController extends BackController
{
    /**
     * @var PdlGalleryCategorieRepository
     */
    private $pdlGalleryCategorieRepository;
    /**
     * @var PdlGalleryRepository
     */
    private $galleryRepository;

    /**
     * GalleryController constructor.
     * @param PdlGalleryCategorieRepository $pdlGalleryCategorieRepository
     * @param PdlGalleryRepository $galleryRepository
     */
    public function __construct(PdlGalleryCategorieRepository $pdlGalleryCategorieRepository, PdlGalleryRepository $galleryRepository)
    {
        parent::__construct();
        $this->pdlGalleryCategorieRepository = $pdlGalleryCategorieRepository;
        $this->galleryRepository = $galleryRepository;
    }

    public function index()
    {
        return view("Back.Route.Pdl.Gallery.index", [
            "categories"    => $this->pdlGalleryCategorieRepository->all()
        ]);
    }

    public function store(Request $request)
    {
        $categorie = $this->pdlGalleryCategorieRepository->store($request->name);

        Toastr::success("La Catégorie ".$categorie->name." à été ajouter", "Ajout d'une catégorie");
        return redirect()->back();
    }

    public function show($category_id)
    {
        //dd($this->pdlGalleryCategorieRepository->get($category_id));
        return view("Back.Route.Pdl.Gallery.show", [
            "category"  => $this->pdlGalleryCategorieRepository->get($category_id),
            "items"     => $this->galleryRepository->getForCategory($category_id)
        ]);
    }

    public function upload(Request $request, $category_id)
    {
        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();

        //$image->move(storage_path('route/pdl/gallery'), $imageName);

        if(env('APP_ENV') == 'production')
        {
            $file = $request->file('file')->storeAs(
                'route/pdl/gallery/', $imageName, 's3'
            );
        }else{
            $file = $request->file('file')->storeAs(
                'route/pdl/gallery/', $imageName, 'public'
            );
        }

        if(env('APP_ENV') == 'production')
        {
            Storage::disk('s3')->setVisibility('route/pdl/gallery/'.$imageName, 'public');
        }else{
            Storage::disk('public')->setVisibility('route/pdl/gallery/'.$imageName, 'public');
        }

        $imageUpload = $this->galleryRepository->store($category_id, $imageName);

        return response()->json(["success" => $imageName]);
    }

    public function fileDelete(Request $request, $category_id)
    {
        $filename = $request->filename;

        $this->galleryRepository->delete($filename);

        $path = storage_path('route/pdl/gallery/'.$filename);

        if(file_exists($path))
        {
            unlink($path);
        }

        return $filename;
    }

    public function delete($category_id)
    {
        $files = $this->galleryRepository->getForCategory($category_id);

        foreach ($files as $file)
        {
            if(env('APP_ENV') == 'production')
            {
                Storage::disk('s3')->delete('route/pdl/gallery/'.$file->filename);
            }else{
                Storage::disk('public')->delete('route/pdl/gallery/'.$file->filename);
            }
            $this->galleryRepository->delete($file->filename);
        }

        $this->pdlGalleryCategorieRepository->delete($category_id);

        return null;
    }
}
