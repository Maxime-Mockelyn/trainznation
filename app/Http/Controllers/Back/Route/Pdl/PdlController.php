<?php

namespace App\Http\Controllers\Back\Route\Pdl;

use App\Http\Controllers\Back\BackController;
use App\Repository\Route\PdlAnomalieRepository;
use App\Repository\Route\PdlBuildRepository;
use App\Repository\Route\PdlCompatibilityRepository;
use App\Repository\Route\PdlRepository;
use App\Repository\Route\PdlTimelineRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Kamaln7\Toastr\Facades\Toastr;
use Storage;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class PdlController extends BackController
{
    /**
     * @var PdlRepository
     */
    private $pdlRepository;
    /**
     * @var PdlBuildRepository
     */
    private $pdlBuildRepository;
    /**
     * @var PdlAnomalieRepository
     */
    private $pdlAnomalieRepository;
    /**
     * @var PdlTimelineRepository
     */
    private $pdlTimelineRepository;
    /**
     * @var PdlCompatibilityRepository
     */
    private $pdlCompatibilityRepository;

    /**
     * PdlController constructor.
     * @param PdlRepository $pdlRepository
     * @param PdlBuildRepository $pdlBuildRepository
     * @param PdlAnomalieRepository $pdlAnomalieRepository
     * @param PdlTimelineRepository $pdlTimelineRepository
     * @param PdlCompatibilityRepository $pdlCompatibilityRepository
     */
    public function __construct(
            PdlRepository $pdlRepository,
            PdlBuildRepository $pdlBuildRepository,
            PdlAnomalieRepository $pdlAnomalieRepository,
            PdlTimelineRepository $pdlTimelineRepository, PdlCompatibilityRepository $pdlCompatibilityRepository)
    {
        Parent::__construct();
        $this->pdlRepository = $pdlRepository;
        $this->pdlBuildRepository = $pdlBuildRepository;
        $this->pdlAnomalieRepository = $pdlAnomalieRepository;
        $this->pdlTimelineRepository = $pdlTimelineRepository;
        $this->pdlCompatibilityRepository = $pdlCompatibilityRepository;
    }

    public function index()
    {
        return view("Back.Route.Pdl.index", [
            "tableaus"  => $this->pdlRepository->all()
        ]);
    }

    public function create()
    {
        return view("Back.Route.Pdl.create");
    }

    public function store(Request $request)
    {
        //dd($request->all());
        if($request->disposition == 'right')
        {
            $right = 1;
            $left = 0;
        }else{
            $right = 0;
            $left = 1;
        }

        try {
            $content = $this->pdlRepository->create(
                $request->title,
                $request->contents,
                $right,
                $left
            );

            try {
                $file = $request->file('images')->storeAs(
                    'route/pdl/', $content->id.'.png', 'public'
                );

                Storage::setVisibility('route/pdl/'.$content->id.'.png', 'public');

                Toastr::success("L'image du contenu à été ajouter", "Nouveau Contenue");
            }catch (FileException $exception) {
                Log::error($exception->getMessage(), ["code" => $exception->getCode()]);
                Toastr::error("Erreur de traitement, consulter les logs", "Erreur 500");
            }

            Toastr::success("Le nouveau contenue à été créer", "Nouveau Contenue");
            return redirect()->route('Back.Route.Pdl.index');

        } catch (\Exception $exception) {
            Log::error($exception->getMessage(), ["code" => $exception->getCode()]);
            Toastr::error("Erreur de traitement, consulter les logs", "Erreur 500");
            return redirect()->back();
        }

    }

    public function edit($pdl_id)
    {
        return view("Back.Route.Pdl.edit", [
                "pdl"   => $this->pdlRepository->get($pdl_id)
        ]);
    }

    public function update(Request $request, $pdl_id)
    {
        if($request->disposition == 'right')
        {
            $right = 1;
            $left = 0;
        }else{
            $right = 0;
            $left = 1;
        }

        $this->pdlRepository->update(
                $pdl_id,
                $request->title,
                $request->contents,
                $right,
                $left
        );

        Toastr::success("Le contenue à été mis à jour", "Edition de contenue");
        return redirect()->route('Back.Route.Pdl.index');
    }

    public function delete($pdl_id)
    {
        try {
            $this->pdlRepository->delete($pdl_id);

            try {
                if(Storage::disk("public")->exists('route/pdl/'.$pdl_id.'.png')) {
                    Storage::disk('public')->delete('route/pdl/'.$pdl_id.'.png');

                    Toastr::success("L'image du contenu à été supprimer");
                }else{
                    Toastr::info("Aucune image à supprimer");
                }
            } catch (FileException $exception) {
                Log::error($exception->getMessage(), ["code" => $exception->getCode()]);
                Toastr::error("Erreur de traitement, consulter les logs", "Erreur 500");
            }

            Toastr::success("Le contenu à été supprimer", "Suppression d'un contenu");
            return back();

        } catch (\Exception $exception) {
            Log::error($exception->getMessage(), ["code" => $exception->getCode()]);
            Toastr::error("Erreur de traitement, consulter les logs", "Erreur 500");
            return back();
        }
    }

    /**
     * Mise à jour vers la prochaine version
     *
     * @param Request $request
     * @param string $nextVersion
     * @return RedirectResponse
     */
    public function upVersion(Request $request, $nextVersion)
    {
        // Import du latest Build
        $latestBuild = $this->pdlBuildRepository->getActualBuild();


        // Ajout des anomalie résolue dans la description du timeline
        $terminates = $this->pdlAnomalieRepository->getTerminatedAnomaly();

        ob_start();
        ?>
        <ul class="bullets">
            <?php foreach ($terminates as $terminate): ?>
            <li><strong class="<?= stateAnomalieText($terminate->state) ?>"><?= $terminate->lieu ?>:</strong><?= $terminate->anomalie ?> / <?= $terminate->correction ?></li>
            <?php endforeach; ?>
        </ul>
        <?php
        $content = ob_get_clean();

        $description = $request->get('description')."<br><br>".$content;

        //Suppression des anomalies terminer
        $this->pdlAnomalieRepository->trashTerminatedAnomalie();

        // Insertion du Timeline
        $timeline = $this->pdlTimelineRepository->create(
            $latestBuild->version,
            $description
        );

        // Insertion Compatibility
        $comp = $this->pdlCompatibilityRepository->create($nextVersion, $request->get('state_37'), $request->get('state_45'), $request->get('state_46'));

        // Création du nouveau build
        $newBuild = $this->pdlBuildRepository->create($nextVersion, $latestBuild->build);

        Toastr::success("La route est passer à la version <strong>".$nextVersion.":".$latestBuild."</strong>", "Passage à la version superieur !");
        return redirect()->back();

    }
}
