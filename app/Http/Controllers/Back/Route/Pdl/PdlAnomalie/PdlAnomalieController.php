<?php

namespace App\Http\Controllers\Back\Route\Pdl\PdlAnomalie;

use App\Http\Controllers\Back\BackController;
use App\Repository\Route\PdlAnomalieRepository;
use App\Repository\Route\PdlBuildRepository;
use App\Repository\Route\PdlCompatibilityRepository;
use App\Repository\Route\PdlTimelineRepository;
use Illuminate\Http\Request;
use Kamaln7\Toastr\Facades\Toastr;

class PdlAnomalieController extends BackController
{
    /**
     * @var PdlAnomalieRepository
     */
    private $pdlAnomalieRepository;
    /**
     * @var PdlBuildRepository
     */
    private $pdlBuildRepository;
    /**
     * @var PdlTimelineRepository
     */
    private $pdlTimelineRepository;
    /**
     * @var PdlCompatibilityRepository
     */
    private $pdlCompatibilityRepository;

    /**
     * PdlController constructor.
     * @param PdlAnomalieRepository $pdlAnomalieRepository
     * @param PdlBuildRepository $pdlBuildRepository
     * @param PdlTimelineRepository $pdlTimelineRepository
     * @param PdlCompatibilityRepository $pdlCompatibilityRepository
     */
    public function __construct(
        PdlAnomalieRepository $pdlAnomalieRepository,
        PdlBuildRepository $pdlBuildRepository,
        PdlTimelineRepository $pdlTimelineRepository,
        PdlCompatibilityRepository $pdlCompatibilityRepository)
    {
        parent::__construct();
        $this->pdlAnomalieRepository = $pdlAnomalieRepository;
        $this->pdlBuildRepository = $pdlBuildRepository;
        $this->pdlTimelineRepository = $pdlTimelineRepository;
        $this->pdlCompatibilityRepository = $pdlCompatibilityRepository;
    }

    public function index()
    {
        return view("Back.Route.Pdl.Anomalie.index", [
            "anomalies" => $this->pdlAnomalieRepository->getAllAnomalie(),
            "build"     => $this->pdlBuildRepository->getActualBuild(),
            "percent"   => $this->pdlAnomalieRepository->percent()
        ]);
    }

    public function store(Request $request)
    {

        if($request->get('state') == 2)
        {
            $actualBuild = $this->pdlBuildRepository->getActualBuild();
            $firstBuild = $this->pdlBuildRepository->getFirstBuild();

            $newBuild = calcNewBuild($actualBuild->build, $firstBuild->build);

            $this->pdlBuildRepository->update($newBuild);
        }


        $anomalie = $this->pdlAnomalieRepository->create(
            $request->get('anomalie'),
            $request->get('correction'),
            $request->get('lieu'),
            $request->get('state')
        );

        Toastr::success("L'anomalie N°".$anomalie->id." à été ajouté", "Nouvelle Anomalie");
        return redirect()->back();
    }

    public function edit($anomalie_id)
    {
        return view("Back.Route.Pdl.Anomalie.edit", [
            "anomaly"   => $this->pdlAnomalieRepository->get($anomalie_id)
        ]);
    }

    public function update(Request $request, $anomalie_id)
    {
        $anomalie = $this->pdlAnomalieRepository->get($anomalie_id);

        if($anomalie->state != 2)
        {
            if($request->get('state') == 2)
            {
                $actualBuild = $this->pdlBuildRepository->getActualBuild();
                $firstBuild = $this->pdlBuildRepository->getFirstBuild();

                $newBuild = calcNewBuild($actualBuild->build, $firstBuild->build);

                $this->pdlBuildRepository->update($newBuild);
            }
        }

        $this->pdlAnomalieRepository->update(
            $anomalie_id,
            $request->get('anomalie'),
            $request->get('correction'),
            $request->get('lieu'),
            $request->get('state')
        );

        Toastr::success("L'anomalie à été modifier", "Mise à jour d'anomalie");
        return redirect()->route('Route.Pdl.Anomalie.index');
    }

    public function delete($anomalie_id)
    {
        $anomaly = $this->pdlAnomalieRepository->get($anomalie_id);

        if($anomaly->state == 2)
        {
            $actualBuild = $this->pdlBuildRepository->getActualBuild();
            $firstBuild = $this->pdlBuildRepository->getFirstBuild();

            $newBuild = calcSubBuild($actualBuild->build, $firstBuild->build);

            $this->pdlBuildRepository->update($newBuild);
        }

        $this->pdlAnomalieRepository->delete($anomalie_id);

        return null;
    }
}
