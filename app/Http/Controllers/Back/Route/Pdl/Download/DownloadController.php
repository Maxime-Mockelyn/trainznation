<?php

namespace App\Http\Controllers\Back\Route\Pdl\Download;

use App\Http\Controllers\Back\BackController;
use App\Repository\Route\PdlDownloadRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Kamaln7\Toastr\Facades\Toastr;

class DownloadController extends BackController
{
    /**
     * @var PdlDownloadRepository
     */
    private $pdlDownloadRepository;

    /**
     * DownloadController constructor.
     * @param PdlDownloadRepository $pdlDownloadRepository
     */
    public function __construct(PdlDownloadRepository $pdlDownloadRepository)
    {
        parent::__construct();
        $this->pdlDownloadRepository = $pdlDownloadRepository;
    }

    public function index()
    {
        return view("Back.Route.Pdl.Download.index", [
            "downloads" => $this->pdlDownloadRepository->getAll()
        ]);
    }

    public function create()
    {
        return view("Back.Route.Pdl.Download.create");
    }

    public function store(Request $request)
    {
        //dd($request->all());

        if ($request->published) {
            $published = 1;
        } else {
            $published = 0;
        }

        try {
            $download = $this->pdlDownloadRepository->store(
                $request->version,
                $request->build,
                $request->typeDownload,
                $request->typeRelease,
                $request->linkDownload,
                $request->note,
                $published
            );

            Toastr::success("Le téléchargement à été créer avec succès", "Création d'un téléchargement");
            return back();
        } catch (\Exception $exception) {
            Log::error($exception->getMessage(), ["code" => $exception->getCode()]);
            Toastr::error("Erreur de traitement, consulter les logs", "Erreur 500");
            return back();
        }
    }

    public function activate($download_id)
    {
        try {
            $download = $this->pdlDownloadRepository->activate($download_id);
            Toastr::success("Le téléchargement à été activer", "Activation d'un téléchargement");
            return back();
        } catch (\Exception $exception) {
            Log::error($exception->getMessage(), ["code" => $exception->getCode()]);
            Toastr::error("Erreur de traitement, consulter les logs", "Erreur 500");
            return back();
        }
    }

    public function desactivate($download_id)
    {
        try {
            $download = $this->pdlDownloadRepository->desactivate($download_id);
            Toastr::success("Le téléchargement à été désactiver", "Désactivation d'un téléchargement");
            return back();
        } catch (\Exception $exception) {
            Log::error($exception->getMessage(), ["code" => $exception->getCode()]);
            Toastr::error("Erreur de traitement, consulter les logs", "Erreur 500");
            return back();
        }
    }

    public function delete($download_id)
    {
        try {
            $download = $this->pdlDownloadRepository->delete($download_id);
            Toastr::success("Le Téléchargement à été supprimer","Suppression d'un téléchargement");
            return back();
        } catch (\Exception $exception) {
            Log::error($exception->getMessage(), ["code" => $exception->getCode()]);
            Toastr::error("Erreur de traitement, consulter les logs", "Erreur 500");
            return back();
        }
    }

}
