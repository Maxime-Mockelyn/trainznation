<?php

namespace App\Http\Controllers\Back\Mailbox;

use App\Http\Controllers\Back\BackController;
use App\Library\ImapConnector\src\ImapSource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MailboxController extends BackController
{
    /**
     * @var ImapSource
     */
    private $imapSource;

    /**
     * MailboxController constructor.
     * @param ImapSource $imapSource
     */
    public function __construct(ImapSource $imapSource)
    {
        parent::__construct();
        $this->imapSource = $imapSource;
    }

    public function index(){
        return view("Back.Mailbox.index", [
            "countMail" => $this->imapSource->countMail()
        ]);
    }

    public function loadMail(Request $request)
    {
        //dd($request->all());
        return response()->json($this->imapSource->MailList());
    }

    public function deleteAll()
    {
        $count = $this->imapSource->deleteMails();

        return response()->json(["count" => $count]);
    }
}
