<?php

namespace App\Http\Controllers\Back\Tutoriel\Comment;

use App\Repository\Tutoriel\TutorielCommentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{
    /**
     * @var TutorielCommentRepository
     */
    private $tutorielCommentRepository;

    /**
     * CommentController constructor.
     * @param TutorielCommentRepository $tutorielCommentRepository
     */
    public function __construct(TutorielCommentRepository $tutorielCommentRepository)
    {
        $this->tutorielCommentRepository = $tutorielCommentRepository;
    }

    public function getComment($tutoriel_id, $comment_id)
    {
        $comment = $this->tutorielCommentRepository->get($comment_id);

        return response()->json([
            "title" => "Commentaire postée le <strong>".$comment->published_at->format('d/m/Y à H:i')."</strong> par <strong>".$comment->user->name."</strong>",
            "content"   => $comment->content
        ]);
    }

    public function publish($tutoriel_id, $comment_id)
    {
        $comment = $this->tutorielCommentRepository->get($comment_id);
        $this->tutorielCommentRepository->publish($comment_id);

        return response()->json(["user" => $comment->user->name]);
    }

    public function unpublish($tutoriel_id, $comment_id)
    {
        $comment = $this->tutorielCommentRepository->get($comment_id);
        $this->tutorielCommentRepository->unpublish($comment_id);

        return response()->json(["user" => $comment->user->name]);
    }
}
