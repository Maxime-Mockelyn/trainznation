<?php

namespace App\Http\Controllers\Back\Tutoriel;

use App\Repository\Tutoriel\TutorielExperienceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExperienceController extends Controller
{
    /**
     * @var TutorielExperienceRepository
     */
    private $tutorielExperienceRepository;

    /**
     * ExperienceController constructor.
     * @param TutorielExperienceRepository $tutorielExperienceRepository
     */
    public function __construct(TutorielExperienceRepository $tutorielExperienceRepository)
    {
        $this->tutorielExperienceRepository = $tutorielExperienceRepository;
    }

    public function store(Request $request, $tutoriel_id)
    {
        $exp = $this->tutorielExperienceRepository->store(
            $tutoriel_id,
            $request->experience
        );

        ob_start();
        ?>
        <tr>
            <td><?= $exp->experience; ?></td>
            <td>
                <button id="btnDelete" data-href="<?= route("Tutoriel.Experience.delete", [$tutoriel_id, $exp->id]); ?>" data-id="<?= $exp->id; ?>" class="btn btn-icon btn-icon-sm btn-danger"><i class="la la-trash"></i> </button>
            </td>
        </tr>
        <?php
        $content = ob_get_clean();

        return response()->json(["experience" => $request->experience, "tr" => $content]);
    }

    public function delete($tutoriel_id, $experience_id)
    {
        $this->tutorielExperienceRepository->delete($experience_id);

        return null;
    }
}
