<?php

namespace App\Http\Controllers\Back\Tutoriel\Video;

use App\Jobs\Tutoriel\UploadStepOne;
use App\Jobs\Tutoriel\UploadStepThree;
use App\Jobs\Tutoriel\uploadStepTwo;
use App\Repository\Tutoriel\TutorielRepository;
use App\Repository\Tutoriel\TutorielVideoRepository;
use App\Repository\Tutoriel\TutorielVideoStateRepository;
use Dawson\Youtube\Facades\Youtube;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Kamaln7\Toastr\Facades\Toastr;

class VideoController extends Controller
{
    /**
     * @var TutorielRepository
     */
    private $tutorielRepository;
    /**
     * @var TutorielVideoRepository
     */
    private $tutorielVideoRepository;
    /**
     * @var TutorielVideoStateRepository
     */
    private $tutorielVideoStateRepository;

    /**
     * VideoController constructor.
     * @param TutorielRepository $tutorielRepository
     * @param TutorielVideoRepository $tutorielVideoRepository
     * @param TutorielVideoStateRepository $tutorielVideoStateRepository
     */
    public function __construct(TutorielRepository $tutorielRepository, TutorielVideoRepository $tutorielVideoRepository, TutorielVideoStateRepository $tutorielVideoStateRepository)
    {
        $this->tutorielRepository = $tutorielRepository;
        $this->tutorielVideoRepository = $tutorielVideoRepository;
        $this->tutorielVideoStateRepository = $tutorielVideoStateRepository;
    }

    /**
     * - Vérifier le format de la vidéo (MP4)
     * - @update -table tutoriel_videos
     * - @update -table tutoriel_video_states
     * - @RunJob UploadStepOne
     *
     * @param Request $request
     * @param $tutoriel_id
     * @return string
     * @throws \Exception
     */
    public function upload(Request $request, $tutoriel_id)
    {

        $file = $request->file('youtube');

        //$file->move(public_path().'/uploads/', $tutoriel_id.'.'.$file->getClientOriginalExtension());
        $file->storeAs('/uploads/', $tutoriel_id.'.'.$file->getClientOriginalExtension(), 'public');
        Storage::disk('public')->setVisibility('/uploads/'.$tutoriel_id.'.'.$file->getClientOriginalExtension(), 'public');

        $tutoriel = $this->tutorielRepository->getTutoForId($tutoriel_id);

        if($request->file('youtube')->getMimeType() != 'video/mp4')
        {
            Toastr::error("L'extension de la vidéo est invalide !", "Upload de vidéo");
            return redirect()->back();
        }else{

            $videoBase = $this->tutorielVideoRepository->getFirst($tutoriel_id);

            $this->tutorielVideoStateRepository->upState($videoBase->id, 1);

            $video = Youtube::upload(public_path('storage/uploads/'.$tutoriel_id.'.mp4'), [
                "title"         => $tutoriel->title,
                "description"   => $tutoriel->description,
                "tags"          => []
            ], 'public');

            $video_id = $video->getVideoId();

            $this->tutorielRepository->upLinkVideo($tutoriel_id, $video_id);
            $this->tutorielVideoRepository->updateStateAndVideo($tutoriel_id, $video_id);
            $this->tutorielVideoStateRepository->upState($videoBase->id, 2);

            dispatch(new uploadStepTwo($tutoriel));

            Toastr::success("La Vidéo est en cours d'upload !", "Upload d'une video");
            return redirect()->back();
        }
    }
}
