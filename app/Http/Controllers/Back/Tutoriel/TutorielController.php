<?php

namespace App\Http\Controllers\Back\Tutoriel;

use App\Events\Tutoriel\TutorielVideoTimer;
use App\Http\Controllers\Back\BackController;
use App\Http\Requests\Tutoriel\StoreTutoRequest;
use App\Jobs\Tutoriel\TutorielImageStore;
use App\Jobs\Tutoriel\UploadVideo;
use App\Library\Youtube\Youtube;
use App\Mail\Tutoriel\TutorielPublish;
use App\Notifications\Tutoriel\TutorielPublishDatabaseNotification;
use App\Notifications\Tutoriel\TutorielPublishNotification;
use App\Repository\Tutoriel\TutorielCategorieRepository;
use App\Repository\Tutoriel\TutorielCommentRepository;
use App\Repository\Tutoriel\TutorielExperienceRepository;
use App\Repository\Tutoriel\TutorielRepository;
use App\Repository\Tutoriel\TutorielSourceRepository;
use App\Repository\Tutoriel\TutorielSubCategorieRepository;
use App\Repository\Tutoriel\TutorielTechnoRepository;
use App\Repository\Tutoriel\TutorielVideoRepository;
use App\Repository\Tutoriel\TutorielVideoStateRepository;
use App\Repository\User\UserRepository;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Kamaln7\Toastr\Facades\Toastr;

class TutorielController extends BackController
{
    /**
     * @var TutorielCategorieRepository
     */
    private $tutorielCategorieRepository;
    /**
     * @var TutorielSubCategorieRepository
     */
    private $tutorielSubCategorieRepository;
    /**
     * @var TutorielRepository
     */
    private $tutorielRepository;
    /**
     * @var TutorielExperienceRepository
     */
    private $tutorielExperienceRepository;
    /**
     * @var TutorielTechnoRepository
     */
    private $tutorielTechnoRepository;
    /**
     * @var TutorielCommentRepository
     */
    private $tutorielCommentRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var TutorielSourceRepository
     */
    private $tutorielSourceRepository;
    private $twitterAccessor;


    /**
     * TutorielController constructor.
     * @param TutorielCategorieRepository $tutorielCategorieRepository
     * @param TutorielSubCategorieRepository $tutorielSubCategorieRepository
     * @param TutorielRepository $tutorielRepository
     * @param TutorielExperienceRepository $tutorielExperienceRepository
     * @param TutorielTechnoRepository $tutorielTechnoRepository
     * @param TutorielCommentRepository $tutorielCommentRepository
     * @param TutorielSourceRepository $tutorielSourceRepository
     * @param UserRepository $userRepository
     */
    public function __construct(
        TutorielCategorieRepository $tutorielCategorieRepository,
        TutorielSubCategorieRepository $tutorielSubCategorieRepository,
        TutorielRepository $tutorielRepository,
        TutorielExperienceRepository $tutorielExperienceRepository,
        TutorielTechnoRepository $tutorielTechnoRepository,
        TutorielCommentRepository $tutorielCommentRepository,
        TutorielSourceRepository $tutorielSourceRepository,
        UserRepository $userRepository
    )
    {
        parent::__construct();
        $this->tutorielCategorieRepository = $tutorielCategorieRepository;
        $this->tutorielSubCategorieRepository = $tutorielSubCategorieRepository;
        $this->tutorielRepository = $tutorielRepository;
        $this->tutorielExperienceRepository = $tutorielExperienceRepository;
        $this->tutorielTechnoRepository = $tutorielTechnoRepository;
        $this->tutorielCommentRepository = $tutorielCommentRepository;
        $this->userRepository = $userRepository;
        $this->tutorielSourceRepository = $tutorielSourceRepository;
    }

    public function index()
    {
        return view("Back.Tutoriel.index", [
            "categories"    => $this->tutorielCategorieRepository->list(),
            "subs"          => $this->tutorielSubCategorieRepository->list(),
            "technos"       => $this->tutorielTechnoRepository->list(),
            "tutoriels"     => $this->tutorielRepository->list()
        ]);
    }

    public function create()
    {
        return view("Back.Tutoriel.create", [
            "categories"    => $this->tutorielCategorieRepository->list(),
            "users"         => $this->userRepository->getAllAdministrator()
        ]);
    }

    public function store(Request $request)
    {
        if ($request->state) {$published = 1;}else{$published = 0;}
        if ($request->sources) {$source = 1;}else{$source = 0;}
        if ($request->premium) {$premium = 1;}else{$premium = 0;}


        try {
            $tutoriel = $this->tutorielRepository->create(
                $request->category_id,
                $request->subcategorie_id,
                $request->uuid,
                $request->title,
                $request->user_id,
                $request->slug,
                $request->short_content,
                $published,
                $request->published_at,
                $source,
                $premium
            );

            Toastr::success("Le Tutoriel à été créer", "Création d'un tutoriel");
            return redirect()->back();
        }catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function show($slug)
    {
        //dd($this->tutorielRepository->getTuto($slug));
        return view("Back.Tutoriel.show", [
            "tutoriel"  => $this->tutorielRepository->getTuto($slug)
        ]);
    }

    public function edit($slug)
    {
        //$tuto = $this->tutorielRepository->getTuto($slug);
        return view("Back.Tutoriel.edit", [
            "tutoriel"  => $this->tutorielRepository->getTuto($slug),
            "categories"=> $this->tutorielCategorieRepository->list(),
            "users"     => $this->userRepository->getAllAdministrator()
        ]);
    }

    /**
     * Met à jour les informations générales
     * @param Request $request
     * @param string $slug
     * @return RedirectResponse
     */
    public function update(Request $request, $slug)
    {
        //dd($request->all(), $slug);

        if ($request->state) {$published = 1;}else{$published = 0;}
        if ($request->sources) {$source = 1;}else{$source = 0;}
        if ($request->premium) {$premium = 1;}else{$premium = 0;}
        if ($request->slug != $slug){$newSlug = $request->slug;}else{$newSlug = $slug;}

        $query = $this->tutorielRepository->update(
                $newSlug,
                $request->title,
                $request->category_id,
                $request->subcategory_id,
                $request->user_id,
                $published,
                $request->published_at,
                $source,
                $premium,
                $request->short_content
        );

        if ($query)
        {
            Toastr::success("Le Tutoriel à été mis à jour", "Mise à jour d'un tutoriel");
            return redirect()->back();
        }else{
            Toastr::error("Erreur serveur", "ERROR 500");
            return redirect()->back();
        }
    }

    /**
     * Execute plusieurs points
     * - Sauvegarde les images 'a la Une" et 'Cover'
     * - Si Youtube_id définie:
     *      -
     * @param Request $request
     * @param string $slug
     * @return RedirectResponse
     */
    public function updateContent(Request $request, $slug)
    {

        $tutoriel = $this->tutorielRepository->getTuto($slug);
        $youtube = new Youtube();

        if($request->file('images')){
            $request->file('images')->storeAs('learning/', $tutoriel->id.'.png', 'public');
        }
        if($request->file('cover')){
            $request->file('cover')->storeAs('learning/', $tutoriel->id.'_cover.png', 'public');
        }

        if($request->youtube_id && $request->youtube_id != $tutoriel->youtube_id){
            $video = $this->tutorielRepository->upYoutubeId($tutoriel->id, $request->youtube_id);
            if($video >= 1){
                $upTime = $this->tutorielRepository->upTime($tutoriel->id, $youtube->getTimeVideo($request->youtube_id));
                if($upTime >= 1){
                    Toastr::info("Le Timer de la vidéo à été mis à jour", "Timer Video");
                }else{
                    Toastr::warning("Le Timer de la vidéo n'à pas été mis à jour", "Timer Video");
                }
            }else{
                Toastr::warning("L'ID de la vidéo Youtube n'à pas été mis à jour", "YOUTUBE ID");
            }
        }

        if($request->sourceFile){
            if(env("APP_ENV") == 'local'){
                $pathSource = '/home/vagrant/code/download.trainznation.io/'.$request->sourceFile;
            }else{
                $pathSource = '/var/www/download.trainznation.eu/'.$request->sourceFile;
            }
            if(file_exists($pathSource)){
                $source = $this->tutorielSourceRepository->update($tutoriel->id, $request->sourceFile);
                if($source){
                    Toastr::success("La Source ".$request->sourceFile." à été mis à jour", "Définition des sources du tutoriel");
                }else{
                    Toastr::error("Erreur lors de la mise à jour de la source", "Définition des sources du tutoriel");
                }
            }else{
                $source = $this->tutorielSourceRepository->store($tutoriel->id, $request->sourceFile);
                if($source){
                    Toastr::success("La Source ".$request->sourceFile." à été publier", "Définition des sources du tutoriel");
                }else{
                    Toastr::error("Erreur lors de la publication de la source", "Définition des sources du tutoriel");
                }
            }
        }

        return redirect()->back();

    }

    public function updateContenu(Request $request, $slug)
    {
        //dd($request->all(), $slug);

        $this->tutorielRepository->upContenu($slug, $request->description);

        Toastr::success("Le contenu du tutoriel à été mis à jour", "Mise à jour de contenue");
        return redirect()->back();
    }

    public function delete($slug)
    {
        $tutoriel = $this->tutorielRepository->getTuto($slug);

        $this->tutorielRepository->delete($slug);

        Storage::disk("s3")->delete('learning/'.$tutoriel->id.'.png');
        Storage::disk("s3")->delete('learning/'.$tutoriel->id.'_cover.png');

        return null;
    }

    public function listSubcategorie($category_id)
    {
        $subs = $this->tutorielSubCategorieRepository->getForCategory($category_id);
        ob_start();
        ?>
        <div class="form-group">
            <label>Sous Catégorie</label>
            <select id="subcategorie_id" class="form-control kt-selectpicker" name="subcategorie_id" data-live-search="true">
                <?php foreach ($subs as $sub): ?>
                    <option value="<?= $sub->id ?>"><?= $sub->name ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <script type="text/javascript">
            (function ($) {
                $(".kt-selectpicker").selectpicker();
            })(jQuery)
        </script>
        <?php
        $content = ob_get_clean();

        return response()->json(["content" => $content]);
    }

    public function publish($tutoriel_id)
    {
        $tutoriel = $this->tutorielRepository->getTutoForId($tutoriel_id);
        $users = $this->userRepository->getAll();

        foreach ($users as $user)
        {
            if($tutoriel->twitter == 1)
            {
                //$user->notify(new TutorielPublishNotification($tutoriel));
                $this->twitterAccessor->postTweetWithImage($tutoriel->twitterText, 'tutoriel', $tutoriel->id);
                $user->notify(new TutorielPublishDatabaseNotification($tutoriel));

                $this->tutorielRepository->publish($tutoriel->id);
                return response()->json(["twitter" => true]);
            }else{
                $user->notify(new TutorielPublishDatabaseNotification($tutoriel));

                $this->tutorielRepository->publish($tutoriel->id);
                return response()->json(["twitter" => false]);
            }
        }
    }

    public function getPublish($tutoriel_id)
    {
        $tutoriel = $this->tutorielRepository->getTutoForId($tutoriel_id);
        $users = $this->userRepository->getAll();

        foreach ($users as $user)
        {
            if($tutoriel->twitter == 1)
            {
                //$user->notify(new TutorielPublishNotification($tutoriel));
                $this->twitterAccessor->postTweetWithImage($tutoriel->twitterText, 'tutoriel', $tutoriel->id);
                $user->notify(new TutorielPublishDatabaseNotification($tutoriel));

                $this->tutorielRepository->publish($tutoriel->id);
                Toastr::success("La Video à été publier sur Twitter", "Publication d'un tutoriel");
            }else{
                $user->notify(new TutorielPublishDatabaseNotification($tutoriel));

                $this->tutorielRepository->publish($tutoriel->id);
                return response()->json(["twitter" => false]);
            }

            Toastr::success("Le Tutoriel à été publier", "Publication d'un tutoriel");
            return redirect()->back();
        }
    }

    public function unpublish($tutoriel_id)
    {
        $tutoriel = $this->tutorielRepository->getTutoForId($tutoriel_id);

        $this->tutorielRepository->unpublish($tutoriel_id);

        return null;
    }

    public function getUnpublish($tutoriel_id)
    {
        $tutoriel = $this->tutorielRepository->getTutoForId($tutoriel_id);

        $this->tutorielRepository->unpublish($tutoriel_id);

        Toastr::success("Le tutoriel à été dépublier", "Publication d'un tutoriel");
    }

    private function getTimerVideo($video_id)
    {
        $youtube = new Youtube();

        $video = $youtube->videoInfo($video_id);

        return $video->duration;
    }

    public function getDelete($slug)
    {
        $this->tutorielRepository->delete($slug);

        Toastr::success("Le Tutoriel à été supprimer", "Suppression du tutoriel");

        return redirect()->route('Back.Tutoriel.index');
    }

    public function getContenue($tutoriel_id)
    {
        $tutoriel = $this->tutorielRepository->getTutoForId($tutoriel_id);
        return response()->json([$tutoriel->content]);
    }
}
