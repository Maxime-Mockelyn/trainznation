<?php

namespace App\Http\Controllers\Back\Tutoriel\Category;

use App\Http\Controllers\Back\BackController;
use App\Http\Requests\Tutoriel\Category\storeCategory;
use App\Repository\Tutoriel\TutorielCategorieRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * @var TutorielCategorieRepository
     */
    private $tutorielCategorieRepository;

    /**
     * CategoryController constructor.
     * @param TutorielCategorieRepository $tutorielCategorieRepository
     */
    public function __construct(TutorielCategorieRepository $tutorielCategorieRepository)
    {
        $this->tutorielCategorieRepository = $tutorielCategorieRepository;
    }

    public function index()
    {
        return view("Back.Tutoriel.Category.index", [
            "categories"    => $this->tutorielCategorieRepository->list()
        ]);
    }

    public function store(storeCategory $request)
    {
        $category = $this->tutorielCategorieRepository->create($request->get('name'));

        ob_start();
        ?>
        <tr>
            <td><?= $category->id; ?></td>
            <td><?= $category->name; ?></td>
            <td>
                <a href="<?= route('Tutoriel.Category.edit', $category->id) ?>" class="btn btn-sm btn-brand btn-elevate btn-icon"><i class="la la-edit"></i></a>
                <button id="btnDelete" type="button" class="btn btn-sm btn-danger btn-elevate btn-icon" data-href="<?= route('Tutoriel.Category.delete', $category->id) ?>"><i class="la la-trash"></i></button>
            </td>
        </tr>
        <?php
        $content = ob_get_clean();

        return response()->json([
            "name"  => $category->name,
            "tr"    => $content
        ]);
    }

    public function delete($category_id)
    {
        $this->tutorielCategorieRepository->delete($category_id);
        return null;
    }
}
