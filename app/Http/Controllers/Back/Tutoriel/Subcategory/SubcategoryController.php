<?php

namespace App\Http\Controllers\Back\Tutoriel\Subcategory;

use App\Http\Controllers\Back\BackController;
use App\Http\Requests\Tutoriel\Subcategory\storeSubcategory;
use App\Repository\Tutoriel\TutorielCategorieRepository;
use App\Repository\Tutoriel\TutorielSubCategorieRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubcategoryController extends BackController
{
    /**
     * @var TutorielSubCategorieRepository
     */
    private $tutorielSubCategorieRepository;
    /**
     * @var TutorielCategorieRepository
     */
    private $tutorielCategorieRepository;

    /**
     * SubcategoryController constructor.
     * @param TutorielSubCategorieRepository $tutorielSubCategorieRepository
     * @param TutorielCategorieRepository $tutorielCategorieRepository
     */
    public function __construct(TutorielSubCategorieRepository $tutorielSubCategorieRepository, TutorielCategorieRepository $tutorielCategorieRepository)
    {
        parent::__construct();
        $this->tutorielSubCategorieRepository = $tutorielSubCategorieRepository;
        $this->tutorielCategorieRepository = $tutorielCategorieRepository;
    }

    public function index()
    {
        return view("Back.Tutoriel.Subcategory.index", [
            "categories" => $this->tutorielCategorieRepository->list(),
            "subs"  => $this->tutorielSubCategorieRepository->list()
        ]);
    }

    public function store(storeSubcategory $request)
    {
        $subcategory = $this->tutorielSubCategorieRepository->create(
            $request->categorie_id,
            $request->name
        );

        ob_start();
        ?>
        <tr>
            <td><?= $subcategory->id ?></td>
            <td><?= $subcategory->name ?></td>
            <td><?= $subcategory->category->name ?></td>
            <td>
                <a href="<?= route('Tutoriel.Subcategory.edit', $subcategory->id) ?>" class="btn btn-sm btn-brand btn-elevate btn-icon"><i class="la la-edit"></i></a>
                <button id="btnDelete" type="button" class="btn btn-sm btn-danger btn-elevate btn-icon" data-href="<?= route('Tutoriel.Subcategory.delete', $subcategory->id) ?>"><i class="la la-trash"></i></button>
            </td>
        </tr>
        <?php

        $content = ob_get_clean();

        return response()->json([
            "name"  => $subcategory->name,
            "tr"    => $content
        ]);
    }

    public function delete($subcategory_id)
    {
        $this->tutorielSubCategorieRepository->delete($subcategory_id);
        return null;
    }
}
