<?php

namespace App\Http\Controllers\Back\Tutoriel;

use App\Repository\Tutoriel\TutorielTechnoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TechnologieController extends Controller
{
    /**
     * @var TutorielTechnoRepository
     */
    private $tutorielTechnoRepository;

    /**
     * TechnologieController constructor.
     * @param TutorielTechnoRepository $tutorielTechnoRepository
     */
    public function __construct(TutorielTechnoRepository $tutorielTechnoRepository)
    {
        $this->tutorielTechnoRepository = $tutorielTechnoRepository;
    }

    public function store(Request $request, $tutoriel_id)
    {
        $techno = $this->tutorielTechnoRepository->store(
            $tutoriel_id,
            $request->technologie
        );

        ob_start();
        ?>
        <tr>
            <td><?= $techno->techno; ?></td>
            <td>
                <button id="btnDelete" data-href="<?= route("Tutoriel.Technologie.delete", [$tutoriel_id, $techno->id]); ?>" data-id="<?= $techno->id; ?>" class="btn btn-icon btn-icon-sm btn-danger"><i class="la la-trash"></i> </button>
            </td>
        </tr>
        <?php
        $content = ob_get_clean();

        return response()->json(["technologie" => $request->techologie, "tr" => $content]);
    }

    public function delete($tutoriel_id, $techno_id)
    {
        $this->tutorielTechnoRepository->delete($techno_id);

        return null;
    }
}
