<?php

namespace App\Http\Controllers\Back\Download;

use App\Http\Controllers\Back\BackController;
use App\Repository\Asset\AssetRepository;
use App\Repository\Asset\AssetVersionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VersionController extends BackController
{
    /**
     * @var AssetVersionRepository
     */
    private $assetVersionRepository;
    /**
     * @var AssetRepository
     */
    private $assetRepository;

    /**
     * VersionController constructor.
     * @param AssetVersionRepository $assetVersionRepository
     * @param AssetRepository $assetRepository
     */
    public function __construct(AssetVersionRepository $assetVersionRepository, AssetRepository $assetRepository)
    {
        parent::__construct();
        $this->assetVersionRepository = $assetVersionRepository;
        $this->assetRepository = $assetRepository;
    }

    public function getAll($asset_id)
    {
        return response()->json($this->assetVersionRepository->getAllFromAsset($asset_id));
    }

    public function getInfo($asset_id, $version_id)
    {
        $version = $this->assetVersionRepository->get($version_id);

        return response()->json([
            "title" => "Information de la version <strong>".$version['version_text']."</strong>",
            "content" => $version['version_log']
        ]);
    }

    public function create($asset_id)
    {
        return view("Back.Download.Version.create", [
            "asset" => $this->assetRepository->get($asset_id)
        ]);
    }

    public function store(Request $request, $asset_id)
    {
        $version = $this->assetVersionRepository->store(
            $asset_id,
            $request->version_text,
            $request->version_log,
            $request->version_link
        );

        $this->assetRepository->updateLink($asset_id, $request->version_link);

        return response()->json(["asset_id" => $asset_id, "version" => $version->version_text]);
    }
}
