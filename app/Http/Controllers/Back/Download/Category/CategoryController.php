<?php

namespace App\Http\Controllers\Back\Download\Category;

use App\Http\Controllers\Back\BackController;
use App\Http\Requests\Download\Category\AddCategoryRequest;
use App\Repository\Asset\AssetCategorieRepository;
use App\Repository\Asset\AssetSubCategorieRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends BackController
{
    /**
     * @var AssetCategorieRepository
     */
    private $assetCategorieRepository;
    /**
     * @var AssetSubCategorieRepository
     */
    private $assetSubCategorieRepository;

    /**
     * CategoryController constructor.
     * @param AssetCategorieRepository $assetCategorieRepository
     * @param AssetSubCategorieRepository $assetSubCategorieRepository
     */
    public function __construct(AssetCategorieRepository $assetCategorieRepository, AssetSubCategorieRepository $assetSubCategorieRepository)
    {
        parent::__construct();
        $this->assetCategorieRepository = $assetCategorieRepository;
        $this->assetSubCategorieRepository = $assetSubCategorieRepository;
    }

    public function index()
    {
        return view("Back.Download.Category.index", [
            "categories"    => $this->assetCategorieRepository->getAll(),
            "subs"          => $this->assetSubCategorieRepository->getAll()
        ]);
    }

    public function store(AddCategoryRequest $request)
    {
        $category = $this->assetCategorieRepository->create($request->get('name'));

        ob_start();
        ?>
        <tr>
            <td><?= $category->id; ?></td>
            <td><?= $category->name; ?></td>
            <td>
                <a href="<?= route('Download.Category.edit', $category->id) ?>" class="btn btn-sm btn-brand btn-elevate btn-icon"><i class="la la-edit"></i></a>
                <button id="btnDelete" type="button" class="btn btn-sm btn-danger btn-elevate btn-icon" data-href="<?= route('Download.Category.delete', $category->id) ?>"><i class="la la-trash"></i></button>
            </td>
        </tr>
        <?php
        $content = ob_get_clean();

        return response()->json([
            "name"  => $category->name,
            "tr"    => $content
        ]);
    }


    public function delete($category_id)
    {
        $this->assetCategorieRepository->delete($category_id);
        return null;
    }

}
