<?php

namespace App\Http\Controllers\Back\Download;

use App\Http\Controllers\Back\BackController;
use App\Repository\Asset\AssetCompatibilityRepository;
use App\Repository\Asset\AssetRepository;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class CompatibilityController extends BackController
{
    /**
     * @var AssetCompatibilityRepository
     */
    private $assetCompatibilityRepository;
    /**
     * @var AssetRepository
     */
    private $assetRepository;

    /**
     * CompatibilityController constructor.
     * @param AssetCompatibilityRepository $assetCompatibilityRepository
     * @param AssetRepository $assetRepository
     */
    public function __construct(AssetCompatibilityRepository $assetCompatibilityRepository, AssetRepository $assetRepository)
    {
        parent::__construct();
        $this->assetCompatibilityRepository = $assetCompatibilityRepository;
        $this->assetRepository = $assetRepository;
    }

    public function create($asset_id)
    {
        return view("Back.Download.Compatibility.create", [
            "asset" => $this->assetRepository->get($asset_id)
        ]);
    }

    public function store(Request $request, $asset_id)
    {
        $comp = $this->assetCompatibilityRepository->store(
            $asset_id,
            $request->trainz_build,
            $request->state
        );

        if($comp)
        {
            return response()->json([
                "data"  => [
                    "trainz_build"  => $request->trainz_build,
                    "asset_id"      => $asset_id
                ],
                "state" => "success"
            ]);
        }else{
            return response()->json([
                "state"     => "error",
                "message"   => "Asset Compatibility: Erreur de création"
            ]);
        }
    }

    public function edit($asset_id, $compatibility_id)
    {
        return view("Back.Download.Compatibility.edit", [
            "asset" => $this->assetRepository->get($asset_id),
            "compatibility" => $this->assetCompatibilityRepository->get($compatibility_id)
        ]);
    }

    public function update(Request $request, $asset_id, $compatibility_id)
    {
        $compatibility = $this->assetCompatibilityRepository->get($compatibility_id);
        $comp = $this->assetCompatibilityRepository->update(
            $asset_id,
            $compatibility_id,
            $request->trainz_build,
            $request->state
        );

        if($comp)
        {
            return response()->json([
                "data"  => [
                    "trainz_build"  => $request->trainz_build,
                    "asset_id"      => $asset_id
                ],
                "state" => "success"
            ]);
        }else{
            return response()->json([
                "state"     => "error",
                "message"   => "Asset Compatibility: Erreur de création"
            ]);
        }
    }

    public function delete($asset_id, $compatibility_id)
    {
        $this->assetCompatibilityRepository->delete($compatibility_id);

        return null;
    }
}
