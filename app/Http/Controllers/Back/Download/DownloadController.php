<?php

namespace App\Http\Controllers\Back\Download;

use App\HelperClass\Download;
use App\Http\Controllers\Back\BackController;
use App\Http\Requests\Download\AddAssetRequest;
use App\Jobs\Asset\AssetImageStore;
use App\Library\Twitter\TwitterAccessor;
use App\Library\ZipExtractor\ZipExtractor;
use App\Notifications\Download\AssetPublished;
use App\Notifications\Download\AssetPublishedDatabase;
use App\Repository\Asset\AssetCategorieRepository;
use App\Repository\Asset\AssetCompatibilityRepository;
use App\Repository\Asset\AssetRepository;
use App\Repository\Asset\AssetSubCategorieRepository;
use App\Repository\User\UserRepository;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Kamaln7\Toastr\Facades\Toastr;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use ZipArchive;

class DownloadController extends BackController
{
    /**
     * @var AssetCategorieRepository
     */
    private $assetCategorieRepository;
    /**
     * @var AssetSubCategorieRepository
     */
    private $assetSubCategorieRepository;
    /**
     * @var AssetRepository
     */
    private $assetRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var AssetCompatibilityRepository
     */
    private $assetCompatibilityRepository;
    /**
     * @var Download
     */
    private $download;
    /**
     * @var TwitterAccessor
     */
    private $twitterAccessor;

    /**
     * DownloadController constructor.
     * @param AssetCategorieRepository $assetCategorieRepository
     * @param AssetSubCategorieRepository $assetSubCategorieRepository
     * @param AssetRepository $assetRepository
     * @param UserRepository $userRepository
     * @param AssetCompatibilityRepository $assetCompatibilityRepository
     * @param Download $download
     * @param TwitterAccessor $twitterAccessor
     */
    public function __construct(
        AssetCategorieRepository $assetCategorieRepository,
        AssetSubCategorieRepository $assetSubCategorieRepository,
        AssetRepository $assetRepository,
        UserRepository $userRepository,
        AssetCompatibilityRepository $assetCompatibilityRepository,
        Download $download, TwitterAccessor $twitterAccessor)
    {
        parent::__construct();
        $this->assetCategorieRepository = $assetCategorieRepository;
        $this->assetSubCategorieRepository = $assetSubCategorieRepository;
        $this->assetRepository = $assetRepository;
        $this->userRepository = $userRepository;
        $this->assetCompatibilityRepository = $assetCompatibilityRepository;
        $this->download = $download;
        $this->twitterAccessor = $twitterAccessor;
    }

    public function index()
    {
        return view('Back.Download.index', [
            "assets"       => $this->assetRepository->getAll(10)
        ]);
    }

    public function create()
    {
        return view("Back.Download.create", [
            "categories"    => $this->assetCategorieRepository->getAll()
        ]);
    }

    public function store(Request $request)
    {

        //dd($request->all());

        if($request->published == 1){$published_at = now();}elseif($request->published == 2){$published_at = $request->published_at;}else{$published_at = null;}
        if($request->twitter == 1){$twitterText = $request->twitterText;}else{$twitterText = null;}


        try {
            $asset = $this->assetRepository->create(
                    $request->category_id,
                    $request->subcategory_id,
                    $request->designation,
                    $request->downloadLink,
                    $request->kuid,
                    $request->twitter,
                    $twitterText,
                    $request->published,
                    $published_at,
                    $request->tree,
                    $request->config,
                    $request->short_description
            );

            //Traitement du fichier 3d si il existe
            if($asset->tree == 1){
                try {
                    $request->file('modeleFile')->storeAs('modele/'.Str::slug($asset->categorie->name).'/'.Str::slug($asset->subcategorie->name).'/'.$asset->id, $asset->id.'.fbx', 'public');
                    Storage::disk('public')->setVisibility('modele/'.Str::slug($asset->categorie->name).'/'.Str::slug($asset->subcategorie->name).'/'.$asset->id.'/'.$asset->id.'.fbx', 'public');
                } catch (FileException $exception){
                    Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
                    Toastr::error("Impossible de transferer le modele 3D, consulter les logs", "Erreur Serveur");
                }
            }

            // Traitement du fichier config si il existe
            if($asset->config == 1) {
                try {
                    $request->file('configFile')->storeAs('config/'.Str::slug($asset->categorie->name).'/'.Str::slug($asset->subcategorie->name), pregReplaceKuid($asset->kuid).'.txt', 'public');
                    Storage::disk('public')->setVisibility('config/'.Str::slug($asset->categorie->name).'/'.Str::slug($asset->subcategorie->name).'/'.pregReplaceKuid($asset->kuid).'.txt', 'public');
                    try {
                        $this->createCompatibility($asset);
                    } catch (\Exception $exception) {
                        Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
                        Toastr::error("Impossible de créer la liste des compatibilités de l'objet, consulter les logs", "Erreur Serveur");
                    }
                }catch (FileException $exception) {
                    Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
                    Toastr::error("Impossible de transferer le fichier config, consulter les logs", "Erreur Serveur");
                }
            }

            Toastr::success("L'objet à été ajouter avec succès", "Nouvelle Objet");
            return \redirect()->route('Back.Download.index');

        } catch (\Exception $exception){
            Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
            Toastr::error("Impossible d'ajouter l'objet à la base, consulter les logs", "Erreur Serveur");
            return back();
        }
    }

    public function delete($download_id)
    {
        $asset = $this->assetRepository->get($download_id);

        try {
            $this->assetRepository->delete($download_id);

            // Suppression de l'image
            try {
                $image = $this->download->deleteImageFile($asset);
                Toastr::success($image["responseText"], "Suppression de l'image");
            } catch (FileException $exception) {
                Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
                Toastr::error($exception->getMessage(), "Erreur Server");
            }

            // suppression du fichier de configuration
            try {
                $config = $this->download->deleteConfigFile($asset);
                Toastr::success($config["responseText"], "Suppression du fichier de configuration");
            } catch (FileException $exception) {
                Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
                Toastr::error($exception->getMessage(), "Erreur Server");
            }

            // suppression du fichier de configuration
            try {
                $modele = $this->download->deleteModeleFile($asset);
                Toastr::success($modele["responseText"], "Suppression du fichier 3D");
            } catch (FileException $exception) {
                Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
                Toastr::error($exception->getMessage(), "Erreur Server");
            }

            Toastr::success("L'objet <strong></strong> à été supprimer", "Suppression d'un objet");
            return redirect()->route("Back.Download.index");

        } catch (\Exception $exception) {
            Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
            Toastr::error($exception->getMessage(), "Erreur Server");
            return back();
        }

    }


    public function listSubcategorie($categorie_id)
    {
        $subs = $this->assetSubCategorieRepository->getForCategorie($categorie_id);
        ob_start();
        ?>
        <div class="form-group">
            <label>Sous-catégorie</label>
            <select id="subcategory_id" class="form-control kt-select2" name="subcategory_id">
                <option value=""></option>
                <?php foreach ($subs as $sub): ?>
                    <option value="<?= $sub->id; ?>"><?= $sub->name; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <script type="text/javascript">
            (function ($) {
                $("#subcategory_id").select2({
                    placeholder: "Selectionnez une sous-catégorie..."
                })
            })(jQuery)
        </script>
        <?php
        $content = ob_get_clean();

        return response()->json(["content" => $content]);
    }

    public function publish($download_id)
    {

        try {
            $asset = $this->assetRepository->get($download_id);
            $users = $this->userRepository->getAll();
            $this->assetRepository->publish($download_id);

            foreach ($users as $user)
            {
                if($asset->twitter == 1)
                {
                    $user->notify(new AssetPublishedDatabase($asset));
                    //$user->notify(new AssetPublished($asset));
                    $this->twitterAccessor->postTweetWithImage($asset->twitterText, 'download', $asset->id);
                    Toastr::success("Le téléchargement à été publier sur Twitter", "Posting Twitter");
                    Toastr::success("Le téléchargement à été publier & notifier à tous les utilisateur", "Publication d'un objet");
                    return back();
                }else{
                    $user->notify(new AssetPublishedDatabase($asset));
                    Toastr::success("Le téléchargement à été publier & notifier à tous les utilisateur", "Publication d'un objet");
                    return back();
                }
            }
            return null;
        } catch (\Exception $exception) {
            Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
            Toastr::error($exception->getMessage(), "Erreur Server");
            return back();
        }

    }


    public function unPublish($download_id)
    {
        try {
            $asset = $this->assetRepository->get($download_id);
            $this->assetRepository->unpublish($download_id);
            Toastr::success("L'objet <strong>".$asset->designation."</strong> à été dépublier", "Dépublication d'un objet");
            return back();

        } catch (\Exception $exception) {
            Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
            Toastr::error($exception->getMessage(), "Erreur Server");
            return back();
        }
    }

    public function redirect($download_id)
    {
        $download = $this->assetRepository->get($download_id);

        return Redirect::to($download->downloadLink);
    }

    public function show($asset_id)
    {
        $asset = $this->assetRepository->get($asset_id);
        return view('Back.Download.show', [
            "asset" => $this->assetRepository->get($asset_id)
        ]);
    }

    public function edit($asset_id)
    {
        return view('Back.Download.edit', [
            "asset" => $this->assetRepository->get($asset_id),
            "categories" => $this->assetCategorieRepository->getAll()
        ]);
    }

    public function update(Request $request, $asset_id)
    {
        //dd($request->all());

        if($request->published == 1){$published_at = now();}elseif($request->published == 2){$published_at = $request->published_at;}else{$published_at = null;}
        if($request->twitter == 1){$twitterText = $request->twitterText;}else{$twitterText = null;}

        try {
            $asset = $this->assetRepository->update(
                $asset_id,
                $request->category_id,
                $request->subcategory_id,
                $request->designation,
                $request->downloadLink,
                $request->kuid,
                $request->twitter,
                $twitterText,
                $request->published,
                $published_at,
                $request->tree,
                $request->config,
                $request->short_description
            );

            //Traitement du fichier 3d si il existe
            if($asset->tree == 1){
                if($request->file('modeleFile')){
                    try {
                        $request->file('modeleFile')->storeAs('modele/'.Str::slug($asset->categorie->name).'/'.Str::slug($asset->subcategorie->name).'/'.$asset->id, $asset->id.'.fbx', 'public');
                        Storage::disk('public')->setVisibility('modele/'.Str::slug($asset->categorie->name).'/'.Str::slug($asset->subcategorie->name).'/'.$asset->id.'/'.$asset->id.'.fbx', 'public');
                    } catch (FileException $exception){
                        Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
                        Toastr::error("Impossible de transferer le modele 3D, consulter les logs", "Erreur Serveur");
                    }
                }
            }

            // Traitement du fichier config si il existe
            if($asset->config == 1) {
                if($request->file('configFile')) {
                    try {
                        $request->file('configFile')->storeAs('config/'.Str::slug($asset->categorie->name).'/'.Str::slug($asset->subcategorie->name), pregReplaceKuid($asset->kuid).'.txt', 'public');
                        Storage::disk('public')->setVisibility('config/'.Str::slug($asset->categorie->name).'/'.Str::slug($asset->subcategorie->name).'/'.pregReplaceKuid($asset->kuid).'.txt', 'public');
                        try {
                            $this->createCompatibility($asset);
                        } catch (\Exception $exception) {
                            Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
                            Toastr::error("Impossible de créer la liste des compatibilités de l'objet, consulter les logs", "Erreur Serveur");
                        }
                    }catch (FileException $exception) {
                        Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
                        Toastr::error("Impossible de transferer le fichier config, consulter les logs", "Erreur Serveur");
                    }
                }
            }

            Toastr::success("L'objet à été mis à jour avec succès", "Mise à jour d'un Objet");
            return \redirect()->route('Back.Download.index');
        } catch (\Exception $exception) {
            Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
            Toastr::error("Impossible de mettre à jours l'objet à la base, consulter les logs", "Erreur Serveur");
            return back();
        }
    }

    public function updateContent(Request $request, $asset_id)
    {
        //dd($request->all());

        if($request->file('images')){
            try {
                $request->file('images')->storeAs('download/', $asset_id.'.png', 'public');
                Storage::disk('public')->setVisibility('download/'.$asset_id.'.png', 'public');
                $this->assetRepository->updateImage($asset_id);
                Toastr::success("L'images à été ajouté", "Mise à jour d'un Objet");
            } catch (FileException $exception) {
                Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
                Toastr::error("Impossible de transferer l'image de l'objet, consulter les logs", "Erreur Serveur");
            }
        }

        try {
            $asset = $this->assetRepository->updateContent($asset_id, $request->percent);
            Toastr::success("L'objet <strong>".$asset->designation."</strong> à été mis à jour", "Mise à jour d'un objet");
            return \redirect()->route('Back.Download.show', $asset_id);
        } catch (\Exception $exception) {
            Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
            Toastr::error("Impossible de mettre à jour l'objet, consulter les logs", "Erreur Serveur");
            return back();
        }

    }

    public function updateContenu(Request $request, $asset_id)
    {
        //dd($request->all());

        try {
            $short_description = Str::limit($request->description, 150);
            $asset = $this->assetRepository->updateContenu($asset_id, $request->description, $short_description);

            Toastr::success("La description de l'objet à été mis à jour", "Mise à jour d'un objet");
            return \redirect()->route("Back.Download.show", $asset_id);
        } catch (\Exception $exception) {
            Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
            Toastr::error("Impossible de mettre à jour l'objet, consulter les logs", "Erreur Serveur");
            return back();
        }
    }

    public function updatePrice(Request $request, $asset_id)
    {
        try {
            $asset = $this->assetRepository->updatePrice($asset_id, $request->pricing, $request->price);

            Toastr::success("La description de l'objet à été mis à jour", "Mise à jour d'un objet");
            return \redirect()->route("Back.Download.show", $asset_id);

        }catch (\Exception $exception)
        {
            Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
            Toastr::error("Impossible de mettre à jour l'objet, consulter les logs", "Erreur Serveur");
            return back();
        }
    }

    public function search($terme)
    {
        if(empty($terme))
        {
            $assets = $this->assetRepository->getSearchNull();
        }else{
            $assets = $this->assetRepository->getSearch($terme);
        }

        ob_start();
        foreach ($assets as $asset):
        ?>
            <div class="kt-portlet" id="portlet">
                <div id="asset_id" data-id="<?= $asset->id ?>"></div>
                <div class="kt-portlet__body">
                    <div class="kt-widget kt-widget--user-profile-3">
                        <div class="kt-widget__top">
                            <div class="kt-widget__media kt-hidden-">
                                <img src="<?= sourceImage('download/'.$asset->id.'.png') ?>" alt="image">
                            </div>
                            <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden">
                                JM
                            </div>
                            <div class="kt-widget__content">
                                <div class="kt-widget__head">
                                    <a href="#" class="kt-widget__username">
                                        <?= $asset->designation ?>
                                        <?= stateDownload($asset->published) ?>
                                    </a>
                                    <div class="kt-widget__action">
                                        <a href="<?= route('Back.Download.show', $asset->id) ?>" class="btn btn-label-info btn-sm btn-upper"><i class="la la-eye"></i> Voir la fiche</a>&nbsp;
                                        <a href="<?= route('Back.Download.edit', $asset->id) ?>" class="btn btn-label-success btn-sm btn-upper"><i class="la la-edit"></i> Editer</a>&nbsp;
                                        <a href="<?= route('Back.Download.delete', $asset->id) ?>" class="btn btn-label-danger btn-sm btn-upper"><i class="la la-times-circle"></i> Supprimer</a>&nbsp;
                                        <?php if($asset->published == 0 || $asset->published == 2): ?>
                                        <a href="<?php route('Back.Download.publish', $asset->id) ?>" class="btn btn-label-success btn-sm btn-upper"><i class="la la-unlock"></i> Publier</a>&nbsp;
                                        <?php else: ?>
                                        <a href="<?php route('Back.Download.unpublish', $asset->id) ?>" class="btn btn-label-danger btn-sm btn-upper"><i class="la la-lock"></i> Dépublier</a>&nbsp;
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="kt-widget__subhead">
                                    <a href="<?= route('Download.redirect', $asset->id) ?>"><i class="flaticon2-download"></i> <?= route('Download.redirect', $asset->id) ?></a>

                                </div>
                                <div class="kt-widget__info">
                                    <div class="kt-widget__desc">
                                        <?= $asset->short_description ?>
                                    </div>
                                    <!--<div class="kt-widget__progress">
                                        <div class="kt-widget__text">
                                            Progress
                                        </div>
                                        <div class="progress" style="height: 5px;width: 100%;">
                                            <div class="progress-bar kt-bg-success" role="progressbar" style="width: 65%;" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <div class="kt-widget__stats">
                                            78%
                                        </div>
                                    </div>-->
                                </div>
                            </div>
                        </div>
                        <div class="kt-widget__bottom">
                            <div class="kt-widget__item">
                                <div class="kt-widget__icon">
                                    <i class="flaticon-download"></i>
                                </div>
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Téléchargement</span>
                                    <span class="kt-widget__value"><?= $asset->count ?></span>
                                </div>
                            </div>

                            <div class="kt-widget__item">
                                <div class="kt-widget__icon">
                                    <i class="flaticon-clock"></i>
                                </div>
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Compatibilité</span>
                                    <div class="kt-section__content kt-section__content--solid">
                                        <div class="kt-badge kt-badge__pics">
                                            <?php foreach($asset->compatibilities as $compatibility): ?>
                                            <?php if($compatibility->state == 0): ?>
                                            <span class="kt-badge kt-badge--danger kt-badge--md" data-toggle="kt-tooltip" title="Incompatible"><?= $compatibility->trainz_build ?></span>
                                            <?php else: ?>
                                            <span class="kt-badge kt-badge--success kt-badge--md" data-toggle="kt-tooltip" title="Compatible"><?= $compatibility->trainz_build ?></span>
                                            <?php endif; ?>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="kt-widget__item">
                                <div class="kt-widget__icon">
                                    <i class="flaticon-twitter-logo"></i>
                                </div>
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Publier sur Twitter</span>
                                    <span class="kt-widget__value">
                                            <?php if($asset->twitter == 0): ?>
                                                <i class="la la-close la-3x text-danger"></i>
                                            <?php else: ?>
                                                <i class="la la-check la-3x text-success" data-toggle="kt-popover" title="Texte publier sur twitter" content="<?= $asset->twitterText ?>"></i>
                                            <?php endif; ?>
                                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        endforeach;

        $content = ob_get_clean();

        return response()->json([$content]);
    }

    private function createCompatibility($asset)
    {

        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $file = file_get_contents(Storage::disk('public')->url('config/'.str_slug($asset->categorie->name).'/'.str_slug($asset->subcategorie->name).'/'.pregReplaceKuid($asset->kuid).'.txt'), false, stream_context_create($arrContextOptions));


        if(preg_match("#trainz-build                            3.1#", $file))
        {
            $this->assetCompatibilityRepository->store($asset->id, '3.1', 2);
            $this->assetCompatibilityRepository->store($asset->id, '3.4', 0);
            $this->assetCompatibilityRepository->store($asset->id, '3.7', 0);
            $this->assetCompatibilityRepository->store($asset->id, '4.2', 0);
            $this->assetCompatibilityRepository->store($asset->id, '4.5', 0);
            $this->assetCompatibilityRepository->store($asset->id, '4.6', 0);

        }elseif(preg_match("#trainz-build                            3.4#", $file))
        {
            $this->assetCompatibilityRepository->store($asset->id, '3.1', 2);
            $this->assetCompatibilityRepository->store($asset->id, '3.4', 2);
            $this->assetCompatibilityRepository->store($asset->id, '3.7', 0);
            $this->assetCompatibilityRepository->store($asset->id, '4.2', 0);
            $this->assetCompatibilityRepository->store($asset->id, '4.5', 0);
            $this->assetCompatibilityRepository->store($asset->id, '4.6', 0);
        }elseif(preg_match("#trainz-build                            3.7#", $file))
        {
            $this->assetCompatibilityRepository->store($asset->id, '3.1', 2);
            $this->assetCompatibilityRepository->store($asset->id, '3.4', 2);
            $this->assetCompatibilityRepository->store($asset->id, '3.7', 2);
            $this->assetCompatibilityRepository->store($asset->id, '4.2', 0);
            $this->assetCompatibilityRepository->store($asset->id, '4.5', 0);
            $this->assetCompatibilityRepository->store($asset->id, '4.6', 0);
        }elseif(preg_match("#trainz-build                            4.2#", $file))
        {
            $this->assetCompatibilityRepository->store($asset->id, '3.1', 2);
            $this->assetCompatibilityRepository->store($asset->id, '3.4', 2);
            $this->assetCompatibilityRepository->store($asset->id, '3.7', 2);
            $this->assetCompatibilityRepository->store($asset->id, '4.2', 2);
            $this->assetCompatibilityRepository->store($asset->id, '4.5', 0);
            $this->assetCompatibilityRepository->store($asset->id, '4.6', 0);
        }elseif(preg_match("#trainz-build                            4.5#", $file))
        {
            $this->assetCompatibilityRepository->store($asset->id, '3.1', 2);
            $this->assetCompatibilityRepository->store($asset->id, '3.4', 2);
            $this->assetCompatibilityRepository->store($asset->id, '3.7', 2);
            $this->assetCompatibilityRepository->store($asset->id, '4.2', 2);
            $this->assetCompatibilityRepository->store($asset->id, '4.5', 2);
            $this->assetCompatibilityRepository->store($asset->id, '4.6', 0);
        }elseif(preg_match("#trainz-build                            4.6#", $file))
        {
            $this->assetCompatibilityRepository->store($asset->id, '3.1', 2);
            $this->assetCompatibilityRepository->store($asset->id, '3.4', 2);
            $this->assetCompatibilityRepository->store($asset->id, '3.7', 2);
            $this->assetCompatibilityRepository->store($asset->id, '4.2', 2);
            $this->assetCompatibilityRepository->store($asset->id, '4.5', 2);
            $this->assetCompatibilityRepository->store($asset->id, '4.6', 2);
        }
    }

    /**
     * TODO
     * - Créer le dossier '/tmp/$asset_id'
     * - Transferer le zip dans le dossier '/tmp/$asset_id'
     * - Créer le dossier dans le cluster S3 ou Public
     * - Dezipper le Zip dans le dossier '/tmp/$asset_id'
     * - Supprimer le zip du dossier '/tmp/$asset_id'
     * - Parcourir le dossier '/tmp/$asset_id'
     * - Transferer chacun des fichiers dans les clusters
     *
     * @param UploadedFile $file
     * @param object $asset
     */
    private function transferFBXModele(UploadedFile $file, $asset)
    {
        $zipExtractor = new ZipExtractor();

        mkdir(public_path().'/tmp/'.$asset->id);
        $file->move(public_path().'/tmp/'.$asset->id);

        $zipExtractor->extract('/tmp/'.$asset->id, '/tmp/'.$asset->id.'/'.$file->getClientOriginalName());
        if(!unlink('/tmp/'.$asset->id.'/'.$file->getClientOriginalName()))
        {
            Log::error('Impossible de supprimer le fichier ZIP temporaire:'.$file->getClientOriginalName(), ['TransferFBXModele']);
        }

        if(env('APP_ENV') == 'production'){
            Storage::disk('s3')->makeDirectory('modele/'.str_slug($asset->categorie->name).'/'.str_slug($asset->subcategorie->name).'/'.$asset->id);

            $dossier = opendir(public_path('/tmp/'.$asset->id.'/'));
            while(false !== ($fichier = readdir($dossier)))
            {
                Storage::disk('s3')->put('modele/'.str_slug($asset->categorie->name).'/'.str_slug($asset->subcategorie->name).'/'.$asset->id, $fichier);
            }

        }else{
            Storage::disk('public')->makeDirectory('modele/'.str_slug($asset->categorie->name).'/'.str_slug($asset->subcategorie->name).'/'.$asset->id);

            $dossier = opendir(public_path('/tmp/'.$asset->id.'/'));
            while(false !== ($fichier = readdir($dossier)))
            {
                Storage::disk('public')->put('modele/'.str_slug($asset->categorie->name).'/'.str_slug($asset->subcategorie->name).'/'.$asset->id, $fichier);
            }
        }
    }

    public function tree_d($asset_id)
    {
        return view("Back.Download.3d", [
            "asset" => $this->assetRepository->get($asset_id)
        ]);
    }

    public function getContenue($asset_id) {
        $asset = $this->assetRepository->get($asset_id);
        return response()->json([$asset->description]);
    }

}
