<?php

namespace App\Http\Controllers\Back\Download\Subcategory;

use App\Http\Controllers\Back\BackController;
use App\Http\Requests\Download\Subcategory\AddSubcategoryRequest;
use App\Repository\Asset\AssetSubCategorieRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubcategoryController extends BackController
{
    /**
     * @var AssetSubCategorieRepository
     */
    private $assetSubCategorieRepository;

    /**
     * SubcategoryController constructor.
     * @param AssetSubCategorieRepository $assetSubCategorieRepository
     */
    public function __construct(AssetSubCategorieRepository $assetSubCategorieRepository)
    {
        parent::__construct();
        $this->assetSubCategorieRepository = $assetSubCategorieRepository;
    }

    public function store(AddSubcategoryRequest $request)
    {
        $subcategory = $this->assetSubCategorieRepository->create(
            $request->get('categorie_id'),
            $request->get('name')
        );

        ob_start();
        ?>
        <tr>
            <td><?= $subcategory->id ?></td>
            <td><?= $subcategory->name ?></td>
            <td><?= $subcategory->categorie->name ?></td>
            <td>
                <a href="<?= route('Download.Subcategory.edit', $subcategory->id) ?>" class="btn btn-sm btn-brand btn-elevate btn-icon"><i class="la la-edit"></i></a>
                <button id="btnDelete" type="button" class="btn btn-sm btn-danger btn-elevate btn-icon" data-href="<?= route('Download.Subcategory.delete', $subcategory->id) ?>"><i class="la la-trash"></i></button>
            </td>
        </tr>
        <?php

        $content = ob_get_clean();

        return response()->json([
            "name"  => $subcategory->name,
            "tr"    => $content
        ]);
    }

    public function delete($subcategory_id)
    {
        $this->assetSubCategorieRepository->delete($subcategory_id);
        return null;
    }
}
