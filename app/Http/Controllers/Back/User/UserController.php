<?php

namespace App\Http\Controllers\Back\User;

use App\Http\Controllers\Back\BackController;
use App\Repository\User\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends BackController
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        parent::__construct();
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        return view("Back.User.index", [
            "users" => $this->userRepository->getAll()
        ]);
    }
}
