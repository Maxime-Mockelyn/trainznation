<?php

namespace App\Http\Controllers\Back\Wiki;

use App\Http\Controllers\Back\BackController;
use App\Repository\Wiki\WikiCategorieRepository;
use App\Repository\Wiki\WikiRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class WikiController extends BackController
{
    /**
     * @var WikiCategorieRepository
     */
    private $wikiCategorieRepository;
    /**
     * @var WikiRepository
     */
    private $wikiRepository;

    /**
     * WikiController constructor.
     * @param WikiCategorieRepository $wikiCategorieRepository
     * @param WikiRepository $wikiRepository
     */
    public function __construct(WikiCategorieRepository $wikiCategorieRepository, WikiRepository $wikiRepository)
    {
        parent::__construct();
        $this->wikiCategorieRepository = $wikiCategorieRepository;
        $this->wikiRepository = $wikiRepository;
    }

    public function index()
    {
        //dd($this->wikiRepository->all());
        return view("Back.Wiki.index", [
            "categories"    => $this->wikiCategorieRepository->all(),
            "articles"      => $this->wikiRepository->all()
        ]);
    }

    public function create()
    {
        return view('Back.Wiki.create', [
            "categories"    => $this->wikiCategorieRepository->all()
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            "title" => "min:2|required",
            "category_id"   => "required",
            "contents"       => "required"
        ]);

        $wiki = $this->wikiRepository->create(
            $request->title,
            $request->category_id,
            $request->contents
        );

        return response()->json(["title" => $wiki->title]);
    }

    public function show($id)
    {
        return view("Back.Wiki.show", [
            "wiki"  => $this->wikiRepository->get($id)
        ]);
    }

    public function edit($id)
    {
        return view("Back.Wiki.edit", [
            "categories"    => $this->wikiCategorieRepository->all(),
            "wiki"          => $this->wikiRepository->get($id)
        ]);
    }

    public function update(Request $request, $id)
    {
        $wiki = $this->wikiRepository->get($id);
        $this->wikiRepository->update(
            $id,
            $request->title,
            $request->category_id,
            $request->contents
        );

        return response()->json(["title" => $wiki->title]);
    }

    public function delete($id)
    {
        $wiki = $this->wikiRepository->get($id);
        $this->wikiRepository->delete($id);

        return response()->json(["title" => $wiki->title]);
    }

    public function publish($id)
    {
        $this->wikiRepository->publish($id);

        return null;
    }

    public function unpublish($id)
    {
        $this->wikiRepository->unpublish($id);
    }

    public function getPublish($id)
    {
        $this->wikiRepository->publish($id);

        Toastr::success("L'article à été publier", "Publication");

        return redirect()->back();
    }

    public function getUnpublish($id)
    {
        $this->wikiRepository->unpublish($id);

        Toastr::success("L'article à été dépublier", "Publication");

        return redirect()->back();
    }

    public function getDelete($id)
    {
        $this->wikiRepository->delete($id);

        Toastr::success("L'article à été supprimer", "Suppression d'un article");

        return redirect()->route('Back.Wiki.index');
    }
}
