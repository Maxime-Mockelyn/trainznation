<?php

namespace App\Http\Controllers\Back\Wiki;

use App\Repository\Wiki\WikiCategorieRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class CategoryController extends Controller
{
    /**
     * @var WikiCategorieRepository
     */
    private $wikiCategorieRepository;

    /**
     * CategoryController constructor.
     * @param WikiCategorieRepository $wikiCategorieRepository
     */
    public function __construct(WikiCategorieRepository $wikiCategorieRepository)
    {
        $this->wikiCategorieRepository = $wikiCategorieRepository;
    }

    public function store(Request $request)
    {
        $category = $this->wikiCategorieRepository->create(
            $request->name
        );

        Toastr::success("La Catégorie <strong>".$category->name."</strong> à été créer", "Création d'une catégorie");
        return redirect()->back();
    }

    public function delete($id)
    {
        $this->wikiCategorieRepository->delete($id);

        return null;
    }
}
