<?php

namespace App\Http\Controllers\Front\Course;

use App\Repository\Learning\CourseRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CourseController extends Controller
{
    /**
     * @var CourseRepository
     */
    private $courseRepository;

    /**
     * CourseController constructor.
     * @param CourseRepository $courseRepository
     */
    public function __construct(CourseRepository $courseRepository)
    {
        $this->courseRepository = $courseRepository;
    }

    public function dashboard()
    {
        return view("Front.Course.dashboard");
    }

    public function index()
    {
        return view("Front.Course.index", [
            "courses" => $this->courseRepository->all()
        ]);
    }

    public function show($slug)
    {
        //dd($this->courseRepository->getForSlug($slug));
        return view("Front.Course.show", [
            "course" => $this->courseRepository->getForSlug($slug)
        ]);
    }
}
