<?php

namespace App\Http\Controllers\Front\Download;

use App\Mail\Checkout\CheckoutDownload;
use App\Mail\Checkout\CheckoutInvoice;
use App\Repository\Asset\AssetRepository;
use App\Repository\Asset\AssetSubCategorieRepository;
use App\Repository\Checkout\InvoiceRepository;
use Cartalyst\Stripe\Api\Customers;
use Cartalyst\Stripe\Exception\CardErrorException;
use Cartalyst\Stripe\Stripe;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Kamaln7\Toastr\Facades\Toastr;

class DownloadController extends Controller
{
    /**
     * @var AssetSubCategorieRepository
     */
    private $assetSubCategorieRepository;
    /**
     * @var AssetRepository
     */
    private $assetRepository;
    /**
     * @var InvoiceRepository
     */
    private $invoiceRepository;

    /**
     * DownloadController constructor.
     * @param AssetSubCategorieRepository $assetSubCategorieRepository
     * @param AssetRepository $assetRepository
     * @param InvoiceRepository $invoiceRepository
     */
    public function __construct(AssetSubCategorieRepository $assetSubCategorieRepository, AssetRepository $assetRepository, InvoiceRepository $invoiceRepository)
    {
        $this->assetSubCategorieRepository = $assetSubCategorieRepository;
        $this->assetRepository = $assetRepository;
        $this->invoiceRepository = $invoiceRepository;
    }

    public function list($subcategory_id)
    {
        return view("Front.Download.list", [
            "subcategorie"  => $this->assetSubCategorieRepository->get($subcategory_id),
            "assets"    => $this->assetRepository->getForSubs($subcategory_id)
        ]);
    }

    public function show($subcategory_id, $asset_id)
    {
        return view("Front.Download.show", [
            "asset" => $this->assetRepository->get($asset_id)
        ]);
    }

    public function tree($subcategory_id, $asset_id)
    {
        return view("Front.Download.tree", [
            "asset" => $this->assetRepository->get($asset_id)
        ]);
    }

    public function getContenue($subcategory_id, $asset_id) {
        $asset = $this->assetRepository->get($asset_id);
        return response()->json([$asset->description]);
    }

    public function cart($subcategory_id, $asset_id)
    {
        return view("Front.Download.cart", [
            "asset" => $this->assetRepository->get($asset_id)
        ]);
    }

    public function checkout(Request $request, $subcategory_id, $asset_id)
    {
        $asset = $this->assetRepository->get($asset_id);
        //dd($request->all());
        if(auth()->check() == true)
        {
            $user_id = auth()->user()->id;
        }else{
            $user_id = null;
        }

        $stripe = new Stripe(env("STRIPE_SECRET_KEY"));

        try {
            $customer = $stripe->customers()->create([
                "email" => $request->email
            ]);

            $token = $stripe->tokens()->create([
                "card" => [
                    "number"    => $request->num_cb,
                    "exp_month" => $request->exp_month,
                    "exp_year"  => $request->exp_year,
                    "cvc"       => $request->cvv_cb
                ]
            ]);

            $card = $stripe->cards()->create($customer['id'], $token['id']);

            $charge = $stripe->charges()->create([
                'customer'  => $customer["id"],
                'currency'  => 'EUR',
                'amount'    => $asset->price,
                "description" => "Trainznation - ".$asset->designation
            ]);

            $invoice = $this->invoiceRepository->create($request->email, $asset_id, $asset->price, $user_id);
            $route = route('Download.redirect', $asset_id);

            \Mail::to($request->email)->send(new CheckoutInvoice($invoice));
            \Mail::to($request->email)->send(new CheckoutDownload($route, $asset));

            //Toastr::success("La Paiement à été effectuer, un mail vous à été envoyer", "Paiement Accepté");
            $message = "Le Paiement à été accepter, un mail vous à été envoyer à <strong>".$request->email."</strong> comportant votre facture et le lien vers l'objet à télécharger";
            return redirect()->route('Front.Download.success', [
                $subcategory_id,
                $asset_id,
                $invoice->id
            ]);

        }catch (\Exception $exception)
        {
            Log::error($exception->getMessage(), ["code" => $exception->getCode(), "line" => $exception->getLine(), "trace" => $exception->getTrace()]);
            Toastr::error("Impossible d'effectuer le paiement, consulter les logs", "Erreur Serveur");
            return back();
        }
    }

    public function success($subcategory_id, $asset_id, $invoice_id)
    {
        return view("Front.Download.success", [
            "invoice"   => $this->invoiceRepository->get($invoice_id),
            "asset"     => $this->assetRepository->get($asset_id)
        ]);
    }
}
