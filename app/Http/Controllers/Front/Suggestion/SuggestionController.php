<?php

namespace App\Http\Controllers\Front\Suggestion;

use App\Mail\Suggestion\Submit;
use App\Repository\Suggestion\SuggestionRepository;
use App\Repository\Suggestion\SuggestionTimelineRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Kamaln7\Toastr\Facades\Toastr;

class SuggestionController extends Controller
{
    /**
     * @var SuggestionRepository
     */
    private $suggestionRepository;
    /**
     * @var SuggestionTimelineRepository
     */
    private $suggestionTimelineRepository;

    /**
     * SuggestionController constructor.
     * @param SuggestionRepository $suggestionRepository
     * @param SuggestionTimelineRepository $suggestionTimelineRepository
     */
    public function __construct(SuggestionRepository $suggestionRepository, SuggestionTimelineRepository $suggestionTimelineRepository)
    {
        $this->suggestionRepository = $suggestionRepository;
        $this->suggestionTimelineRepository = $suggestionTimelineRepository;
    }

    public function index()
    {
        return view("Front.Suggestion.index", [
            "suggestions"   => $this->suggestionRepository->allForLimit(10)
        ]);
    }

    public function store(Request $request)
    {
        try{
            $suggestion = $this->suggestionRepository->createForAll(
                $request->name,
                $request->type,
                $request->sujet,
                $request->suggestion,
                $request->email
            );

            $timeline = $this->suggestionTimelineRepository->store(
                $suggestion->id,
                1,
                "Soumission de la requete N°".$suggestion->id,
                "Votre Requete <strong>".$suggestion->sujet."</strong> à été soumis à un administrateur"
            );

            Mail::to($suggestion->email)->send(new Submit($suggestion));
            Notification::send(auth()->user(), new \App\Notifications\Suggestion\Submit($suggestion));


            Toastr::success("La suggestion à bien été poster", "Nouvelle suggestion");
            return back();
        }catch (\Exception $exception)
        {
            Log::error($exception->getMessage(), ["code" => $exception->getCode()]);
            Toastr::error("Erreur de traitement, consulter les logs", "Erreur 500");
            return back();
        }
    }

    public function show($suggestion_id)
    {
        return view("Front.Suggestion.show", [
            "suggestion"    => $this->suggestionRepository->get($suggestion_id)
        ]);
    }
}
