<?php

namespace App\Http\Controllers\Front\Blog;

use App\Repository\Blog\BlogCommentRepository;
use App\Repository\Blog\BlogRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class BlogController extends Controller
{
    /**
     * @var BlogRepository
     */
    private $blogRepository;
    /**
     * @var BlogCommentRepository
     */
    private $blogCommentRepository;

    /**
     * BlogController constructor.
     * @param BlogRepository $blogRepository
     * @param BlogCommentRepository $blogCommentRepository
     */
    public function __construct(BlogRepository $blogRepository, BlogCommentRepository $blogCommentRepository)
    {
        $this->blogRepository = $blogRepository;
        $this->blogCommentRepository = $blogCommentRepository;
    }

    public function index()
    {
        return view("Front.Blog.index", [
            "blogs" => $this->blogRepository->getPosts()
        ]);
    }

    public function show($slug)
    {
        return view("Front.Blog.show", [
            "blog"  => $this->blogRepository->getPost($slug)
        ]);
    }

    public function postComment(Request $request, $slug)
    {
        $blog = $this->blogRepository->getPost($slug);
        $comment = $this->blogCommentRepository->addComment(
            $blog->id,
            auth()->user()->id,
            $request->get('comment')
        );

        addingActivity("Commentaire poster sur l'article: <strong>".$blog->title."</strong>", 'la la-comment', 2);

        Toastr::success("Nouveau Commentaire", "Votre commentaire à été posté !");
        return redirect()->back();
    }

    public function getContenue($slug) {
        $blog = $this->blogRepository->getPost($slug);
        return response()->json([$blog->content]);
    }
}
