<?php

namespace App\Http\Controllers\Front\Route;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RouteController extends Controller
{
    public function index()
    {
        return view("Front.Route.index");
    }
}
