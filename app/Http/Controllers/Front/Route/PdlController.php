<?php

namespace App\Http\Controllers\Front\Route;

use App\Packages\Trainznation\Route;
use App\Repository\Route\PdlAnomalieRepository;
use App\Repository\Route\PdlBuildRepository;
use App\Repository\Route\PdlCompatibilityRepository;
use App\Repository\Route\PdlDownloadRepository;
use App\Repository\Route\PdlGalleryCategorieRepository;
use App\Repository\Route\PdlGalleryRepository;
use App\Repository\Route\PdlRepository;
use App\Repository\Route\PdlTimelineRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdlController extends Controller
{
    /**
     * @var PdlAnomalieRepository
     */
    private $pdlAnomalieRepository;
    /**
     * @var PdlBuildRepository
     */
    private $pdlBuildRepository;
    /**
     * @var PdlTimelineRepository
     */
    private $pdlTimelineRepository;
    /**
     * @var PdlCompatibilityRepository
     */
    private $pdlCompatibilityRepository;
    /**
     * @var PdlGalleryCategorieRepository
     */
    private $pdlGalleryCategorieRepository;
    /**
     * @var PdlGalleryRepository
     */
    private $pdlGalleryRepository;
    /**
     * @var PdlRepository
     */
    private $pdlRepository;
    /**
     * @var PdlDownloadRepository
     */
    private $pdlDownloadRepository;
    /**
     * @var Route
     */
    private $route;

    /**
     * PdlController constructor.
     * @param PdlAnomalieRepository $pdlAnomalieRepository
     * @param PdlBuildRepository $pdlBuildRepository
     * @param PdlTimelineRepository $pdlTimelineRepository
     * @param PdlCompatibilityRepository $pdlCompatibilityRepository
     * @param PdlGalleryCategorieRepository $pdlGalleryCategorieRepository
     * @param PdlGalleryRepository $pdlGalleryRepository
     * @param PdlRepository $pdlRepository
     * @param PdlDownloadRepository $pdlDownloadRepository
     * @param Route $route
     */
    public function __construct(
        PdlAnomalieRepository $pdlAnomalieRepository,
        PdlBuildRepository $pdlBuildRepository,
        PdlTimelineRepository $pdlTimelineRepository,
        PdlCompatibilityRepository $pdlCompatibilityRepository,
        PdlGalleryCategorieRepository $pdlGalleryCategorieRepository,
        PdlGalleryRepository $pdlGalleryRepository,
        PdlRepository $pdlRepository,
        PdlDownloadRepository $pdlDownloadRepository,
        Route $route
    )
    {
        $this->pdlAnomalieRepository = $pdlAnomalieRepository;
        $this->pdlBuildRepository = $pdlBuildRepository;
        $this->pdlTimelineRepository = $pdlTimelineRepository;
        $this->pdlCompatibilityRepository = $pdlCompatibilityRepository;
        $this->pdlGalleryCategorieRepository = $pdlGalleryCategorieRepository;
        $this->pdlGalleryRepository = $pdlGalleryRepository;
        $this->pdlRepository = $pdlRepository;
        $this->pdlDownloadRepository = $pdlDownloadRepository;
        $this->route = $route;
    }

    public function index()
    {
        return view("Front.Route.Pdl.index");
    }

    public function labs()
    {
        return view("Front.Route.Pdl.labs", [
            "info"  => $this->route->getInfo()
        ]);
    }

    public function gallerie()
    {
        return view('Front.Route.Pdl.gallerie', [
            "categories"    => $this->pdlGalleryCategorieRepository->all()
        ]);
    }

    public function versionOne()
    {
        return view('Front.Route.Pdl.version.one');
    }

    public function versionTwo()
    {
        return view('Front.Route.Pdl.version.two');
    }

    public function versionThree()
    {
        return view('Front.Route.Pdl.version.three');
    }

    public function versionFour()
    {
        return view('Front.Route.Pdl.version.four');
    }

    public function versionFive()
    {
        return view('Front.Route.Pdl.version.five');
    }

    public function versionSix()
    {
        return view('Front.Route.Pdl.version.six');
    }

    public function versionSeven()
    {
        return view('Front.Route.Pdl.version.seven');
    }

    public function versionHuit()
    {
        return view('Front.Route.Pdl.version.huit');
    }

    public function download()
    {
        return view("Front.Route.Pdl.download", [
            "latest" => $this->pdlDownloadRepository->getLatestBuildActivate(),
            "downloads" => $this->pdlDownloadRepository->getAllForPublish()
        ]);
    }
}
