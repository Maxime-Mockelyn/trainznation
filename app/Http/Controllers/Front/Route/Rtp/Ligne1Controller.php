<?php

namespace App\Http\Controllers\Front\Route\Rtp;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Ligne1Controller extends Controller
{
    public function __construct()
    {
    }

    public function index()
    {
        return view("Front.Route.Rtp.Ligne1.index");
    }
}
