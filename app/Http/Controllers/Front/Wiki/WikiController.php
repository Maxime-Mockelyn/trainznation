<?php

namespace App\Http\Controllers\Front\Wiki;

use App\Repository\Wiki\WikiCategorieRepository;
use App\Repository\Wiki\WikiRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WikiController extends Controller
{
    /**
     * @var WikiCategorieRepository
     */
    private $wikiCategorieRepository;
    /**
     * @var WikiRepository
     */
    private $wikiRepository;

    /**
     * WikiController constructor.
     * @param WikiCategorieRepository $wikiCategorieRepository
     * @param WikiRepository $wikiRepository
     */
    public function __construct(WikiCategorieRepository $wikiCategorieRepository, WikiRepository $wikiRepository)
    {
        $this->wikiCategorieRepository = $wikiCategorieRepository;
        $this->wikiRepository = $wikiRepository;
    }

    public function index()
    {
        return view("Wiki.home", [
            "categories"   => $this->wikiCategorieRepository->all(),
            "latests"      => $this->wikiRepository->lastWiki()
        ]);
    }

    public function showCategory($category_id)
    {
        return view("Wiki.showCategory", [
            "categories"  => $this->wikiCategorieRepository->all(),
            "category"    => $this->wikiCategorieRepository->get($category_id),
            "articles"    => $this->wikiRepository->getForCategory($category_id)
        ]);
    }

    public function show($category_id, $article_id)
    {
        return view("Wiki.show", [
            "categories"=> $this->wikiCategorieRepository->all(),
            "article"   => $this->wikiRepository->get($article_id)
        ]);
    }

    public function getContenue($category_id, $wiki_id)
    {
        $wiki = $this->wikiRepository->get($wiki_id);
        return response()->json([$wiki->content]);
    }
}
