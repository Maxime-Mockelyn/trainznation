<?php

namespace App\Http\Controllers\Front\Tutoriel;

use App\Repository\Tutoriel\TutorielCommentRepository;
use App\Repository\Tutoriel\TutorielRepository;
use App\Repository\Tutoriel\TutorielSubCategorieRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class TutorielController extends Controller
{
    /**
     * @var TutorielRepository
     */
    private $tutorielRepository;
    /**
     * @var TutorielSubCategorieRepository
     */
    private $tutorielSubCategorieRepository;
    /**
     * @var TutorielCommentRepository
     */
    private $tutorielCommentRepository;

    /**
     * TutorielController constructor.
     * @param TutorielRepository $tutorielRepository
     * @param TutorielSubCategorieRepository $tutorielSubCategorieRepository
     * @param TutorielCommentRepository $tutorielCommentRepository
     */
    public function __construct(TutorielRepository $tutorielRepository, TutorielSubCategorieRepository $tutorielSubCategorieRepository, TutorielCommentRepository $tutorielCommentRepository)
    {
        $this->tutorielRepository = $tutorielRepository;
        $this->tutorielSubCategorieRepository = $tutorielSubCategorieRepository;
        $this->tutorielCommentRepository = $tutorielCommentRepository;
    }

    public function list($subcategories_id)
    {
        return view("Front.Tutoriel.list", [
            "subcategorie"  => $this->tutorielSubCategorieRepository->get($subcategories_id),
            "tutoriels"     => $this->tutorielRepository->listWithCategory($subcategories_id)
        ]);
    }

    public function show($subcategory_id, $tutoriel_slug)
    {
        //dd($this->tutorielRepository->getTuto($tutoriel_slug));
        return view("Front.Tutoriel.show", [
            "tutoriel"  => $this->tutorielRepository->getTuto($tutoriel_slug)
        ]);
    }

    public function storeComment(Request $request, $subcategory_id, $tutoriel_id)
    {
        $tutoriel = $this->tutorielRepository->getTutoForId($tutoriel_id);

        $comment = $this->tutorielCommentRepository->store(
            $tutoriel_id,
            auth()->user()->id,
            $request->comment
        );

        addingActivity("Commentaire poster sur le tutoriel: <strong>".$tutoriel->title."</strong>", 'la la-comment', 2);

        Toastr::success("Votre commentaire à été poster", "Nouveau Commentaire");
        return redirect()->back();
    }

    public function getContenue($sub_id, $slug) {
        $tutoriel = $this->tutorielRepository->getTutoWithoutLoad($slug);
        return response()->json([$tutoriel->content]);
    }
}
