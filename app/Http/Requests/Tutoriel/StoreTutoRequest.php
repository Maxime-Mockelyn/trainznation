<?php

namespace App\Http\Requests\Tutoriel;

use Illuminate\Foundation\Http\FormRequest;

class StoreTutoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "categorie_id"      => "required",
            "subcategorie_id"   => "required",
            "title"             => "required",
            "content"           => "required",
            "image"             => "required"
        ];
    }
}
