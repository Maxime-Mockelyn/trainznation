<?php

namespace App\Http\Requests\Download;

use Illuminate\Foundation\Http\FormRequest;

class AddAssetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "categorie_id"          => "required",
            "subcategorie_id"       => "required",
            "designation"           => "required",
            "description"           => "required|min:5",
            "kuid"                  => "required",
            "downloadLink"          => "required"
        ];
    }
}
