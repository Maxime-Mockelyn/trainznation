<?php

namespace App\Console;

use App\Http\Controllers\Automate\AutomateController;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->call(function (){
            $automate = new AutomateController();
            $automate::now();
        })->everyMinute();

        $schedule->call(function (){
            $automate = new AutomateController();
            $automate::daily();
        })->dailyAt('23:00');

        $schedule->call(function (){
            $automate = new AutomateController();
            $automate::thursday();
        })->thursdays()->at("02:00");


    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
