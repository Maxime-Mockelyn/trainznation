<?php


namespace App\Packages\Trainznation;


class Route extends API
{
    public function getInfo()
    {
        $call = $this->get('route/getInfo');

        return $call['data'];
    }
}
