<?php


namespace App\Packages\Trainznation;


class API
{
    private $endpoint = "https://trainznation.eu/api/";

    public function get($url, $params = []) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->endpoint.$url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        return json_decode($response, true);
    }
}
