<?php
/**
 * Created by PhpStorm.
 * User: Sylth
 * Date: 19/03/2019
 * Time: 16:41
 */

namespace App\Subscriber;


use App\Events\Badge\Createur;
use App\Events\Badge\Nessie;
use App\Model\Badge\Badge;
use App\Model\Blog\BlogComment;
use App\Notifications\Badge\BadgeUnlock;
use App\Repository\Badge\BadgeReprository;
use App\User;
use Illuminate\Auth\Events\Registered;
use Kamaln7\Toastr\Facades\Toastr;

class BadgeSubscriber
{
    /**
     * @var BadgeReprository
     */
    private $badgeReprository;

    /**
     * BadgeSubscriber constructor.
     * @param BadgeReprository $badgeReprository
     */
    public function __construct(BadgeReprository $badgeReprository)
    {
        $this->badgeReprository = $badgeReprository;
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'eloquent.saved: App\Model\Blog\BlogComment',
            [$this, 'onNewComment']
        );

        $events->listen(Nessie::class, [$this, 'onNessie']);
        $events->listen(Createur::class, [$this, 'onCreateur']);
    }

    public function onNewComment(BlogComment $comment)
    {
        $user = $comment->user;
        $comments_count = $user->blogcomments()->count();
        $badge = $this->badgeReprository->unlockActionFor($user->id, 'comments', $comments_count);
        $this->badgeReprository->notifyBadgeUnlocked($user, $badge);
    }
    public function onNessie(Nessie $event)
    {
        //dd($event);
        $badge = $this->badgeReprository->unlockActionFor($event->user_id->id, 'nessie');
        $this->badgeReprository->notifyBadgeUnlocked($event->user_id, $badge);
    }

    public function onCreateur(Createur $event)
    {
        $badge = $this->badgeReprository->unlockActionFor($event->user_id->id, 'createur');
        $this->badgeReprository->notifyBadgeUnlocked($event->user_id, $badge);
    }

}
