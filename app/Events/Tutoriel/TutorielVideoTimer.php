<?php

namespace App\Events\Tutoriel;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TutorielVideoTimer
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $video_id;

    /**
     * Create a new event instance.
     *
     * @param int $video_id
     */
    public function __construct($video_id)
    {
        //
        $this->video_id = $video_id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
