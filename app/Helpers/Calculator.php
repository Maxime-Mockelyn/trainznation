<?php
if(!function_exists('calcNewBuild')){
    function calcNewBuild($prevBuild, $firstBuild){
        $build = $prevBuild+$firstBuild/100;
        return round($build, 0);
    }
}

if(!function_exists('calcSubBuild')){
    function calcSubBuild($prevBuild, $firstBuild){
        $build = $prevBuild-$firstBuild/100;
        return round($build, 0);
    }
}
