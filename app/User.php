<?php

namespace App;

use App\Model\Blog\BlogComment;
use App\Model\Suggestion\SuggestionTimeline;
use App\Model\Tutoriel\Tutoriel;
use App\Model\Tutoriel\TutorielComment;
use App\Model\User\UserAccount;
use App\Model\User\UserActivity;
use App\Model\Wiki\Wiki;
use App\Traits\Badge\Badgeable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Laravel\Cashier\Invoice;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use Badgeable;
    use HasApiTokens;
    use Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'group'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function account()
    {
        return $this->hasOne(UserAccount::class);
    }

    public function blogcomments()
    {
        return $this->hasMany(BlogComment::class);
    }

    public function tutoriel()
    {
        return $this->hasMany(Tutoriel::class);
    }

    public function tutorielcomments()
    {
        return $this->hasMany(TutorielComment::class);
    }

    public function activities()
    {
        return $this->hasMany(UserActivity::class);
    }

    public function wikis()
    {
        return $this->hasMany(Wiki::class);
    }

    public function timelines()
    {
        return $this->hasMany(SuggestionTimeline::class);
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function routeNotificationForSlack() {
        return env('SLACK_WEBHOOK_URL');
    }

    public function routeNotificationForDiscord()
    {
        return $this->discord_channel;
    }

}
