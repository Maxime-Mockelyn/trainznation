<?php

namespace Tests\Feature;

use App\Model\Blog\Blog;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BlogTest extends TestCase
{
    use DatabaseTransactions;
    use DatabaseMigrations;

    public function testPostArticle()
    {
        $blog = factory(Blog::class)->create();

        $this->assertEquals(1, $blog->count());
    }
}
