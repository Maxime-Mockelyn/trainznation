<?php

namespace Tests\Feature;

use App\Events\Badge\Nessie;
use App\Model\Badge\Badge;
use App\Model\Blog\Blog;
use App\Model\Blog\BlogComment;
use App\Notifications\Badge\BadgeUnlock;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class BadgeTest extends TestCase
{
    use DatabaseTransactions;
    use DatabaseMigrations;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUnlockBadgeAuto()
    {
        Badge::create([
            "name"          => "Pipelette",
            "action"        => "comments",
            "action_count"  => 2
        ]);

        $user = factory(User::class)->create();
        $blog = factory(Blog::class)->create();

        factory(BlogComment::class, 3)->create(['user_id' => $user->id, 'blog_id' => $blog->id]);

        $this->assertEquals(1, $user->badges()->count());
    }

    public function testDontUnlockBadgeAuto()
    {
        Badge::create([
            "name"          => "Pipelette",
            "action"        => "comments",
            "action_count"  => 2
        ]);

        $user = factory(User::class)->create();
        $blog = factory(Blog::class)->create();

        factory(BlogComment::class, 1)->create(['user_id' => $user->id, 'blog_id' => $blog->id]);

        $this->assertEquals(0, $user->badges()->count());
    }

    public function testUnlockDoubleBadge()
    {
        Badge::create([
            "name"          => "Pipelette",
            "action"        => "comments",
            "action_count"  => 2
        ]);

        $user = factory(User::class)->create();
        $blog = factory(Blog::class)->create();

        factory(BlogComment::class, 2)->create(['user_id' => $user->id, 'blog_id' => $blog->id]);

        $this->assertEquals(1, $user->badges()->count());

        BlogComment::first()->delete();
        factory(BlogComment::class, 2)->create(['user_id' => $user->id, 'blog_id' => $blog->id]);

        $this->assertEquals(1, $user->badges()->count());
    }

    public function testNotificationSent()
    {
        Notification::fake();

        Badge::create([
            "name"          => "Pipelette",
            "action"        => "comments",
            "action_count"  => 2
        ]);

        $user = factory(User::class)->create();
        $blog = factory(Blog::class)->create();

        factory(BlogComment::class, 3)->create(['user_id' => $user->id, 'blog_id' => $blog->id]);

        Notification::assertSentTo([$user], BadgeUnlock::class);
    }

    public function testUnlockNessieBadge()
    {
        $user = factory(User::class)->create();
        Badge::create([
            "name"          => "Nessie",
            "action"        => "nessie",
            "action_count"  => 0
        ]);

        event(new Nessie($user));
        $this->assertEquals(1, $user->badges()->count());
    }

    public function testUnlockRegisteredBadge()
    {
        $user = factory(User::class)->create();
        Badge::create([
            "name"          => "Registered",
            "action"        => "register",
            "action_count"  => 0
        ]);

        event(new Registered($user));
        $this->assertEquals(1, $user->badges()->count());
    }
}
