<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTutorielCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutoriel_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tutoriel_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->text("content");
            $table->integer('published')->default(0)->comment("0: Non Publier |1: Publier");
            $table->dateTime("published_at")->nullable();

            $table->foreign('tutoriel_id')->references("id")->on("users")->onDelete("cascade");
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutoriel_comments');
    }
}
