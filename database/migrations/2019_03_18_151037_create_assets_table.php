<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('asset_categorie_id')->unsigned();
            $table->integer('asset_sub_categorie_id')->unsigned();
            $table->string('designation');
            $table->text('description')->nullable();
            $table->string('kuid')->nullable();
            $table->string('downloadLink')->nullable();
            $table->integer('twitter')->default(0)->comment("Verifie si le post est publier sur twitter");
            $table->string("twitterText")->nullable();
            $table->integer('published')->default(0)->comment("0: Non Publier |1: Publier |2: Prévue");
            $table->dateTime("published_at")->nullable();
            $table->timestamps();

            $table->foreign('asset_categorie_id')->references('id')->on('asset_categories')->onDelete('cascade');
            $table->foreign('asset_sub_categorie_id')->references('id')->on('asset_sub_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets');
    }
}
