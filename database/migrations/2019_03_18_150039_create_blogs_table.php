<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('categorie_id')->unsigned();
            $table->string('title');
            $table->string('slug');
            $table->longText('content');
            $table->integer('published')->default(0)->comment("0: Brouillon |1: Publier");
            $table->dateTime("published_at")->nullable();
            $table->integer('twitter')->default(0)->comment("Vérifie si le post est publier sur Twitter");
            $table->text('twitterText')->nullable();

            $table->foreign('categorie_id')->references('id')->on('categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
