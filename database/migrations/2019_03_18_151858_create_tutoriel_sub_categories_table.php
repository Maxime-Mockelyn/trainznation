<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTutorielSubCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutoriel_sub_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tutoriel_category_id')->unsigned();
            $table->string('name');

            $table->foreign('tutoriel_category_id')->references("id")->on('tutoriel_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutoriel_sub_categories');
    }
}
