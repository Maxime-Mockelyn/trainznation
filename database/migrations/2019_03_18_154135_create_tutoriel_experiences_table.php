<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTutorielExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutoriel_experiences', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tutoriel_id')->unsigned();
            $table->string("experience");

            $table->foreign('tutoriel_id')->references("id")->on("tutoriels")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutoriel_experiences');
    }
}
