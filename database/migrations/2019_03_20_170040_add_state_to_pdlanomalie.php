<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStateToPdlanomalie extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pdl_anomalies', function (Blueprint $table) {
            $table->integer('state')->default(0)->comment("0: Non commencé |1: En cours |2: Terminer");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pdl_anomalies', function (Blueprint $table) {
            //
        });
    }
}
