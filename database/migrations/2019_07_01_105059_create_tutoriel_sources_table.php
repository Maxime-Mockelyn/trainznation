<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTutorielSourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutoriel_sources', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('tutoriel_id')->unsigned();
            $table->string('pathSource');

            $table->foreign('tutoriel_id')->references('id')->on('tutoriels')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutoriel_sources');
    }
}
