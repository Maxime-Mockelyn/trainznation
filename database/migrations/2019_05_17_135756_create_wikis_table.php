<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWikisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wikis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('wiki_category_id')->unsigned();
            $table->string('title');
            $table->longText('content');
            $table->integer('published')->default(0)->comment("0: Brouillon |1: Publier");
            $table->dateTime('published_at')->nullable();
            $table->timestamps();

            $table->foreign('wiki_category_id')->references('id')->on('wiki_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wikis');
    }
}
