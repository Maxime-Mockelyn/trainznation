<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePdlCompatibilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pdl_compatibilities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('version');
            $table->string('trainz_build');
            $table->integer('state')->default(0)->comment("0: Non Compatible |1: Compatible");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pdl_compatibilities');
    }
}
