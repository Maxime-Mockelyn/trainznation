<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTutorielTechnosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutoriel_technos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tutoriel_id')->unsigned();
            $table->string('techno');
            $table->string('slug');

            $table->foreign('tutoriel_id')->references("id")->on("tutoriels")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutoriel_technos');
    }
}
