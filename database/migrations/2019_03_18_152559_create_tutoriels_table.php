<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTutorielsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutoriels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tutoriel_category_id')->unsigned();
            $table->integer('tutoriel_sub_category_id')->unsigned();
            $table->integer('users_id')->unsigned();
            $table->string('uuid')->nullable();
            $table->string('title');
            $table->string('slug');
            $table->longText('content')->nullable();
            $table->integer('published')->default(0)->comment("0: Non publier |1: Publier |2: En attente de publication");
            $table->dateTime("published_at")->nullable();
            $table->string('pathVideo')->nullable();
            $table->string('youtube_id')->nullable();
            $table->integer('source')->default(0);
            $table->integer('premium')->default(0);

            $table->foreign('tutoriel_category_id')->references("id")->on("tutoriel_categories")->onDelete('cascade');
            $table->foreign('tutoriel_sub_category_id')->references("id")->on("tutoriel_sub_categories")->onDelete('cascade');
            $table->foreign('users_id')->references("id")->on("users")->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutoriels');
    }
}
