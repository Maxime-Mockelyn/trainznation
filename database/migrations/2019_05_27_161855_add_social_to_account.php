<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSocialToAccount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_accounts', function (Blueprint $table) {
            $table->string('pseudo_discord')->nullable();
            $table->string('pseudo_google')->nullable();
            $table->string('pseudo_microsoft')->nullable();
            $table->string('pseudo_twitch')->nullable();
            $table->string('pseudo_youtube')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_accounts', function (Blueprint $table) {
            $table->removeColumn('pseudo_discord');
            $table->removeColumn('pseudo_google');
            $table->removeColumn('pseudo_microsoft');
            $table->removeColumn('pseudo_twitch');
            $table->removeColumn('pseudo_youtube');
        });
    }
}
