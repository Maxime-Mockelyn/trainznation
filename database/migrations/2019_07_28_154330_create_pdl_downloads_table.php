<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePdlDownloadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pdl_downloads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("version");
            $table->string('build');
            $table->integer('typeDownload')->comment("1: Map |2: Dépendance |3: Session");
            $table->integer('typeRelease')->default(0)->comment("0: Correctif |1: Alpha |2: Beta |3: RC |4: Final");
            $table->string('linkDownload');
            $table->longText("note")->nullable();
            $table->integer('published')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pdl_downloads');
    }
}
