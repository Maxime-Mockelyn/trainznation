<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('course_categorie_id');
            $table->integer('course_subcategorie_id');
            $table->string('title');
            $table->string('slug');
            $table->integer('level')->default(0)->comment("0: Débutant |1: Intermédiaire |2: Confirmé |3: Expert");
            $table->integer("user_id");
            $table->longText("description");
            $table->integer('published')->default(0);
            $table->dateTime('published_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
