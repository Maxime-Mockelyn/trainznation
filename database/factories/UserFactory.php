<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});

$factory->define(\App\Model\Blog\BlogComment::class, function (\Faker\Generator $faker){
    return [
        'comment' => $faker->text(),
    ];
});

$factory->define(\App\Model\Blog\Blog::class, function (\Faker\Generator $faker){

    $title = $faker->text(50);

    return [
        'categorie_id'  => 1,
        'title'         => $title,
        'slug'          => str_slug($title),
        'content'       => $faker->text(),
        'published'     => 1,
        'published_at'  => now(),
        'twitter'       => 0
    ];
});
