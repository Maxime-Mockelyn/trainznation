<?php

use Illuminate\Database\Seeder;

class TutorielTechnoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (env("APP_ENV") == "local" || env("APP_ENV") == "testing")
        {
            \Illuminate\Support\Facades\DB::table('tutoriel_technos')
                ->insert([
                    "tutoriel_id"   => 1,
                    "techno"        => "TRS2019",
                    "slug"          => "trs2019"
                ]);

            \Illuminate\Support\Facades\DB::table('tutoriel_technos')
                ->insert([
                    "tutoriel_id"   => 1,
                    "techno"        => "T:ANE",
                    "slug"          => "tane"
                ]);
        }

    }
}
