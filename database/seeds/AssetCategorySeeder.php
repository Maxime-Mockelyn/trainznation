<?php

use Illuminate\Database\Seeder;

class AssetCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('asset_categories')->insert(["name" => "Matériel Roulant"]);
        \Illuminate\Support\Facades\DB::table('asset_categories')->insert(["name" => "Objets"]);
    }
}
