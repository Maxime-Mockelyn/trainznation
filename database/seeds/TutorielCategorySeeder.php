<?php

use Illuminate\Database\Seeder;

class TutorielCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('tutoriel_categories')
            ->insert([
                "name"  => "Trainz en Général"
            ]);

        \Illuminate\Support\Facades\DB::table('tutoriel_categories')
            ->insert([
                "name"  => "Création de contenue"
            ]);

    }
}
