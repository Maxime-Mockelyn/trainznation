<?php

use Illuminate\Database\Seeder;

class AssetSubCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('asset_sub_categories')->insert([
            "asset_categorie_id"        => 1,
            "name"                      => "Locomotive Diesel"
        ]);

        \Illuminate\Support\Facades\DB::table('asset_sub_categories')->insert([
            "asset_categorie_id"        => 1,
            "name"                      => "Locomotive Electrique"
        ]);

        \Illuminate\Support\Facades\DB::table('asset_sub_categories')->insert([
            "asset_categorie_id"        => 1,
            "name"                      => "Locomotive Vapeur"
        ]);

        \Illuminate\Support\Facades\DB::table('asset_sub_categories')->insert([
            "asset_categorie_id"        => 1,
            "name"                      => "TGV"
        ]);

        \Illuminate\Support\Facades\DB::table('asset_sub_categories')->insert([
            "asset_categorie_id"        => 1,
            "name"                      => "Autorail/Automotrice Diesel"
        ]);

        \Illuminate\Support\Facades\DB::table('asset_sub_categories')->insert([
            "asset_categorie_id"        => 1,
            "name"                      => "Autorail/Automotrice Electrique"
        ]);

        \Illuminate\Support\Facades\DB::table('asset_sub_categories')->insert([
            "asset_categorie_id"        => 1,
            "name"                      => "RIO/RIB"
        ]);

        \Illuminate\Support\Facades\DB::table('asset_sub_categories')->insert([
            "asset_categorie_id"        => 1,
            "name"                      => "Voiture Voyageur"
        ]);

        \Illuminate\Support\Facades\DB::table('asset_sub_categories')->insert([
            "asset_categorie_id"        => 1,
            "name"                      => "Wagon Marchandise"
        ]);

        \Illuminate\Support\Facades\DB::table('asset_sub_categories')->insert([
            "asset_categorie_id"        => 2,
            "name"                      => "Animaux"
        ]);

        \Illuminate\Support\Facades\DB::table('asset_sub_categories')->insert([
            "asset_categorie_id"        => 2,
            "name"                      => "Décors"
        ]);

        \Illuminate\Support\Facades\DB::table('asset_sub_categories')->insert([
            "asset_categorie_id"        => 2,
            "name"                      => "Gare Fonctionnel"
        ]);

        \Illuminate\Support\Facades\DB::table('asset_sub_categories')->insert([
            "asset_categorie_id"        => 2,
            "name"                      => "Gare Non Fonctionnel"
        ]);

        \Illuminate\Support\Facades\DB::table('asset_sub_categories')->insert([
            "asset_categorie_id"        => 2,
            "name"                      => "Industrie"
        ]);

        \Illuminate\Support\Facades\DB::table('asset_sub_categories')->insert([
            "asset_categorie_id"        => 2,
            "name"                      => "Marchandise"
        ]);

        \Illuminate\Support\Facades\DB::table('asset_sub_categories')->insert([
            "asset_categorie_id"        => 2,
            "name"                      => "Objet de Gare"
        ]);

        \Illuminate\Support\Facades\DB::table('asset_sub_categories')->insert([
            "asset_categorie_id"        => 2,
            "name"                      => "Objet de Voie"
        ]);

        \Illuminate\Support\Facades\DB::table('asset_sub_categories')->insert([
            "asset_categorie_id"        => 2,
            "name"                      => "Pont"
        ]);

        \Illuminate\Support\Facades\DB::table('asset_sub_categories')->insert([
            "asset_categorie_id"        => 2,
            "name"                      => "Tunnel"
        ]);

        \Illuminate\Support\Facades\DB::table('asset_sub_categories')->insert([
            "asset_categorie_id"        => 2,
            "name"                      => "Voie"
        ]);
    }
}
