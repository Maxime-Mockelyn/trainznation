<?php

use Illuminate\Database\Seeder;

class UserAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('user_accounts')
            ->insert([
                "user_id"       => 1,
                "last_login"    => null,
                "avatar"        => 1,
                "site_web"      => "https://trainznation.eu",
                "pseudo_twitter"    => "trainznation",
                "pseudo_facebook"   => "trainznation",
                "trainz_id"         => "400722"
            ]);
    }
}
