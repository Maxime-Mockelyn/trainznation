<?php

use Illuminate\Database\Seeder;

class TutorielSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(env("APP_ENV") == 'local' || env("APP_ENV") == 'testing')
        {
            \Illuminate\Support\Facades\DB::table('tutoriels')
                ->insert([
                    "tutoriel_category_id"         => 1,
                    "tutoriel_sub_category_id"     => 3,
                    "users_id"                      => 1,
                    "title"                         => "Définir les filtres dans le content manager",
                    "slug"                          => str_slug("Définir les filtres dans le content manager"),
                    "content"                       => generateLorem(),
                    "published"                     => 1,
                    "published_at"                  => now(),
                    "linkVideo"                     => "https://www.youtube.com/watch?v=HUInep7tcY0"
                ]);
        }

    }
}
