<?php

use Illuminate\Database\Seeder;

class TutorielSubCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('tutoriel_sub_categories')
            ->insert([
                "tutoriel_category_id" => 1,
                "name"  => "Mode Concepteur"
            ]);

        \Illuminate\Support\Facades\DB::table('tutoriel_sub_categories')
            ->insert([
                "tutoriel_category_id" => 1,
                "name"  => "Mode Conducteur"
            ]);

        \Illuminate\Support\Facades\DB::table('tutoriel_sub_categories')
            ->insert([
                "tutoriel_category_id" => 1,
                "name"  => "Options & Paramètre"
            ]);

        \Illuminate\Support\Facades\DB::table('tutoriel_sub_categories')
            ->insert([
                "tutoriel_category_id" => 2,
                "name"  => "Modélisation"
            ]);

        \Illuminate\Support\Facades\DB::table('tutoriel_sub_categories')
            ->insert([
                "tutoriel_category_id" => 2,
                "name"  => "Configuration de contenue"
            ]);

        \Illuminate\Support\Facades\DB::table('tutoriel_sub_categories')
            ->insert([
                "tutoriel_category_id" => 2,
                "name"  => "Création & Paramétrage de script"
            ]);

        \Illuminate\Support\Facades\DB::table('tutoriel_sub_categories')
            ->insert([
                "tutoriel_category_id" => 2,
                "name"  => "Terminologie"
            ]);

        \Illuminate\Support\Facades\DB::table('tutoriel_sub_categories')
            ->insert([
                "tutoriel_category_id" => 2,
                "name"  => "Validation de contenue"
            ]);

    }
}
