<?php

use Illuminate\Database\Seeder;

class PdlCompatibilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table("pdl_compatibilities")->insert([
            "version"       => "1",
            "trainz_build"  => "3.5",
            "state"         => 1
        ]);
        \Illuminate\Support\Facades\DB::table("pdl_compatibilities")->insert([
            "version"       => "1",
            "trainz_build"  => "4.5",
            "state"         => 1
        ]);
        \Illuminate\Support\Facades\DB::table("pdl_compatibilities")->insert([
            "version"       => "1",
            "trainz_build"  => "4.6",
            "state"         => 1
        ]);
    }
}
