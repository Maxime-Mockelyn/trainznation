<?php

use Illuminate\Database\Seeder;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(env("APP_ENV") == "local" || env("APP_ENV") == "test")
        {
            for ($i=0; $i < 9; $i++)
            {
                \Illuminate\Support\Facades\DB::table('blogs')->insert([
                    "categorie_id"  => rand(1,3),
                    "title"         => "Titre ".$i,
                    "slug"          => str_slug("Titre ".$i),
                    "content"       => generateLorem(),
                    "published"     => 1,
                    "published_at"  => \Carbon\Carbon::now()
                ]);
            }
        }

    }
}
