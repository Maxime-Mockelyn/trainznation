<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('users')->insert([
            "name"  => "Contact Trainznation",
            "email" => "contact@trainznation.eu",
            "password" => bcrypt("1992_Maxime"),
            "group"     => 1
        ]);
    }
}
