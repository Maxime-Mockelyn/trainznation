<?php

use Illuminate\Database\Seeder;

class BlogCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('categories')->insert([
            "name"  => "Trainz en général"
        ]);

        \Illuminate\Support\Facades\DB::table('categories')->insert([
            "name"  => "Release"
        ]);

        \Illuminate\Support\Facades\DB::table('categories')->insert([
            "name"  => "Création"
        ]);
    }
}
