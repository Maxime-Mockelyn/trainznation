<?php

use Illuminate\Database\Seeder;

class PdlBuildSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('pdl_builds')->insert([
            "version"   => "1",
            "build"     => "9580"
        ]);
    }
}
