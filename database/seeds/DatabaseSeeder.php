<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BadgeSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(UserAccountSeeder::class);
        $this->call(BlogCategorySeeder::class);
        //$this->call(BlogSeeder::class);
        $this->call(TutorielCategorySeeder::class);
        $this->call(TutorielSubCategorySeeder::class);
        //$this->call(TutorielSeeder::class);
        //$this->call(TutorielTechnoSeeder::class);
        $this->call(TutorielExperienceSeeder::class);
        $this->call(AssetCategorySeeder::class);
        $this->call(AssetSubCategorySeeder::class);
        $this->call(PdlBuildSeeder::class);
        $this->call(PdlCompatibilitySeeder::class);
        $this->call(PdlTimelineSeeder::class);
    }
}
