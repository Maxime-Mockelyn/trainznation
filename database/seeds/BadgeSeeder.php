<?php

use Illuminate\Database\Seeder;

class BadgeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('badges')->insert([
            "name"          => "Trainznation Forever",
            "action"        => "register",
            "action_count"  => 0,
            "description"   => "Vous êtes inscrit sur le site de Trainznation"
        ]);

        \Illuminate\Support\Facades\DB::table('badges')->insert([
            "name"          => "Jeunot",
            "action"        => "annual",
            "action_count"  => 1,
            "description"   => "Vous êtes inscrit sur le site de Trainznation depuis 1 an"
        ]);

        \Illuminate\Support\Facades\DB::table('badges')->insert([
            "name"          => "Habitué",
            "action"        => "annual",
            "action_count"  => 2,
            "description"   => "Vous êtes inscrit sur le site de Trainznation depuis 2 an"
        ]);

        \Illuminate\Support\Facades\DB::table('badges')->insert([
            "name"          => "Ancien",
            "action"        => "annual",
            "action_count"  => 3,
            "description"   => "Vous êtes inscrit sur le site de Trainznation depuis 3 an"
        ]);

        \Illuminate\Support\Facades\DB::table('badges')->insert([
            "name"          => "Curieux",
            "action"        => "nessie",
            "action_count"  => 0,
            "description"   => "Vous avez trouver Nessie"
        ]);

        \Illuminate\Support\Facades\DB::table('badges')->insert([
            "name"          => "Timide",
            "action"        => "comments",
            "action_count"  => 10,
            "description"   => "Vous êtes poster 10 commentaires"
        ]);

        \Illuminate\Support\Facades\DB::table('badges')->insert([
            "name"          => "Bavard",
            "action"        => "comments",
            "action_count"  => 50,
            "description"   => "Vous êtes poster 50 commentaires"
        ]);

        \Illuminate\Support\Facades\DB::table('badges')->insert([
            "name"          => "Pipelette",
            "action"        => "comments",
            "action_count"  => 100,
            "description"   => "Vous êtes poster 100 commentaires"
        ]);

        \Illuminate\Support\Facades\DB::table('badges')->insert([
            "name"          => "Arretez-vous",
            "action"        => "comments",
            "action_count"  => 200,
            "description"   => "Vous êtes poster 200 commentaires et plus...heu"
        ]);

        \Illuminate\Support\Facades\DB::table('badges')->insert([
            "name"          => "Créateur",
            "action"        => "trainzid",
            "action_count"  => 0,
            "description"   => "Vous avez renseigné votre TRAINZ ID"
        ]);
    }
}
