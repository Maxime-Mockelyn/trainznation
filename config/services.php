<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_PUB_KEY'),
        'secret' => env('STRIPE_SECRET_KEY'),
        'webhook' => [
            'secret' => env('STRIPE_WEBHOOK_SECRET'),
            'tolerance' => env('STRIPE_WEBHOOK_TOLERANCE', 300),
        ],
    ],
    'twitter' => [
        'consumer_key'    => "CW3IB5TFJQfoJ3WwYrKiQp31h",
        'consumer_secret' => "oovTnCZ6b5Ib4eGjZW59UkHR2B1wwGzzwNIBKQWqcrL5y1t0Vk",
        'access_token'    => "1009074959304544257-4iN8TvGKEWhTmgk1jr2rxs9nENzle0",
        'access_secret'   => "CpSm3IqIDZO7JNf0r9ern3yHF7pQuxyx9BvnQ325fUkIP",
        'client_id' => 'CW3IB5TFJQfoJ3WwYrKiQp31h',
        'client_secret' => 'oovTnCZ6b5Ib4eGjZW59UkHR2B1wwGzzwNIBKQWqcrL5y1t0Vk',
        'redirect' => env("APP_URL").'/auth/twitter/callback'
    ],

    'discord' => [
       'token'  => 'NDI5MjkyODc0ODgzMDA2NDY1.XN7gbg.MP1K8NOAn8fRUoADYqnhGHCHWac',
        'client_id' => '429292874883006465',
        'client_secret' => '40eXsxKJG0K0aw4gcy0Fn8-tjE2sYWyZ',
        'redirect' => env('APP_URL').'/auth/discord/callback'
    ],

    'google' => [
        'client_id' => env('GOOGLE_CLIENT_ID'),
        'client_secret' => env('GOOGLE_CLIENT_SECRET'),
        'redirect' => env('GOOGLE_CALLBACK_URL'),
    ],

    'facebook' => [
        'client_id' => '348094415702383',
        'client_secret' => 'd3bd79de359f99258c2f6528e6e38bdb',
        'redirect' => env("APP_URL").'/auth/facebook/callback'
    ],

    'twitch' => [
        'client_id' => '5ao48zmvwja3jv8lc53dt6froc31wb',
        'client_secret' => 'lbw8c61fqyfcu8zqxoc3xscwnv08rg',
        'redirect' => env('APP_URL').'/auth/twitch/callback'
    ],

    'youtube' => [
        'client_id' => env('YOUTUBE_CLIENT_ID'),
        'client_secret' => env('YOUTUBE_CLIENT_SECRET'),
        'redirect' => env('APP_URL').'/auth/youtube/callback'
    ],

    'botman' => [
        'hipchat_urls' => [
            'YOUR-INTEGRATION-URL-1',
            'YOUR-INTEGRATION-URL-2',
        ],
        'nexmo_key' => 'YOUR-NEXMO-APP-KEY',
        'nexmo_secret' => 'YOUR-NEXMO-APP-SECRET',
        'microsoft_bot_handle' => 'YOUR-MICROSOFT-BOT-HANDLE',
        'microsoft_app_id' => 'YOUR-MICROSOFT-APP-ID',
        'microsoft_app_key' => 'YOUR-MICROSOFT-APP-KEY',
        'slack_token' => 'YOUR-SLACK-TOKEN-HERE',
        'telegram_token' => 'YOUR-TELEGRAM-TOKEN-HERE',
        'facebook_token' => 'YOUR-FACEBOOK-TOKEN-HERE',
        'facebook_app_secret' => 'YOUR-FACEBOOK-APP-SECRET-HERE', // Optional - this is used to verify incoming API calls,
        'wechat_app_id' => 'YOUR-WECHAT-APP-ID',
        'wechat_app_key' => 'YOUR-WECHAT-APP-KEY',
    ],


];
